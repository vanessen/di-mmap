/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _PERMA_DELAY_H
#define _PERMA_DELAY_H

#include "process_tsc.h"

extern int ReadDelay; /* Read delay per page access */
extern int WriteDelay; /* Write delay per page access */
extern int PageSize; /* Native page size of the memory technology: 64B PCM, 256KB Flash */
extern int rTransitLatency; /* Read transit latency picoseconds per B*/
extern int wTransitLatency; /* Write transit latency picoseconds per B */

/* Latency / Inverse Bandwidth numbers (picoseconds per B) from Moneta research paper, which used PCIe 1.1 x8 (2GB/s Full Duplex) 
 * Published numbers for PCIe 1.1 8 lanes are Write = 0.93us/KB and Read = 0.65us/KB
 *                   for DMA access Write = 0.28us/KB and Read 0.43us/KB
 *                Write       Read
 * PCIe 8x     930ns/KB   650ns/KB
 * PCIe 4x    1860ns/KB  1300ns/KB
 * PCIe 2x    3720ns/KB  2600ns/KB
 * PCIe 1x    7440ns/KB  5200ns/KB (7265.625ps/B 5078.125ps/B)
 * DMA         280ns/KB   430ns/KB
 */
/* Express the bandwidth in terms of picoseconds to avoid floating point numbers in the kernel */
#define PCIE_READ_DMA_LATENCY 5078 /* PCIE latency (picoseconds per B) */
#define DEV_READ_DMA_LATENCY 420 /* Internal device latency (picoseconds per B), modeled on the Moneta Ring architecture */
#define PCIE_WRITE_DMA_LATENCY 7266 /* PCIE latency (picoseconds per B) */
#define DEV_WRITE_DMA_LATENCY 273 /* Internal device latency (picoseconds per B), modeled on the Moneta Ring architecture */

/* 
 * PCIe 1.1 Ideal numbers
 * 2.5 GT/s with an 8b/10b encoding for 250 MB/s
 * 0.4 ns/T -> 4 ns/B -> 4096 us/KB or 4000 ps/B
 *
 * PCIe 2.0 Ideal numbers
 * 5.0 GT/s with an 8b/10b encoding for 500 MB/s
 * 0.2 ns/T -> 2 ns/B -> 2048 us/KB or 2000 ps/B
 */
#define PCIE_1_IDEAL_LATENCY 4000 /* Ideal PCIe latency (picoseconds per B) */
#define PCIE_2_IDEAL_LATENCY 2000 /* Ideal PCIe latency (picoseconds per B) */

extern int driverLatency;
extern int busReadLatency;
extern int busWriteLatency;
extern int devReadLatency;
extern int devWriteLatency;

extern int PeripheralBusMultiplier;
#define PCIeInf 0
#define PCIe1x  1
#define PCIe2x  2
#define PCIe4x  4
#define PCIe8x  8
#define PCIe16x 16
#define PCIe32x 32
#define Moneta_PeripheralBusBaseline "Moneta_PCIe_1.1"
#define Ideal1_PeripheralBusBaseline "Ideal_PCIe_1.1"
#define Ideal2_PeripheralBusBaseline "Ideal_PCIe_2.0"
#define Custom_PeripheralBusBaseline "Custom_PCIe"

#define CYCLES_PER_NS 2.4

extern int DelayStyle; /* Are delays implemented with spin-waits (0) or a combination of spin-waits and schedule yields (1) */
#define HYBRID_DELAY_STYLE 1 /* Combination of spin-waits for short delay and schedule yields */
#define SPINWAIT_DELAY_STYLE 0

extern int SchedYield_Threshold; /* At what delay value should the driver spin-wait versus yield the processor */
extern int Sleep_Threshold; /* At what delay value should the driver sleep versus yield the processor */
extern int PageAccessStyle; /* For accesses larger than a single page, how much internal parallelism is there in the memory controller
			       0 - parallel (infinite controllers)
			       1 - seriel 
			       n - n-way parallel (n controllers)
			    */

#define PARALLEL_PAGE_ACCESS 0
#define SERIAL_PAGE_ACCESS 1

#define MAX_DELAY_THRESHOLD 2147483648 /* Really this delay model should not be requesting multiple seconds of delay */

typedef struct delay_info_t {
  unsigned long long counted_clock_cycles;
  int num_delay_loops;
  int num_cpu_transitions;
}delay_info;

/* Function Prototypes */
void initDelays(void);
void initTransitLatency(void);
unsigned long cyclesPerNS(unsigned long delay);
unsigned long nsPerCycle(unsigned long num_cc);
delay_info vdelay(unsigned long delay);
long compute_expected_delay(int access_count,
			    int TransitLatency,
			    int DeviceAccessDelay,
			    int size,
			    long long service_time);
#endif /* _PERMA_DELAY_H */
