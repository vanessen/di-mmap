/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/* -*- C -*-
 * Character device interface to the block ramdisk core - used to provide mmap capability
 *
 * Derived from the scullp char module from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 * 3/3/2011: Brian Van Essen - vanessen1@llnl.gov
 *   - Added support for automatically creating minor devices in the /dev file system
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/aio.h>
#include <asm/uaccess.h>

#include <linux/init.h>
/* #include <linux/gfp.h> */
#include <linux/radix-tree.h>
#include <linux/buffer_head.h> /* invalidate_bh_lrus() */
#include <linux/device.h>
#include <linux/cpufreq.h>

#include "perma_crd.h"	/* local definitions */
#include "mmap_buffer_interface.h"

version_number_t version = {1, 0, 6};
char comp_flags[256] = "";

int perma_crd_major =   0; /* Request an available major device ID */
int perma_devs =    PERMA_DEVS;	/* number of bare perma devices */
int allocate_devices = 1;
/* int scullp_qset =    SCULLP_QSET; */
int scullp_order =   ALLOC_PAGES_ORDER; //SCULLP_ORDER;
long perma_crd_max_size = 0;

/* Set the readability to S_IRUGO so that the parameters are list in /sys/module/perma_mmap_ramdisk/parameters/ */
//module_param(perma_crd_major, int, 0);
module_param(perma_devs, int, S_IRUGO);
module_param(perma_crd_max_size, long, S_IRUGO);
module_param(perma_mmap_buf_size, long, S_IRUGO);
/* module_param(scullp_qset, int, 0); */
/* module_param(scullp_order, int, 0); */
MODULE_AUTHOR("Brian Van Essen");
MODULE_LICENSE("Dual BSD/GPL");

/* Track the number of concurrent accesses to each block ramdisk device */
atomic_t global_num_concurrent_read_accesses;
atomic_t global_num_concurrent_write_accesses;
int simulate_unified_channel;

struct scullp_dev *scullp_devices; /* allocated in scullp_init */
struct class *perma_class;

int scullp_trim(struct scullp_dev *dev);
void scullp_cleanup(void);


static int NUMA_node_mask = 0; /* A mask to interleave memory across the available NUMA nodes */
static struct page *zero_page; /* Create a page of zeros for using when sparse files are read via the char interface */

void cleanup_devices(void) {
}

void setup_devices(void) {
}

void reset_device_parameters(void) {
  int i;
  initDelays();
  reset_mmap_params();
  reset_mmap_state();

  /* Whenever the global counter is reset, clear all of the timestamps on the pages in the radix tree */
  for (i = 0; i < perma_devs; i++) {
    perma_crd_init_pages(scullp_devices + i);
  }

  DEBUG_LOGGING = 0;
}

dev_t inline get_ith_device_id(int i) {
  struct scullp_dev *crd;

  crd = &scullp_devices[i % perma_devs];

  BUG_ON(!crd);
  BUG_ON(crd && MINOR(crd->dev_id) != i % perma_devs);
/*   printk(KERN_INFO "crd_number =%d i=%d out of %d devices\n", crd->crd_number, i, perma_devs); */
  return get_device_id(crd);
}

profile_info *select_ith_device(int i) {
  struct scullp_dev *crd;

  crd = &scullp_devices[i % perma_devs];

  BUG_ON(!crd);
  BUG_ON(crd && MINOR(crd->dev_id) != i % perma_devs);
/*   printk(KERN_INFO "crd_number =%d i=%d out of %d devices\n", crd->crd_number, i, perma_devs); */
  return &crd->perf_cntr;
}

void print_io_stats_header(char *msg, char line_leader) {
  char msg1[MAX_STR_LEN];
  sprintf(msg, "%c\tCh ", line_leader);
  sprintf(msg1, "%16s %16s %16s %16s %16s %16s %16s %16s %16s %16s", 
	  "Sim. Access", 
	  "Req. Delay (ns)", 
	  "Xfer Size (B)", 
	  "Act. Delay (CC)", 
	  "Num. Loops",
	  "Lookup (CC)",
	  "Copy (CC)",
	  "Zap Req",
	  "Num Zaps",
	  "Num. Requests");
  strcat(msg, msg1);
  sprintf(msg1, " | "); 
  strcat(msg, msg1);
  sprintf(msg1, "%16s %16s %16s %16s %16s %16s %16s %16s", 
	  "Sim. Access", 
	  "Req. Delay (ns)", 
	  "Xfer Size (B)", 
	  "Act. Delay (CC)", 
	  "Num. Loops",
	  "Lookup (CC)",
	  "Copy (CC)",
	  "Num. Requests");
  strcat(msg, msg1);
  sprintf(msg1, "\n");
  strcat(msg, msg1);
  return;
}

void prettyprint_device_specifics(struct seq_file *s, char *msg, char line_leader) {
  char msg1[MAX_STR_LEN];
  int i;

  sprintf(msg, "mmap_Page_Cache_Size=%ldb DirtyPages: num_written=%ldpg Unified Channel=%d Skipped zap page range=%ld PerMA stall=%ld\n", 
	  perma_mmap_buf_size*PAGE_SIZE, atomic64_read(&total_num_pages_written),
	  simulate_unified_channel, atomic64_read(&skipped_zap_page_range), atomic64_read(&perma_buffer_stalled));

  prettyprint_specifics(buf_data, s, msg, line_leader);

  sprintf(msg1, "%c     /dev/perma-mmap-ramdisk:", 
	  line_leader);
  strcat(msg, msg1);

  for(i = 0; i < perma_devs; i++) {
    char mmaped_address_ranges[MAX_STR_LEN] = "";
    vm_address_regions(&(scullp_devices[i].active_vmas.vmas), mmaped_address_ranges);
    sprintf(msg1, " %d (%s)=%ldb", i,  mmaped_address_ranges, scullp_devices[i].size);
    strcat(msg, msg1);
  }

  return;
}

int tune_device_specifics(char *cmd, int param1, int param2, int param3, int param4) {
  int err = 0, i;
  if(strcasecmp(cmd, MMAP_BUFFER) == 0) {
    perma_mmap_buf_size = param1;
  }else if(strcasecmp(cmd, RESET_MMAP) == 0) {
    reset_mmap_state();
    printk("Resetting mmap state\n");
  }else if(strcasecmp(cmd, SCAN_PTE) == 0) {
    for (i = 0; i < perma_devs; i++) {
      scan_page_tables(&scullp_devices[i].active_vmas);
    }
  }else {
    err = /* buf_ops-> */tune_specifics(buf_data, cmd, param1, param2, param3, param4); //1;
  }

  init_mmap_params();
  return err;
}


void explain_device_tuning_params() {
  printk("\t%s\n", MMAP_BUFFER);
  printk("\t%s\n", RESET_MMAP);
  printk("\t%s\n", SCAN_PTE);

  /* buf_ops-> */explain_tuning_params(buf_data);
  return;
}

void initDeviceSpecificProfileCounters(void) {
  atomic64_set(&total_num_pages_written, 0);

  /* buf_ops-> */init_specific_counters(buf_data);

  return;
}

/*
 * Look up and return a crd's page for a given sector.
 */
struct page *perma_crd_lookup_page(struct scullp_dev *crd, sector_t sector)
{
	pgoff_t idx;
	struct page *page;

	/*
	 * The page lifetime is protected by the fact that we have opened the
	 * device node -- brd pages will never be deleted under us, so we
	 * don't need any further locking or refcounting.
	 *
	 * This is strictly true for the radix-tree nodes as well (ie. we
	 * don't actually need the rcu_read_lock()), however that is not a
	 * documented feature of the radix-tree API so it is better to be
	 * safe here (we don't have total exclusion from radix tree updates
	 * here, only deletes).
	 */
	idx = sector >> PAGE_SECTORS_SHIFT; /* sector to page index */
	rcu_read_lock();
	page = radix_tree_lookup(&crd->mmap_rd_pages, idx);
	rcu_read_unlock();

	BUG_ON(page && page->index != idx);

	return page;
}

/*
 * Look up and return a crd's page for a given sector.
 * If one does not exist, allocate an empty page, and insert that. Then
 * return it.
 */
struct page *perma_crd_lookup_or_insert_page(struct scullp_dev *crd, sector_t sector)
{
	pgoff_t idx;
	struct page *page;
	gfp_t gfp_flags;
	int numa_node;

	page = perma_crd_lookup_page(crd, sector);
	if (page)
		return page;

	/*
	 * Must use NOIO because we don't want to recurse back into the
	 * block or filesystem layers from page reclaim.
	 *
	 * Cannot support XIP and highmem, because our ->direct_access
	 * routine for XIP must return memory that is always addressable.
	 * If XIP was reworked to use pfns and kmap throughout, this
	 * restriction might be able to be lifted.
	 */
	gfp_flags = GFP_NOIO | __GFP_ZERO |  __GFP_HIGHMEM;

	/*
	 * alloc_page(gfp_mask) is the same as alloc_pages(gfp_mask, 0) - gfp.h
	 * alloc_pages(gfp_mask, order)	-> alloc_pages_node(numa_node_id(), gfp_mask, order) - gfp.h
	 * numa_node_id() - gives the socket id for the current running thread
	 * num_online_cpus() - how many cores there are in the system
	 * num_online_nodes() - how many processors (with memory) there are in the system
	 *
	 * in file: mm/mempolicy.c
	 * alloc_page_interleave(gfp_t gfp, unsigned order, unsigned nid)
	 * alloc_page_vma(gfp_t gfp, struct vm_area_struct *vma, unsigned long addr)
	 *
	 * alloc_pages_exact(size_t size, gfp_t gfp_mask) - request a block of pages
	 */
/* 	page = alloc_page(gfp_flags); */
	idx = sector >> PAGE_SECTORS_SHIFT;
	numa_node = idx & NUMA_node_mask;
	page = alloc_pages_current(gfp_flags, ALLOC_PAGES_ORDER);
	//	page = alloc_pages(gfp_flags, ALLOC_PAGES_ORDER);
	//	page = alloc_pages_node(numa_node, gfp_flags, ALLOC_PAGES_ORDER);
	if (!page)
		return NULL;

	if (radix_tree_preload(GFP_NOIO)) {
		__free_pages(page, ALLOC_PAGES_ORDER);
		return NULL;
	}

	spin_lock(&crd->mmap_rd_lock);
	if (radix_tree_insert(&crd->mmap_rd_pages, idx, page)) {
	  	__free_pages(page, ALLOC_PAGES_ORDER);
		page = radix_tree_lookup(&crd->mmap_rd_pages, idx);
		BUG_ON(!page);
		BUG_ON(page->index != idx);
	} else
		page->index = idx;
	spin_unlock(&crd->mmap_rd_lock);

	radix_tree_preload_end();
	return page;
}

/*
 * Free all backing store pages and radix tree. This must only be called when
 * there are no other users of the device.
 */
void perma_crd_free_pages(struct scullp_dev *crd)
{
	unsigned long pos = 0;
	struct page *pages[FREE_BATCH];
	int nr_pages;

	do {
		int i;

		nr_pages = radix_tree_gang_lookup(&crd->mmap_rd_pages,
				(void **)pages, pos, FREE_BATCH);

		for (i = 0; i < nr_pages; i++) {
			void *ret;

			BUG_ON(pages[i]->index < pos);
			pos = pages[i]->index;
			ret = radix_tree_delete(&crd->mmap_rd_pages, pos);
			BUG_ON(!ret || ret != pages[i]);
			__free_pages(pages[i], ALLOC_PAGES_ORDER);
		}

		pos++;

		/*
		 * This assumes radix_tree_gang_lookup always returns as
		 * many pages as possible. If the radix-tree code changes,
		 * so will this have to.
		 */
	} while (nr_pages == FREE_BATCH);
}

void perma_crd_init_pages(struct scullp_dev *crd)
{
	unsigned long pos = 0;
	struct page *pages[FREE_BATCH];
	int nr_pages;

	do {
		int i;
		nr_pages = radix_tree_gang_lookup(&crd->mmap_rd_pages,
				(void **)pages, pos, FREE_BATCH);

		for (i = 0; i < nr_pages; i++) {
		  BUG_ON(pages[i]->index < pos);
		  pos = pages[i]->index; /* Update the position so that it is equal to the index of the last page returned */

		  /* Clear all of the dirty page bits when initializing the device */
		  ClearPageDirty(pages[i]);
		}
		pos++;
		/*
		 * This assumes radix_tree_gang_lookup always returns as
		 * many pages as possible. If the radix-tree code changes,
		 * so will this have to.
		 */
	} while (nr_pages == FREE_BATCH);
}

/*
 * Open and close
 */

int scullp_open (struct inode *inode, struct file *filp)
{
	struct scullp_dev *dev; /* device information */

	/*  Find the device */
	dev = container_of(inode->i_cdev, struct scullp_dev, cdev);

    	/* now trim to 0 the length of the device if open was write-only */
	if ( (filp->f_flags & O_ACCMODE) == O_WRONLY) {
		if (down_interruptible (&dev->sem))
			return -ERESTARTSYS;
		scullp_trim(dev); /* ignore errors */
		up (&dev->sem);
	}

	/* and use filp->private_data to point to the device data */
	filp->private_data = dev;

	/* provide a link to map the device back to its inode */
	dev->inode = inode;

	return 0;          /* success */
}

int scullp_release (struct inode *inode, struct file *filp)
{
	return 0;
}

/*
 * Delay management: simulate the read and write delay
 */
static inline void insert_read_delay(struct scullp_dev *dev, unsigned long lookup_tsc_delta, unsigned short lookup_cpuID_diff, unsigned long copy_tsc_delta, unsigned short copy_cpuID_diff) {
  long access_count, num_requests;
  long total_delay;
  delay_info measured_delays;
  atomic_t *concurrent_access_counter;
  channel_profile_info *channel_stats;
  long long service_time = 0, avg_lookup_time = 0, avg_copy_time = 0;
  tsc_stats tsc_pair0, tsc_pair1;
  int req_zap_page_range = 0;

  tsc_pair0.clock_cycles = 0;
  tsc_pair0.faults = 0;
  tsc_pair1.clock_cycles = 0;
  tsc_pair1.faults = 0;

  /* Add a fixed cost overhead for accessing the driver */
  if(driverLatency > 0) {
    ndelay(driverLatency);
  }

  concurrent_access_counter = &dev->num_concurrent_read_accesses;
  channel_stats = &dev->perf_cntr.read;
  
  /* Track the number of concurrent accesses */
  if(PeripheralBusMultiplier == PCIeInf) {
    access_count = 0;
  }else {
    if(simulate_unified_channel) {
      access_count = atomic_inc_return(&global_num_concurrent_read_accesses);
    }else {
      access_count = atomic_inc_return(concurrent_access_counter);
    }
  }

  num_requests = atomic64_read(&channel_stats->num_requests);
  if(num_requests > 0) {
    avg_lookup_time = channel_stats->aggregate.tsc_pair0.clock_cycles / num_requests;
    avg_copy_time = channel_stats->aggregate.tsc_pair1.clock_cycles / num_requests;	    
  }else {
    avg_lookup_time = 0;
    avg_copy_time = 0;
  }

  validate_tsc(&tsc_pair0, lookup_tsc_delta, lookup_cpuID_diff, "Lookup time", avg_lookup_time);
  validate_tsc(&tsc_pair1, copy_tsc_delta, copy_cpuID_diff, "Copy time", avg_copy_time);

  service_time = tsc_pair0.clock_cycles + tsc_pair1.clock_cycles;

  total_delay = compute_expected_delay(access_count, rTransitLatency, ReadDelay, PAGE_SIZE, service_time);

  measured_delays = vdelay(total_delay);
  /* Once the delay that models reading in the data is complete, update the counters and insert the new page into the buffer 
     management scheme */
  update_statistics(channel_stats, access_count, total_delay, PAGE_SIZE, measured_delays, tsc_pair0, tsc_pair1, req_zap_page_range);

  /* Track the number of concurrent accesses */
  if(PeripheralBusMultiplier == PCIeInf) {
    access_count = 0;
  }else {
    if(simulate_unified_channel) {
      atomic_dec(&global_num_concurrent_read_accesses);
    }else {
      atomic_dec(concurrent_access_counter);
    }
  }

  return;
}

static inline void insert_write_delay(struct scullp_dev *dev, unsigned long lookup_tsc_delta, unsigned short lookup_cpuID_diff, unsigned long copy_tsc_delta, unsigned short copy_cpuID_diff) {
  atomic_t *concurrent_access_counter;
  channel_profile_info *channel_stats;
  int access_count;
  int total_delay;
  delay_info measured_delays;
  tsc_stats tsc_pair0, tsc_pair1;
  long num_requests;
  long long avg_lookup_time = 0, avg_copy_time = 0;

  tsc_pair0.clock_cycles = 0;
  tsc_pair0.faults = 0;
  tsc_pair1.clock_cycles = 0;
  tsc_pair1.faults = 0;

  atomic64_inc(&total_num_pages_written);

  concurrent_access_counter = &dev->num_concurrent_write_accesses;
  channel_stats = &dev->perf_cntr.write;

  /* Track the number of concurrent accesses */
  if(PeripheralBusMultiplier == PCIeInf) {
    access_count = 0;
  }else {
    if(simulate_unified_channel) {
      access_count = atomic_inc_return(&global_num_concurrent_write_accesses);
    }else {
      access_count = atomic_inc_return(concurrent_access_counter);
    }
  }

  /* Give an invalid init CPU ID and a 0 service time to get a basic write delay */
  total_delay = compute_expected_delay(access_count, wTransitLatency, WriteDelay, PAGE_SIZE, 0);
  
  measured_delays = vdelay(total_delay);
  
  num_requests = atomic64_read(&channel_stats->num_requests);
  if(num_requests > 0) {
    avg_lookup_time = channel_stats->aggregate.tsc_pair0.clock_cycles / num_requests;
    avg_copy_time = channel_stats->aggregate.tsc_pair1.clock_cycles / num_requests;	    
  }else {
    avg_lookup_time = 0;
    avg_copy_time = 0;
  }

  /* Add TSC here */
  validate_tsc(&tsc_pair0, lookup_tsc_delta, lookup_cpuID_diff, "Lookup time", avg_lookup_time);
  validate_tsc(&tsc_pair1, copy_tsc_delta, copy_cpuID_diff, "Copy time", avg_copy_time);

  update_statistics(channel_stats, access_count, total_delay, PAGE_SIZE, measured_delays, tsc_pair0, tsc_pair1, 0);

  /* Track the number of concurrent accesses */
  if(PeripheralBusMultiplier == PCIeInf) {
    access_count = 0;
  }else {
    if(simulate_unified_channel) {
      atomic_dec(&global_num_concurrent_write_accesses);
    }else {
      atomic_dec(concurrent_access_counter);
    }
  }

  return;
}

/*
 * Device interface: allow the mmap runtime to introspect on the device
 */
dev_t inline get_device_id(void *device) {
  struct scullp_dev *crd = (struct scullp_dev *) device;
  dev_t dev_id = crd->dev_id;
  return dev_id;
}

prot_vm_area_set_t *get_device_active_vmas(void *device) {
  struct scullp_dev *crd = (struct scullp_dev *) device;
  return &crd->active_vmas;
}

/*
 * Data management: read and write
 */
int read_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep) {
  struct scullp_dev *crd = (struct scullp_dev *) device;
  struct page *page = *pagep;
  struct page *dev_page;
  unsigned long linear_page_address;
  sector_t sector;
  int error = 0;
  void *src, *dst;
  volatile unsigned long long ini_lookup, end_lookup, ini_copy, end_copy;
  volatile int init_lookup_cpuID, end_lookup_cpuID, init_copy_cpuID, end_copy_cpuID;

  init_lookup_cpuID = GET_CPU_ID();
  /* Read the timestamp counter (TSC) */
  rdtscll(ini_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */

  linear_page_address = device_linear_address >> PAGE_SHIFT;
  sector = linear_page_address << PAGE_SECTORS_SHIFT;  // Remove this and change the lookup to take page offsets
  
  dev_page = perma_crd_lookup_or_insert_page(crd, sector); /* insert pages to fill hole or extend end-of-file */

  if (!dev_page) {
    error =  -ENOMEM;
    goto out; /* out of memory */
  }

  if (device_linear_address > crd->size) { /* out of range */
    /* update the size */
    down(&crd->sem);
    /* Now that we have the semaphore, double check the value */
    if (device_linear_address > crd->size) {
      crd->size = (linear_page_address << PAGE_SHIFT) + PAGE_SIZE;
      i_size_write(crd->inode, crd->size);  /* Note that the number of blocks allocated is not supported for character devices */
    }
    up (&crd->sem);
  }

  rdtscll(end_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_lookup_cpuID = GET_CPU_ID();
  init_copy_cpuID = end_lookup_cpuID;
  ini_copy = end_lookup;

#ifdef SIM_NO_PAGE_COPY    
    BUG_ON(*pagep != NULL); /* If the simulator does not copy pages, then this pointer better be NULL; */
#endif

  if(page == NULL) {
    /* If there is no valid target page to copy to, return a pointer to the device page */
    //    printk("No valid target page to copy to\n");
    *pagep = dev_page;
  }else {
    /* Get the virtual address for the pages */
    src = page_address(dev_page);
    dst = page_address(page);
    memcpy(dst, src, PAGE_SIZE);
  }

  rdtscll(end_copy); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_copy_cpuID = GET_CPU_ID();


  insert_read_delay(crd, end_lookup-ini_lookup, init_lookup_cpuID != end_lookup_cpuID, end_copy-ini_copy, init_copy_cpuID != end_copy_cpuID);

out:
  return error;
}

int write_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep) {
  struct scullp_dev *crd = (struct scullp_dev *) device;
  struct page *page = *pagep;
  struct page *dev_page;
  unsigned long linear_page_address;
  sector_t sector;
  int error = 0;
  void *src, *dst;
  volatile unsigned long long ini_lookup, end_lookup, ini_copy, end_copy;
  volatile int init_lookup_cpuID, end_lookup_cpuID, init_copy_cpuID, end_copy_cpuID;

  init_lookup_cpuID = GET_CPU_ID();
  /* Read the timestamp counter (TSC) */
  rdtscll(ini_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */

  linear_page_address = device_linear_address >> PAGE_SHIFT;
  sector = linear_page_address << PAGE_SECTORS_SHIFT;  // Remove this and change the lookup to take page offsets
  
  dev_page = perma_crd_lookup_or_insert_page(crd, sector); /* insert pages to fill hole or extend end-of-file */

  if (!dev_page) {
    error =  -ENOMEM;
    goto out; /* out of memory */
  }

  if (device_linear_address > crd->size) { /* out of range */
    /* update the size */
    down(&crd->sem);
    /* Now that we have the semaphore, double check the value */
    if (device_linear_address > crd->size) {
      crd->size = (linear_page_address << PAGE_SHIFT) + PAGE_SIZE;
      i_size_write(crd->inode, crd->size);  /* Note that the number of blocks allocated is not supported for character devices */
    }
    up (&crd->sem);
  }

  if(page == NULL) {
    printk("No valid target page to copy to\n");
  }

  rdtscll(end_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_lookup_cpuID = GET_CPU_ID();
  init_copy_cpuID = end_lookup_cpuID;
  ini_copy = end_lookup;

#ifdef SIM_NO_PAGE_COPY    
  /* There is no writeback if the simulator does not copy data from the device */
#else
  /* Get the virtual address for the pages */
  src = page_address(page);
  dst = page_address(dev_page);
  memcpy(dst, src, PAGE_SIZE);
#endif

  rdtscll(end_copy); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_copy_cpuID = GET_CPU_ID();

  insert_write_delay(crd, end_lookup-ini_lookup, init_lookup_cpuID != end_lookup_cpuID, end_copy-ini_copy, init_copy_cpuID != end_copy_cpuID);
out:
  return error;
}

int flush_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep) {
#ifdef SIM_NO_PAGE_COPY    
  *pagep = NULL;
  /* There is no writeback if the simulator does not copy data from the device */
#endif
  return 0;
}

ssize_t scullp_read (struct file *filp, char __user *buf, size_t count,
                loff_t *f_pos)
{
	struct scullp_dev *dev = filp->private_data; /* the first listitem */
	int quantum = PAGE_SIZE << dev->order;
	ssize_t retval = 0;
	sector_t sector = ((long) *f_pos) >> SECTOR_SHIFT; /* Turn the byte offset into a sector */
	unsigned int offset = ((long) *f_pos) & ~PAGE_MASK;
	struct page *page;
	struct page *src_page;
	void *src;

	if (down_interruptible (&dev->sem))
		return -ERESTARTSYS;

	if (*f_pos > dev->size) 
		goto nothing;
	if (*f_pos + count > dev->size)
		count = dev->size - *f_pos;

	page = perma_crd_lookup_page(dev, sector);
	if(!page) {
	  if(*f_pos < dev->size) {
	    src_page = zero_page; /* Similar to behavior of shmem */
	  }else {
	    goto nothing;
	  }
	}else {
	  src_page = page;
	}

	if (count > quantum - offset)
		count = quantum - offset; /* read only up to the end of this quantum */

 	src = page_address(src_page);
	if (copy_to_user (buf, src+offset, count)) {
		retval = -EFAULT;
		goto nothing;
	}

	up (&dev->sem);

	*f_pos += count;
	return count;

  nothing:
	up (&dev->sem);
	return retval;
}



ssize_t scullp_write (struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{
	struct scullp_dev *dev = filp->private_data; /* the first listitem */
	int quantum = PAGE_SIZE << dev->order;
	ssize_t retval = -ENOMEM; /* our most likely error */
	sector_t sector = ((long) *f_pos) >> SECTOR_SHIFT; /* Turn the byte offset into a sector */
	unsigned int offset = ((long) *f_pos) & ~PAGE_MASK;
	struct page *page;
	void *dst;

	if (down_interruptible (&dev->sem))
		return -ERESTARTSYS;

	page = perma_crd_lookup_or_insert_page(dev, sector);
	if(!page) {
	  goto nomem;
	}

/* 		memset(dptr->data[s_pos], 0, PAGE_SIZE << dptr->order); */

	if (count > quantum - offset)
		count = quantum - offset; /* write only up to the end of this quantum */

	dst = page_address(page);
	if (copy_from_user (dst+offset, buf, count)) {
		retval = -EFAULT;
		goto nomem;
	}

	*f_pos += count;
 
    	/* update the size */
	if (dev->size < *f_pos) {
		dev->size = *f_pos;
		i_size_write(filp->f_dentry->d_inode, dev->size); /* Note that the number of blocks allocated is not supported for character devices */
	}

	up (&dev->sem);
	return count;

  nomem:
	up (&dev->sem);
	return retval;
}

/*
 * The ioctl() implementation
 */

int perma_crd_ioctl (struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long arg)
{

	int err = 0, ret = 0, tmp;

	/* don't even decode wrong cmds: better returning  ENOTTY than EFAULT */
	if (_IOC_TYPE(cmd) != SCULLP_IOC_MAGIC) return -ENOTTY;
	if (_IOC_NR(cmd) > SCULLP_IOC_MAXNR) return -ENOTTY;

	/*
	 * the type is a bitmask, and VERIFY_WRITE catches R/W
	 * transfers. Note that the type is user-oriented, while
	 * verify_area is kernel-oriented, so the concept of "read" and
	 * "write" is reversed
	 */
	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
	else if (_IOC_DIR(cmd) & _IOC_WRITE)
		err =  !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));
	if (err)
		return -EFAULT;

	switch(cmd) {

	case SCULLP_IOCRESET:
/* 		scullp_qset = SCULLP_QSET; */
		scullp_order = SCULLP_ORDER;
		break;

	case SCULLP_IOCSORDER: /* Set: arg points to the value */
		ret = __get_user(scullp_order, (int __user *) arg);
		break;

	case SCULLP_IOCTORDER: /* Tell: arg is the value */
		scullp_order = arg;
		break;

	case SCULLP_IOCGORDER: /* Get: arg is pointer to result */
		ret = __put_user (scullp_order, (int __user *) arg);
		break;

	case SCULLP_IOCQORDER: /* Query: return it (it's positive) */
		return scullp_order;

	case SCULLP_IOCXORDER: /* eXchange: use arg as pointer */
		tmp = scullp_order;
		ret = __get_user(scullp_order, (int __user *) arg);
		if (ret == 0)
			ret = __put_user(tmp, (int __user *) arg);
		break;

	case SCULLP_IOCHORDER: /* sHift: like Tell + Query */
		tmp = scullp_order;
		scullp_order = arg;
		return tmp;

/* 	case SCULLP_IOCSQSET: */
/* 		ret = __get_user(scullp_qset, (int __user *) arg); */
/* 		break; */

/* 	case SCULLP_IOCTQSET: */
/* 		scullp_qset = arg; */
/* 		break; */

/* 	case SCULLP_IOCGQSET: */
/* 		ret = __put_user(scullp_qset, (int __user *)arg); */
/* 		break; */

/* 	case SCULLP_IOCQQSET: */
/* 		return scullp_qset; */

/* 	case SCULLP_IOCXQSET: */
/* 		tmp = scullp_qset; */
/* 		ret = __get_user(scullp_qset, (int __user *)arg); */
/* 		if (ret == 0) */
/* 			ret = __put_user(tmp, (int __user *)arg); */
/* 		break; */

/* 	case SCULLP_IOCHQSET: */
/* 		tmp = scullp_qset; */
/* 		scullp_qset = arg; */
/* 		return tmp; */

	default:  /* redundant, as cmd was checked against MAXNR */
		return -ENOTTY;
	}

	return ret;
}

/*
 * The "extended" operations
 */

loff_t scullp_llseek (struct file *filp, loff_t off, int whence)
{
	struct scullp_dev *dev = filp->private_data; /* the first listitem */
	long newpos;

	switch(whence) {
	case 0: /* SEEK_SET */
		newpos = off;
		break;

	case 1: /* SEEK_CUR */
		newpos = filp->f_pos + off;
		break;

	case 2: /* SEEK_END */
		newpos = dev->size + off;
		break;

	default: /* can't happen */
		return -EINVAL;
	}
	if (newpos<0) return -EINVAL;
	filp->f_pos = newpos;
	return newpos;
}


/*
 * A simple asynchronous I/O implementation.
 */

struct async_work {
	struct kiocb *iocb;
	int result;
#ifdef RHEL6
	struct delayed_work work;
#else
	struct work_struct work;
#endif
};

/*
 * "Complete" an asynchronous operation.
 */
#ifndef RHEL6
#ifdef RHEL6
static void scullp_do_deferred_op(struct work_struct *work)
{
        struct async_work *stuff = container_of((struct delayed_work *) work, struct async_work, work);
#else
static void scullp_do_deferred_op(void *p)
{
	struct async_work *stuff = (struct async_work *) p;
#endif
	aio_complete(stuff->iocb, stuff->result, 0);
	kfree(stuff);
}
#endif


#ifndef RHEL6
static int scullp_defer_op(int write, struct kiocb *iocb, char __user *buf,
		size_t count, loff_t pos)
{
	struct async_work *stuff;
	int result;

	/* Copy now while we can access the buffer */
	if (write)
		result = scullp_write(iocb->ki_filp, buf, count, &pos);
	else
		result = scullp_read(iocb->ki_filp, buf, count, &pos);

	/* If this is a synchronous IOCB, we return our status now. */
	if (is_sync_kiocb(iocb))
		return result;

	/* Otherwise defer the completion for a few milliseconds. */
	stuff = kmalloc (sizeof (*stuff), GFP_KERNEL);
	if (stuff == NULL)
		return result; /* No memory, just complete now */
	stuff->iocb = iocb;
	stuff->result = result;
#ifdef RHEL6
	INIT_DELAYED_WORK(&stuff->work, scullp_do_deferred_op);
#else
	INIT_WORK(&stuff->work, scullp_do_deferred_op, stuff);
#endif
	schedule_delayed_work(&stuff->work, HZ/100);
	return -EIOCBQUEUED;
}
#endif


#ifndef RHEL6
static ssize_t scullp_aio_read(struct kiocb *iocb, char __user *buf, size_t count,
		loff_t pos)
{
	return scullp_defer_op(0, iocb, buf, count, pos);
}

static ssize_t scullp_aio_write(struct kiocb *iocb, const char __user *buf,
		size_t count, loff_t pos)
{
	return scullp_defer_op(1, iocb, (char __user *) buf, count, pos);
}
#endif


 
/*
 * Mmap *is* available, but confined in a different file
 */
extern int perma_mmap(struct file *filp, struct vm_area_struct *vma);


/*
 * The fops
 */

struct file_operations scullp_fops = {
	.owner =     THIS_MODULE,
	.llseek =    scullp_llseek,
	.read =	     scullp_read,
	.write =     scullp_write,
	.ioctl =     perma_crd_ioctl,
	.mmap =	     perma_mmap,
	.open =	     scullp_open,
	.release =   scullp_release,
#ifndef RHEL6
	.aio_read =  scullp_aio_read,
	.aio_write = scullp_aio_write,
#endif
};

int scullp_trim(struct scullp_dev *dev)
{
	if (dev->active_vmas.num_vmas) /* don't trim: there are active mappings */
		return -EBUSY;

	perma_crd_free_pages(dev);
	dev->size = 0;
	dev->order = scullp_order;
	return 0;
}

static void scullp_setup_cdev(struct scullp_dev *dev, int index)
{
	int err, devno = MKDEV(perma_crd_major, index);
    
	cdev_init(&dev->cdev, &scullp_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &scullp_fops;
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding scull%d", err, index);

	if(allocate_devices) {
	  char str[64] = "";
	  sprintf(str, "%s%d", MODULE_NAME, index);
#ifdef RHEL6
	  device_create(perma_class, NULL, devno, NULL, str, index);
#else
	  class_device_create(perma_class, NULL, devno, NULL, str, index);
#endif
	}
}

/*
 * Finally, the module stuff
 */

int perma_crd_init(void)
{
	int result, i;
	dev_t dev = MKDEV(perma_crd_major, 0);
	allocate_devices = perma_devs > 16 ? 0 : 1;

	/* WARNING: This code does not support hot insertion of cpu codes */
	NUMA_node_mask = num_online_nodes() - 1;

	/*
	 * Register your major, and accept a dynamic number.
	 */
	result = alloc_chrdev_region(&dev, 0, perma_devs, MODULE_NAME);
	perma_crd_major = MAJOR(dev);
	if (result < 0)
		return result;
	
	if(allocate_devices) {
	  perma_class = class_create(THIS_MODULE, MODULE_NAME);
	  if (IS_ERR(perma_class)) {
	    printk(KERN_ERR "Error creating %s class.\n", MODULE_NAME);
                result = PTR_ERR(perma_class);
                goto fail_malloc;
	  }
	}

	atomic_set(&global_num_concurrent_read_accesses, 0);
	atomic_set(&global_num_concurrent_write_accesses, 0);
	simulate_unified_channel = 1;

/* 	buf_ops = init_mmap_buffer_ops(); */
	buf_data = init_mmap_buffer_data(buf_data);

	/* 
	 * allocate the devices -- we can't have them static, as the number
	 * can be specified at load time
	 */
	scullp_devices = kmalloc(perma_devs*sizeof (struct scullp_dev), GFP_KERNEL);
	if (!scullp_devices) {
		result = -ENOMEM;
		goto fail_malloc;
	}
	memset(scullp_devices, 0, perma_devs*sizeof (struct scullp_dev));
	for (i = 0; i < perma_devs; i++) {
	  	scullp_devices[i].dev_id = MKDEV(perma_crd_major, i);
		init_prot_vm_area_set(&scullp_devices[i].active_vmas);
		scullp_devices[i].order = scullp_order;
		sema_init (&scullp_devices[i].sem, 1);
		spin_lock_init(&scullp_devices[i].mmap_rd_lock);
		INIT_RADIX_TREE(&scullp_devices[i].mmap_rd_pages, GFP_ATOMIC);
		atomic_set(&scullp_devices[i].num_concurrent_read_accesses, 0);
		atomic_set(&scullp_devices[i].num_concurrent_write_accesses, 0);

		scullp_setup_cdev(scullp_devices + i, i);
	}

	/* Once all of the data structures are allocated, initialize them */
	reset_device_parameters();

	if(ENABLE_PROFILING) {
	  initProfileCounters();
	}

	zero_page = alloc_page(GFP_NOIO | __GFP_ZERO);

#ifdef PTE_CLEAR_FLUSH
	strcat(comp_flags, "-PTECF");
#endif	

#ifdef SIM_NO_PAGE_COPY
	strcat(comp_flags, "-SIM_NPC");
#endif	

	printk(KERN_INFO "%s: module v%d.%d.%d%s\n", MODULE_NAME, version.major, version.minor, version.patch, comp_flags);

	perma_proc_setup(THIS_MODULE, "perma-ramdisk", version, comp_flags, perma_devs, 0);

	return 0; /* succeed */

  fail_malloc:
	unregister_chrdev_region(dev, perma_devs);
	return result;
}



void scullp_cleanup(void)
{
	int i;

	perma_proc_cleanup("perma-ramdisk");
	cleanup_mmap_params();

	__free_page(zero_page);

	for (i = 0; i < perma_devs; i++) {
	  if(allocate_devices) {
#ifdef RHEL6
	        device_destroy(perma_class, MKDEV(perma_crd_major, i));
#else
		class_device_destroy(perma_class, MKDEV(perma_crd_major, i));
#endif
	  }
	  cdev_del(&scullp_devices[i].cdev);
	  free_prot_vm_area_set(&scullp_devices[i].active_vmas);
	  scullp_trim(scullp_devices + i);
	}
	/* Do any cleanup that the buffer requires */
	cleanup_mmap_buffer_data(buf_data);
	
	if(allocate_devices) {
	  class_destroy(perma_class);
	}
	kfree(scullp_devices);
	unregister_chrdev_region(MKDEV (perma_crd_major, 0), perma_devs);
}


module_init(perma_crd_init);
module_exit(scullp_cleanup);
