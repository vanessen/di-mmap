/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/*
 * Ram backed block device driver.
 *
 * Copyright (C) 2007 Nick Piggin
 * Copyright (C) 2007 Novell Inc.
 *
 * Parts derived from drivers/block/rd.c, and drivers/block/loop.c, copyright
 * of their respective owners.
 */

/*
 *  12/10/2010: Brian Van Essen - vanessen1@llnl.gov
 *    - Tweaked the v2.6.25 version to compile in v2.6.18
 * 
 *  12/13/2010: Brian Van Essen - vanessen1@llnl.gov
 *    - Integrated delay support from Moneta Group at UCSD:
 *      "Modified to support variable latencies through writes to procfs entry
 *       Adrian Caulfield <acaulfie@cs.ucsd.edu>
 *       Sept 2009"
 *    - Added support for a wider range of delays and updated the reporting information.
 *    - Added support to allow a user to resize the device by writing to a procfs file
 *
 *  12/15/2010: Brian Van Essen - vanessen1@llnl.gov
 *    - Added support for modeling variable page sizes up to 4KB, where the read and write
 *      delay is incurred for each page access
 *
 *  12/16/2010: Brian Van Essen - vanessen1@llnl.gov
 *    - Added support for parallel versus serial delay insertion
 *    - Added support for a hybrid mix of schedule yields and short spin-waits to improve
 *      multi-threaded performance
 *    - Added a proc file to allow tuning of the new options.
 *
 *   1/13/2011: Brian Van Essen - vanessen1@llnl.gov
 *    - Added support for a transit delay that is proportional to the number of concurrent accesses
 *
 *   1/20/2011: Brian Van Essen - vanessen1@llnl.gov
 *    - Split the transit delay into read and write delays and made it programmable through the proc
 *      file interface.
 *
 *   2/4/2011: Brian Van Essen - vanessen1@llnl.gov
 *    - Instrumented the code with profiling counters and improved the output to the proc file system.
 *
 *   2/15/2011: Brian Van Essen - vanessen1@llnl.gov
 *     - Changed the page allocation scheme to interleave across NUMA nodes
 *     - Added additional instrumentation to identify source of inefficiency in radix_tree_lookup
 *     - Minor tweaks to support compiling with RHEL6
 *     - Changed variable delay call for parallel access to include time already waiting for copying data into
 *       the local target buffer
 *
 *   2/16/2011: Brian Van Essen - vanessen1@llnl.gov
 *     - Added the option to use a small two level page table rather than the radix tree for mapping pages, to 
 *       improve performance.  Currently it provides no significant performance gains and has some bugs when mapping
 *       >75% of the address space.
 *
 *   2/18/2011: Brian Van Essen - vanessen1@llnl.gov
 *     - Got rid of the old model of having multipler virtual channels in favor of having one channel per 
 *       brd device (minor number) and using software raid to strip accesses across brd devices.
 *
 *  Usage:
 *    insmod perma.ko rd_size=12582912 rd_nr=4
 *    mdadm --create --raid-devices=4 --level=0 /dev/md0 /dev/perma-ram0 /dev/perma-ram1 /dev/perma-ram2 /dev/perma-ram3 --chunk=256
 *    mdadm --stop /dev/md0
 *    rmmod perma.ko
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/major.h>
#include <linux/blkdev.h>
#include <linux/bio.h>
#include <linux/highmem.h>
#include <linux/gfp.h>
#include <linux/radix-tree.h>
#include <linux/buffer_head.h> /* invalidate_bh_lrus() */

#include <asm/uaccess.h>

#include "perma_delay.h"
#include "perma_stats.h"

//#define USE_PAGE_TABLE             /* Use custom build page tables */
#define NUM_TOP_LEVEL_PAGES 32
#define NUM_ENTRIES_PER_PAGE 512
#define PERMA_PGD_MASKE 0x0000000000003fff
#define PERMA_PTE_MASK 0x00000000000001ff

#ifdef USE_PAGE_TABLE
#define ALLOC_PAGES_ORDER       3
#else
#define ALLOC_PAGES_ORDER       0 /* The radix tree is really designed for 0th order pages */
#endif

#define SECTOR_SHIFT		9
#define PAGE_SECTORS_SHIFT	(PAGE_SHIFT - SECTOR_SHIFT)
#define PAGE_SECTORS		(1 << PAGE_SECTORS_SHIFT)

#define CONFIG_BLK_DEV_XIP

static int NUMA_node_mask = 0; /* A mask to interleave memory across the available NUMA nodes */

/*
 * Variables to hold the number of partitions and device size
 */
static int rd_nr = 4; /* By default create four ramdisks */
int rd_size = CONFIG_BLK_DEV_RAM_SIZE;

static int version = 5;

/* Function prototypes */
static void brd_cleanup_disks(void);
static int brd_setup_disks(void);

void cleanup_devices(void) {
  brd_cleanup_disks();
}

void setup_devices(void) {
  brd_setup_disks();
}

void reset_device_parameters(void) {
  initDelays();
}

void prettyprint_device_specifics(char *msg, char line_leader) {
  msg[0] = 0;
  return;
}

int tune_device_specifics(char *cmd, /* int tuning_param, */ int param1, int param2, int param3, int param4) {
  return 0;
}

void explain_device_tuning_params(void) {
  return;
}

void initDeviceSpecificProfileCounters(void) {
  return;
}

/*
 * The device scheme is derived from loop.c. Keep them in synch where possible
 * (should share code eventually).
 */
static LIST_HEAD(brd_devices);
static DEFINE_MUTEX(brd_devices_mutex);

/*
 * Each block ramdisk device has a radix_tree brd_pages of pages that stores
 * the pages containing the block device's contents. A brd page's ->index is
 * its offset in PAGE_SIZE units. This is similar to, but in no way connected
 * with, the kernel's pagecache or buffer cache (which sit above our block
 * device).
 */
struct brd_device {
	int		brd_number;
	int		brd_refcnt;
	loff_t		brd_offset;
	loff_t		brd_sizelimit;
	unsigned	brd_blocksize;

	struct request_queue	*brd_queue;
	struct gendisk		*brd_disk;
	struct list_head	brd_list;

	/*
	 * Backing store of pages and lock to protect it. This is the contents
	 * of the block device.
	 */
	spinlock_t		brd_lock;
#ifdef USE_PAGE_TABLE
	struct page ***page_table;
#else
	struct radix_tree_root	brd_pages;
#endif
	/* Add some performance counters each block ramdisk device */
	profile_info perf_cntr;
	/* Track the number of concurrent accesses to each block ramdisk device */
	atomic_t num_concurrent_read_accesses;
	atomic_t num_concurrent_write_accesses;
};

profile_info *select_ith_device(int i) {
  struct brd_device *brd;

  /* Iterate over all of the brd_devices to find the right one */
  list_for_each_entry(brd, &brd_devices, brd_list) {
    if (brd->brd_number == (i % rd_nr)) {
      goto out;
    }
  }
out:
  BUG_ON(!brd);
  BUG_ON(brd && brd->brd_number != i % rd_nr);
  return &brd->perf_cntr;
}

#ifdef USE_PAGE_TABLE
/*
 * Page table insertion
 * Implement a page table that uses 64KB pages and can allocate space for 256GB.
 * Use a two-level page table with 32 contiguous pages at the top level and 512 entries per page.
 * Starting with a super_page_index (not a byte address)
 * page table entry (PTE) - 9 bits : 8 ... 0
 * page global directory (PGD) - 5 + 9 bits : 22 ... 9
 */
static int page_table_insertion(struct brd_device *brd, unsigned long super_page_idx, struct page *page) {
  int pgd = (super_page_idx >> 9) & PERMA_PGD_MASKE;
  int pte_idx = super_page_idx & PERMA_PTE_MASK;
  struct page **pte = NULL;

  if(unlikely(brd->page_table == NULL)) {
    /* The top level of the page table has 16 pages with 512 entries each */
    brd->page_table = kzalloc(NUM_TOP_LEVEL_PAGES * NUM_ENTRIES_PER_PAGE * sizeof(struct page *), GFP_ATOMIC);
    if(brd->page_table == NULL) {
      printk(KERN_INFO "Unable to allocate the page table %p\n", brd->page_table);      
      printk(KERN_INFO "Failed to insert a page for super page %lu, with pgd=%x and pte=%x\n", super_page_idx, pgd, pte_idx);
      return -ENOMEM;
    }
  }

  if(unlikely(brd->page_table[pgd] == NULL)) {
    pte = kzalloc(NUM_ENTRIES_PER_PAGE * sizeof(struct page *), GFP_ATOMIC);
    brd->page_table[pgd] = pte;
    if(brd->page_table[pgd] == NULL) {
      printk(KERN_INFO "Unable to allocate the pgd[%d]= %p\n", pgd, brd->page_table[pgd]);
      printk(KERN_INFO "Failed to insert a page entry for super page %lu, with pgd=%x and pte=%x\n", super_page_idx, pgd, pte_idx);
      return -ENOMEM;
    }
  }else {
    pte = brd->page_table[pgd];
  }

  if(pte[pte_idx] != NULL) {
      return 1;
  }
  pte[pte_idx] = page;

  return 0;
}

static struct page *page_table_lookup(struct brd_device *brd, unsigned long super_page_idx) {
  int pgd = (super_page_idx >> 9) & PERMA_PGD_MASKE;
  int pte_idx = super_page_idx & PERMA_PTE_MASK;
  struct page **pte;
  struct page *page = NULL;
  volatile unsigned long long tsc_ini0, tsc_end0, tsc_ini1, tsc_end1, tsc_ini2, tsc_end2;
  volatile long long delta0, delta1, delta2;

  rdtscll(tsc_ini0);
  if(unlikely(brd->page_table == NULL)) {
    return NULL;
  }else {
    rdtscll(tsc_ini1);
    if(unlikely(brd->page_table[pgd] == NULL)) {
      return NULL;
    }else {
      pte = brd->page_table[pgd];
    
      rdtscll(tsc_ini2);
      if(unlikely(pte[pte_idx] == NULL)) {
	return NULL;
      }else {
	page = pte[pte_idx];
      }
      rdtscll(tsc_end2);
    }
    rdtscll(tsc_end1);
  }
  rdtscll(tsc_end0);


  delta2 = tsc_end2 - tsc_ini2;
  delta1 = (tsc_end1 - tsc_ini1) - delta2;
  delta0 = (tsc_end0 - tsc_ini0) - delta1 - delta2;
  if(delta1 > 1750 || delta0 > 1750 || delta2 > 1750) {
/*     printk(KERN_INFO "Thread %d on core %d: while in the page_table_lookup section pgd=%lld pte=%lld and pte_entry=%lld\n", current->pid, GET_CPU_ID(), delta0, delta1, delta2); */
    num_high++;
  }else{
    num_low++;
  }

  return page;
}

static int full_page_table_initialization(struct brd_device *brd) {
  int pgd;
  struct page **pte = NULL;

  printk(KERN_INFO "thread=%d on core=%d initializing page table locally.\n",  current->pid, GET_CPU_ID());

  if(brd->page_table == NULL) {
    brd->page_table = kzalloc(NUM_TOP_LEVEL_PAGES * NUM_ENTRIES_PER_PAGE * sizeof(struct page *), GFP_ATOMIC | __GFP_THISNODE);
    if(brd->page_table == NULL) {
      brd->page_table = kzalloc(NUM_TOP_LEVEL_PAGES * NUM_ENTRIES_PER_PAGE * sizeof(struct page *), GFP_ATOMIC);
      printk(KERN_INFO "thread=%d on core=%d Failed to insert a local page for page table init\n", current->pid, GET_CPU_ID());
      
      if(brd->page_table == NULL) {
	printk(KERN_INFO "Unable to allocate the page table %p\n", brd->page_table);      
	printk(KERN_INFO "Failed to insert a page for page table init\n");
	return -ENOMEM;
      }
    }
  }

  for(pgd = 0; pgd < NUM_TOP_LEVEL_PAGES * NUM_ENTRIES_PER_PAGE; pgd++) {
    if(brd->page_table[pgd] == NULL) {
      pte = kzalloc(NUM_ENTRIES_PER_PAGE * sizeof(struct page *), GFP_ATOMIC | __GFP_THISNODE);
      brd->page_table[pgd] = pte;
      
      if(brd->page_table[pgd] == NULL) {
	pte = kzalloc(NUM_ENTRIES_PER_PAGE * sizeof(struct page *), GFP_ATOMIC);
	brd->page_table[pgd] = pte;
	printk(KERN_INFO "thread=%d on core=%d Failed to insert a local page table entry for page table init\n",  current->pid, GET_CPU_ID());
	
	if(brd->page_table[pgd] == NULL) {
	  printk(KERN_INFO "Unable to allocate the pgd[%d]= %p\n", pgd, brd->page_table[pgd]);
	  printk(KERN_INFO "Failed to insert a page entry for page table init\n");
	  return -ENOMEM;
	}
      }
    }
  }

  return 0;
}
#endif

/*
 * Look up and return a brd's page for a given sector.
 */
static struct page *brd_lookup_page(struct brd_device *brd, sector_t sector)
{
	pgoff_t idx;
	struct page *page;
	unsigned long super_page_idx;

	/*
	 * The page lifetime is protected by the fact that we have opened the
	 * device node -- brd pages will never be deleted under us, so we
	 * don't need any further locking or refcounting.
	 *
	 * This is strictly true for the radix-tree nodes as well (ie. we
	 * don't actually need the rcu_read_lock()), however that is not a
	 * documented feature of the radix-tree API so it is better to be
	 * safe here (we don't have total exclusion from radix tree updates
	 * here, only deletes).
	 */
	idx = sector >> PAGE_SECTORS_SHIFT; /* sector to page index */
	super_page_idx = idx >> ALLOC_PAGES_ORDER; /* page index to super page index */
#ifdef USE_PAGE_TABLE
	page = page_table_lookup(brd, super_page_idx);
	BUG_ON(page && (page->index != (super_page_idx << ALLOC_PAGES_ORDER)));
#else
	rcu_read_lock();
	page = radix_tree_lookup(&brd->brd_pages, idx);
	rcu_read_unlock();

	BUG_ON(page && page->index != idx);
#endif

	return page;
}

/*
 * Look up and return a brd's page for a given sector.
 * If one does not exist, allocate an empty page, and insert that. Then
 * return it.
 */
static struct page *brd_insert_page(struct brd_device *brd, sector_t sector)
{
	pgoff_t idx;
	struct page *page;
	gfp_t gfp_flags;
	int numa_node;
	unsigned long super_page_idx;

	page = brd_lookup_page(brd, sector);
	if (page)
		return page;

	/*
	 * Must use NOIO because we don't want to recurse back into the
	 * block or filesystem layers from page reclaim.
	 *
	 * Cannot support XIP and highmem, because our ->direct_access
	 * routine for XIP must return memory that is always addressable.
	 * If XIP was reworked to use pfns and kmap throughout, this
	 * restriction might be able to be lifted.
	 */
	gfp_flags = GFP_NOIO | __GFP_ZERO/*  | __GFP_REPEAT */; /* Make sure to try really hard to make the allocation */
#ifndef CONFIG_BLK_DEV_XIP
	gfp_flags |= __GFP_HIGHMEM;
#endif
	/*
	 * alloc_page(gfp_mask) is the same as alloc_pages(gfp_mask, 0) - gfp.h
	 * alloc_pages(gfp_mask, order)	-> alloc_pages_node(numa_node_id(), gfp_mask, order) - gfp.h
	 * numa_node_id() - gives the socket id for the current running thread
	 * num_online_cpus() - how many cores there are in the system
	 * num_online_nodes() - how many processors (with memory) there are in the system
	 *
	 * in file: mm/mempolicy.c
	 * alloc_page_interleave(gfp_t gfp, unsigned order, unsigned nid)
	 * alloc_page_vma(gfp_t gfp, struct vm_area_struct *vma, unsigned long addr)
	 *
	 * alloc_pages_exact(size_t size, gfp_t gfp_mask) - request a block of pages
	 */
/* 	page = alloc_page(gfp_flags); */
	idx = sector >> PAGE_SECTORS_SHIFT;
	super_page_idx = (idx >> ALLOC_PAGES_ORDER);
	numa_node = super_page_idx & NUMA_node_mask;
	page = alloc_pages_node(numa_node, gfp_flags, ALLOC_PAGES_ORDER);
	if (!page)
		return NULL;

#ifdef USE_PAGE_TABLE
	spin_lock(&brd->brd_lock);
	if(page_table_insertion(brd, super_page_idx, page)) {
	  	__free_pages(page, ALLOC_PAGES_ORDER);
		/* If you fail to insert the page into the page table, check to see if there is already a valid page there. */
		page = page_table_lookup(brd, super_page_idx);
		BUG_ON(!page);
		BUG_ON(page->index != (super_page_idx << ALLOC_PAGES_ORDER));
	} else {
		page->index = super_page_idx << ALLOC_PAGES_ORDER;
	}
	spin_unlock(&brd->brd_lock);
#else
	if (radix_tree_preload(GFP_NOIO)) {
		__free_pages(page, ALLOC_PAGES_ORDER);
		return NULL;
	}

	spin_lock(&brd->brd_lock);
	if (radix_tree_insert(&brd->brd_pages, idx, page)) {
	  	__free_pages(page, ALLOC_PAGES_ORDER);
		page = radix_tree_lookup(&brd->brd_pages, idx);
		BUG_ON(!page);
		BUG_ON(page->index != idx);
	} else
		page->index = idx;
	spin_unlock(&brd->brd_lock);

	radix_tree_preload_end();
#endif

	return page;
}

/*
 * Free all backing store pages and radix tree. This must only be called when
 * there are no other users of the device.
 */
#define FREE_BATCH 16
static void brd_free_pages(struct brd_device *brd)
{
#ifdef USE_PAGE_TABLE
	struct page *page;
	struct page **pte;
	int i, j;
#else
	unsigned long pos = 0;
	struct page *pages[FREE_BATCH];
	int nr_pages;
#endif

#ifdef USE_PAGE_TABLE
	if(brd->page_table != NULL) {
	  for(i = 0; i < NUM_TOP_LEVEL_PAGES * NUM_ENTRIES_PER_PAGE; i++) {
	    if(brd->page_table[i] != NULL) {
	      pte = brd->page_table[i];
	      for(j = 0; j < NUM_ENTRIES_PER_PAGE; j++) {
		if(pte[j] != NULL) {
		  page = pte[j];
		  __free_pages(page, ALLOC_PAGES_ORDER);
		}
	      }
	      kfree(pte);
	    }
	  }
	  kfree(brd->page_table);
	}
#else
	do {
		int i;

		nr_pages = radix_tree_gang_lookup(&brd->brd_pages,
				(void **)pages, pos, FREE_BATCH);

		for (i = 0; i < nr_pages; i++) {
			void *ret;

			BUG_ON(pages[i]->index < pos);
			pos = pages[i]->index;
			ret = radix_tree_delete(&brd->brd_pages, pos);
			BUG_ON(!ret || ret != pages[i]);
			__free_pages(pages[i], ALLOC_PAGES_ORDER);
		}

		pos++;

		/*
		 * This assumes radix_tree_gang_lookup always returns as
		 * many pages as possible. If the radix-tree code changes,
		 * so will this have to.
		 */
	} while (nr_pages == FREE_BATCH);
#endif
}

/*
 * copy_to_brd_setup must be called before copy_to_brd. It may sleep.
 */
static int copy_to_brd_setup(struct brd_device *brd, sector_t sector, size_t n)
{
	unsigned int offset = (sector & (PAGE_SECTORS-1)) << SECTOR_SHIFT;
	size_t copy;

	copy = min_t(size_t, n, PAGE_SIZE - offset);
	if (!brd_insert_page(brd, sector))
		return -ENOMEM;
	if (copy < n) {
		sector += copy >> SECTOR_SHIFT;
		if (!brd_insert_page(brd, sector))
			return -ENOMEM;
	}
	return 0;
}

/*
 * Copy n bytes from src to the brd starting at sector. Does not sleep.
 */
static void copy_to_brd(struct brd_device *brd, const void *src,
			sector_t sector, size_t n)
{
	struct page *page;
	void *dst;
	unsigned int offset = (sector & (PAGE_SECTORS-1)) << SECTOR_SHIFT;
	size_t copy;
	int num_page_accesses;

	copy = min_t(size_t, n, PAGE_SIZE - offset);
	page = brd_lookup_page(brd, sector);
	BUG_ON(!page);

	/* Compute the total number of page accesses required */
	num_page_accesses = copy/PageSize + (copy % PageSize != 0 ? 1 : 0);

	dst = kmap_atomic(page, KM_USER1);
	if(copy == PAGE_SIZE) {
	  copy_page(dst, (void *) src);
	}else {
	  memcpy(dst + offset, src, copy);
	}
	if(PageAccessStyle == SERIAL_PAGE_ACCESS) {
	  /* Wait WriteDelay ns for every page accessed */
	  vdelay(WriteDelay * num_page_accesses);
	}
	kunmap_atomic(dst, KM_USER1);

	if (copy < n) {
		src += copy;
		sector += copy >> SECTOR_SHIFT;
		copy = n - copy;
		page = brd_lookup_page(brd, sector);
		BUG_ON(!page);

		/* Compute the total number of page accesses required */
		num_page_accesses = copy/PageSize + (copy % PageSize != 0 ? 1 : 0);

		dst = kmap_atomic(page, KM_USER1);
		if(copy == PAGE_SIZE) {
		  copy_page(dst, (void *)src);
		}else {
		  memcpy(dst, src, copy);
		}
		if(PageAccessStyle == SERIAL_PAGE_ACCESS) {
		  /* Wait WriteDelay ns for every page accessed */
		  vdelay(WriteDelay * num_page_accesses);
		}
		kunmap_atomic(dst, KM_USER1);
	}
}

/*
 * Copy n bytes to dst from the brd starting at sector. Does not sleep.
 */
static void copy_from_brd(void *dst, struct brd_device *brd,
			sector_t sector, size_t n)
{
	struct page *page;
	void *src;
	unsigned int offset = (sector & (PAGE_SECTORS-1)) << SECTOR_SHIFT;
	size_t copy;
	int num_page_accesses;
	volatile unsigned long long tsc_ini0, tsc_end0, tsc_ini1, tsc_end1;
	volatile long long delta0, delta1;
/* 	volatile unsigned long nvcsw, nivcsw; */

/* 	nvcsw = current->nvcsw; */
/* 	nivcsw = current->nivcsw; */

	rdtscll(tsc_ini0);
	copy = min_t(size_t, n, PAGE_SIZE - offset);
	rdtscll(tsc_ini1);
	page = brd_lookup_page(brd, sector);
	rdtscll(tsc_end1);
	/* Compute the total number of page accesses required */
	num_page_accesses = copy/PageSize + (copy % PageSize != 0 ? 1 : 0);
	if (page) {
		src = kmap_atomic(page, KM_USER1);
		if(copy == PAGE_SIZE) {
		  copy_page(dst, src);
		}else {
		  memcpy(dst, src + offset, copy);
		}
		if(PageAccessStyle == SERIAL_PAGE_ACCESS) {
		  /* Wait ReadDelay ns for every page read */
		  vdelay(ReadDelay * num_page_accesses);
		}
		kunmap_atomic(src, KM_USER1);
	} else {
		memset(dst, 0, copy);
		if(PageAccessStyle == SERIAL_PAGE_ACCESS) {
		  /* Wait ReadDelay ns for every page read */
		  vdelay(ReadDelay * num_page_accesses);
		}
	}
	rdtscll(tsc_end0);
	delta1 = tsc_end1 - tsc_ini1;
	delta0 = (tsc_end0 - tsc_ini0) - delta1;
	if(delta1 > 1750 || delta0 > 6000) {
/*  	  printk(KERN_INFO "Thread %d on core %d: while in the copy_from_brd section copying one page took %lld and looking up the page took %lld\n", current->pid, GET_CPU_ID(), delta0, delta1); */
	  num_high++;
	}else {
	  num_low++;
	}

/* 	if(current->nvcsw != nvcsw || current->nivcsw != nivcsw) { */
/*  	  printk(KERN_INFO "Thread %d on core %d: has had a context switch: v=%lu oldv=%lu, i=%lu oldi=%lu\n",  */
/* 		 current->pid, GET_CPU_ID(), current->nvcsw, nvcsw, current->nivcsw, nivcsw);	   */
/* 	} */

	if (copy < n) {
		dst += copy;
		sector += copy >> SECTOR_SHIFT;
		copy = n - copy;
		page = brd_lookup_page(brd, sector);
		/* Compute the total number of page accesses required */
		num_page_accesses = copy/PageSize + (copy % PageSize != 0 ? 1 : 0);
		if (page) {
			src = kmap_atomic(page, KM_USER1);
			if(copy == PAGE_SIZE) {
			  copy_page(dst, src);
			}else {
			  memcpy(dst, src, copy);
			}
			if(PageAccessStyle == SERIAL_PAGE_ACCESS) {
			  /* Wait ReadDelay ns for every page read */
			  vdelay(ReadDelay * num_page_accesses);
			}
			kunmap_atomic(src, KM_USER1);
		} else {
			memset(dst, 0, copy);
			if(PageAccessStyle == SERIAL_PAGE_ACCESS) {
			  /* Wait ReadDelay ns for every page read */
			  vdelay(ReadDelay * num_page_accesses);
			}
		}

	}
}

static int log_once = 1;
/*
 * Process a single bvec of a bio.
 */
static int brd_do_bvec(struct brd_device *brd, struct page *page,
			unsigned int len, unsigned int off, int rw,
			sector_t sector)
{
	void *mem;
	int err = 0;
	unsigned long long ini, end;
	int init_cpuID, cur_cpuID;

	if (rw != READ) {
		err = copy_to_brd_setup(brd, sector, len);
		if (err)
			goto out;
	}

	mem = kmap_atomic(page, KM_USER0);
	init_cpuID = GET_CPU_ID();
	rdtscll(ini); /* Read the timestamp counter (TSC) */

	if (rw == READ) {
		copy_from_brd(mem + off, brd, sector, len);
		flush_dcache_page(page);
	} else {
#ifdef RHEL6
		flush_dcache_page(page);
#endif
		copy_to_brd(brd, mem + off, sector, len);
	}

	rdtscll(end);/* Read the timestamp counter (TSC) */
	kunmap_atomic(mem, KM_USER0);

	err = (int) (end - ini);
	if(log_once && (err < 0 || end < ini)) {
	  printk(KERN_INFO "Thread %d: while in the kmap atomic section the processor counters have rolled over or I switched cpus diff = %llu or %d\n", current->pid, end-ini, err);
	  log_once = 0;
	}
	cur_cpuID = GET_CPU_ID();
	if(init_cpuID != cur_cpuID) {
	  printk(KERN_INFO "Thread %d: while in the kmap atomic section  has switched processors from %d to %d with a reported time of %d\n", current->pid, init_cpuID, cur_cpuID, err);
	  // Cycle counters are no longer reliable, break out of the loop
	}
out:
	return err;
}

static int brd_make_request(struct request_queue *q, struct bio *bio)
{
	struct block_device *bdev = bio->bi_bdev;
	struct brd_device *brd = bdev->bd_disk->private_data;
	int rw;
	struct bio_vec *bvec;
	sector_t sector;
	int i;
	int err = -EIO;
	int access_count, pipeline_delay;
	int TransitDelay; /* Transit delay to get request to / from NVRAM controller */
	int total_delay, ns_already_delayed;
	delay_info measured_delays;
	atomic_t *concurrent_access_counter;
	int DeviceAccessDelay;
	channel_profile_info *channel_stats;
	volatile unsigned long long ini, end;
	volatile long long mapping_time = 0, service_time = 0;
	volatile int init_cpuID, cur_cpuID;
/* 	cpumask_t old_mask, new_mask; */
	volatile unsigned long msr1 = 0xdeadbeef, msr2 = 0xdeadbeef, msr3 = 0xdeadbeef, msr4 = 0xdeadbeef;

	sector = bio->bi_sector;
	if (sector + (bio->bi_size >> SECTOR_SHIFT) >
						get_capacity(bdev->bd_disk))
		goto out;

	rw = bio_rw(bio);
	if (rw == READA)
		rw = READ;

	/* Select the variables for the read versus write path */
	if (rw == READ) {
	  concurrent_access_counter = &brd->num_concurrent_read_accesses;
	  channel_stats = &brd->perf_cntr.read;
	  /* Compute the transit time based on the size of the IO request and the modeled bandwidth */
	  TransitDelay = (bio->bi_size * rTransitLatency)/1000; /* convert back from picoseconds to nanoseconds */
	  DeviceAccessDelay = ReadDelay;
	}else {
	  concurrent_access_counter = &brd->num_concurrent_write_accesses;
	  channel_stats = &brd->perf_cntr.write;
	  /* Compute the transit time based on the size of the IO request and the modeled bandwidth */
	  TransitDelay = (bio->bi_size * wTransitLatency)/1000; /* convert back from picoseconds to nanoseconds */
	  DeviceAccessDelay = WriteDelay;
	}

	/* Track the number of concurrent accesses */
	if(PeripheralBusMultiplier == PCIeInf) {
	  access_count = 0;
	}else {
	  access_count = atomic_inc_return(concurrent_access_counter);
	}
	/* Note that the transit delay (pipeline delay) is modeled as being proportional
	   to the number of outstanding accesses to the NVRAM */

	/* update statistics */

	/* Pipeline delay is proportional by the number of concurrent accesses to NVRAM */
	pipeline_delay = access_count * TransitDelay;
	if(PageAccessStyle == PARALLEL_PAGE_ACCESS) {
	  /* Wait the DeviceAccessDelay plus pipeline delay ns for each access to the memory */
	  total_delay = DeviceAccessDelay + pipeline_delay;
	}else { /* e.g. SERIAL_PAGE_ACCESS */
	  /* Access to the individual pages will be added later. */
	  total_delay = pipeline_delay;
	}

	init_cpuID = GET_CPU_ID();
	/* Read the timestamp counter (TSC) */
	asm("cpuid");
	rdtsc_barrier();
	rdtscll(ini);

	wrmsrl(0xc0010004, 0);
	wrmsrl(0xc0010000, 0x1 << 22 | 0x3 << 16 | 0x01 << 8 | 0x42);
	wrmsrl(0xc0010005, 0);
	wrmsrl(0xc0010001, 0x1 << 22 | 0x3 << 16 | 0x1f << 8 | 0x43);
	wrmsrl(0xc0010006, 0);
	wrmsrl(0xc0010002, 0x1 << 22 | 0x3 << 16 | 0x1f << 8 | 0x45);
	wrmsrl(0xc0010007, 0);
	wrmsrl(0xc0010003, 0x1 << 22 | 0x3 << 16 | 0x1f << 8 | 0x46);
	
	bio_for_each_segment(bvec, bio, i) {
		unsigned int len = bvec->bv_len;
		err = brd_do_bvec(brd, bvec->bv_page, len,
					bvec->bv_offset, rw, sector);
		if (err < 0)
			break;
		sector += len >> SECTOR_SHIFT;
		mapping_time += err;
		err = 0;
	}
	rdmsrl(0xc0010004, msr1);
	rdmsrl(0xc0010005, msr2);
	rdmsrl(0xc0010006, msr3);
	rdmsrl(0xc0010007, msr4);
/* 	if(msr1 > 0 || msr2 > 0 || msr3 > 0 || msr4 > 0) { */
/* 	  printk(KERN_INFO "Thread %d on cpu %d: I saw %lx l1 cache misses serviced from northbridge and %lx l1 cache misses serviced from elsewhere and %lu l1-dtlb miss and %lu l2-dtlb miss -- reported mapping time = %lld\n", current->pid, init_cpuID, msr1, msr2, msr3, msr4, mapping_time); */
/* 	} */

	rdtsc_barrier();
	rdtscll(end);
	asm("cpuid");

	service_time = end-ini;
	if(service_time <= 0 || end < ini) {
	  printk(KERN_INFO "Thread %d: while kmaping the processor counters have rolled over or I switched cpus service_time = %llu\n", current->pid, end-ini);
	  /* Cycle counters are no longer reliable, set the service time to the current average */
	  if(atomic_read(&channel_stats->num_requests) > 0) {
	    service_time = channel_stats->aggregate.clock_cycles_to_service_kmap_request / atomic_read(&channel_stats->num_requests);
	  }else {
	    service_time = 0;
	  }
	}
	cur_cpuID = GET_CPU_ID();
	if(init_cpuID != cur_cpuID) {
	  printk(KERN_INFO "Thread %d: while kmaping has switched processors from %d to %d with a reported time of %llu\n", current->pid, init_cpuID, cur_cpuID, service_time);
	  /* Cycle counters are no longer reliable, set the service time to the current average */
	  if(atomic_read(&channel_stats->num_requests) > 0) {
	    service_time = channel_stats->aggregate.clock_cycles_to_service_kmap_request / atomic_read(&channel_stats->num_requests);
	  }else {
	    service_time = 0;
	  }
	}
	/* Divide the number of clock cycles by 2.5 to get the number of nanoseconds already delayed
	 * cc * 1/2.5 => cc * 2/5 => 6.5 / 16. ie. Y = ( 6 * X + (X >> 1)) >> 4 (this is a 2/4.92 divisor)
	 */
	ns_already_delayed = (6 * service_time + (service_time >> 1)) >> 4;

/* 	  printk(KERN_INFO "Thread %d on cpu %d: I have already taken %d ns(%lld cc) so the remaining delay is %d\n", current->pid, init_cpuID, ns_already_delayed, service_time, total_delay - ns_already_delayed); */
	if(ns_already_delayed >= total_delay) {
	  total_delay = 0;
	}else {
	  total_delay -= ns_already_delayed;
	}
	/* Make sure that the process does not migrate during its delay, otherwise the local target buffer may become remote. */
/* 	cpumask_set_cpu(init_cpuID, &new_mask); */
/* 	cpumask_and(&old_mask, &current->cpus_allowed, cpu_online_mask); /\* sched_getaffinity() *\/ */
/* 	cpumask_copy(&current->cpus_allowed, &new_mask); /\* sched_setaffinity() *\/ */
	measured_delays = vdelay(total_delay);
/* 	cpumask_copy(&current->cpus_allowed, &old_mask); /\* sched_setaffinity() *\/ */

	update_statistics(channel_stats, access_count, total_delay, bio->bi_size, measured_delays, service_time, mapping_time);

	/* Track the number of concurrent accesses */
	if(PeripheralBusMultiplier == PCIeInf) {
	  access_count = 0;
	}else {
 	  atomic_dec(concurrent_access_counter);
	}

out:
#ifdef RHEL6
	bio_endio(bio, err);
#else
	bio_endio(bio, bio->bi_size, err);
#endif

	return 0;
}

#ifdef CONFIG_BLK_DEV_XIP
#ifdef RHEL6
static int brd_direct_access (struct block_device *bdev, sector_t sector,
			      void **kaddr, unsigned long *pfn)
#else
static int brd_direct_access (struct block_device *bdev, sector_t sector,
			unsigned long *data)
#endif
{
	struct brd_device *brd = bdev->bd_disk->private_data;
	struct page *page;

	if (!brd)
		return -ENODEV;
	if (sector & (PAGE_SECTORS-1))
		return -EINVAL;
	if (sector + PAGE_SECTORS > get_capacity(bdev->bd_disk))
		return -ERANGE;
	page = brd_insert_page(brd, sector);
	if (!page)
		return -ENOMEM;
#ifdef RHEL6
	*kaddr = page_address(page);
	*pfn = page_to_pfn(page);
#else
	*data = (unsigned long)page_address(page);
#endif

	return 0;
}
#endif

#ifdef RHEL6
static int brd_ioctl(struct block_device *bdev, fmode_t mode,
			unsigned int cmd, unsigned long arg)
#else
static int brd_ioctl(struct inode *inode, struct file *file,
			unsigned int cmd, unsigned long arg)
#endif
{
	int error;
#ifndef RHEL6
	struct block_device *bdev = inode->i_bdev;
#endif
	struct brd_device *brd = bdev->bd_disk->private_data;

	if (cmd != BLKFLSBUF)
		return -ENOTTY;

	/*
	 * ram device BLKFLSBUF has special semantics, we want to actually
	 * release and destroy the ramdisk data.
	 */
	mutex_lock(&bdev->bd_mutex);
	error = -EBUSY;
	if (bdev->bd_openers <= 1) {
		/*
		 * Invalidate the cache first, so it isn't written
		 * back to the device.
		 *
		 * Another thread might instantiate more buffercache here,
		 * but there is not much we can do to close that race.
		 */
		invalidate_bh_lrus();
		truncate_inode_pages(bdev->bd_inode->i_mapping, 0);
		brd_free_pages(brd);
		error = 0;
	}
	mutex_unlock(&bdev->bd_mutex);

	return error;
}

#ifdef RHEL6
static const struct block_device_operations brd_fops = {
#else
static struct block_device_operations brd_fops = {
#endif
	.owner =		THIS_MODULE,
#ifdef RHEL6
	.locked_ioctl =		brd_ioctl,
#else
	.ioctl =		brd_ioctl,
#endif
#ifdef CONFIG_BLK_DEV_XIP
	.direct_access =	brd_direct_access,
#endif
};

/*
 * And now the modules code and kernel interface.
 */
#ifdef RHEL6
static int max_part;
static int part_shift;
#endif
module_param(rd_nr, int, 0);
MODULE_PARM_DESC(rd_nr, "Maximum number of brd devices");
module_param(rd_size, int, 0);
MODULE_PARM_DESC(rd_size, "Size of each RAM disk in kbytes.");
#ifdef RHEL6
module_param(max_part, int, 0);
MODULE_PARM_DESC(max_part, "Maximum number of partitions per RAM disk");
#endif
MODULE_LICENSE("GPL");
/* MODULE_ALIAS_BLOCKDEV_MAJOR(RAMDISK_MAJOR); */
static int PERMA_RAMDISK_MAJOR = 0; /* Request an available major device ID */
#ifdef RHEL6
MODULE_ALIAS("perma-rd");
#endif

#ifndef MODULE
/* Legacy boot options - nonmodular */
static int __init ramdisk_size(char *str)
{
	rd_size = simple_strtol(str, NULL, 0);
	return 1;
}
#ifdef RHEL6
__setup("ramdisk_size=", ramdisk_size);
#else
static int __init ramdisk_size2(char *str)
{
	return ramdisk_size(str);
}
__setup("ramdisk=", ramdisk_size);
__setup("ramdisk_size=", ramdisk_size2);
#endif
#endif

/*
 * The device scheme is derived from loop.c. Keep them in synch where possible
 * (should share code eventually).
 */
/* static LIST_HEAD(brd_devices); */
/* static DEFINE_MUTEX(brd_devices_mutex); */

static struct brd_device *brd_alloc(int i)
{
	struct brd_device *brd;
	struct gendisk *disk;

	brd = kzalloc(sizeof(*brd), GFP_KERNEL);
	if (!brd)
		goto out;

	brd->brd_number		= i;
	spin_lock_init(&brd->brd_lock);
#ifdef USE_PAGE_TABLE
	brd->page_table = NULL;
	full_page_table_initialization(brd);
#else
	INIT_RADIX_TREE(&brd->brd_pages, GFP_ATOMIC);
#endif

	atomic_set(&brd->num_concurrent_read_accesses, 0);
	atomic_set(&brd->num_concurrent_write_accesses, 0);

	brd->brd_queue = blk_alloc_queue(GFP_KERNEL);
	if (!brd->brd_queue)
		goto out_free_dev;
	blk_queue_make_request(brd->brd_queue, brd_make_request);
#ifdef RHEL6
	blk_queue_ordered(brd->brd_queue, QUEUE_ORDERED_TAG, NULL);
	blk_queue_max_hw_sectors(brd->brd_queue, 1024);
#else
	blk_queue_max_sectors(brd->brd_queue, 1024);
#endif
	blk_queue_bounce_limit(brd->brd_queue, BLK_BOUNCE_ANY);

#ifdef RHEL6
	disk = brd->brd_disk = alloc_disk(1 << part_shift);
#else
	disk = brd->brd_disk = alloc_disk(1);
#endif
	if (!disk)
		goto out_free_queue;
	disk->major		= PERMA_RAMDISK_MAJOR;
#ifdef RHEL6
	disk->first_minor	= i << part_shift;
#else
	disk->first_minor	= i;
#endif
	disk->fops		= &brd_fops;
	disk->private_data	= brd;
	disk->queue		= brd->brd_queue;
#ifdef RHEL6
	disk->flags |= GENHD_FL_SUPPRESS_PARTITION_INFO;
#endif
	sprintf(disk->disk_name, "perma-ram%d", i);
	set_capacity(disk, rd_size * 2);

	return brd;

out_free_queue:
	blk_cleanup_queue(brd->brd_queue);
out_free_dev:
	kfree(brd);
out:
	return NULL;
}

static void brd_free(struct brd_device *brd)
{
	put_disk(brd->brd_disk);
	blk_cleanup_queue(brd->brd_queue);
	brd_free_pages(brd);
	kfree(brd);
}

static struct brd_device *brd_init_one(int i)
{
	struct brd_device *brd;

	list_for_each_entry(brd, &brd_devices, brd_list) {
		if (brd->brd_number == i)
			goto out;
	}

	brd = brd_alloc(i);
	if (brd) {
		add_disk(brd->brd_disk);
		list_add_tail(&brd->brd_list, &brd_devices);
	}
out:
	return brd;
}

static void brd_del_one(struct brd_device *brd)
{
	list_del(&brd->brd_list);
	del_gendisk(brd->brd_disk);
	brd_free(brd);
}

static struct kobject *brd_probe(dev_t dev, int *part, void *data)
{
	struct brd_device *brd;
	struct kobject *kobj;

	mutex_lock(&brd_devices_mutex);
	brd = brd_init_one(dev & MINORMASK);
	kobj = brd ? get_disk(brd->brd_disk) : ERR_PTR(-ENOMEM);
	mutex_unlock(&brd_devices_mutex);

	*part = 0;
	return kobj;
}

/* Refactored the cleanup code so that it can be executed when resizing the device. */
static int brd_setup_disks(void) {
	int i, nr;
	unsigned long range;
	struct brd_device *brd, *next;

	/*
	 * brd module now has a feature to instantiate underlying device
	 * structure on-demand, provided that there is an access dev node.
	 * However, this will not work well with user space tool that doesn't
	 * know about such "feature".  In order to not break any existing
	 * tool, we do the following:
	 *
	 * (1) if rd_nr is specified, create that many upfront, and this
	 *     also becomes a hard limit.
	 * (2) if rd_nr is not specified, create 1 rd device on module
	 *     load, user can further extend brd device by create dev node
	 *     themselves and have kernel automatically instantiate actual
	 *     device on-demand.
	 */
#ifdef RHEL6
	part_shift = 0;
	if (max_part > 0)
		part_shift = fls(max_part);

	if (rd_nr > 1UL << (MINORBITS - part_shift))
#else
	if (rd_nr > 1UL << MINORBITS)
#endif
		return -EINVAL;

	if (rd_nr) {
		nr = rd_nr;
		range = rd_nr;
	} else {
		nr = CONFIG_BLK_DEV_RAM_COUNT;
#ifdef RHEL6
		range = 1UL << (MINORBITS-part_shift);
#else
		range = 1UL << MINORBITS;
#endif
	}

	for (i = 0; i < nr; i++) {
		brd = brd_alloc(i);
		if (!brd)
			goto out_free;
		list_add_tail(&brd->brd_list, &brd_devices);
	}

	/* point of no return */

	list_for_each_entry(brd, &brd_devices, brd_list)
		add_disk(brd->brd_disk);

	blk_register_region(MKDEV(PERMA_RAMDISK_MAJOR, 0), range,
				  THIS_MODULE, brd_probe, NULL, NULL);

	return 0;

out_free:
	list_for_each_entry_safe(brd, next, &brd_devices, brd_list) {
		list_del(&brd->brd_list);
		brd_free(brd);
	}

	return -ENOMEM;
}

static int __init brd_init(void)
{
#ifdef DEBUG
	printk(KERN_INFO "BRD INIT: Entering: %s:%i\n", __FILE__, __LINE__);
#endif

	/* WARNING: This code does not support hot insertion of cpu codes */
	NUMA_node_mask = num_online_nodes() - 1;

	PERMA_RAMDISK_MAJOR = register_blkdev(PERMA_RAMDISK_MAJOR, "perma-ramdisk");
	if (PERMA_RAMDISK_MAJOR <= 0)
		return -EIO;

	if(ENABLE_PROFILING) {
	  initProfileCounters();
	}

#ifdef DEBUG
	printk(KERN_INFO "BRD INIT: registered block device, setting up disks: %s:%i\n", __FILE__, __LINE__);
#endif

	if(brd_setup_disks() != 0)
	  goto out_err;

#ifdef DEBUG
	printk(KERN_INFO "BRD INIT: disks setup, establishing proc files: %s:%i\n", __FILE__, __LINE__);
#endif

	printk(KERN_INFO "perma_brd: module v%d\n", version);

	initDelays();

	perma_proc_setup(THIS_MODULE, version, rd_nr, rd_size);

	return 0;

out_err:
#ifdef DEBUG
	printk(KERN_INFO "BRD INIT: encountered error, unregistering device: %s:%i\n", __FILE__, __LINE__);
#endif
	unregister_blkdev(PERMA_RAMDISK_MAJOR, "perma_brd");
	return -ENOMEM;
}

/* Refactored the cleanup code so that it can be executed when resizing the device. */
static void brd_cleanup_disks(void) {
	unsigned long range;
	struct brd_device *brd, *next;

#ifdef RHEL6
	range = rd_nr ? rd_nr :  1UL << (MINORBITS - part_shift);
#else
	range = rd_nr ? rd_nr :  1UL << MINORBITS;
#endif

	list_for_each_entry_safe(brd, next, &brd_devices, brd_list)
		brd_del_one(brd);

	blk_unregister_region(MKDEV(PERMA_RAMDISK_MAJOR, 0), range);
	return;
}

static void __exit brd_exit(void)
{
	brd_cleanup_disks();
	unregister_blkdev(PERMA_RAMDISK_MAJOR, "perma-ramdisk");

	perma_proc_cleanup();
}

module_init(brd_init);
module_exit(brd_exit);

