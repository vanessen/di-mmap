/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/delay.h>

#include "perma_delay.h"
#include "rhel_compatibility.h"

int ReadDelay; /* Read delay per page access */
int WriteDelay; /* Write delay per page access */
int PageSize; /* Native page size of the memory technology: 64B PCM, 256KB Flash */
int rTransitLatency; /* Read transit latency picoseconds per B*/
int wTransitLatency; /* Write transit latency picoseconds per B */

int driverLatency;
int busReadLatency;
int busWriteLatency;
int devReadLatency;
int devWriteLatency;

int PeripheralBusMultiplier;

int DelayStyle; /* Are delays implemented with spin-waits (0) or a combination of spin-waits and schedule yields (1) */

int SchedYield_Threshold; /* At what delay value should the driver spin-wait versus yield the processor */
int Sleep_Threshold; /* At what delay value should the driver sleep versus yield the processor */
int PageAccessStyle; /* For accesses larger than a single page, how much internal parallelism is there in the memory controller
			0 - parallel (infinite controllers)
			1 - seriel 
			n - n-way parallel (n controllers)
		     */

#ifdef DEBUG
static int main_pid = 0;
#endif

void initDelays(void) {
  ReadDelay = 0;
  WriteDelay = 0;
  PageSize = 4096; /* 64 */
  DelayStyle = HYBRID_DELAY_STYLE;
  /* It seems that we can start getting reliable yields for things greater than 350ns */
  SchedYield_Threshold = 350;
  Sleep_Threshold = 1000000;
  PageAccessStyle = PARALLEL_PAGE_ACCESS;

  driverLatency = 0;
  busReadLatency = PCIE_READ_DMA_LATENCY;
  busWriteLatency = PCIE_WRITE_DMA_LATENCY;
  devReadLatency = DEV_READ_DMA_LATENCY;
  devWriteLatency = DEV_WRITE_DMA_LATENCY;
  PeripheralBusMultiplier = PCIeInf;
  initTransitLatency();
}


/* Compute the read and write transit latencies. */
void initTransitLatency(void) {
  if(PeripheralBusMultiplier == PCIeInf) {
    rTransitLatency = 0;
    wTransitLatency = 0;
  }else {
    rTransitLatency = busReadLatency/PeripheralBusMultiplier + devReadLatency;
    wTransitLatency = busWriteLatency/PeripheralBusMultiplier + devWriteLatency;
  }
  return;
}

/*
 * Converts from nanoseconds to approximate cpu cycles without using fp division
 */
unsigned long cyclesPerNS(unsigned long delay) {
  unsigned long cpu_mhz = (cpu_khz / 1000) % 1000;
  unsigned long cpu_ghz = cpu_khz / 1000000;
  unsigned long cycles;

  cycles = delay * cpu_ghz;

  if(cpu_mhz > 125 && cpu_mhz <= 325) { /* Approximate with 0.25 */
    cycles += delay >> 2; /* Add delay / 4 */
  }else if(cpu_mhz > 325 && cpu_mhz <= 625) { /* Approximate with 0.5 */
    cycles += delay >> 1; /* Add delay / 2 */
  }else if(cpu_mhz > 625 && cpu_mhz <= 875) { /* Approximate with 0.75 */
    cycles += (delay >> 1) + (delay >> 2); /* Add delay / 2 + delay / 4 */
  }else if(cpu_mhz > 875) {/* Approximate with 1 */
    cycles += delay;
  }

  return cycles;
}

/*
 * Converts from clock cycles to nanoseconds
 */
unsigned long nsPerCycle(unsigned long num_cc) {
  unsigned long cpu_mhz = cpu_khz / 1000;
  unsigned long cpu_ghz = cpu_mhz / 1000;
  unsigned long ns;

  /* Provide a special case for ~2500 MHz since we have processors at that speed */
  if(cpu_mhz > 2250 && cpu_mhz <= 2750) {
    /* Divide the number of clock cycles by 2.5 to get the number of nanoseconds already delayed
     * cc * 1/2.5 => cc * 2/5 => 6.5 / 16. ie. Y = ( 6 * X + (X >> 1)) >> 4 (this is a 2/4.92 divisor)
     */
    ns = (6 * num_cc + (num_cc >> 1)) >> 4;
  }else {
    ns = num_cc / cpu_ghz;
  }
  
  return ns;
}

delay_info vdelay(unsigned long delay) {
  volatile unsigned long long ini, end;
  volatile long long diff;
  unsigned long num_cycles_to_delay;
  delay_info measured_delays;
  volatile int init_cpuID, cur_cpuID;
#ifdef DEBUG
  unsigned long delta;
#endif

  init_cpuID = GET_CPU_ID();

#ifdef DEBUG
  /* Technically this is the wrong way to check the value of an atomic value,
     but since it is only for debug purposes it is okay. */
  //  if(atomic_read(&num_concurrent_read_accesses) == 1) {
    main_pid = current->pid;
/*     printk("Thread %d is setting the pid and executing on cpu = %d\n", current->pid, init_cpuID); */
    //  }
#endif

  measured_delays.counted_clock_cycles = 0;
  measured_delays.num_delay_loops = 0;
  measured_delays.num_cpu_transitions = 0;
  if(delay == 0) {return measured_delays;}
  if((long) delay < 0) {
    printk("WARNING: vdelay received a negative delay %ld\n", (long) delay);
    return measured_delays;
  }
  if(delay > MAX_DELAY_THRESHOLD) {
    printk("WARNING: vdelay received an excessively large delay %lu\n", delay);
  }
  num_cycles_to_delay = cyclesPerNS(delay);

  if(DelayStyle == HYBRID_DELAY_STYLE) {
    if(delay <= SchedYield_Threshold) {
      ndelay(delay);
      /* To minimize overhead, approximate that the ndelay is precise */
      measured_delays.counted_clock_cycles = num_cycles_to_delay;
      measured_delays.num_delay_loops = 1;
    }else {
      /* Read the timestamp counter (TSC) */
      rdtscll(ini);
      do {
	/* Do not use the schedule() call directly as mentioned in the LDD3 book, it
	 * fails to properly switch between different ready threads in the same process.
	 * Furthermore, 
	 * 	set_current_state(TASK_INTERRUPTIBLE);
	 * 	schedule_timeout(1); 
	 * only work at the jiffy resolution, which is ~1 msec on current kernels.
	 */
	if(delay > Sleep_Threshold) {
	  set_current_state(TASK_INTERRUPTIBLE);
	  schedule_timeout(1);
	}else {
	  yield();
	}
	rdtscll(end);
	diff = end-ini;
	measured_delays.num_delay_loops++;
	if(diff <= 0 || end < ini) {
	  if(DEBUG_LOGGING >= 1) {
	    printk(KERN_INFO "Thread %d: the processor counters have rolled over or I switched cpus diff = %lld\n", current->pid, diff);
	  }
	  measured_delays.counted_clock_cycles = 0;
	  measured_delays.num_delay_loops = 0;
	  measured_delays.num_cpu_transitions = 1;
	  return measured_delays;
	}
	cur_cpuID = GET_CPU_ID();
	if(init_cpuID != cur_cpuID) {
/* 	  char msg[256]; */
/* 	  cpumask_scnprintf(msg, 256, &current->cpus_allowed); */
// BVE Fix thread switching
	  if(DEBUG_LOGGING >= 2) {
	    printk(KERN_INFO "Thread %d: has switched processors from %d to %d\n", current->pid, init_cpuID, cur_cpuID);
	  }
	  //	  printk(KERN_INFO "Thread %d: has switched processors from %d to %d while the cpumask_set is %s\n", current->pid, init_cpuID, cur_cpuID/* , msg */);
	  // Cycle counters are no longer reliable, break out of the loop
	  // Clear measured delays and increment counter to mark the number of cpu switches
	  measured_delays.counted_clock_cycles = 0;
	  measured_delays.num_delay_loops = 0;
	  measured_delays.num_cpu_transitions = 1;
	  return measured_delays;
	}
      }while(diff < num_cycles_to_delay);
      measured_delays.counted_clock_cycles = diff;
#ifdef DEBUG
      delta =  diff - num_cycles_to_delay;
      if(current->pid == main_pid) {
	printk("TID %d Delay %6lu - Microsecond time lapse: %llu %llu %lli (delta=%li)\n", current->pid, delay, ini, end, diff, delta);
      }
#endif
    }
  }else {
    if(delay < 1000) {
      ndelay(delay);
    }else {
      /* Scale from nanoseconds to microseconds */
      delay = delay / 1000;
      if(delay < 1000) { /* microseconds */
	udelay(delay);
      }else {
	/* Scale from microseconds to milliseconds */
	delay = delay / 1000;
	mdelay(delay);
      }
    }
  }
  return measured_delays;
}

long inline compute_expected_delay(int access_count,
				  int TransitLatency,
				  int DeviceAccessDelay,
				  int size,
				  long long service_time) {

	long pipeline_delay;
	int TransitDelay; /* Transit delay to get request to / from NVRAM controller */
	long total_delay, ns_already_delayed;

	TransitDelay = (size * TransitLatency)/1000; /* convert back from picoseconds to nanoseconds */

	/* Note that the transit delay (pipeline delay) is modeled as being proportional
	   to the number of outstanding accesses to the NVRAM */

	/* Pipeline delay is proportional by the number of concurrent accesses to NVRAM */
	pipeline_delay = access_count * TransitDelay;
	if(PageAccessStyle == PARALLEL_PAGE_ACCESS) {
	  /* Wait the DeviceAccessDelay plus pipeline delay ns for each access to the memory */
	  total_delay = DeviceAccessDelay + pipeline_delay;
	}else { /* e.g. SERIAL_PAGE_ACCESS */
	  /* Access to the individual pages will be added later. */
	  total_delay = pipeline_delay;
	}

	if(service_time == 0) {
	  /* If an initial cpu and service time is not provided ignore the ns_already_delayed */
	  ns_already_delayed = 0;
	}else {
	  ns_already_delayed = nsPerCycle(service_time);
	}

	if(ns_already_delayed >= total_delay) {
	  total_delay = 0;
	}else if(ns_already_delayed < 0) {
	  if(DEBUG_LOGGING >= 1) {
	    printk("WARNING: compute delay thinks that things have already delayed by a negative number %ld ns_already_delayed for %lld service_time\n", ns_already_delayed, service_time);
	  }
	  /* Don't change total_delay */
	}else {
	  total_delay -= ns_already_delayed;
	}
	return total_delay;
}
