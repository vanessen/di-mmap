/*
 * mm.h
 * Return true if this page is mapped into pagetables.
 */
static inline int page_mapped(struct page *page)
{
        return atomic_read(&(page)->_mapcount) >= 0;
}

SHMEM uses 
find_lock_page(mapping, idx

and 

unlock_page(filepage);


Need to check the page_mapcount and get a handle on what it should be and when

	if(page_count(tagged_page->page) != 1) {
	  printk("EXPECTED: page %p has a page count is %d and mapping is %d\n",
		 page_address(tagged_page->page), page_count(tagged_page->page), page_mapcount(tagged_page->page));
	}

	if(page_count(tagged_page->page) > 3) {
	  printk("INTERESTING: page %p has a page count is %d and mapping is %d\n",
		 page_address(tagged_page->page), page_count(tagged_page->page), page_mapcount(tagged_page->page));
	}


TODO

Fix check_vma_page_table
Fix max size of 32GB on linear array