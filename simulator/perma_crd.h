/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/* -*- C -*-
 * Character device interface to the block ramdisk core - used to provide mmap capability
 *
 * Derived from the scullp char module from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 */

#ifndef _PERMA_CRD_H_
#define _PERMA_CRD_H_

#include <linux/ioctl.h>
#include <linux/cdev.h>
#include <linux/workqueue.h>

#include "perma_delay.h"
#include "perma_stats.h"
#include "mmap_fault_handler.h"
#include "vma_set_list.h"
#include "rhel_compatibility.h"

/* Use some preprocessor voodoo to create parameterizable module names */
#ifndef MODULE_ID
#define MODULE_ID
#endif

#ifndef BASE_MODULE_NAME
#define BASE_MODULE_NAME perma-mmap-ramdisk
#endif

#define XMODULE_NAME(s) #s
#define YMODULE_NAME(b,i) XMODULE_NAME(b ## i)
#define BUILD_MODULE_NAME(b,i) YMODULE_NAME(b, i)

#define MODULE_NAME BUILD_MODULE_NAME(BASE_MODULE_NAME, MODULE_ID)

/*
 * Macros to help debugging
 */

#undef PDEBUG             /* undef it, just in case */
#ifdef SCULLP_DEBUG
#  ifdef __KERNEL__
     /* This one if debugging is on, and kernel space */
#    define PDEBUG(fmt, args...) printk( KERN_DEBUG "scullp: " fmt, ## args)
#  else
     /* This one for user space */
#    define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

#undef PDEBUGG
#define PDEBUGG(fmt, args...) /* nothing: it's a placeholder */

#define SCULLP_MAJOR 0   /* dynamic major by default */

#define PERMA_DEVS 2    /* default with 2 devices */

/*
 * The bare device is a variable-length region of memory.
 * Use a linked list of indirect blocks.
 *
 * "scullp_dev->data" points to an array of pointers, each
 * pointer refers to a memory page.
 *
 * The array (quantum-set) is SCULLP_QSET long.
 */
#define SCULLP_ORDER    0 /* one page at a time */
#define SCULLP_QSET     500

#define SECTOR_SHIFT		9
#define PAGE_SECTORS_SHIFT	(PAGE_SHIFT - SECTOR_SHIFT)
#define PAGE_SECTORS		(1 << PAGE_SECTORS_SHIFT)
#define ALLOC_PAGES_ORDER 0 /* mmaping is more straightforward with 0th order pages */

/* Track the number of concurrent accesses to each block ramdisk device */
extern atomic_t global_num_concurrent_read_accesses;
extern atomic_t global_num_concurrent_write_accesses;
extern int simulate_unified_channel;

struct scullp_dev {
	dev_t dev_id; /* Device number: MAJOR,MINOR */
	prot_vm_area_set_t active_vmas; /* Set of VMAs that are mapped onto this device */
  	int order;                /* the current allocation order */
	size_t size;
	struct semaphore sem;     /* Mutual exclusion */
	struct cdev cdev;
	struct inode *inode;
	/*
	 * Backing store of pages and lock to protect it. This is the contents
	 * of the block device.
	 */
	spinlock_t		mmap_rd_lock;
	struct radix_tree_root	mmap_rd_pages;

	/* Add some performance counters each character ramdisk device */
	profile_info perf_cntr;
	/* Track the number of concurrent accesses to each block ramdisk device */
	atomic_t num_concurrent_read_accesses;
	atomic_t num_concurrent_write_accesses;
};

#define FREE_BATCH 16

extern struct scullp_dev *scullp_devices;

extern struct file_operations scullp_fops;

/*
 * The different configurable parameters
 */
extern int scullp_major;     /* main.c */
extern int perma_devs;
extern int scullp_order;

extern long perma_crd_max_size;

/*
 * Prototypes for shared functions
 */
int scullp_trim(struct scullp_dev *dev);
struct scullp_dev *scullp_follow(struct scullp_dev *dev, int n);
struct page *perma_crd_lookup_page(struct scullp_dev *crd, sector_t sector);
struct page *perma_crd_lookup_or_insert_page(struct scullp_dev *crd, sector_t sector);
void perma_crd_free_pages(struct scullp_dev *crd);
void perma_crd_init_pages(struct scullp_dev *crd);

/*
 * Prototypes for externally used functions
 */
dev_t get_device_id(void *device);
prot_vm_area_set_t *get_device_active_vmas(void *device);
dev_t inline get_ith_device_id(int i);
int read_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep);
int write_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep);
int flush_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep);

#ifdef SCULLP_DEBUG
#  define SCULLP_USE_PROC
#endif

/*
 * Ioctl definitions
 */

/* Use 'K' as magic number */
#define SCULLP_IOC_MAGIC  'K'

#define SCULLP_IOCRESET    _IO(SCULLP_IOC_MAGIC, 0)

/*
 * S means "Set" through a ptr,
 * T means "Tell" directly
 * G means "Get" (to a pointed var)
 * Q means "Query", response is on the return value
 * X means "eXchange": G and S atomically
 * H means "sHift": T and Q atomically
 */
#define SCULLP_IOCSORDER   _IOW(SCULLP_IOC_MAGIC,  1, int)
#define SCULLP_IOCTORDER   _IO(SCULLP_IOC_MAGIC,   2)
#define SCULLP_IOCGORDER   _IOR(SCULLP_IOC_MAGIC,  3, int)
#define SCULLP_IOCQORDER   _IO(SCULLP_IOC_MAGIC,   4)
#define SCULLP_IOCXORDER   _IOWR(SCULLP_IOC_MAGIC, 5, int)
#define SCULLP_IOCHORDER   _IO(SCULLP_IOC_MAGIC,   6)
#define SCULLP_IOCSQSET    _IOW(SCULLP_IOC_MAGIC,  7, int)
#define SCULLP_IOCTQSET    _IO(SCULLP_IOC_MAGIC,   8)
#define SCULLP_IOCGQSET    _IOR(SCULLP_IOC_MAGIC,  9, int)
#define SCULLP_IOCQQSET    _IO(SCULLP_IOC_MAGIC,  10)
#define SCULLP_IOCXQSET    _IOWR(SCULLP_IOC_MAGIC,11, int)
#define SCULLP_IOCHQSET    _IO(SCULLP_IOC_MAGIC,  12)

#define SCULLP_IOC_MAXNR 12



#define MMAP_BUFFER "mmap_buffer"
#define RESET_MMAP "reset_mmap"
#define SCAN_PTE "scan_pte"

#endif // _PERMA_CRD_H_
