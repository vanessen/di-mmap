/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _PERMA_STATS_H
#define _PERMA_STATS_H

#include <linux/module.h>
#include "report_stats.h"
#include "process_tsc.h"
#include "perma_delay.h"

typedef struct io_stats_t{
  unsigned long long concurrent_accesses;
  unsigned long long requested_delay;
  unsigned long long transfer_size;
  unsigned long long measured_clock_cycles;
  unsigned long long num_delay_loops;
  tsc_stats tsc_pair0;
  tsc_stats tsc_pair1;
}io_stats;

typedef struct channel_profile_info_t {
  io_stats max;
  io_stats aggregate;
  io_stats min;
  atomic64_t num_requests;
  atomic64_t num_zap_requests;
  atomic64_t num_zapped_page_ranges;
}channel_profile_info;

typedef struct profile_info_t {
  channel_profile_info read;
  channel_profile_info write;
}profile_info;

extern long num_high, num_low;

/* Function Prototypes */
void init_io_stats(io_stats *stats);
void initProfileCounters(void);

void prettyprint_delay_style(char *msg);
void prettyprint_page_access_style(char *msg);

void compute_average_io_stats(io_stats *aggregate, io_stats *average, unsigned long num_requests, unsigned long num_zap_requests);

void print_read_stats(char *msg);
void print_write_stats(char *msg);
void print_timing_stats(char *msg);

extern void print_io_stats_header(char *msg, char line_leader);

/*
 * Output to perma_stats file using a sequence file
 */

#define NUM_STANDARD_PAGES 7
#define NUM_STANDARD_TUNING_PARAMS 3

/* Tuning strings */
#define HELP "help"
#define RESET_PARAMS "reset_params"
#define RESET "reset"
#define MISC "misc"
#define VDELAY_THRESHOLDS "vdelay_thresholds"
#define BUS_LATENCY "bus_latency"
#define CTRLR_LATENCY "ctrlr_latency"
#define CLEAR_COUNTERS "clear_counters"
#define CLEAR "clear"
#define DEV_LATENCY "dev_latency"
#define ACCESS_TIME "access_time"
#define SET_PAGE_SIZE "page_size"
#define SET_PAGESIZE "PageSize"
#define BUS_WIDTH "bus_width"
#define BUS_MULT "bus_mult"
#define DRIVER_LATENCY "driver_latency"
#define DEVICE_SIZE "device_size"
#define DEBUG_DRIVER "debug"

#define PROC_STATS "stats"
#define PROC_TUNING "tuning"
#define PROC_HISTOGRAM "histogram"

void explain_tuning_params(void);

/* int perma_proc_open(struct inode *inode, struct file *file); */
void perma_proc_setup(struct module *owner, char *module_name, version_number_t driver_version, char driver_comp_flags[256], int driver_num_devices, int driver_device_size);
void perma_proc_cleanup(char *module_name);

/*
 * Provide handler functions to setup, cleanup, and iterate over the specific devices
 */
extern void cleanup_devices(void);
extern void setup_devices(void);
extern profile_info *select_ith_device(int i);
extern dev_t get_ith_device_id(int i);
extern void reset_device_parameters(void);
extern void prettyprint_device_specifics(struct seq_file *s, char *msg, char line_leader);
extern int tune_device_specifics(char *cmd, int param1, int param2, int param3, int param4);
extern void explain_device_tuning_params(void);
extern void report_page_fault_histogram(dev_t device_id, char *line0, char *line1, char *line2, char *line3, char *line4);


void update_statistics(channel_profile_info *channel_stats, 
		       int access_count, 
		       int total_delay, 
		       unsigned long long size, 
		       delay_info measured_delays, 
		       tsc_stats tsc_pair0, 
		       tsc_stats tsc_pair1,
		       int zapped_page_range);

extern void initDeviceSpecificProfileCounters(void);

#endif /* _PERMA_STATS_H */
