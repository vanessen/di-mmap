/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/* -*- C -*-
 * Character device interface to the block ramdisk core - used to provide mmap capability
 *
 * Derived from the scullp char module from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 */

#ifndef _DI_MMAP_H_
#define _DI_MMAP_H_

#include <linux/types.h>
#include <linux/ioctl.h>
#include <linux/cdev.h>
/* #include <linux/workqueue.h> */

#include "di-mmap-ctrl.h"
#include "di-mmap_stats.h"
#include "mmap_fault_handler.h"
#include "vma_set_list.h"
#include "rhel_compatibility.h"
#include "report_stats.h"

/* Use some preprocessor voodoo to create parameterizable module names */
#ifndef MODULE_ID
#define MODULE_ID A
#endif

#ifndef BASE_MODULE_NAME
#define BASE_MODULE_NAME di-mmap-runtime
#endif

#ifndef BASE_RUNTIME_NAME
#define BASE_RUNTIME_NAME runtime
#endif

#define XMODULE_NAME(s) #s
#define YMODULE_NAME(b,i) XMODULE_NAME(b ## i)
#define BUILD_MODULE_NAME(b,i) YMODULE_NAME(b, i)

#define MODULE_NAME BUILD_MODULE_NAME(BASE_MODULE_NAME, MODULE_ID)
#define RUNTIME_NAME BUILD_MODULE_NAME(BASE_RUNTIME_NAME, MODULE_ID)

#define SECTOR_SHIFT		9
#define PAGE_SECTORS_SHIFT	(PAGE_SHIFT - SECTOR_SHIFT)
#define PAGE_SECTORS		(1 << PAGE_SECTORS_SHIFT)
#define ALLOC_PAGES_ORDER 0 /* mmaping is more straightforward with 0th order pages */

/* Track the number of concurrent accesses to each block ramdisk device */
extern atomic_t global_num_concurrent_read_accesses;
extern atomic_t global_num_concurrent_write_accesses;
extern int simulate_unified_channel;

#define DIMMAP_NULL 0
#define DIMMAP_BLOCKDEV 1
#define DIMMAP_FILEMAP  2

struct raw_device_data {
  union {
	struct block_device *dev;
	struct file *filp;
  }binding;
	int type;
	int inuse;
/* 	int fd; */
	char filename[MAX_FILENAME_LEN];

	dev_t dev_id; /* Device number: MAJOR,MINOR */
	prot_vm_area_set_t active_vmas; /* Set of VMAs that are mapped onto this device */
	/* Add some performance counters each character ramdisk device */
	profile_info perf_cntr;
	/* Track the number of concurrent accesses to each block ramdisk device */
	atomic_t num_concurrent_read_accesses;
	atomic_t num_concurrent_write_accesses;

	const struct inode_operations *orig_i_ops; /* Store the inode ops of the /dev file */
	struct inode_operations *inode_ops;
};

/*
 * The different configurable parameters
 */
extern int perma_crd_major;     /* main.c */
extern int perma_devs;
extern int scullp_order;

extern long perma_crd_max_size;

/*
 * Prototypes for externally used functions
 */
dev_t inline get_ith_device_id(int i);
dev_t get_device_id(void *device);
prot_vm_area_set_t *get_device_active_vmas(void *device);
int read_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep);
int write_page(/* struct vm_area_struct *vma, */ struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep);
int flush_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep);

inline void print_file_pointer_info(struct file *filp, char *str);
inline void print_address_space_ops_info(const struct address_space_operations *a_ops, char *str);

int flush_file(struct address_space *mapping,  loff_t offset, loff_t len);
inline int redirect_IO(struct raw_device_data *dimmap_dev, struct file *filp, struct address_space *tgt_mapping);
inline int release_IO(struct raw_device_data *dimmap_dev, struct inode *inode);

/*
 * Ioctl definitions
 */


#define MMAP_BUFFER "mmap_buffer"
#define RESET_MMAP "reset_mmap"
#define SCAN_PTE "scan_pte"

extern const struct file_operations raw_fops;

#endif // _DI_MMAP_H_
