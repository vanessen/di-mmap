/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/*
 * DI-MMAP shadow file system
 *  - This was derived from the simple ram filesystem
 *  - Files that are created here are backed by files on a proper file system
 *  - di-mmap handler is invoked for all files
 */
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/pagemap.h>
#include <linux/highmem.h>
#include <linux/time.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/backing-dev.h>
#include <linux/sched.h>
#include <linux/parser.h>
#include <linux/magic.h>
#include <asm/uaccess.h>
#include <linux/file.h>
#include <linux/fsnotify.h>
#include <linux/smp_lock.h>
#include <linux/buffer_head.h> /* invalidate_bh_lrus() */

#include "di-mmap.h"
#include "di-mmap-fs.h"
//#include "internal.h"

#define RAMFS_DEFAULT_MODE	0755

static const struct super_operations ramfs_ops;
static const struct inode_operations ramfs_dir_inode_operations;

static struct backing_dev_info ramfs_backing_dev_info = {
	.name		= "ramfs",
	.ra_pages	= 0,	/* No readahead */
	.capabilities	= BDI_CAP_NO_ACCT_AND_WRITEBACK |
			  BDI_CAP_MAP_DIRECT | BDI_CAP_MAP_COPY |
			  BDI_CAP_READ_MAP | BDI_CAP_WRITE_MAP | BDI_CAP_EXEC_MAP,
};

static int ramfs_parse_options(char *data, struct ramfs_mount_opts *opts);


struct inode *ramfs_get_inode(struct super_block *sb, int mode, dev_t dev)
{
	struct inode * inode = new_inode(sb);

	printk(KERN_ALERT "I have been asked to get an inode\n");
	if (inode) {
		inode->i_mode = mode;
		inode->i_uid = current_fsuid();
		inode->i_gid = current_fsgid();
		inode->i_mapping->a_ops = &ramfs_aops;
		inode->i_mapping->backing_dev_info = &ramfs_backing_dev_info;
		mapping_set_gfp_mask(inode->i_mapping, GFP_HIGHUSER);
		mapping_set_unevictable(inode->i_mapping);
		inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;
		switch (mode & S_IFMT) {
		default:
			init_special_inode(inode, mode, dev);
			break;
		case S_IFREG:
			inode->i_op = &ramfs_file_inode_operations;
			inode->i_fop = &dimmap_fs_file_operations;
			break;
		case S_IFDIR:
			inode->i_op = &ramfs_dir_inode_operations;
			inode->i_fop = &simple_dir_operations;

			/* directory inodes start off with i_nlink == 2 (for "." entry) */
			inc_nlink(inode);
			break;
		case S_IFLNK:
			inode->i_op = &page_symlink_inode_operations;
			break;
		}
	}
	return inode;
}

/*
 * File creation. Allocate an inode, and we're done..
 */
/* SMP-safe */
static int
ramfs_mknod(struct inode *dir, struct dentry *dentry, int mode, dev_t dev)
{
	struct inode * inode = ramfs_get_inode(dir->i_sb, mode, dev);
	int error = -ENOSPC;

	if (inode) {
		if (dir->i_mode & S_ISGID) {
			inode->i_gid = dir->i_gid;
			if (S_ISDIR(mode))
				inode->i_mode |= S_ISGID;
		}
		d_instantiate(dentry, inode);
		dget(dentry);	/* Extra count - pin the dentry in core */
		error = 0;
		dir->i_mtime = dir->i_ctime = CURRENT_TIME;
	}
	return error;
}

static int ramfs_mkdir(struct inode * dir, struct dentry * dentry, int mode)
{
	int retval = ramfs_mknod(dir, dentry, mode | S_IFDIR, 0);
	printk(KERN_ALERT "I was asked to create a director with mode %x\n", mode);
	if (!retval)
		inc_nlink(dir);
	return retval;
}

static int ramfs_create(struct inode *dir, struct dentry *dentry, int mode, struct nameidata *nd)
{
  int err;
  char backing_filename[MAX_FILENAME_LEN] = "";
  struct raw_device_data *dimmap_dev = kmalloc(sizeof(struct raw_device_data), GFP_KERNEL);
  struct ramfs_mount_opts mount_opts;

  memset(dimmap_dev, 0, sizeof(struct raw_device_data));
  init_prot_vm_area_set(&(dimmap_dev->active_vmas));
  atomic_set(&dimmap_dev->num_concurrent_read_accesses, 0);
  atomic_set(&dimmap_dev->num_concurrent_write_accesses, 0);

  printk(KERN_ALERT "I was asked to create a node %s with mode %x\n", dentry->d_name.name, mode);
	err = ramfs_mknod(dir, dentry, mode | S_IFREG, 0);

	err = ramfs_parse_options(dir->i_sb->s_options, &mount_opts);
	if(err) printk(KERN_ALERT "Unable to parse options on file creation\n");

	strcat(backing_filename, mount_opts.backing_dir);
	strcat(backing_filename, "/");
	strcat(backing_filename, dentry->d_name.name);
/* 	printk(KERN_ALERT "I am going to make a shadow file in %s\n", backing_filename); */
        dimmap_dev->type = DIMMAP_FILEMAP;
        strncpy(dimmap_dev->filename, backing_filename, MAX_FILENAME_LEN);

/* 	printk(KERN_ALERT "The new inode has the following private data %p\n", dentry->d_inode->i_private); */
        dentry->d_inode->i_private = dimmap_dev;
	return err;
}


static int ramfs_unlink(struct inode *dir, struct dentry *dentry) {
  struct raw_device_data *dimmap_dev = dentry->d_inode->i_private;
  printk(KERN_ALERT "I was asked to unlink a node %s and deleting %s\n", dentry->d_name.name, dimmap_dev->filename);
/*   unlink(dimmap_dev->filename); */
//  vfs_unlink(NULL, NULL);
  free_prot_vm_area_set(&dimmap_dev->active_vmas);
  kfree(dimmap_dev);
  return simple_unlink(dir, dentry);
}

static int ramfs_symlink(struct inode * dir, struct dentry *dentry, const char * symname)
{
	struct inode *inode;
	int error = -ENOSPC;

	inode = ramfs_get_inode(dir->i_sb, S_IFLNK|S_IRWXUGO, 0);
	if (inode) {
		int l = strlen(symname)+1;
		error = page_symlink(inode, symname, l);
		if (!error) {
			if (dir->i_mode & S_ISGID)
				inode->i_gid = dir->i_gid;
			d_instantiate(dentry, inode);
			dget(dentry);
			dir->i_mtime = dir->i_ctime = CURRENT_TIME;
		} else
			iput(inode);
	}
	return error;
}

static int dimmap_fs_open(struct inode *inode, struct file *filp) {
	const int minor = iminor(inode);
	struct raw_device_data *dimmap_dev;
	struct file *tgt_filp;
	struct address_space *tgt_mapping;

	printk(KERN_ALERT "DIMMAP_FS The correct handler is called, what is minor %d and is there private data %p\n", minor, inode->i_private);


	print_address_space_ops_info(filp->f_path.dentry->d_inode->i_mapping->a_ops, "filp");

	lock_kernel();
/* 	mutex_lock(&raw_mutex); */

	BUG_ON(inode->i_private == NULL); /* The dimmap filesystem stashes the dimmap_dev data structure in the inode's private data */
	  printk(KERN_ALERT "FOUND some data\n");
          dimmap_dev = inode->i_private;
	  BUG_ON(dimmap_dev->type != DIMMAP_FILEMAP);
/*           if(dimmap_dev->type != DIMMAP_FILEMAP) { */
/*             printk(KERN_ALERT "WARNING: dimmap device has an unexpected type %d\n", dimmap_dev->type); */
/*           } */


	/* DIMMAP_FILEMAP */
	  if(dimmap_dev->binding.filp == NULL) {
	    //	    printk(KERN_ALERT "The mode of the target file is %x\n", inode->i_mode);
	    /* BVE FIXME - need to be careful about the mode here and error handling if a file cannot be created */
	    dimmap_dev->binding.filp = filp_open(dimmap_dev->filename, O_RDWR | O_CREAT | O_LARGEFILE, 0777);
	    if (IS_ERR(dimmap_dev->binding.filp)) {
	      printk(KERN_ALERT "Unable to open file %s: %p\n", dimmap_dev->filename, dimmap_dev->binding.filp);
	      dimmap_dev->binding.filp = NULL;
	      //	      goto out;
	    } else {
	      fsnotify_open(dimmap_dev->binding.filp->f_path.dentry);
	    }

	    flush_file(dimmap_dev->binding.filp->f_mapping, 0, i_size_read(dimmap_dev->binding.filp->f_mapping->host));
	  }
	  tgt_filp = dimmap_dev->binding.filp;
	  tgt_mapping = tgt_filp->f_mapping;
	
	redirect_IO(dimmap_dev, filp, tgt_mapping);

/* 	mutex_unlock(&raw_mutex); */
	unlock_kernel();
	print_file_pointer_info(filp, "DIMMAPFS RAW_OPEN-opening the file");
	printk(KERN_ALERT "DIMMAPFS Returning from the open function\n");
	return 0;
}

static int dimmap_fs_release(struct inode *inode, struct file *filp) {

/* 	struct block_device *bdev = NULL; */
	struct file *tgt_filp;
	int /* dev_mode, */ device_inuse;
	struct raw_device_data *dimmap_dev;

	printk(KERN_ALERT "When is the dimmap release function called\n");

/* 	mutex_lock(&raw_mutex); */
	BUG_ON(inode->i_private == NULL); /* The dimmap filesystem stashes the dimmap_dev data structure in the inode's private data */
	dimmap_dev = inode->i_private;
	BUG_ON(dimmap_dev->type != DIMMAP_FILEMAP);

/* 	if(dimmap_dev->type != DIMMAP_FILEMAP) { */
/* 	  printk(KERN_ALERT "WARNING: dimmap device has an unexpected type %d\n", dimmap_dev->type); */
/* 	} */
/* 	dev_mode = dimmap_dev->type; */

	tgt_filp = dimmap_dev->binding.filp;
	  
	device_inuse = release_IO(dimmap_dev, inode);

	if(device_inuse == 0) {
	  if(/* dev_mode == DIMMAP_FILEMAP &&  */dimmap_dev->binding.filp != NULL) {
	    filp_close(dimmap_dev->binding.filp, current->files);
	    dimmap_dev->binding.filp = NULL;
	  }
	}

/* 	mutex_unlock(&raw_mutex); */

	return 0;

}

/*
 * Generic function to fsync a file or block device.
 *
 * filp may be NULL if called via the msync of a vma.
 */
int dimmap_fs_fsync(struct file *filp, struct dentry *dentry, int datasync)
{
  struct raw_device_data *dimmap_dev = filp->private_data;
  printk(KERN_ALERT "dimmap fs sync was called\n");
  BUG_ON(dimmap_dev->type != DIMMAP_FILEMAP);
  return file_fsync(filp, dentry, datasync);
}

/*
 * Mmap *is* available, but confined in a different file
 */
extern int perma_mmap(struct file *filp, struct vm_area_struct *vma);

static const struct inode_operations ramfs_dir_inode_operations = {
	.create		= ramfs_create,
	.lookup		= simple_lookup,
	.link		= simple_link,
	.unlink		= /* simple */ramfs_unlink,
	.symlink	= ramfs_symlink,
	.mkdir		= ramfs_mkdir,
	.rmdir		= simple_rmdir,
	.mknod		= ramfs_mknod,
	.rename		= simple_rename,
};

static const struct super_operations ramfs_ops = {
	.statfs		= simple_statfs,
	.drop_inode	= generic_delete_inode,
	.show_options	= generic_show_options,
};

const struct address_space_operations ramfs_aops = {
	.readpage	= simple_readpage,
	.write_begin	= simple_write_begin,
	.write_end	= simple_write_end,
	//	.set_page_dirty = __set_page_dirty_no_writeback,
};

const struct file_operations dimmap_fs_file_operations = {
	.read		= do_sync_read,
	.aio_read	= generic_file_aio_read,
	.write		= do_sync_write,
	.aio_write	= generic_file_aio_write,
	.mmap		= perma_mmap/* generic_file_mmap */,
	.fsync		= dimmap_fs_fsync/* simple_sync_file */,
	.open		= dimmap_fs_open,
	.release	= dimmap_fs_release,
	.splice_read	= generic_file_splice_read,
	.splice_write	= generic_file_splice_write,
	.llseek		= generic_file_llseek,
	.owner		= THIS_MODULE,
};

const struct inode_operations ramfs_file_inode_operations = {
	.getattr	= simple_getattr,
};

static const match_table_t tokens = {
	{Opt_tgt_dir, "backing=%s"},
	{Opt_mode, "mode=%o"},
	{Opt_err, NULL}
};

struct ramfs_fs_info {
	struct ramfs_mount_opts mount_opts;
};

static int ramfs_parse_options(char *data, struct ramfs_mount_opts *opts)
{
	substring_t args[MAX_OPT_ARGS];
	int option;
	int token;
	char *p;

	opts->mode = DIMMAPFS_DEFAULT_MODE;
	/* By default put backing files in /tmp */
	strcpy(opts->backing_dir, "/tmp");

	while ((p = strsep(&data, ",")) != NULL) {
		if (!*p)
			continue;

		token = match_token(p, tokens, args);
		switch (token) {
		case Opt_tgt_dir:
		  match_strlcpy(opts->backing_dir, &args[0], 256);
		  printk(KERN_ALERT " I think that I got a target dir %s\n", opts->backing_dir);
		  break;
		case Opt_mode:
			if (match_octal(&args[0], &option))
				return -EINVAL;
			opts->mode = option & S_IALLUGO;
			break;
		/*
		 * We might like to report bad mount options here;
		 * but traditionally ramfs has ignored all mount options,
		 * and as it is used as a !CONFIG_SHMEM simple substitute
		 * for tmpfs, better continue to ignore other mount options.
		 */
		}
	}

	return 0;
}

static int ramfs_fill_super(struct super_block * sb, void * data, int silent)
{
	struct ramfs_fs_info *fsi;
	struct inode *inode = NULL;
	struct dentry *root;
	int err;

	save_mount_options(sb, data);

	fsi = kzalloc(sizeof(struct ramfs_fs_info), GFP_KERNEL);
	sb->s_fs_info = fsi;
	if (!fsi) {
		err = -ENOMEM;
		goto fail;
	}

	err = ramfs_parse_options(data, &fsi->mount_opts);
	if (err)
		goto fail;

	sb->s_maxbytes		= MAX_LFS_FILESIZE;
	sb->s_blocksize		= PAGE_CACHE_SIZE;
	sb->s_blocksize_bits	= PAGE_CACHE_SHIFT;
	sb->s_magic		= RAMFS_MAGIC;
	sb->s_op		= &ramfs_ops;
	sb->s_time_gran		= 1;

	inode = ramfs_get_inode(sb, S_IFDIR | fsi->mount_opts.mode, 0);
	if (!inode) {
		err = -ENOMEM;
		goto fail;
	}

	root = d_alloc_root(inode);
	sb->s_root = root;
	if (!root) {
		err = -ENOMEM;
		goto fail;
	}

	return 0;
fail:
	kfree(fsi);
	sb->s_fs_info = NULL;
	iput(inode);
	return err;
}

int ramfs_get_sb(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data, struct vfsmount *mnt)
{
	return get_sb_nodev(fs_type, flags, data, ramfs_fill_super, mnt);
}

/* static int rootfs_get_sb(struct file_system_type *fs_type, */
/* 	int flags, const char *dev_name, void *data, struct vfsmount *mnt) */
/* { */
/* 	return get_sb_nodev(fs_type, flags|MS_NOUSER, data, ramfs_fill_super, */
/* 			    mnt); */
/* } */

static void ramfs_kill_sb(struct super_block *sb)
{
	kfree(sb->s_fs_info);
	kill_litter_super(sb);
}

static struct file_system_type ramfs_fs_type = {
	.name		= "di-mmap-fs",
	.get_sb		= ramfs_get_sb,
	.kill_sb	= ramfs_kill_sb,
	.owner		= THIS_MODULE,
};

int register_di_mmap_fs(void) 
{
	return register_filesystem(&ramfs_fs_type);
}

int unregister_di_mmap_fs(void) 
{
	return unregister_filesystem(&ramfs_fs_type);
}

//MODULE_LICENSE("LGPL");
