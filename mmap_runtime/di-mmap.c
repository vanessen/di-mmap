/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/* -*- C -*-
 * DI-MMAP: Data-Intensive Memory-Map Runtime and Interface
 *
 * Character device interface to block devices - used to provide mmap capability
 *
 * Derived from:
 *
 * The scullp char module from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.
 *
 * and
 *
 * linux/drivers/char/raw.c
 *
 * Front-end raw character devices.  These can be bound to any block
 * devices to provide genuine Unix raw character device semantics.
 *
 * We reserve minor number 0 for a control interface.  ioctl()s on this
 * device are used to bind the other minor numbers to block devices.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/aio.h>
#include <asm/uaccess.h>

#include <linux/major.h>
#include <linux/blkdev.h>
#include <linux/mutex.h>
#include <linux/smp_lock.h>

#include <linux/init.h>
/* #include <linux/gfp.h> */
#include <linux/radix-tree.h>
#include <linux/buffer_head.h> /* invalidate_bh_lrus() */
#include <linux/device.h>
#include <linux/cpufreq.h>
#include <linux/xattr.h>
#include <linux/file.h>
#include <linux/fsnotify.h>

/* #include <linux/wait.h> */
/* #include <linux/kthread.h> */

#include "di-mmap.h"	/* local definitions */
#include "di-mmap-fs.h"
#include "mmap_buffer_interface.h"

version_number_t version = {1, 0, 7};
char comp_flags[256] = "";

/* int perma_crd_major =   0; /\* Request an available major device ID *\/ */
int perma_devs =    1;	/* number of bare perma devices */
/* int allocate_devices = 1; */
/* /\* int scullp_qset =    SCULLP_QSET; *\/ */
/* int scullp_order =   ALLOC_PAGES_ORDER; //SCULLP_ORDER; */
/* long perma_crd_max_size = 0; */

/* Set the readability to S_IRUGO so that the parameters are list in /sys/module/perma_mmap_ramdisk/parameters/ */
//module_param(perma_crd_major, int, 0);
/* module_param(perma_devs, int, S_IRUGO); */
/* module_param(perma_crd_max_size, long, S_IRUGO); */
module_param(perma_mmap_buf_size, long, S_IRUGO);
/* module_param(scullp_qset, int, 0); */
/* module_param(scullp_order, int, 0); */
MODULE_AUTHOR("Brian Van Essen");
MODULE_LICENSE("GPL");
//MODULE_LICENSE("Dual BSD/GPL");

static struct class *raw_class;
static struct raw_device_data raw_devices[MAX_DI_MMAP_MINORS];
static DEFINE_MUTEX(raw_mutex);
static const struct file_operations raw_ctl_fops; /* forward declaration */
/* static const struct inode_operations dimmap_inode_ops; */

/* Internal declarations */
//static inline void print_file_pointer_info(struct file *filp, char *str);

int di_mmap_major = 0; /* Request an available major device ID */

/* Track the number of concurrent accesses to each block ramdisk device */
atomic_t global_num_concurrent_read_accesses;
atomic_t global_num_concurrent_write_accesses;
int simulate_unified_channel;

static int NUMA_node_mask = 0; /* A mask to interleave memory across the available NUMA nodes */

int DEBUG_READING = 0;
int DEBUG_WRITING = 0;

void cleanup_devices(void) {
}

void setup_devices(void) {
}

//void reset_driver_parameters(void) {
void reset_device_parameters(void) {
  reset_mmap_params();
  reset_mmap_state();

  DEBUG_LOGGING = 0;
}

dev_t inline get_ith_device_id(int i) {
  struct raw_device_data *dev;

  dev = &raw_devices[i /* % perma_devs */];

  BUG_ON(!dev);
  BUG_ON(dev && MINOR(dev->dev_id) != i /* % perma_devs */);
/*   printk(KERN_INFO "crd_number =%d i=%d out of %d devices\n", crd->crd_number, i, perma_devs); */
  return get_device_id(dev);
}

profile_info inline *select_ith_device(int i) {
  profile_info *perf_cntr;

  perf_cntr = &raw_devices[i/*  % perma_devs */].perf_cntr;


/*   BUG_ON(!crd); */
/*   BUG_ON(crd && MINOR(crd->dev_id) != i % perma_devs); */
/*   printk(KERN_INFO "crd_number =%d i=%d out of %d devices\n", crd->crd_number, i, perma_devs); */
  return perf_cntr;
}

void print_io_stats_header(char *msg, char line_leader) {
  char msg1[MAX_STR_LEN];
  sprintf(msg, "%c\tCh ", line_leader);
  sprintf(msg1, "%16s %16s %16s %16s", 
	  "Xfer Size (B)",
	  "Lookup (CC)",
	  "Copy (CC)",
	  "Num. Requests");
  strcat(msg, msg1);
  sprintf(msg1, " | ");
  strcat(msg, msg1);
  sprintf(msg1, "%16s %16s %16s %16s",
	  "Xfer Size (B)",
	  "Lookup (CC)",
	  "Copy (CC)",
	  "Num. Requests");
  strcat(msg, msg1);
  sprintf(msg1, "\n");
  strcat(msg, msg1);
  return;
}

void prettyprint_device_specifics(struct seq_file *s, char *msg, char line_leader) {
  char msg1[MAX_STR_LEN];
  int i;

  sprintf(msg, "mmap_Page_Cache_Size=%ldb DirtyPages: num_written=%ldpg Unified Channel=%d Skipped zap page range=%ld PerMA stall=%ld\n", 
	  perma_mmap_buf_size*PAGE_SIZE, atomic64_read(&total_num_pages_written),
	  simulate_unified_channel, atomic64_read(&skipped_zap_page_range), atomic64_read(&perma_buffer_stalled));

  prettyprint_specifics(buf_data, s, msg, line_leader);

  sprintf(msg1, "%c     /dev/perma-mmap-ramdisk:", 
	  line_leader);
  strcat(msg, msg1);

  for(i = 0; i < MAX_DI_MMAP_MINORS; i++) {
    if(raw_devices[i].inuse) {
      char mmaped_address_ranges[MAX_STR_LEN] = "";
      vm_address_regions(&(raw_devices[i].active_vmas.vmas), mmaped_address_ranges);
      sprintf(msg1, " %d (%s)=%ldb", i,  mmaped_address_ranges, (long) 0 /* raw_devices[i].size */);
      strcat(msg, msg1);
    }
  }

  return;
}

int tune_device_specifics(char *cmd, int param1, int param2, int param3, int param4) {
  int err = 0, i;
  if(strcasecmp(cmd, MMAP_BUFFER) == 0) {
    perma_mmap_buf_size = param1;
  }else if(strcasecmp(cmd, RESET_MMAP) == 0) {
    reset_mmap_state();
    printk("Resetting mmap state\n");
  }else if(strcasecmp(cmd, SCAN_PTE) == 0) {
  for(i = 0; i < MAX_DI_MMAP_MINORS; i++) {
    if(raw_devices[i].inuse) {
      scan_page_tables(&raw_devices[i].active_vmas);
    }
  }
  }else {
    err = /* buf_ops-> */tune_specifics(buf_data, cmd, param1, param2, param3, param4); //1;
  }

  init_mmap_params();    
  return err;
}


void explain_device_tuning_params() {
  printk("\t%s\n", MMAP_BUFFER);
  printk("\t%s\n", RESET_MMAP);
  printk("\t%s\n", SCAN_PTE);

  /* buf_ops-> */explain_tuning_params(buf_data);
  return;
}

void initDeviceSpecificProfileCounters(void) {
  atomic64_set(&total_num_pages_written, 0);

  /* buf_ops-> */init_specific_counters(buf_data);

  return;
}

static inline void track_statistics(channel_profile_info *channel_stats, unsigned long lookup_tsc_delta, unsigned short lookup_cpuID_diff, unsigned long copy_tsc_delta, unsigned short copy_cpuID_diff) {
  tsc_stats tsc_pair0, tsc_pair1;
  long num_requests;
  long long avg_lookup_time = 0, avg_copy_time = 0;

  tsc_pair0.clock_cycles = 0;
  tsc_pair0.faults = 0;
  tsc_pair1.clock_cycles = 0;
  tsc_pair1.faults = 0;

  num_requests = atomic64_read(&channel_stats->num_requests);
  if(num_requests > 0) {
    avg_lookup_time = channel_stats->aggregate.tsc_pair0.clock_cycles / num_requests;
    avg_copy_time = channel_stats->aggregate.tsc_pair1.clock_cycles / num_requests;	    
  }else {
    avg_lookup_time = 0;
    avg_copy_time = 0;
  }

  /* Add TSC here */
  validate_tsc(&tsc_pair0, lookup_tsc_delta, lookup_cpuID_diff, "Lookup time", avg_lookup_time);
  validate_tsc(&tsc_pair1, copy_tsc_delta, copy_cpuID_diff, "Copy time", avg_copy_time);

  update_statistics(channel_stats, PAGE_SIZE, tsc_pair0, tsc_pair1);

  return;
}

/************************************************************************
 *
 * Memory-map runtime handler functions
 *
 ***********************************************************************/
/* Hash function taken from
 * http://www.concentric.net/~ttwang/tech/inthash.htm
 * Which derived it from Robert Jenkins' work
 */

static unsigned int hash6432shift(unsigned long key)
{
  key = (~key) + (key << 18); // key = (key << 18) - key - 1;
  key = key ^ (key >> 31); /* logical right shift */
  key = key * 21; // key = (key + (key << 2)) + (key << 4);
  key = key ^ (key >> 11); /* logical right shift */
  key = key + (key << 6);
  key = key ^ (key >> 22); /* logical right shift */
  return (int) key;
}

/*
 * Device interface: allow the mmap runtime to introspect on the device
 */
dev_t get_device_id(void *device) {
  //  struct block_device *bdev = (struct block_device *) device;
  struct raw_device_data *dimmap_dev = (struct raw_device_data *) device;
  struct block_device *bdev;
  dev_t dev_id;

  if(device == NULL) {
    WARN_ON(device == NULL);
    return MKDEV(0,0);
  }

  if(dimmap_dev->type == DIMMAP_BLOCKDEV) {
    bdev = dimmap_dev->binding.dev;
    dev_id = bdev->bd_dev;
  }else {
    struct inode *d_inode = dimmap_dev->binding.filp->f_path.dentry->d_inode;

    if(d_inode == NULL) { /* BUG_ON(d_inode == NULL); */
      return MKDEV(0,0);
    }

    /* Use a combination of the inode number and the superblocks device id.
     * Remember that the device ID is used to spread the data
     * in the buffer */
    dev_id = hash6432shift((((unsigned long long )d_inode->i_sb->s_dev) << 32 ) | d_inode->i_ino);
/*     printk(KERN_ALERT "I was asked to get the device id and I like sb->s_dev=%d and inode i_ino=%d and combination is %x\n",  */
/* 	   MAJOR(d_inode->i_sb->s_dev), */
/* 	   d_inode->i_ino, dev_id); */


/*     char str[256] = ""; */
/*     if(dimmap_dev->binding.filp == NULL) { return 0; } */
/*     if(dimmap_dev->binding.filp->f_mapping == NULL) { return 0; } */
/*     print_file_pointer_info(dimmap_dev->binding.filp, str); */
/*     /\* S_ISBLK(dimmap_dev->binding.filp->f_mapping->host->i_mode) *\/ */
/*     printk(KERN_ALERT "%s with d_inode=%d and host_dev=%d\n", str,  */
/* 	   dimmap_dev->binding.filp->f_path.dentry->d_inode->i_bdev->bd_dev, */
/* 	   dimmap_dev->binding.filp->f_mapping->host->i_bdev->bd_dev); */
    /* It is unclear why the host pointer to a block deviec is null */
    //    bdev = dimmap_dev->binding.filp->f_path.dentry->d_inode->i_bdev;
    //    bdev = dimmap_dev->binding.filp->f_mapping->host->i_bdev;
  }
  return dev_id;
}

prot_vm_area_set_t *get_device_active_vmas(void *device) {
  prot_vm_area_set_t *active_vmas = NULL;
  struct raw_device_data *device_metadata = (struct raw_device_data *) device;
  
  active_vmas = &device_metadata->active_vmas;
  
  return active_vmas;
}

/************************************************************************
 *
 * Code snippets that reproduce code from other parts of the kernel
 *
 ***********************************************************************/

static int add_page_to_radix_tree(struct address_space *mapping, struct page *page) {
  int err = 1;
  struct page *tmp_page;

  if((tmp_page = radix_tree_lookup(&mapping->page_tree, page->index))) {
    if(tmp_page != page) {
      printk(KERN_ALERT "I found that a page %p already exists at slot %lx vs %p\n",
	     tmp_page, page->index, page);
      return err;
    }else{
      return 0;
    }
  }

  if (radix_tree_preload(GFP_NOIO)) {
    return err;
  }

  spin_lock(&mapping->tree_lock);
  
  err = radix_tree_insert(&mapping->page_tree, page->index, page);
  if(err) {
    if(err == -EEXIST) {
      printk(KERN_ALERT "Unable to add the page to the radix tree it already exists: err %d\n", err);
    }else {
      printk(KERN_ALERT "Unable to add the page to the radix tree: err %d\n", err);
    }
  }else {
    mapping->nrpages++;
  }
/*     page->index = idx; */
  spin_unlock(&mapping->tree_lock);
  
  radix_tree_preload_end();
  return err;
}

static int del_page_from_radix_tree(struct address_space *mapping, struct page *page) {
  int err = 0;
  void *ptr;
  struct page *tmp_page;

  if((tmp_page = radix_tree_lookup(&mapping->page_tree, page->index))) {
    if(tmp_page != page) {
      printk(KERN_ALERT "I am not going to delete the page that I found that a page %p already exists at slot %lx vs %p\n",
	     tmp_page, page->index, page);
      return err;
    }
  }

  spin_lock(&mapping->tree_lock);
  
  ptr = radix_tree_delete(&mapping->page_tree, page->index);
  if(!ptr) {
    printk(KERN_ALERT "Unable to delete the page to the radix tree\n");
  }
  spin_unlock(&mapping->tree_lock);
  
  return err;
}

/*
 * Local implementation of the fadvise function's POSIX_FADV_DOTNEED case statement.
 * Reference implementation is in fadvice.c
 */
int flush_file(struct address_space *mapping,  loff_t offset, loff_t len) {
  /* Careful about overflows. Len == 0 means "as much as possible" */
  struct backing_dev_info *bdi;
  int ret = 0;
  loff_t endbyte = offset + len;
  pgoff_t start_index;
  pgoff_t end_index;

  if (!len || endbyte < len)
    endbyte = -1;
  else
    endbyte--;              /* inclusive */
  
  bdi = mapping->backing_dev_info;

  if (!bdi_write_congested(mapping->backing_dev_info)) {
    ret = filemap_flush(mapping);
    printk(KERN_ALERT "Flushed the file with ret=%d\n", ret);
  }

  /* First and last FULL page! */
  start_index = (offset+(PAGE_CACHE_SIZE-1)) >> PAGE_CACHE_SHIFT;
  end_index = (endbyte >> PAGE_CACHE_SHIFT);
   
  if (end_index >= start_index) {
    ret = invalidate_mapping_pages(mapping, start_index,
				   end_index);
    printk(KERN_ALERT "Invalidated the mapping pages with ret=%d\n", ret);
  }

  return ret;
}


static inline void print_page_info(struct page *page, char *str) {
  printk(KERN_ALERT
	 "%s : page:%p flags:%p count:%d mapcount:%d mapping:%p index:%lx private=%p\n",
	 str,
	 page, (void *)page->flags, page_count(page),
	 page_mapcount(page), page->mapping, page->index, (void *)page->private);
}

/* static */ inline void print_file_pointer_info(struct file *filp, char *str) {
  if(filp == NULL) {
    printk(KERN_ALERT "%s: NULL pointer\n", str);
  }else {
    printk(KERN_ALERT
	   "%s: filp:%p dentry:%p i_mapping:%p f_ops:%p mode:%x private_data:%p mapping:%p\n",
	   str,
	   filp, filp->f_path.dentry, 
	   filp->f_path.dentry->d_inode->i_mapping,
	   filp->f_op, filp->f_mode, 
	   filp->private_data,
	   filp->f_mapping);
  }
  //filp->f_ra, 
}

static inline void print_inode_ops_info(const struct inode_operations *i_ops, char *str) {
  printk(KERN_ALERT "%s %p->create=%p lookup=%p link=%p unlink=%p symlink=%p mkdir=%p rmdir=%p mknod=%p rename=%p\n",
	 str, i_ops,
	 i_ops->create,
	 i_ops->lookup,
	 i_ops->link,  
	 i_ops->unlink,
	 i_ops->symlink,
	 i_ops->mkdir, 
	 i_ops->rmdir,
	 i_ops->mknod,
	 i_ops->rename);

  printk(KERN_ALERT "readlink=%p follow_link=%p put_link=%p truncate=%p permission=%p check_acl=%p\n",
	 i_ops->readlink,
	 i_ops->follow_link,
	 i_ops->put_link,
	 i_ops->truncate,
	 i_ops->permission,
	 i_ops->check_acl);

  printk(KERN_ALERT "setattr=%p getattr=%p setxattr=%p getxattr=%p listxattr=%p removexattr=%p truncate_range=%p fallocate=%p fiemap=%p\n",
	 i_ops->setattr,
	 i_ops->getattr,
	 i_ops->setxattr,
	 i_ops->getxattr,
	 i_ops->listxattr,
	 i_ops->removexattr,
	 i_ops->truncate_range,
	 i_ops->fallocate,
	 i_ops->fiemap);
}

static inline void copy_inode_ops(struct inode_operations *tgt_i_ops, const struct inode_operations *src_i_ops) {
  tgt_i_ops->create  	    = src_i_ops->create;
  tgt_i_ops->lookup  	    = src_i_ops->lookup;
  tgt_i_ops->link    	    = src_i_ops->link;
  tgt_i_ops->unlink  	    = src_i_ops->unlink;
  tgt_i_ops->symlink 	    = src_i_ops->symlink;
  tgt_i_ops->mkdir   	    = src_i_ops->mkdir; 
  tgt_i_ops->rmdir   	    = src_i_ops->rmdir;
  tgt_i_ops->mknod   	    = src_i_ops->mknod;
  tgt_i_ops->rename  	    = src_i_ops->rename;
  tgt_i_ops->readlink       = src_i_ops->readlink;
  tgt_i_ops->follow_link    = src_i_ops->follow_link;
  tgt_i_ops->put_link 	    = src_i_ops->put_link;
  tgt_i_ops->truncate 	    = src_i_ops->truncate;
  tgt_i_ops->permission     = src_i_ops->permission;
  tgt_i_ops->check_acl      = src_i_ops->check_acl;
  tgt_i_ops->setattr        = src_i_ops->setattr;
  tgt_i_ops->getattr 	    = src_i_ops->getattr;
  tgt_i_ops->setxattr	    = src_i_ops->setxattr;
  tgt_i_ops->getxattr	    = src_i_ops->getxattr;
  tgt_i_ops->listxattr      = src_i_ops->listxattr;
  tgt_i_ops->removexattr    = src_i_ops->removexattr;
  tgt_i_ops->truncate_range = src_i_ops->truncate_range;
  tgt_i_ops->fallocate      = src_i_ops->fallocate;
  tgt_i_ops->fiemap         = src_i_ops->fiemap;
  return;
}

inline void print_address_space_ops_info(const struct address_space_operations *a_ops, char *str) {

  printk(KERN_ALERT "%s: writepage=%p readpage=%p sync_page=%p writepages=%p\n",
	 str, a_ops->writepage, a_ops->readpage, a_ops->sync_page, a_ops->writepages);


  printk(KERN_ALERT "set_page_dirty=%p readpages=%p write_begin=%p write_end=%p\n", 
	 a_ops->set_page_dirty, a_ops->readpages, a_ops->write_begin, a_ops->write_end);

    printk(KERN_ALERT "invalidatepage=%p releasepage=%p migratepage=%p launder_page=%p is_partially_uptodate=%p\n",
	   a_ops->invalidatepage, a_ops->releasepage, a_ops->migratepage, a_ops->launder_page, a_ops->is_partially_uptodate);
}

/* static inline void copy_inode_ops_info(const struct inode_operations *i_ops, struct inode_operations *new_i_ops) { */
/*   new_i_ops->create = i_ops->create; */
/*   new_i_ops->lookup = i_ops->lookup; */
/*   new_i_ops->link = i_ops->link; */
/*   new_i_ops->unlink = i_ops->unlink; */
/*   new_i_ops->symlink = i_ops->symlink; */
/*   new_i_ops->mkdir = i_ops->mkdir; */
/*   new_i_ops->rmdir = i_ops->rmdir; */
/*   new_i_ops->mknod = i_ops->mknod; */
/*   new_i_ops->rename = i_ops->rename; */
/*   new_i_ops->readlink = i_ops->readlink; */
/*   new_i_ops->follow_link = i_ops->follow_link; */
/*   new_i_ops->put_link = i_ops->put_link; */
/*   new_i_ops->truncate = i_ops->truncate; */
/*   new_i_ops->permission = i_ops->permission; */
/*   new_i_ops->check_acl = i_ops->check_acl; */
/*   new_i_ops->setattr = i_ops->setattr; */
/*   new_i_ops->getattr = i_ops->getattr; */
/*   new_i_ops->setxattr = i_ops->setxattr; */
/*   new_i_ops->getxattr = i_ops->getxattr; */
/*   new_i_ops->listxattr = i_ops->listxattr; */
/*   new_i_ops->removexattr = i_ops->removexattr; */
/*   new_i_ops->truncate_range = i_ops->truncate_range; */
/*   new_i_ops->fallocate = i_ops->fallocate; */
/*   new_i_ops->fiemap = i_ops->fiemap; */
/*   return; */
/* } */

/* Redirect a fstat call to the underlying inode in the address space */
int dimmap_vfs_getattr(struct vfsmount *mnt, struct dentry *dentry, struct kstat *stat)
{
        struct inode *inode = dentry->d_inode;
	struct raw_device_data *dimmap_dev = dentry->d_inode->i_private;
	io_stats *read_stats, *write_stats;
	char msg[256];

	read_stats = &(dimmap_dev->perf_cntr.read.aggregate);
	write_stats = &(dimmap_dev->perf_cntr.write.aggregate);

	print_io_stats_header(msg, ' ');
	printk(KERN_ALERT "%s", msg);
	//    seq_printf(s, "%c\t%2d ", line_leader, io_channel);

	printk(KERN_ALERT "%16llu %16llu %16llu %16ld %16llu %16llu %16llu %16ld",
	       read_stats->transfer_size,
	       read_stats->tsc_pair0.clock_cycles,
	       read_stats->tsc_pair1.clock_cycles,
	       atomic64_read(&dimmap_dev->perf_cntr.read.num_requests),
	       write_stats->transfer_size,
	       write_stats->tsc_pair0.clock_cycles,
	       write_stats->tsc_pair1.clock_cycles,
	       atomic64_read(&dimmap_dev->perf_cntr.write.num_requests));


	/* inode for this is coming from filp->f_path.dentry->d_inode
	 * the i_mapping field points to the address space, which has a host inode
	 * we want this host inode data
	 */
        generic_fillattr(inode->i_mapping->host, stat);
        return 0;
}

/*
 * Data management: read and write
 */

/*
 * The readpage function is often defined to be the mpage_readpage function in fs/mpage.c
 */


static inline int read_page_mmap(struct file *filp, void *device, unsigned long device_linear_address, struct page *page) {
  int error = 0;
  char *buf;

  struct address_space *mapping = filp->f_mapping;
  struct inode *inode= mapping->host;

  int j;

  /* Build the proper page structure */

  BUG_ON(page->mapping != NULL && page->mapping != mapping); /* If this page is pointing to another mapping, error. */

//  printk("Previously this page was pointing to the following mapping %p ---", page->mapping);
  page->mapping = mapping; //bdev->bd_inode;
  //  add_page_to_radix_tree(mapping, page); /* Add the page to the radix tree so that when we leave the mapping in place other functions don't fail. */

//  printk("no it is i pointing to mapping %p \n", page->mapping);
/*   printk("Previously this page was pointing to the following mapping %p and inode %p ---", page->mapping, page->mapping->host); */
/*   page->mapping = mapping; //bdev->bd_inode; */
/*   printk("no it is i pointing to mapping %p and inod %p\n", page->mapping, page->mapping->host); */


/*   do { */
/*     tmp_page = page_cache_alloc_cold(mapping); */
/*     if (!tmp_page) */
/*       return -ENOMEM; */
    
/*     ret = add_to_page_cache_lru(page, mapping, offset, GFP_KERNEL); */
/*     printk("What was my return code %d\n", ret); */
/*     if (ret == 0) { */
  if(DEBUG_READING) {
    printk(KERN_ALERT "Reading for page %lx  device_linear_address %lx count %d  on a device of size %llx (%llx) with inode %p\n", page->index, device_linear_address, page_count(page), i_size_read(inode/* bdev->bd_inode */),
	   i_size_read(filp->f_path.dentry->d_inode), filp->f_path.dentry->d_inode);

    printk(KERN_ALERT "The inode ops for the mapping are %p the filep %p and generic %p\n", inode->i_op->getattr, filp->f_path.dentry->d_inode->i_op->getattr, generic_fillattr);
    print_inode_ops_info(inode->i_op, "inode");
    print_inode_ops_info(filp->f_path.dentry->d_inode->i_op, "d_inode");

    printk(KERN_ALERT
	   "About to fill a page : page:%p flags:%p count:%d mapcount:%d mapping:%p index:%lx\n",
	   page, (void *)page->flags, page_count(page),
	   page_mapcount(page), page->mapping, page->index);

  if(PagePrivate(page)) {
    printk(KERN_ALERT "page %p is private\n", page);
    //    ClearPagePrivate(page);
  }

  if(PageReferenced(page)) {
    printk(KERN_ALERT "page %p is referenced\n", page);
  }
  if(PageUptodate(page)) {
    printk(KERN_ALERT "page %p is uptodate\n", page);
  }
  if(PageDirty(page)) {
    printk(KERN_ALERT "page %p is dirty\n", page);
  }
  }
  get_page(page);
  lock_page(page);
      error = mapping->a_ops->readpage(filp, page);
      BUG_ON(error);
        if (!error) {
                wait_on_page_locked(page);
/* 		printk("We have finished waiting on the locked page %p and what is the count now %d\n", page, page_count(page)); */
                if (!PageUptodate(page)) {
                        error = -EIO;
			printk(KERN_ALERT "I waited for the read to complete but why is page %p not up to date\n", page);
	
		}
        }

	if(DEBUG_READING) {
	buf = page_address(page);
	  printk("I just read a page at offset %lx \n", page->index);
	  for(j=0; j < PAGE_SIZE; j++) {
	    printk("%x", buf[j]);
	  }
	  printk("\n");
	}
  lock_page(page);

  //  set_page_dirty(page);

  //  del_page_from_radix_tree(mapping, page);
  page->mapping = NULL; /* We cannot let the page keep the mapping without being in the radix tree */

  if(DEBUG_READING) {
  printk(KERN_ALERT
	 "read a page : page:%p flags:%p count:%d mapcount:%d mapping:%p index:%lx\n",
	 page, (void *)page->flags, page_count(page),
	 page_mapcount(page), page->mapping, page->index);

  if(PagePrivate(page)) {
    printk(KERN_ALERT "page %p is private\n", page);
    //    ClearPagePrivate(page);
  }

  if(PageReferenced(page)) {
    printk(KERN_ALERT "page %p is referenced\n", page);
  }
  if(PageUptodate(page)) {
    printk(KERN_ALERT "page %p is uptodate\n", page);
  }
  if(PageDirty(page)) {
    printk(KERN_ALERT "page %p is dirty\n", page);
  }

	//	put_page(page);

      //      put_page(page);
/*       dst = page_address(page); */
      printk("Was my read successfull %d and what is the count now %d\n", error, page_count(page));
  }
      if (!PageUptodate(page)) {
	printk(KERN_ALERT "Why is page %p not up to date\n", page);
      }

/*       for(i = 0; i < 4096; i++) { */
/* 	printk("%c", ((char *)dst)[i]); */
/*       } */
/*       printk("\n"); */
//      unlock_page(page);
/*     }else if (ret == -EEXIST) */
/*       ret = 0; /\* losing race to add is OK *\/ */
    
/*     page_cache_release(tmp_page); */
    
/*   } while (ret == AOP_TRUNCATED_PAGE); */

      return error;
}

/* struct thread_data { */
/*   struct file *filp; */
/*   void *device; */
/*   unsigned long device_linear_address; */
/*   struct page *page; */
/*   wait_queue_head_t lo_event; */
/* }; */

/* static int read_page_fileIO_thread(void *data){  */

/*   struct thread_data *tdata = (struct thread_data *)data; */

/*   printk(KERN_ALERT "I have a thread request\n"); */
/*   read_page_fileIO(tdata->filp, tdata->device, tdata->device_linear_address, tdata->page); */
/*   wake_up(&tdata->lo_event); */
/*   do_exit(0); */
/* } */

void check_page_buffers(struct page *page, struct address_space *mapping, struct inode *inode) {
  sector_t block;
  sector_t last_block;
  struct buffer_head *bh, *head;

  if (!page_has_buffers(page)) {
    printk(KERN_ALERT "Hey I thought that page %p should have buffers\n", page);
    return;
  }

  last_block = (i_size_read(inode) - 1) >> inode->i_blkbits;

  block = (sector_t)page->index << (PAGE_CACHE_SHIFT - inode->i_blkbits);
  head = page_buffers(page);
  bh = head;

  /*
   * Get all the dirty buffers mapped to disk addresses and
   * handle any aliases from the underlying blockdev's mapping.
   */
  do {
    if (block > last_block) {
      printk(KERN_ALERT "The block exceeds the last block\n");
    } else if ((!buffer_mapped(bh) || buffer_delay(bh)) &&
	       buffer_dirty(bh)) {

      printk(KERN_ALERT "I am here\n");
    }
    bh = bh->b_this_page;
    block++;
  } while (bh != head);

  do {
    int j;
    if (buffer_mapped(bh)) {printk(KERN_ALERT "The buffer is mapped\n"); }
    if (buffer_delay(bh)) {printk(KERN_ALERT "The buffer is delay\n"); }
    if (buffer_dirty(bh)) {printk(KERN_ALERT "The buffer is dirty\n"); }
    printk(KERN_ALERT "state=%lx page=%p(address=%p) nr=%lx size=%lx data=%p\n", bh->b_state, bh->b_page, page_address(bh->b_page), bh->b_blocknr, bh->b_size, bh->b_data);
    for(j=0; j < 512; j++) {
      printk("%x", bh->b_data[j]);
    }
    printk("\n");
  
  } while ((bh = bh->b_this_page) != head);

}

void mark_page_buffers(struct page *page, struct address_space *mapping, struct inode *inode) {
  sector_t block;
  sector_t last_block;
  struct buffer_head *bh, *head;

  if (!page_has_buffers(page)) { return; }

  last_block = (i_size_read(inode) - 1) >> inode->i_blkbits;

  block = (sector_t)page->index << (PAGE_CACHE_SHIFT - inode->i_blkbits);
  head = page_buffers(page);
  bh = head;


  do {
    test_set_buffer_dirty(bh);
  
  } while ((bh = bh->b_this_page) != head);
}

/************************************************************************
 *
 * Page management functions: read / write / flush
 *
 ***********************************************************************/

/*
 * The readpage function is often defined to be the mpage_readpage function in fs/mpage.c
 * Note that if the read function sets up the radix tree then for some reason the page is never marked as diry.
 */
int read_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep) {
  struct page *page = *pagep;
  int error = 0;
  volatile unsigned long long ini_lookup, end_lookup, ini_copy, end_copy;
  volatile int init_lookup_cpuID, end_lookup_cpuID, init_copy_cpuID, end_copy_cpuID;
  struct raw_device_data *device_metadata;

  init_lookup_cpuID = GET_CPU_ID();
  /* Read the timestamp counter (TSC) */
  rdtscll(ini_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */

  read_page_mmap(filp, device, device_linear_address, page);
  if(DEBUG_READING) {
    //    check_page_buffers(page, filp->f_mapping, inode);
  }

  rdtscll(end_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_lookup_cpuID = GET_CPU_ID();
  init_copy_cpuID = end_lookup_cpuID;
  ini_copy = end_lookup;

  rdtscll(end_copy); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_copy_cpuID = GET_CPU_ID();

  device_metadata = (struct raw_device_data *)device;

  track_statistics(&device_metadata->perf_cntr.read, end_lookup-ini_lookup, init_lookup_cpuID != end_lookup_cpuID, end_copy-ini_copy, init_copy_cpuID != end_copy_cpuID);

  return error;
}



int write_page(/* struct vm_area_struct *vma, */ struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep) {
  struct raw_device_data *dev = (struct raw_device_data *) device;
  struct page *page = *pagep;
  int error = 0;
  volatile unsigned long long ini_lookup, end_lookup, ini_copy, end_copy;
  volatile int init_lookup_cpuID, end_lookup_cpuID, init_copy_cpuID, end_copy_cpuID;
  struct raw_device_data *device_metadata;
  char *buf;

  struct address_space *mapping;

  int j;

  struct writeback_control wbc = {
    .sync_mode = WB_SYNC_NONE, //ALL, // NONE
    .nr_to_write = 1,
/*     .range_start = page->index, */
/*     .range_end = page->index + PAGE_SIZE, */
  };

  BUG_ON(dev->type == DIMMAP_NULL);
  if(dev->type == DIMMAP_BLOCKDEV) {
    struct block_device *bdev = dev->binding.dev;
    mapping = bdev->bd_inode->i_mapping;
  }else { /* DIMMAP_FILEMMAP */
    struct file *m_filp = dev->binding.filp;
    mapping = m_filp->f_mapping;
  }

/*   printk("I am about to do a write to device %d.%d\n", block_major, block_minor); */

  if(page == NULL) {
    printk("No valid target page to copy to\n");
  }
#if 1
  /* If this page is pointing to another mapping or is null, error. */
  //  BUG_ON((page->mapping != NULL && page->mapping != mapping));
/*   BUG_ON(page->mapping == NULL || (page->mapping != NULL && page->mapping != mapping)); */

  page->mapping = mapping; //bdev->bd_inode;
  add_page_to_radix_tree(mapping, page);
#endif

  init_lookup_cpuID = GET_CPU_ID();
  /* Read the timestamp counter (TSC) */
  rdtscll(ini_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */

  //set_page_dirty(page);

  if(DEBUG_WRITING) {
    printk(KERN_ALERT "Writing page %lx count %d using function %p block_write_full_page=%p is the page private %d does the page have buffer=%d also the inode is %p and the inode size is %llx\n", 
	   page->index, page_count(page), mapping->a_ops->writepage, block_write_full_page, PagePrivate(page), page_has_buffers(page), page->mapping->host, i_size_read(page->mapping->host));
    printk(KERN_ALERT "The pointer to writepage is %p and generic_writepage %p and write_cache_page is %p\n", mapping->a_ops->writepage, generic_writepages, write_cache_pages);
    print_page_info(page, "DEBUG_WRITING");
  }

  lock_page(page);
  buf = page_address(page);
  if(DEBUG_WRITING) {
  printk("Before I write the page to location %lx \n", page->index);
  for(j=0; j < PAGE_SIZE; j++) {
    printk("%x", buf[j]);
  }
  printk("\n");
  }

  mark_page_buffers(page, mapping, mapping->host);
  if(DEBUG_WRITING) {
  check_page_buffers(page, mapping, mapping->host);
  }

  // Don't manually set the page to be dirty, it seems to crash things
  //set_page_dirty(page);  // When I set the page as dirty I now git a kernel null pointer bug in generic_make_request
  
  //  tag_pages_for_writeback(mapping, page->index, page->index+4096);
  error = mapping->a_ops->writepage(page, &wbc);
  if (!error) {
    wait_on_page_writeback(page);
    //    printk(KERN_ALERT "We have finished waiting on the page writeback for page %p\n", page);
    if (PageError(page))
      error = -EIO;
  }

  if(DEBUG_WRITING) {
  printk("I got a write err=%d and wbc nr to write is %ld\n", error, wbc.nr_to_write);
  for(j=0; j < PAGE_SIZE; j++) {
    printk("%x", buf[j]);
  }
  printk("\n");
  }
  //  mapping->a_ops->sync_page(page);

  // write_one_page

  ClearPageDirty(page);
  unlock_page(page);

#if 1
  del_page_from_radix_tree(mapping, page);
  page->mapping = NULL;
#endif
  //  printk("Was my write successfull %d and what is the count now %d\n", error, page_count(page));

  //      page->mapping == NULL;

  rdtscll(end_lookup); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_lookup_cpuID = GET_CPU_ID();
  init_copy_cpuID = end_lookup_cpuID;
  ini_copy = end_lookup;

  /* Get the virtual address for the pages */
/*   src = page_address(page); */
/*   dst = page_address(dev_page); */
/*   memcpy(dst, src, PAGE_SIZE); */

  rdtscll(end_copy); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
  end_copy_cpuID = GET_CPU_ID();

  if(device == NULL) {
    printk(KERN_DEBUG "The device was null in the write page handler.\n");
    goto out;
  }

  device_metadata = (struct raw_device_data *)device;

  if(device_metadata == NULL) {
    printk(KERN_DEBUG "The device metadata was null in the write page handler.\n");
    goto out;
  }

  track_statistics(&device_metadata->perf_cntr.write, end_lookup-ini_lookup, init_lookup_cpuID != end_lookup_cpuID, end_copy-ini_copy, init_copy_cpuID != end_copy_cpuID);

out:
  return error;
}

int flush_page(struct file *filp, void *device, unsigned long device_linear_address, struct page **pagep) {
  struct page *page = *pagep;
  int ret;

  lock_page(page);
  if (page_has_buffers(page)) {
    ret = try_to_free_buffers(page);
    if(!ret) {
      printk(KERN_ALERT "******************** WARNING flush_page is unable to free page buffers for %p ret code=%d ********************\n", page, ret);
    }
  }
  unlock_page(page);

  //  printk("flushing page %lx count %d \n", page->index, page_count(page));
  //  remove_from_page_cache(page);
/*   atomic_dec(&page->_count); */
//  err = del_page_from_radix_tree(mapping, page);
  page->mapping = NULL;
  page->index = 0;

  /* Make sure to clear the page flags when the page is flushed so that it can be reused safely.
   * Note that the data does not have to be cleared, since it will be overwritten.
   */
  if(PagePrivate(page)) {
    //    printk(KERN_ALERT "flush: page %p is private: %lx\n", page, page->flags);
    ClearPagePrivate(page);
  }

  if(PageReferenced(page)) { /* I need to double check if this should be cleared or what here. */
    //    printk(KERN_ALERT "flush: page %p is referenced: %lx\n", page, page->flags);
    ClearPageReferenced(page);
  }
  if(PageUptodate(page)) {
    //    printk(KERN_ALERT "flush: page %p is uptodate: %lx\n", page, page->flags);
    ClearPageUptodate(page);
  }
  if(PageDirty(page)) { /* I think that this should be a bug for now */
    printk(KERN_ALERT "flush: page %p is dirty: %lx\n", page, page->flags);
    ClearPageDirty(page);
  }

  if(PageActive(page)) { /* I think that this should be a bug for now */
    printk("Why is the page still active: %lx\n", page->flags);
    ClearPageActive(page);
  }

  put_page(page);
  return 0;
}

inline int redirect_IO(struct raw_device_data *dimmap_dev, struct file *filp, struct address_space *tgt_mapping) {
  int device_inuse;

  filp->f_flags |= O_DIRECT;
  filp->f_mapping = tgt_mapping;
  device_inuse = ++dimmap_dev->inuse;
  if (device_inuse == 1) {
    /* 	  printk(KERN_ALERT "I am about to overwrite the i_mapping pointer %p with %p\n",  */
    /* 		 filp->f_path.dentry->d_inode->i_mapping, */
    /* 		   tgt_mapping); */
    filp->f_path.dentry->d_inode->i_mapping = tgt_mapping;
    /* Override the default (NULL) function for getting attributes 
       so that the pseudo-device looks like the target device */
    dimmap_dev->orig_i_ops = filp->f_path.dentry->d_inode->i_op;
    dimmap_dev->inode_ops = kmalloc(sizeof(struct inode_operations), GFP_KERNEL);
    copy_inode_ops(dimmap_dev->inode_ops, filp->f_path.dentry->d_inode->i_op);
    /* Install a custom getattr function to redirect a stat to the target file */
    dimmap_dev->inode_ops->getattr = dimmap_vfs_getattr;
    /* 		print_inode_ops_info(dimmap_dev->inode_ops, "dimmap inode"); */
    /* 		print_inode_ops_info(filp->f_path.dentry->d_inode->i_op, "d_inode"); */
    filp->f_path.dentry->d_inode->i_op = dimmap_dev->inode_ops;
  }
  filp->private_data = dimmap_dev;

  return device_inuse;
}

inline int release_IO(struct raw_device_data *dimmap_dev, struct inode *inode) {
  int device_inuse;

  device_inuse = --dimmap_dev->inuse;
  if (device_inuse == 0) {
    /* Here  inode->i_mapping == bdev->bd_inode->i_mapping  */
    inode->i_mapping = &inode->i_data;
    inode->i_mapping->backing_dev_info = &default_backing_dev_info;
    if(inode->i_op == dimmap_dev->inode_ops) {
      //		  printk(KERN_ALERT "reseting the getattr function pointer\n");
      inode->i_op = dimmap_dev->orig_i_ops;
      kfree(dimmap_dev->inode_ops);
    }
  }

  return device_inuse;
}

/************************************************************************
 *
 * Character Device handler functions
 *
 ***********************************************************************/

/*
 * Open/close code for raw IO.
 *
 * We just rewrite the i_mapping for the /dev/raw/rawN file descriptor to
 * point at the blockdev's address_space and set the file handle to use
 * O_DIRECT.
 *
 *
 * Check to make sure that the block size is set to PAGE_SIZE otherwise 
 * the performance will suffer.
 */
static int raw_open(struct inode *inode, struct file *filp)
{
	const int minor = iminor(inode);
	struct raw_device_data *dimmap_dev;
	struct block_device *bdev;
	int err;
	struct address_space *tgt_mapping;

	printk(KERN_ALERT "The correct handler is called, what is minor %d and is there private data %p\n", minor, inode->i_private);

	if (minor == 0/*  && inode->i_private == NULL */) {	/* It is the control device */
		filp->f_op = &raw_ctl_fops;
		return 0;
	}

	print_address_space_ops_info(filp->f_path.dentry->d_inode->i_mapping->a_ops, "filp");

	lock_kernel();
	mutex_lock(&raw_mutex);

/*         if(inode->i_private != NULL) { */
/* 	  printk(KERN_ALERT "FOUND some data\n"); */
/*           dimmap_dev = inode->i_private; */
/*           if(dimmap_dev->type != DIMMAP_FILEMAP) { */
/*             printk(KERN_ALERT "WARNING: dimmap device has an unexpected type %d\n", dimmap_dev->type); */
/*           } */
/*         }else { */
          dimmap_dev = &raw_devices[minor];
/*         } */

	  BUG_ON(dimmap_dev->type != DIMMAP_BLOCKDEV);
/* 	if(dimmap_dev->type == DIMMAP_BLOCKDEV) { */
	  /*
	   * All we need to do on open is check that the device is bound.
	   */
	  bdev = dimmap_dev->binding.dev;
	  err = -ENODEV;
	  if (!bdev)
	    goto out;
	  igrab(bdev->bd_inode);
	  err = blkdev_get(bdev, filp->f_mode);
	  if (err)
	    goto out;
	  err = bd_claim(bdev, raw_open);
	  if (err)
	    goto out1;

	  if(bdev->bd_block_size != PAGE_SIZE) {
	    printk(KERN_ALERT "**************************************** WARNING: Performance degredation is likely, the device's block size is %d not %ld ****************************************\n", bdev->bd_block_size, PAGE_SIZE);
	    /* Should the driver bail here */
	    /* 		goto out2; */
	  }
	  tgt_mapping = bdev->bd_inode->i_mapping;
/* 	}else { /\* DIMMAP_FILEMAP *\/ */
/* 	  if(dimmap_dev->binding.filp == NULL) { */
/* 	    //	    printk(KERN_ALERT "The mode of the target file is %x\n", inode->i_mode); */
/* 	    /\* BVE FIXME - need to be careful about the mode here and error handling if a file cannot be created *\/ */
/* 	    dimmap_dev->binding.filp = filp_open(dimmap_dev->filename, O_RDWR | O_CREAT | O_LARGEFILE, 0777); */
/* 	    if (IS_ERR(dimmap_dev->binding.filp)) { */
/* 	      printk(KERN_ALERT "Unable to open file %s: %p\n", dimmap_dev->filename, dimmap_dev->binding.filp); */
/* 	      dimmap_dev->binding.filp = NULL; */
/* 	      goto out; */
/* 	    } else { */
/* 	      fsnotify_open(dimmap_dev->binding.filp->f_path.dentry); */
/* 	    } */

/* 	    flush_file(dimmap_dev->binding.filp->f_mapping, 0, i_size_read(dimmap_dev->binding.filp->f_mapping->host)); */
/* 	  } */
/* 	  tgt_mapping = dimmap_dev->binding.filp->f_mapping; */
/* 	} */

	redirect_IO(dimmap_dev, filp, tgt_mapping);

/* 	filp->f_flags |= O_DIRECT; */
/* 	filp->f_mapping = tgt_mapping; */
/* 	if (++dimmap_dev->inuse == 1) { */
/* /\* 	  printk(KERN_ALERT "I am about to overwrite the i_mapping pointer %p with %p\n",  *\/ */
/* /\* 		 filp->f_path.dentry->d_inode->i_mapping, *\/ */
/* /\* 		   tgt_mapping); *\/ */
/* 		filp->f_path.dentry->d_inode->i_mapping = tgt_mapping; */
/* 		/\* Override the default (NULL) function for getting attributes  */
/* 		   so that the pseudo-device looks like the target device *\/ */
/* 		dimmap_dev->orig_i_ops = filp->f_path.dentry->d_inode->i_op; */
/* 		dimmap_dev->inode_ops = kmalloc(sizeof(struct inode_operations), GFP_KERNEL); */
/* 		copy_inode_ops(dimmap_dev->inode_ops, filp->f_path.dentry->d_inode->i_op); */
/* 		/\* Install a custom getattr function to redirect a stat to the target file *\/ */
/* 		dimmap_dev->inode_ops->getattr = dimmap_vfs_getattr; */
/* /\* 		print_inode_ops_info(dimmap_dev->inode_ops, "dimmap inode"); *\/ */
/* /\* 		print_inode_ops_info(filp->f_path.dentry->d_inode->i_op, "d_inode"); *\/ */
/* 		filp->f_path.dentry->d_inode->i_op = dimmap_dev->inode_ops; */
/* 	} */
/* 	filp->private_data = dimmap_dev; */

	mutex_unlock(&raw_mutex);
	unlock_kernel();
	print_file_pointer_info(filp, "RAW_OPEN-opening the file");
	printk(KERN_ALERT "Returning from the open function\n");
	return 0;

/*out2:*/
/*	bd_release(bdev); */
out1:
	blkdev_put(bdev, filp->f_mode);
out:
	mutex_unlock(&raw_mutex);
	unlock_kernel();
	return err;
}

/*
 * When the final fd which refers to this character-special node is closed, we
 * make its ->mapping point back at its own i_data.
 */
static int raw_release(struct inode *inode, struct file *filp)
{
	const int minor= iminor(inode);
	struct block_device *bdev = NULL;
	int /*dev_mode, err = 0,*/ device_inuse;
	struct raw_device_data *dimmap_dev;

	printk(KERN_ALERT "When is the raw release function called\n");

	mutex_lock(&raw_mutex);
/*         if(inode->i_private != NULL) { */
/* 	  printk(KERN_ALERT "raw release FOUND some data\n"); */
/*           dimmap_dev = inode->i_private; */
/*           if(dimmap_dev->type != DIMMAP_FILEMAP) { */
/*             printk(KERN_ALERT "WARNING: dimmap device has an unexpected type %d\n", dimmap_dev->type); */
/*           } */
/*         }else { */
          dimmap_dev = &raw_devices[minor];
/*         } */
/* 	dev_mode = dimmap_dev->type; */
/* 	if(dev_mode == DIMMAP_BLOCKDEV) { */
	  bdev = dimmap_dev->binding.dev;
/* 	} */
	
	device_inuse = release_IO(dimmap_dev, inode);

	mutex_unlock(&raw_mutex);

/* 	if(dev_mode == DIMMAP_BLOCKDEV) { */
	  bd_release(bdev);
	  blkdev_put(bdev, filp->f_mode);
/* 	}else { */
/* 	  fput(dimmap_dev->binding.filp); */
/* 	  printk(KERN_ALERT "Release is closing the file\n"); */
/* 	  err = filp_close(dimmap_dev->binding.filp, current->files); */
/* 	  printk(KERN_ALERT "Release closed the file with code %d\n", err); */
/* 	} */

	return 0;
}

/*
 * Forward ioctls to the underlying block device.
 */
static int
raw_ioctl(struct inode *inode, struct file *filp,
		  unsigned int command, unsigned long arg)
{
	struct raw_device_data *dev = filp->private_data;
	struct block_device *bdev;
	struct file *m_filp;

	if(dev->type == DIMMAP_BLOCKDEV) {
	  bdev = dev->binding.dev;
	  return blkdev_ioctl(bdev, 0, command, arg);
	}else {
	  m_filp = dev->binding.filp;
	  if(m_filp->f_op->ioctl) {
	    return m_filp->f_op->ioctl(m_filp->f_path.dentry->d_inode, m_filp, command, arg);
	  }else {
	    return -1;
	  }
	}

}

static void bind_device(struct dimmap_config_request *rq)
{
	struct raw_device_data *rawdev = &raw_devices[rq->raw_minor];
  	char str[64] = "";
	sprintf(str, "%s-mmap%d", RUNTIME_NAME, rq->raw_minor);

	device_destroy(raw_class, MKDEV(di_mmap_major, rq->raw_minor));
	free_prot_vm_area_set(&rawdev->active_vmas);

	device_create(raw_class, NULL, MKDEV(di_mmap_major, rq->raw_minor), NULL,
		      str/* "raw%d" */, rq->raw_minor);


	rawdev->dev_id = MKDEV(di_mmap_major, rq->raw_minor);
	
	//	init_prot_vm_area_set(&rawdev->active_vmas);

	atomic_set(&rawdev->num_concurrent_read_accesses, 0);
	atomic_set(&rawdev->num_concurrent_write_accesses, 0);

	return;
}

/* static void bind_file() { */
/* 	/\* command == DIMMAP_SETBIND_FILE *\/ */
/* 	  struct address_space *f_mapping; */
/* /\* 	  printk(KERN_ALERT "I received a request to bind to file descriptor %d\n", rq.fd); *\/ */
/* 	  rawdev->type = DIMMAP_FILEMAP; */
/* 	  strncpy(rawdev->filename, rq.filename, MAX_FILENAME_LEN); */
/* 	  rawdev->fd = rq.fd; */
/* 	  if(rawdev->fd != -1) { */
/* 	    rawdev->binding.filp = fget(rq.fd); */
/* 	    if(rawdev->binding.filp == NULL) { */
/* 	      err = -EBADF; */
/* 	      mutex_unlock(&raw_mutex); */
/* 	      goto out; */
/* 	    } */
/* 	    f_mapping = rawdev->binding.filp->f_mapping; */
/* 	    printk(KERN_ALERT "Successfully mapped the file %s, now flush it size=%ld\n",  */
/* 		   rawdev->filename, i_size_read(f_mapping->host)); */
/* 	    flush_file(f_mapping, 0, i_size_read(f_mapping->host)); */
/* 	  } */
/* 	  /\* else the file does not exist.  Wait until the dimmap device is opened then create the file *\/ */

/* } */

/*
 * Deal with ioctls against the raw-device control interface, to bind
 * and unbind other raw devices.
 */
static int raw_ctl_ioctl(struct inode *inode, struct file *filp,
			unsigned int command, unsigned long arg)
{
  struct dimmap_config_request rq;
  struct raw_device_data *rawdev;
  int err = 0;

  switch (command) {
  case RAW_SETBIND:
  case RAW_GETBIND:

    /* First, find out which raw minor we want */

    if (copy_from_user(&rq, (void __user *) arg, sizeof(rq))) {
      err = -EFAULT;
      goto out;
    }

    if (rq.raw_minor <= 0 || rq.raw_minor >= MAX_DI_MMAP_MINORS) {
      err = -EINVAL;
      goto out;
    }
    rawdev = &raw_devices[rq.raw_minor];

    if (command == RAW_SETBIND) {
      dev_t dev;

      /*
       * This is like making block devices, so demand the
       * same capability
       */
      if (!capable(CAP_SYS_ADMIN)) {
	err = -EPERM;
	goto out;
      }

      /*
       * For now, we don't need to check that the underlying
       * block device is present or not: we can do that when
       * the raw device is opened.  Just check that the
       * major/minor numbers make sense.
       */

      dev = MKDEV(rq.block_major, rq.block_minor);
      if ((rq.block_major == 0 && rq.block_minor != 0) ||
	  MAJOR(dev) != rq.block_major ||
	  MINOR(dev) != rq.block_minor) {
	printk(KERN_ALERT "Major / minor numbers do not make sense %llu %llu\n",
	       rq.block_major, rq.block_minor);
	err = -EINVAL;
	goto out;
      }

      mutex_lock(&raw_mutex);
      if (rawdev->inuse) {
	mutex_unlock(&raw_mutex);
	err = -EBUSY;
	goto out;
      }
      /* If there device is already bound to something, release it */
      if (rawdev->binding.dev) {
	if(rawdev->type == DIMMAP_NULL) {
	  /* BUG_ON(rawdev->type == DIMMAP_NULL) */
	  mutex_unlock(&raw_mutex);
	  err = -EPERM;
	  goto out;

	}
	BUG_ON(rawdev->type != DIMMAP_BLOCKDEV);
	bdput(rawdev->binding.dev);
	module_put(THIS_MODULE);
      }
      /* Now check to see if we are binding a new device or just cleaning up */
      if (rq.block_major == 0 && rq.block_minor == 0) {
	/* unbind */
	if(rawdev->type == DIMMAP_NULL) {
	  /* BUG_ON(rawdev->type == DIMMAP_NULL) */
	  mutex_unlock(&raw_mutex);
	  err = -EPERM;
	  goto out;

	}
	rawdev->binding.dev = NULL;
	rawdev->type = DIMMAP_NULL;
	device_destroy(raw_class,
		       MKDEV(di_mmap_major, rq.raw_minor));
	free_prot_vm_area_set(&rawdev->active_vmas);
      } else {
	rawdev->binding.dev = bdget(dev);
	if (rawdev->binding.dev == NULL) {
	  err = -ENOMEM;
	}else {
	  rawdev->type = DIMMAP_BLOCKDEV;
	  __module_get(THIS_MODULE);
	  bind_device(&rq);
	}
      }
      mutex_unlock(&raw_mutex);
    } else {
      struct block_device *bdev;

      mutex_lock(&raw_mutex);
      bdev = rawdev->binding.dev;
      if (bdev) {
	rq.block_major = MAJOR(bdev->bd_dev);
	rq.block_minor = MINOR(bdev->bd_dev);
      } else {
	rq.block_major = rq.block_minor = 0;
      }
      mutex_unlock(&raw_mutex);
      if (copy_to_user((void __user *)arg, &rq, sizeof(rq))) {
	err = -EFAULT;
	goto out;
      }
    }
    break;
  default:
    err = -EINVAL;
    break;
  }
 out:
  return err;
}

/*
 * Generic function to fsync a file or block device.
 *
 * filp may be NULL if called via the msync of a vma.
 */
int dimmap_fsync(struct file *filp, struct dentry *dentry, int datasync)
{
  struct raw_device_data *dimmap_dev = filp->private_data;
  printk(KERN_ALERT "sync was called\n");
  BUG_ON(dimmap_dev->type != DIMMAP_BLOCKDEV);

/*   if(dimmap_dev->type == DIMMAP_BLOCKDEV) { */
    return blkdev_fsync(filp, dentry, datasync);    
/*   }else { */
/*     return file_fsync(filp, dentry, datasync); */
/*   } */
}
 
/*
 * Mmap *is* available, but confined in a different file
 */
extern int perma_mmap(struct file *filp, struct vm_area_struct *vma);


/*
 * The fops
 */

/* static */ const struct file_operations raw_fops = {
	.read	=	do_sync_read, // std_read,
	.aio_read = 	generic_file_aio_read,
	 .write	=	do_sync_write, // std_write,
	.aio_write =	blkdev_aio_write,
	.fsync	=	dimmap_fsync,
	.open	=	raw_open,
	.release=	raw_release,
	.ioctl	=	raw_ioctl,
	.mmap   =	perma_mmap,
	.owner	=	THIS_MODULE,
};

static const struct file_operations raw_ctl_fops = {
	.ioctl	=	raw_ctl_ioctl,
	.open	=	raw_open,
	.owner	=	THIS_MODULE,
};

/* Devices in the /dev file system are actually on a tmpfs file system.
 * As a result, they use the same inode operation functions as tmpfs files */
/* static const struct inode_operations dimmap_inode_ops = { */
/* 	.setattr        = generic_setattr, */
/* 	.getattr        = dimmap_vfs_getattr, */
/*         .truncate_range = NULL, */
/*         .setxattr       = generic_setxattr, */
/*         .getxattr       = generic_getxattr, */
/*         .listxattr      = generic_listxattr, */
/*         .removexattr    = generic_removexattr, */
/* 	//	.check_acl      = shmem_check_acl, // eventually this is replaced with generic_check_acl */
/* }; */

static struct cdev raw_cdev;

static char *raw_devnode(struct device *dev, mode_t *mode)
{
	return kasprintf(GFP_KERNEL, "di-mmap/%s", dev_name(dev));
}

static int __init raw_init(void)
{
	dev_t dev = MKDEV(di_mmap_major, 0);
	int ret;
	char str[64] = "";
	int i;

	/*
	 * Register your major, and accept a dynamic number.
	 */
	ret = alloc_chrdev_region(&dev, 0, MAX_DI_MMAP_MINORS, MODULE_NAME);
	if (ret)
		goto error;

	/* Find the major number that was assigned to this device instance */
	di_mmap_major = MAJOR(dev);

	cdev_init(&raw_cdev, &raw_fops);
	ret = cdev_add(&raw_cdev, dev, MAX_DI_MMAP_MINORS);
	if (ret) {
		kobject_put(&raw_cdev.kobj);
		goto error_region;
	}

	raw_class = class_create(THIS_MODULE, MODULE_NAME);
	if (IS_ERR(raw_class)) {
	  printk(KERN_ERR "Error creating %s class.\n", MODULE_NAME);
		cdev_del(&raw_cdev);
		ret = PTR_ERR(raw_class);
		goto error_region;
	}
	raw_class->devnode = raw_devnode;
	sprintf(str, "%s-ctl", RUNTIME_NAME);
	device_create(raw_class, NULL, MKDEV(di_mmap_major, 0), NULL, str);

	/* WARNING: This code does not support hot insertion of cpu codes */
	NUMA_node_mask = num_online_nodes() - 1;

	atomic_set(&global_num_concurrent_read_accesses, 0);
	atomic_set(&global_num_concurrent_write_accesses, 0);
	simulate_unified_channel = 1;

/* 	buf_ops = init_mmap_buffer_ops(); */
	buf_data = init_mmap_buffer_data(buf_data);

	/* Clear out the data structures */
	memset(&raw_devices, 0, MAX_DI_MMAP_MINORS * sizeof(struct raw_device_data));
	for(i = 0; i < MAX_DI_MMAP_MINORS; i++) {
	  init_prot_vm_area_set(&(raw_devices[i].active_vmas));
	}

	/* Once all of the data structures are allocated, initialize them */
	reset_device_parameters();
	//	reset_driver_parameters();

	if(ENABLE_PROFILING) {
	  initProfileCounters();
	}

#ifdef PTE_CLEAR_FLUSH
	strcat(comp_flags, "-PTECF");
#endif	

	printk(KERN_INFO "%s: module v%d.%d.%d%s\n", MODULE_NAME, version.major, version.minor, version.patch, comp_flags);

	perma_proc_setup(THIS_MODULE, MODULE_NAME, version, comp_flags, perma_devs, 0);

	register_di_mmap_fs();

	return 0;

error_region:
	unregister_chrdev_region(dev, MAX_DI_MMAP_MINORS);
error:
	return ret;
}

static void __exit raw_exit(void)
{
	unregister_di_mmap_fs();

	perma_proc_cleanup(MODULE_NAME);
	cleanup_mmap_params();

	/* Do any cleanup that the buffer requires */
	cleanup_mmap_buffer_data(buf_data);

	device_destroy(raw_class, MKDEV(di_mmap_major, 0));

	class_destroy(raw_class);
	cdev_del(&raw_cdev);
	unregister_chrdev_region(MKDEV(di_mmap_major, 0), MAX_DI_MMAP_MINORS);
}

module_init(raw_init);
module_exit(raw_exit);
