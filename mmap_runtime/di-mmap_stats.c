/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include "process_tsc.h"
#include "di-mmap_stats.h"

static version_number_t version;
static char comp_flags[256];
int DEBUG_LOGGING;
int num_devices;
int device_size;

long num_high = 0, num_low = 0;
int num_status_pages=NUM_STANDARD_PAGES + 4;
int num_histogram_pages=0;

void init_io_stats(io_stats *stats) {
/*   stats->concurrent_accesses = 0; */
/*   stats->requested_delay = 0; */
  stats->transfer_size = 0;
/*   stats->measured_clock_cycles = 0; */
/*   stats->num_delay_loops = 0; */
  init_tsc_stats(&(stats->tsc_pair0));
  init_tsc_stats(&(stats->tsc_pair1));
  return;
}

void initProfileCounters(void) {
  int i;
  profile_info *perf_cntr;

  for(i = 0; i < num_devices; i++) {
    perf_cntr = select_ith_device(i);
    atomic64_set(&perf_cntr->read.num_requests, 0);
/*     atomic64_set(&perf_cntr->read.num_zap_requests, 0); */
/*     atomic64_set(&perf_cntr->read.num_zapped_page_ranges, 0); */
/*     init_io_stats(&(perf_cntr->read.max)); */
    init_io_stats(&(perf_cntr->read.aggregate));
/*     init_io_stats(&(perf_cntr->read.min)); */
    atomic64_set(&perf_cntr->write.num_requests, 0);
/*     atomic64_set(&perf_cntr->write.num_zap_requests, 0); */
/*     atomic64_set(&perf_cntr->write.num_zapped_page_ranges, 0); */
/*     init_io_stats(&(perf_cntr->write.max)); */
    init_io_stats(&(perf_cntr->write.aggregate));
/*     init_io_stats(&(perf_cntr->write.min)); */
  }

  num_high = 0;
  num_low = 0;

  initDeviceSpecificProfileCounters();
}

/* void prettyprint_delay_style(char *msg) { */
/*   sprintf(msg, "%s(%d)", DelayStyle == HYBRID_DELAY_STYLE ? "Hybrid" : "SpinWait", DelayStyle); */
/*   return; */
/* } */

/* void prettyprint_page_access_style(char *msg) { */
/*   if(PageAccessStyle == PARALLEL_PAGE_ACCESS) { */
/*     sprintf(msg, "%s(%d)", "Parallel", PageAccessStyle); */
/*   }else if(PageAccessStyle == SERIAL_PAGE_ACCESS) { */
/*     sprintf(msg, "%s(%d)", "Serial", PageAccessStyle); */
/*   }else { */
/*     sprintf(msg, "%s(%d)", "ParallelN", PageAccessStyle); */
/*   } */
/*   return; */
/* } */

void compute_average_io_stats(io_stats *aggregate, io_stats *average, unsigned long num_requests, unsigned long num_zap_requests) {
  if(num_requests != 0) {
/*     average->concurrent_accesses = aggregate->concurrent_accesses / num_requests; */
/*     average->requested_delay = aggregate->requested_delay / num_requests; */
    average->transfer_size = aggregate->transfer_size / num_requests;
/*     average->measured_clock_cycles = aggregate->measured_clock_cycles / num_requests; */
/*     average->num_delay_loops = aggregate->num_delay_loops / num_requests; */
    average->tsc_pair0.clock_cycles = aggregate->tsc_pair0.clock_cycles / num_requests;
    average->tsc_pair0.faults = aggregate->tsc_pair0.faults / num_requests;
  }else {
    init_io_stats(average);
  }

  if(num_zap_requests != 0) {
    average->tsc_pair1.clock_cycles = aggregate->tsc_pair1.clock_cycles / num_zap_requests;
    average->tsc_pair1.faults = aggregate->tsc_pair1.faults / num_zap_requests;
  }else {
    average->tsc_pair1.clock_cycles = 0;
    average->tsc_pair1.faults = 0;
  }
}

void seq_print_io_stats(struct seq_file *s, io_stats *stats) {
  seq_printf(s, "%16llu %16llu %16llu",
/* 	     stats->concurrent_accesses, */
/* 	     stats->requested_delay, */
	     stats->transfer_size,
/* 	     stats->measured_clock_cycles, */
/* 	     stats->num_delay_loops, */
	     stats->tsc_pair0.clock_cycles,
	     stats->tsc_pair1.clock_cycles);
}

/* void print_read_stats(char *msg) { */
/*   sprintf(msg, "Read_Delay=%d ns, PeripheralReadLatency=%d ps/B", */
/* 	  ReadDelay, rTransitLatency); */
/*   return; */
/* } */

/* void print_write_stats(char *msg) { */
/*   sprintf(msg, "Write_Delay=%d ns, PeripheralWriteLatency=%d ps/B", */
/* 	  WriteDelay, wTransitLatency); */
/*   return; */
/* } */

/* void print_timing_stats(char *msg) { */
/*   int scaledBusReadLatency = busReadLatency; */
/*   int scaledBusWriteLatency = busWriteLatency; */
/*   if(PeripheralBusMultiplier != PCIeInf) { */
/*     scaledBusReadLatency = busReadLatency/PeripheralBusMultiplier; */
/*     scaledBusWriteLatency = busWriteLatency/PeripheralBusMultiplier; */
/*   } */
/*   sprintf(msg, "Driver Latency=%d ns, Bus Latencies: read=%d ps/B write=%d ps/B, Device Ctrlr Latencies: read=%d ps/B write=%d ps/B", */
/* 	  driverLatency, scaledBusReadLatency, scaledBusWriteLatency, devReadLatency, devWriteLatency); */
/*   return; */
/* } */

int perma_ramdisk_tuning_proc_write(struct file* filp, const char __user *buff, unsigned long len, void* data) {
/*	char msg1[MAX_STR_LEN], msg2[MAX_STR_LEN], msg3[MAX_STR_LEN];*/
	char cmd[64];
	int param1, param2, param3, param4;

	sscanf(buff, "%s %d %d %d %d", &cmd[0], &param1, &param2, &param3, &param4);
	if(strcasecmp(cmd, HELP) == 0) {
	  explain_tuning_params();
	  return len;
	}else if(strcasecmp(cmd, DEBUG_DRIVER) == 0) {
	  DEBUG_LOGGING = param1;
	  if(DEBUG_LOGGING >= 1) {
	    printk("Enabled debugging mode -- logging level %d\n", DEBUG_LOGGING);
	  }else {
	    printk("Disabled debugging mode\n");
	  }
	  return len;
	}else if(strcasecmp(cmd, RESET_PARAMS) == 0 || strcasecmp(cmd, RESET) == 0) {
	  reset_device_parameters();
	  printk("Resetting device parameters to defaults\n");
	  if(ENABLE_PROFILING) {
	    initProfileCounters();
	  }
	  printk("Clearing device counters\n");
/* 	}else if(strcasecmp(cmd, MISC) == 0) { */
/* 	  DelayStyle = param1; */
/* 	  PageAccessStyle = param4; */
/* 	}else if(strcasecmp(cmd, VDELAY_THRESHOLDS) == 0) { */
/* 	  SchedYield_Threshold = param1; */
/* 	  Sleep_Threshold = param2; */
/* 	}else if(strcasecmp(cmd, BUS_LATENCY) == 0) { */
/* 	  busReadLatency = param1; */
/* 	  busWriteLatency = param2; */
/* 	  initTransitLatency(); */
/* 	}else if(strcasecmp(cmd, Moneta_PeripheralBusBaseline) == 0) { */
/* 	  busReadLatency = PCIE_READ_DMA_LATENCY; */
/* 	  busWriteLatency = PCIE_WRITE_DMA_LATENCY; */
/* 	  initTransitLatency(); */
/* 	}else if(strcasecmp(cmd, Ideal1_PeripheralBusBaseline) == 0) { */
/* 	  busReadLatency = PCIE_1_IDEAL_LATENCY; */
/* 	  busWriteLatency = PCIE_1_IDEAL_LATENCY; */
/* 	  initTransitLatency(); */
/* 	}else if(strcasecmp(cmd, Ideal2_PeripheralBusBaseline) == 0) { */
/* 	  busReadLatency = PCIE_2_IDEAL_LATENCY; */
/* 	  busWriteLatency = PCIE_2_IDEAL_LATENCY; */
/* 	  initTransitLatency(); */
/* 	}else if(strcasecmp(cmd, CTRLR_LATENCY) == 0) { */
/* 	  devReadLatency = param1; */
/* 	  devWriteLatency = param2; */
/* 	  initTransitLatency(); */
	}else if(strcasecmp(cmd, CLEAR_COUNTERS) == 0 || strcasecmp(cmd, CLEAR) == 0) {
	  if(ENABLE_PROFILING) {
	    initProfileCounters();
	  }
	  printk("Clearing device counters\n");
/* 	}else if(strcasecmp(cmd, DEV_LATENCY) == 0 || strcasecmp(cmd, ACCESS_TIME) == 0) { */
/* 	  ReadDelay = param1; */
/* 	  WriteDelay = param2; */
/* 	  initTransitLatency(); */
/* 	  printk("PerMA RAMDISK Timings Changed to Read: %d ns, Write: %d ns, PeripheralBusMultiplier: %d, with a Page Size: %d, and a driver latency: %d\n", ReadDelay, WriteDelay, PeripheralBusMultiplier, PageSize, driverLatency); */
/* 	}else if(strcasecmp(cmd, SET_PAGE_SIZE) == 0 || strcasecmp(cmd, SET_PAGESIZE) == 0) { */
/* 	  PageSize = param1; */
/* 	  if(PageSize > 4096) { */
/* 	    printk("PerMA RAMDISK model cannot faithfully model a page size greater than 4KB, setting page size to 4KB\n"); */
/* 	    PageSize = 4096; */
/* 	  } */
/* 	}else if(strcasecmp(cmd, BUS_WIDTH) == 0 || strcasecmp(cmd, BUS_MULT) == 0) { */
/* 	  PeripheralBusMultiplier = param1; */
/* 	  initTransitLatency(); */
/* 	  printk("PerMA RAMDISK Timings Changed to Read: %d ns, Write: %d ns, PeripheralBusMultiplier: %d, with a Page Size: %d, and a driver latency: %d\n", ReadDelay, WriteDelay, PeripheralBusMultiplier, PageSize, driverLatency); */
/* 	}else if(strcasecmp(cmd, DRIVER_LATENCY) == 0) { */
/* 	  driverLatency = param1; */
	}else if(strcasecmp(cmd, DEVICE_SIZE) == 0) { /* Change the max device size */
	  device_size = param1;

	  printk("PerMA RAMDISK cleaning up old block devices\n");
	  cleanup_devices();
	  
	  printk("PerMA RAMDISK setting up new block devices\n");
	  setup_devices();

	  printk("PerMA RAMDISK Size Changed to: %d KB\n",device_size);
	}else {
	  int result = tune_device_specifics(cmd, param1, param2, param3, param4);
	  if(result) {
	    printk("WARNING: Invalid tuning requst received: %s! Query help via: echo help > /proc/perma-ramdisk-tuning\n", cmd);
/* 	    explain_tuning_params(); */
	  }
	}

/* 	prettyprint_delay_style(msg1); */
/* 	prettyprint_page_access_style(msg2); */
/* 	print_timing_stats(msg3); */
/* 	printk("PerMA RAMDISK Tuning: Delay Style = %s, Schedule Yield Threshold = %d ns, Sleep Threshold = %d ns, Page Access Style = %s, %s\n",  */
/* 	       msg1, SchedYield_Threshold, Sleep_Threshold, msg2, msg3); */

	return len;
}

void explain_tuning_params() {
  printk("PerMA tuning parameters are: \n");
  printk("\t%s\n", HELP);
  printk("\t%s\n", DEBUG_DRIVER);
  printk("\t%s or %s\n", RESET_PARAMS, RESET);
  printk("\t%s\n", MISC);
  printk("\t%s\n", BUS_LATENCY);
/*   printk("\t%s\n", Moneta_PeripheralBusBaseline); */
/*   printk("\t%s\n", Ideal1_PeripheralBusBaseline); */
/*   printk("\t%s\n", Ideal2_PeripheralBusBaseline); */
  printk("\t%s\n", CTRLR_LATENCY);
  printk("\t%s or %s\n", CLEAR_COUNTERS, CLEAR);
  printk("\t%s or %s\n", DEV_LATENCY, ACCESS_TIME);
  printk("\t%s or %s\n", SET_PAGE_SIZE, SET_PAGESIZE);
  printk("\t%s or %s\n", BUS_WIDTH, BUS_MULT);
  printk("\t%s\n", DRIVER_LATENCY);  
  printk("\t%s\n", DEVICE_SIZE);
  printk("\t%s\n", VDELAY_THRESHOLDS);
  explain_device_tuning_params();
  return;
}

/*
 * Output to perma_stats file using a sequence file
 */

static void *perma_seq_start(struct seq_file *s, loff_t *pos) {
  if(*pos >= num_status_pages) {
    return NULL; /* No more to read */
  }
  return pos++;
}

static void *perma_seq_next(struct seq_file *s, void *v, loff_t *pos) {
  (*pos)++;
  if(*pos >= num_status_pages) {
    return NULL;
  }
  return pos++;
}

static void perma_seq_stop(struct seq_file *s, void *v) {
  return;
};

static int perma_seq_show(struct seq_file *s, void *v) {
  int *i_p = (int *) v;
  int i = *i_p;
  int io_channel = i - NUM_STANDARD_PAGES;
  io_stats *read_io_stats, *write_io_stats;
  io_stats avg_read_stats, avg_write_stats;
  char msg[MAX_STR_LEN] = "";
  profile_info *perf_cntr;
  char line_leader = '#';

  switch(i) {
  case 0: {
    seq_printf(s, "%c v%d.%d.%d%s: cpu_freq=%d MHz, Max_Device_Size=%d KB\n", 
	       line_leader, version.major, version.minor, version.patch, 
	       comp_flags, cpu_khz / 1000, device_size);
    break;
  }
  case 1: {
    	prettyprint_device_specifics(s, msg, line_leader);
	seq_printf(s, "%c     %s\n", line_leader, msg);
	break;
  }
  case 2: {
	seq_printf(s, "%c\t%70s | %67s\n", 
		   line_leader,
		   "Read Stats", 
		   "Write Stats");
/* 	seq_printf(s, "%c\t%172s | %135s\n",  */
/* 		   line_leader, */
/* 		   "Read Stats",  */
/* 		   "Write Stats"); */
	print_io_stats_header(msg, line_leader);
	seq_printf(s, "%s", msg);
    break;
  }
  default: {
    /* Iterate over all of the brd_devices to find the right one */
    perf_cntr = select_ith_device(io_channel);

    if(io_channel < num_devices) {
/*       if(io_channel == 0) { */
/* 	seq_printf(s, "%c Max.\n", line_leader); */
/*       } */
/*       read_io_stats = &perf_cntr->read.max; */
/*       write_io_stats = &perf_cntr->write.max; */
      io_channel = io_channel - num_devices;
      if(io_channel == 0) {
	seq_printf(s, "%c Avg.\n", line_leader);
      }
      compute_average_io_stats(&perf_cntr->read.aggregate, &avg_read_stats, atomic64_read(&perf_cntr->read.num_requests), atomic64_read(&perf_cntr->read.num_requests));
      //      compute_average_io_stats(&perf_cntr->read.aggregate, &avg_read_stats, atomic64_read(&perf_cntr->read.num_requests), atomic64_read(&perf_cntr->read.num_zap_requests));
      read_io_stats = &avg_read_stats;
      compute_average_io_stats(&perf_cntr->write.aggregate, &avg_write_stats, atomic64_read(&perf_cntr->write.num_requests),  atomic64_read(&perf_cntr->write.num_requests));
      //      compute_average_io_stats(&perf_cntr->write.aggregate, &avg_write_stats, atomic64_read(&perf_cntr->write.num_requests),  atomic64_read(&perf_cntr->write.num_zap_requests));
      write_io_stats = &avg_write_stats;
    }else if(io_channel >= num_devices && io_channel < 2*num_devices) {
      io_channel = io_channel - 2*num_devices;
      if(io_channel == 0) {
	seq_printf(s, "%c Aggregate\n", line_leader);
      }
      read_io_stats = &perf_cntr->read.aggregate;
      write_io_stats = &perf_cntr->write.aggregate;
/*     }else if(io_channel >= 2*num_devices && io_channel < 3*num_devices) { */
/*     }else if(io_channel >= 3*num_devices && io_channel < 4*num_devices) { */
/*       io_channel = io_channel - 3*num_devices; */
/*       if(io_channel == 0) { */
/* 	seq_printf(s, "%c Min.\n", line_leader); */
/*       } */
/*       read_io_stats = &perf_cntr->read.min; */
/*       write_io_stats = &perf_cntr->write.min; */
    }else {
      seq_printf(s, "%c Unknown iteration of the show command %d\n", line_leader, i);
      break;
    }

    seq_printf(s, "%c\t%2d ", line_leader, io_channel);
    seq_print_io_stats(s, read_io_stats);
/*     seq_printf(s, " %16lu %16lu", atomic64_read(&perf_cntr->read.num_zap_requests), atomic64_read(&perf_cntr->read.num_zapped_page_ranges)); */
    seq_printf(s, " %16lu | ", atomic64_read(&perf_cntr->read.num_requests));
    seq_print_io_stats(s, write_io_stats);
    seq_printf(s, " %16lu\n", atomic64_read(&perf_cntr->write.num_requests));
    break;
  }
  }
  return 0;
};

static struct seq_operations perma_seq_ops = {
  .start = perma_seq_start,
  .next = perma_seq_next,
  .stop = perma_seq_stop,
  .show = perma_seq_show
};

static int perma_proc_open(struct inode *inode, struct file *file) {
  return seq_open(file, &perma_seq_ops);
}

static struct file_operations perma_proc_ops = {
  .owner = THIS_MODULE,
  .open  = perma_proc_open,
  .read  = seq_read,
  .llseek = seq_lseek,
  .release = seq_release
};

static void *perma_hist_seq_start(struct seq_file *s, loff_t *pos) {
  if(*pos >= num_histogram_pages) {
    return NULL; /* No more to read */
  }
  return pos++;
}

static void *perma_hist_seq_next(struct seq_file *s, void *v, loff_t *pos) {
  (*pos)++;
  if(*pos >= num_histogram_pages) {
    return NULL;
  }
  return pos++;
}

static void perma_hist_seq_stop(struct seq_file *s, void *v) {
  return;
};

static int perma_hist_seq_show(struct seq_file *s, void *v) {
  int *i_p = (int *) v;
  int i = *i_p;
  char line_leader = '#';
  char line0[LONG_STR_LEN];
  char line1[LONG_STR_LEN];
  char line2[LONG_STR_LEN];
  char line3[LONG_STR_LEN];
  char line4[LONG_STR_LEN];

  report_page_fault_histogram(get_ith_device_id(i), line0, line1, line2, line3, line4);
  seq_printf(s, "%c %s%c %s%c %s%c %s%c %s", line_leader, line0, line_leader, line1, line_leader, line2, line_leader, line3, line_leader, line4);

  return 0;
};

static struct seq_operations perma_histogram_seq_ops = {
  .start = perma_hist_seq_start,
  .next = perma_hist_seq_next,
  .stop = perma_hist_seq_stop,
  .show = perma_hist_seq_show
};

static int perma_histogram_proc_open(struct inode *inode, struct file *file) {
  return seq_open(file, &perma_histogram_seq_ops);
}

static struct file_operations perma_histogram_proc_ops = {
  .owner = THIS_MODULE,
  .open  = perma_histogram_proc_open,
  .read  = seq_read,
  .llseek = seq_lseek,
  .release = seq_release
};

static struct proc_dir_entry* proc_entry;
static struct proc_dir_entry* proc_entry_stats;
static struct proc_dir_entry* proc_entry_hist;

void perma_proc_setup(struct module *owner, char *module_name, version_number_t driver_version, char driver_comp_flags[256], int driver_num_devices, int driver_device_size) {
  char proc_stats[64] = "";
  char proc_tuning[64] = "";
  char proc_histogram[64] = "";

  if(perma_proc_ops.owner != owner) {
    printk(KERN_INFO "The perma_stats was not able to get the proper owner\n");
    perma_proc_ops.owner = owner;
  }
  
  version = driver_version;
  strcpy(comp_flags, driver_comp_flags);
  num_devices = driver_num_devices;
  device_size = driver_device_size;
  num_status_pages = NUM_STANDARD_PAGES + 2*num_devices;
  num_histogram_pages = num_devices;

  sprintf(proc_stats, "%s-%s", module_name, PROC_STATS);
  sprintf(proc_tuning, "%s-%s", module_name, PROC_TUNING);
  sprintf(proc_histogram, "%s-%s", module_name, PROC_HISTOGRAM);

  /* create proc entry - make them writeable by anyone */
  proc_entry = create_proc_entry(proc_tuning, S_IFREG | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH, NULL);
  if (proc_entry == NULL) {
    printk("Unable to create proc entry for %s\n", proc_tuning);
  } else {
    proc_entry->write_proc = perma_ramdisk_tuning_proc_write;
#ifndef RHEL6
    proc_entry->owner = owner;
#endif
  }

  proc_entry_stats = create_proc_entry(proc_stats, S_IFREG, NULL);
  if (proc_entry_stats == NULL) {
    printk("Unable to create proc entry for %s\n", proc_stats);
  } else {
    proc_entry_stats->proc_fops = &perma_proc_ops;

  }

  proc_entry_hist = create_proc_entry(proc_histogram, S_IFREG, NULL);
  if (proc_entry_hist == NULL) {
    printk("Unable to create proc entry for %s\n", proc_histogram);
  } else {
    proc_entry_hist->proc_fops = &perma_histogram_proc_ops;
  }

#ifdef DEBUG
  printk(KERN_INFO "BRD INIT: proc files created: %s:%i\n", __FILE__, __LINE__);
#endif
  return;
}

void perma_proc_cleanup(char *module_name) {
  /* clean up proc entry */
  char proc_stats[64] = "";
  char proc_tuning[64] = "";
  char proc_histogram[64] = "";

  sprintf(proc_stats, "%s-%s", module_name, PROC_STATS);
  sprintf(proc_tuning, "%s-%s", module_name, PROC_TUNING);
  sprintf(proc_histogram, "%s-%s", module_name, PROC_HISTOGRAM);

  remove_proc_entry(proc_tuning, NULL);
  remove_proc_entry(proc_stats, NULL);
  remove_proc_entry(proc_histogram, NULL);
  return;
}

void update_statistics(channel_profile_info *channel_stats, unsigned long long size, 
		       tsc_stats tsc_pair0, tsc_stats tsc_pair1) {
  if(ENABLE_PROFILING) {
/*     if((access_count == 0 || (measured_delays.num_delay_loops == 0 && measured_delays.num_cpu_transitions == 0)) && (total_delay != 0 || measured_delays.counted_clock_cycles != 0)) { */
/*       if(DEBUG_LOGGING >= 1) { */
/* 	printk("WARNING: Illegal combination of statistics access count = %d, num. delay loops = %d, requested delay = %d ns, measured delay = %lld cc num_cpu_transitions = %d\n", */
/* 	       access_count, measured_delays.num_delay_loops, total_delay, measured_delays.counted_clock_cycles, measured_delays.num_cpu_transitions); */
/*       } */
/*     } */
/*     if(access_count > channel_stats->max.concurrent_accesses) { */
/*       channel_stats->max.concurrent_accesses = access_count; */
/*     } */
/*     if(total_delay > channel_stats->max.requested_delay) { */
/*       channel_stats->max.requested_delay = total_delay; */
/*     } */
/*     if(size > channel_stats->max.transfer_size) { */
/*       channel_stats->max.transfer_size = size; */
/*     } */
/*     if(measured_delays.counted_clock_cycles > channel_stats->max.measured_clock_cycles) { */
/*       channel_stats->max.measured_clock_cycles = measured_delays.counted_clock_cycles; */
/*     } */
/*     if(measured_delays.num_delay_loops > channel_stats->max.num_delay_loops) { */
/*       channel_stats->max.num_delay_loops = measured_delays.num_delay_loops; */
/*     } */
/*     if(tsc_pair0.clock_cycles > channel_stats->max.tsc_pair0.clock_cycles) { */
/*       channel_stats->max.tsc_pair0.clock_cycles = tsc_pair0.clock_cycles; */
/*     } */
/*     if(tsc_pair1.clock_cycles > channel_stats->max.tsc_pair1.clock_cycles) { */
/*       channel_stats->max.tsc_pair1.clock_cycles = tsc_pair1.clock_cycles; */
/*     } */
	
/*     channel_stats->aggregate.concurrent_accesses += access_count; */
/*     channel_stats->aggregate.requested_delay += total_delay; */
    channel_stats->aggregate.transfer_size += size;
/*     channel_stats->aggregate.measured_clock_cycles += measured_delays.counted_clock_cycles; */
/*     channel_stats->aggregate.num_delay_loops += measured_delays.num_delay_loops; */
    channel_stats->aggregate.tsc_pair0.clock_cycles += tsc_pair0.clock_cycles;
    channel_stats->aggregate.tsc_pair0.faults += tsc_pair0.faults;
    channel_stats->aggregate.tsc_pair1.clock_cycles += tsc_pair1.clock_cycles;
    channel_stats->aggregate.tsc_pair1.faults += tsc_pair1.faults;

    atomic64_inc(&channel_stats->num_requests);
/*     if(req_zap_page_range) { */
/*       atomic64_inc(&channel_stats->num_zap_requests); */
/*     } */

/*     if(access_count < channel_stats->min.concurrent_accesses || channel_stats->min.concurrent_accesses == 0) { */
/*       channel_stats->min.concurrent_accesses = access_count; */
/*     } */
/*     if(total_delay < channel_stats->min.requested_delay || channel_stats->min.requested_delay == 0) { */
/*       channel_stats->min.requested_delay = total_delay; */
/*     } */
/*     if(size < channel_stats->min.transfer_size || channel_stats->min.transfer_size == 0) { */
/*       channel_stats->min.transfer_size = size; */
/*     } */
/*     if(measured_delays.counted_clock_cycles < channel_stats->min.measured_clock_cycles || channel_stats->min.measured_clock_cycles == 0) { */
/*       channel_stats->min.measured_clock_cycles = measured_delays.counted_clock_cycles; */
/*     } */
/*     if(measured_delays.num_delay_loops < channel_stats->min.num_delay_loops || channel_stats->min.num_delay_loops == 0) { */
/*       channel_stats->min.num_delay_loops = measured_delays.num_delay_loops; */
/*     } */
/*     if(tsc_pair0.clock_cycles < channel_stats->min.tsc_pair0.clock_cycles || channel_stats->min.tsc_pair0.clock_cycles == 0) { */
/*       channel_stats->min.tsc_pair0.clock_cycles = tsc_pair0.clock_cycles; */
/*     } */
/*     if(tsc_pair1.clock_cycles < channel_stats->min.tsc_pair1.clock_cycles || channel_stats->min.tsc_pair1.clock_cycles == 0) { */
/*       channel_stats->min.tsc_pair1.clock_cycles = tsc_pair1.clock_cycles; */
/*     } */
  }
  return;
}
