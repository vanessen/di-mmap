Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
Produced at the Lawrence Livermore National Laboratory. 
Written by Brian Van Essen <vanessen1@llnl.gov>. 
LLNL-CODE-559080. 
All rights reserved.

This file is part of DI-MMAP, Version 1.0. 
For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html

--------------------------------------------------------------------------------

Thank you for downloading the Data-Intensive Memory-Map Runtime
(DI-MMAP) and Persistent Memory Architecture Simulator (PerMA Sim) repository.
This code was developed at Lawrence Livermore National Laboratory to
provide a high-performance memory-map runtime and a simulator to test
future NVRAM storage technologies at scale.

DI-MMAP:

The DI-MMAP runtime is a high performance memory-map runtime that
offers the following features:

1) a fixed sized page buffer
2) minimal dymaic memory allocation
3) a simple FIFO buffer replacement policy (optimized for unstructured
	data access patterns)
4) preferential caching for frequently accessed pages

To use the DI-MMAP runtime a kernel module must be compiled and
inserted into the running kernel.  Currently it has been tested with
Linux RHEL6 2.6.32 kernels.  Instructions for compiling the code are
found in README_compiling.txt.

Once the DI-MMAP module is loaded, the DI-MMAP runtime can be used in
one of two ways.

1) Direct file link: A di-mmap pseudo file is created in the /dev file
   system and it is linked to a block device.

2) DI-MMAP file system: A virtual file system (di-mmap-fs) is mounted
   and it is configured to use a directory as a backing store

DI-MMAP Publications:

A paper describing DI-MMAP was published at the DISCS 2012 workshop.
The paper "DI-MMAP: A High Performance Memory-Map Runtime for
Data-Intensive Application" and its associated slides can be found at:
http://discl.cs.ttu.edu/discs/docs/discs2012_submission_13_1.pdf
http://discl.cs.ttu.edu/discs/docs/DI-MMAP_A_High_Performance_Memory_Map_Runtime_1.pdf

PerMA Simulator:

PerMA Simulator Publications:

A paper describing the PerMA simulator was published at IPDPS 2012.
The paper "On the Role of NVRAM in Data-intensive Architectures: An
Evaluation" can be downloaded from the IEEE with DOI 10.1109/IPDPS.2012.69.
