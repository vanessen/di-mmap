Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
Produced at the Lawrence Livermore National Laboratory. 
Written by Brian Van Essen <vanessen1@llnl.gov>. 
LLNL-CODE-559080. 
All rights reserved.

This file is part of DI-MMAP, Version 1.0. 
For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html

--------------------------------------------------------------------------------

DI-MMAP:

cd mmap_runtime
make PTECF=n SIM_NPC=n

PerMA Sim:

cd simulator
make PTECF=n SIM_NPC=n

Options:

PTECF=[yn] -	Should eviction clear individual pages from the TLB when
		they are removed from the PTE, or should a bulk TLB flush be used.
SIM_NPC=[yn] -	Simulate with no page copy.  Should the similator copy
		data from the ramdisk or just share the page.


Kernel Patches:

Currently to work with Linux RHEL6 it is required the the kernel
exports a few functions that were previously exported, but are no
longer exported as of 2.6.2X.  A patch file is included in the git
repository with the name linux-2.6.32-dimmap.patch.  Its contents are
also included here.

------------------------------------ CUT HERE ------------------------------------

diff -Naur vanilla/arch/x86/mm/tlb.c latest/arch/x86/mm/tlb.c
--- vanilla/arch/x86/mm/tlb.c	2012-01-19 10:42:02.000000000 -0800
+++ latest/arch/x86/mm/tlb.c	2012-02-24 16:26:13.158927000 -0800
@@ -263,6 +263,8 @@
 	preempt_enable();
 }
 
+EXPORT_SYMBOL(flush_tlb_mm);
+
 void flush_tlb_page(struct vm_area_struct *vma, unsigned long va)
 {
 	struct mm_struct *mm = vma->vm_mm;
@@ -282,6 +284,8 @@
 	preempt_enable();
 }
 
+EXPORT_SYMBOL(flush_tlb_page);
+
 static void do_flush_tlb_all(void *info)
 {
 	unsigned long cpu = smp_processor_id();
diff -Naur vanilla/arch/x86/mm/track.c latest/arch/x86/mm/track.c
--- vanilla/arch/x86/mm/track.c	2012-02-23 11:49:28.147371000 -0800
+++ latest/arch/x86/mm/track.c	2012-02-23 11:51:15.533336000 -0800
@@ -53,6 +53,8 @@
 		atomic_inc(&mm_tracking_struct.count);
 }
 
+EXPORT_SYMBOL(do_mm_track_pte);
+
 #define LARGE_PMD_SIZE	(1 << PMD_SHIFT)
 
 void do_mm_track_pmd(void *val)
diff -Naur vanilla/mm/rmap.c latest/mm/rmap.c
--- vanilla/mm/rmap.c	2012-02-22 21:53:08.397939000 -0800
+++ latest/mm/rmap.c	2012-02-22 21:54:09.116659000 -0800
@@ -1117,6 +1117,8 @@
 	return ret;
 }
 
+EXPORT_SYMBOL(try_to_unmap_one);
+
 /*
  * objrmap doesn't work for nonlinear VMAs because the assumption that
  * offset-into-file correlates with offset-into-virtual-addresses does not hold.
@@ -1432,6 +1434,8 @@
 	return ret;
 }
 
+EXPORT_SYMBOL(try_to_unmap);
+
 /**
  * try_to_munlock - try to munlock a page
  * @page: the page to be munlocked


------------------------------------ END CUT ------------------------------------


