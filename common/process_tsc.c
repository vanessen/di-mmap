/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/delay.h>

#include "process_tsc.h"
#include "rhel_compatibility.h"

void inline init_tsc_stats(tsc_stats *tsc) {
  tsc->clock_cycles = 0;
  tsc->faults = 0;
  memset(tsc->label, 0, TSC_LABEL_SIZE);
}

void inline validate_tsc(tsc_stats *valid_tsc_diff, long long tsc_diff, int cpuID_diff, char *label, long long tsc_avg) {
  valid_tsc_diff->clock_cycles = tsc_diff;
  valid_tsc_diff->faults = 0;
  strcpy(valid_tsc_diff->label, label);

  if(cpuID_diff) {
    if(DEBUG_LOGGING >= 1) {
      printk(KERN_INFO "Thread %d: %s switched processors with a reported tsc of %llu replacing with avg tsc = %llu\n", 
	     current->pid, label, tsc_diff, tsc_avg);
    }
    /* Cycle counters are no longer reliable, set the tsc difference to the current average */
    valid_tsc_diff->clock_cycles = tsc_avg;
    valid_tsc_diff->faults = 1;
  }

  if(tsc_diff <= 0) {
    if(DEBUG_LOGGING >= 1) {
      printk(KERN_INFO "Thread %d: %s the processor counters have rolled over: tsc diff = %llu replacing with avg tsc = %llu\n", 
	     current->pid, label, tsc_diff, tsc_avg);
    }
    /* Cycle counters are no longer reliable, set the tsc difference to the current average */
    valid_tsc_diff->clock_cycles = tsc_avg;
    valid_tsc_diff->faults = 1;
  }
}

