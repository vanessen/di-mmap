/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/* 
 * This is a multi-level array that simulate a large contiguous array, but does not require
 * a big chunk of contiguous memory, which is hard to get in the kernel.
 * Note that it is modeled after a multi-level page table.
 */


#include <linux/mm.h>		/* everything */
#include <linux/errno.h>	/* error codes */
#include <linux/gfp.h>
#include "multi_level_array.h"
#include "rhel_compatibility.h"

/* unsigned long byte_address_to_super_page_index(unsigned long byte_address) { */
/*   sector_t sector = byte_address >> SECTOR_SHIFT; /\* Turn the byte offset into a sector *\/ */
/*   pgoff_t idx = sector >> PAGE_SECTORS_SHIFT; /\* Turn the sector offset into a page index *\/ */
/*   unsigned long super_page_idx = (idx >> MULTI_LEVEL_ARRAY_ALLOC_PAGES_ORDER); /\* Turn the page index into a superpage index if ORDER > 1 *\/ */

/*   return super_page_idx; */
/* } */

static void *kzalloc_local_first(int size) {
  void *ptr;

  ptr = kzalloc(size, GFP_ATOMIC/*  | __GFP_THISNODE */);
  if(ptr == NULL) {
    ptr = kzalloc(size, GFP_ATOMIC);
    printk(KERN_INFO "thread=%d on core=%d Failed to insert a local page for page table init\n", current->pid, GET_CPU_ID());
    
    if(ptr == NULL) {
      printk(KERN_INFO "Unable to allocate the page table %p\n", ptr);      
      printk(KERN_INFO "Failed to insert a page for page table init\n");
      //      return -ENOMEM;
    }
  }

  return ptr;
}

/* Top level pages : Buffer Size */
/* 32 : 256 GB */
/* 16 : 128 GB */
/* 8 : 64 GB */
/* 4 : 32 GB */
/* 2 : 16 GB */
/* 1 : 8 GB */
/*
 * Page table insertion
 * Implement a page table that uses 4KB to 32KB pages and can allocate space for 32GB to 256GB.
 * Use a two-level page table with 32 contiguous pages at the top level and 512 entries per page.
 * Starting with a super_page_index (not a byte address)
 * page table entry (PTE) - 9 bits : 8 ... 0
 * page global directory (PGD) - 5 + 9 bits : 22 ... 9
 */
int multi_level_array_insertion(multi_level_array_t *multi_level_array, unsigned long super_page_idx, void *page) {
  int pgd = (super_page_idx >> multi_level_array->pte_bit_shift) & multi_level_array->pgd_mask;
  int pte_idx = super_page_idx & multi_level_array->pte_mask;
  void **pte = NULL;

  if(unlikely(multi_level_array->array == NULL)) {
    multi_level_array->array = kzalloc_local_first(multi_level_array->num_top_level_entries * sizeof(void *));
    if(multi_level_array->array == NULL/*  || multi_level_array == -ENOMEM */) {
      return -ENOMEM;
    }
  }

  if(unlikely(multi_level_array->array[pgd] == NULL)) {
    pte = kzalloc_local_first(multi_level_array->num_entries_per_pgd * sizeof(void *));
    multi_level_array->array[pgd] = pte;
    if(multi_level_array->array[pgd] == NULL/*  || multi_level_array->array[pgd] == -ENOMEM */) {
      return -ENOMEM;
    }
  }else {
    pte = multi_level_array->array[pgd];
  }

  pte[pte_idx] = page;

  return 0;
}


int multi_level_array_clear_entry(multi_level_array_t *multi_level_array, unsigned long super_page_idx) {
  int pgd = (super_page_idx >> multi_level_array->pte_bit_shift) & multi_level_array->pgd_mask;
  int pte_idx = super_page_idx & multi_level_array->pte_mask;
  void **pte = NULL;

  pte = multi_level_array->array[pgd];
  pte[pte_idx] = NULL;

  return 0;
}

void *multi_level_array_lookup(multi_level_array_t *multi_level_array, unsigned long super_page_idx) {
  int pgd_idx = (super_page_idx >> multi_level_array->pte_bit_shift) & multi_level_array->pgd_mask;
  int pte_idx = super_page_idx & multi_level_array->pte_mask;
  void **pte;
  void *page = NULL;

  if(unlikely(multi_level_array == NULL || multi_level_array->array == NULL)) {
    return NULL;
  }else {
    if(unlikely(multi_level_array->array[pgd_idx] == NULL)) {
      return NULL;
    }else {
      pte = multi_level_array->array[pgd_idx];
    
      if(unlikely(pte[pte_idx] == NULL)) {
	return NULL;
      }else {
	page = pte[pte_idx];
      }
    }
  }

  return page;
}

void *multi_level_array_xchg(multi_level_array_t *multi_level_array, unsigned long super_page_idx, void *new_page) {
  int pgd_idx = (super_page_idx >> multi_level_array->pte_bit_shift) & multi_level_array->pgd_mask;
  int pte_idx = super_page_idx & multi_level_array->pte_mask;
  void **pte;
  void *page = NULL;

  if(unlikely(multi_level_array == NULL || multi_level_array->array == NULL)) {
    return NULL;
  }else {
    if(unlikely(multi_level_array->array[pgd_idx] == NULL)) {
      return NULL;
    }else {
      pte = multi_level_array->array[pgd_idx];    
      page = xchg(&pte[pte_idx], new_page);
    }
  }

  return page;
}

multi_level_array_t *multi_level_array_initialization(long num_array_entries) {
  multi_level_array_t *multi_level_array = NULL;
  int pgd_idx;
  void **pte = NULL;
  long num_top_level_entries = num_array_entries / MAX_NUM_ENTRIES_PER_PAGE + (num_array_entries % MAX_NUM_ENTRIES_PER_PAGE == 0 ? 0 : 1); /* Each top level entry has a fixed number of entries per page */
  long tmp_num_top_level_entries = num_top_level_entries;
  unsigned long clogb2 = 0;
  
  /* Find the ceiling of the log-base-2 of a number
   * 'Verilog-2001 Behavioral and Synthesis Enhancements' by Clifford
   * Cummings (available online)
   */
  clogb2 = 0;
  while((1<<clogb2) < tmp_num_top_level_entries) {
    clogb2 += 1;
  }

  multi_level_array = kzalloc_local_first(sizeof(multi_level_array_t));
  multi_level_array->num_top_level_entries = num_top_level_entries;
  multi_level_array->pgd_mask = (1 << clogb2) - 1;
  multi_level_array->num_entries_per_pgd = MAX_NUM_ENTRIES_PER_PAGE;
  multi_level_array->pte_mask = PERMA_PTE_MASK;
  multi_level_array->pte_bit_shift = PTE_BIT_SHIFT;

  printk(KERN_INFO "thread=%d on core=%d initializing multi-level array with %ld entries (%ld top level entries and a mask of %lx).\n",  current->pid, GET_CPU_ID(), num_array_entries, multi_level_array->num_top_level_entries, multi_level_array->pgd_mask);

  if(multi_level_array->array == NULL) {
    multi_level_array->array = kzalloc_local_first(multi_level_array->num_top_level_entries * sizeof(void *));
    if(multi_level_array->array == NULL) {
      return NULL;
    }
  }

  /* Allocate a page for each entry in the top level of the page table */
  for(pgd_idx = 0; pgd_idx < multi_level_array->num_top_level_entries; pgd_idx++) {
    if(multi_level_array->array[pgd_idx] == NULL) {
      pte = kzalloc_local_first(multi_level_array->num_entries_per_pgd * sizeof(void *));
      multi_level_array->array[pgd_idx] = pte;
      
      if(multi_level_array->array[pgd_idx] == NULL) {
	return NULL;
      }
    }
  }

  return multi_level_array;
}

void multi_level_array_free(multi_level_array_t *multi_level_array) {
  int pgd_idx/* , pte_idx */;
  void **pte;

  if(multi_level_array != NULL) {
    for(pgd_idx = 0; pgd_idx < multi_level_array->num_top_level_entries; pgd_idx++) {
      if(multi_level_array->array[pgd_idx] != NULL) {
	pte = multi_level_array->array[pgd_idx];
	kfree(pte);
	multi_level_array->array[pgd_idx] = NULL;
      }
    }
    kfree(multi_level_array->array);
    multi_level_array->array = NULL;
    kfree(multi_level_array);
    /* Caller has to set the pointer to multi_level_array to NULL after this call */
  }
  return;
}
