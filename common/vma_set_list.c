/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/sched.h>
#include "vma_set_list.h"

void init_prot_vm_area_set(prot_vm_area_set_t *vma_set) {
  int i;
  printk(KERN_ALERT "A prot_vm_area_set_t is now %ld bytes\n", sizeof(prot_vm_area_set_t));
  printk(KERN_ALERT "A vm_area_set_t is now %ld bytes\n", sizeof(vm_area_set_t));
  vma_set->num_vmas = 0;
  sema_init(&vma_set->sem, 1);
  INIT_LIST_HEAD(&(vma_set->vmas.list));
  vma_set->vmas.vma = NULL;
  vma_set->vmas.ref_count = 0;
  vma_set->vmas.owner = 0;
  vma_set->vmas.global_cntr = NULL;

  INIT_LIST_HEAD(&(vma_set->unused_vmas.list));
  vma_set->unused_vmas.vma = NULL;
  vma_set->unused_vmas.active = 0;
  vma_set->unused_vmas.ref_count = 0;
  vma_set->unused_vmas.owner = 0;
  vma_set->unused_vmas.global_cntr = NULL;

  for(i= 0; i < MAX_VMAS_PER_BANK; i++) {
    vma_set->vma_set_pool[i].vma = NULL;
    vma_set->vma_set_pool[i].active = 0;
    vma_set->vma_set_pool[i].ref_count = 0;
    vma_set->vma_set_pool[i].owner = 0;
    vma_set->vma_set_pool[i].global_cntr = NULL;
    list_add(&(vma_set->vma_set_pool[i].list),&(vma_set->unused_vmas.list));
  }
  
  return;
}

void free_prot_vm_area_set(prot_vm_area_set_t *vma_set) {
  vm_area_set_t *tmp;
  struct list_head *pos, *q;

  if(vma_set == NULL) {
    return;
  }

  down(&vma_set->sem);
  list_for_each_safe(pos, q, &vma_set->vmas.list){
    tmp = list_entry(pos, vm_area_set_t, list);
    list_move(&(tmp->list), &(vma_set->unused_vmas.list));
    vma_set->num_vmas--;
  }
  up(&vma_set->sem);

  return;
}

void add_vma_to_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma, vm_area_set_tlb_cntr_t *vma_tlb_cntr) {
  vm_area_set_t *tmp;

  down(&vma_set->sem);
  vma_set->num_vmas++;
  if(list_empty(&(vma_set->unused_vmas.list))) {
    printk(KERN_ALERT "I ran out of vmas when I tried to add one to the list\n");
  }
  BUG_ON(list_empty(&(vma_set->unused_vmas.list)));
  tmp = list_entry(vma_set->unused_vmas.list.prev, vm_area_set_t, list);
  tmp->vma = vma;
  tmp->active = 1;
  tmp->ref_count = 0;
  tmp->global_cntr = vma_tlb_cntr;
  tmp->owner = vma->vm_mm->owner->pid;
  list_move(&(tmp->list), &(vma_set->vmas.list));
  up(&vma_set->sem);

  return;
}

void remove_vma_from_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma) {
  vm_area_set_t *tmp;
  struct list_head *pos, *q;

  down(&vma_set->sem);
  list_for_each_safe(pos, q, &vma_set->vmas.list){
    tmp = list_entry(pos, vm_area_set_t, list);
    if(tmp->vma == vma) {
      tmp->active = 0;
      if(tmp->ref_count != 0) {
	printk(KERN_ALERT "I am removing a vma from a list that is still referenced\n");
      }
      // BUG_ON(tmp->ref_count != 0);
      tmp->global_cntr = NULL;
      tmp->owner = 0;
      list_move(&(tmp->list), &(vma_set->unused_vmas.list));
      vma_set->num_vmas--;
    }
  }
  up(&vma_set->sem);

  return;
}

int inactivate_vma_in_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma) {
  vm_area_set_t *tmp;
  struct list_head *pos, *q;
  int is_referenced = 0;

  down(&vma_set->sem);
  list_for_each_safe(pos, q, &vma_set->vmas.list){
    tmp = list_entry(pos, vm_area_set_t, list);
    if(tmp->vma == vma) {
      tmp->active = 0;
      is_referenced = tmp->ref_count;
    }
  }
  up(&vma_set->sem);

  return (is_referenced == 0);
}

void remove_unused_vma_from_list(prot_vm_area_set_t *vma_set) {
  vm_area_set_t *tmp;
  struct list_head *pos, *q;

  down(&vma_set->sem);
  list_for_each_safe(pos, q, &vma_set->vmas.list){
    tmp = list_entry(pos, vm_area_set_t, list);
    if(tmp->ref_count == 0 && tmp->active == 0) {
      tmp->global_cntr = NULL;
      tmp->owner = 0;
      list_move(&(tmp->list), &(vma_set->unused_vmas.list));
      vma_set->num_vmas--;
    }
  }
  up(&vma_set->sem);

  return;
}

vm_area_set_t *find_vma_in_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma) {
  vm_area_set_t *tmp, *vma_set_elem = NULL;
  struct list_head *pos, *q;

  down(&vma_set->sem);
  list_for_each_safe(pos, q, &vma_set->vmas.list){
    tmp = list_entry(pos, vm_area_set_t, list);
    if(tmp->vma == vma && tmp->active == 1) {
      vma_set_elem = tmp;
    }
  }
  up(&vma_set->sem);

  return vma_set_elem;
}

/*------------------------------------------------------------------------*/

void init_prot_vm_area_tlb_cntr_set(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set) {
  int i;
  printk(KERN_ALERT "A prot_vm_area_set_tlb_cntr_t is now %ld bytes\n", sizeof(prot_vm_area_set_tlb_cntr_t));
  printk(KERN_ALERT "A vm_area_set_tlb_cntr_t is now %ld bytes\n", sizeof(vm_area_set_tlb_cntr_t));
  vma_tlb_cntr_set->num_cntrs = 0;
  sema_init(&vma_tlb_cntr_set->sem, 1);
  vma_tlb_cntr_set->tlb_cntrs.vma = NULL;
#ifdef ATOMIC_TLB_CNTR
  atomic64_set(&vma_tlb_cntr_set->tlb_cntrs.tlb_flush_cntr, 0);
#else
  vma_tlb_cntr_set->tlb_cntrs.tlb_flush_cntr = 0;
#endif
  vma_tlb_cntr_set->tlb_cntrs.active = 0;
  vma_tlb_cntr_set->tlb_cntrs.owner = 0;
  //  init_rwsem(&vma_tlb_cntr_set->tlb_cntrs->sem);
  INIT_LIST_HEAD(&(vma_tlb_cntr_set->tlb_cntrs.list));


  INIT_LIST_HEAD(&(vma_tlb_cntr_set->unused_tlb_cntrs.list));
  vma_tlb_cntr_set->unused_tlb_cntrs.vma = NULL;
  vma_tlb_cntr_set->unused_tlb_cntrs.active = 0;
  vma_tlb_cntr_set->unused_tlb_cntrs.owner = 0;
#ifdef ATOMIC_TLB_CNTR
  atomic64_set(&vma_tlb_cntr_set->unused_tlb_cntrs.tlb_flush_cntr, 0);
#else
  vma_tlb_cntr_set->unused_tlb_cntrs.tlb_flush_cntr = 0;
#endif

  for(i= 0; i < MAX_VMAS_PER_BANK; i++) {
    vma_tlb_cntr_set->tlb_cntr_pool[i].vma = NULL;
    vma_tlb_cntr_set->tlb_cntr_pool[i].active = 0;
    vma_tlb_cntr_set->tlb_cntr_pool[i].owner = 0;
#ifdef ATOMIC_TLB_CNTR
  atomic64_set(&vma_tlb_cntr_set->tlb_cntr_pool[i].tlb_flush_cntr, 0);
#else
  vma_tlb_cntr_set->tlb_cntr_pool[i].tlb_flush_cntr = 0;
#endif
    list_add(&(vma_tlb_cntr_set->tlb_cntr_pool[i].list),&(vma_tlb_cntr_set->unused_tlb_cntrs.list));
  }

  return;
}

void free_prot_vm_area_tlb_cntr_set(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set) {
  vm_area_set_tlb_cntr_t *tmp;
  struct list_head *pos, *q;

  if(vma_tlb_cntr_set == NULL) {
    return;
  }

  down(&vma_tlb_cntr_set->sem);
  list_for_each_safe(pos, q, &vma_tlb_cntr_set->tlb_cntrs.list){
    tmp = list_entry(pos, vm_area_set_tlb_cntr_t, list);
    list_move(&(tmp->list), &(vma_tlb_cntr_set->unused_tlb_cntrs.list));
    vma_tlb_cntr_set->num_cntrs--;
  }
  up(&vma_tlb_cntr_set->sem);

  return;
}

vm_area_set_tlb_cntr_t * add_tlb_cntr_to_list(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set, struct vm_area_struct *vma) {
  vm_area_set_tlb_cntr_t *tmp;

  down(&vma_tlb_cntr_set->sem);
  vma_tlb_cntr_set->num_cntrs++;
  if(list_empty(&(vma_tlb_cntr_set->unused_tlb_cntrs.list))) {
    printk(KERN_ALERT "I ran out of tlb_cntrs when I tried to add one to the list\n");
  }
  BUG_ON(list_empty(&(vma_tlb_cntr_set->unused_tlb_cntrs.list)));
  tmp = list_entry(vma_tlb_cntr_set->unused_tlb_cntrs.list.prev, vm_area_set_tlb_cntr_t, list);

#ifdef ATOMIC_TLB_CNTR
  atomic64_set(&tmp->tlb_flush_cntr, 0);
#else
  tmp->tlb_flush_cntr = 0;
#endif
  
  tmp->vma = vma;
  tmp->active = 1;
  tmp->owner = vma->vm_mm->owner->pid;
  list_move(&(tmp->list), &(vma_tlb_cntr_set->tlb_cntrs.list));
  up(&vma_tlb_cntr_set->sem);

  return tmp;
}

void remove_tlb_cntr_from_list(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set, struct vm_area_struct *vma) {
  vm_area_set_tlb_cntr_t *tmp;
  struct list_head *pos, *q;

  down(&vma_tlb_cntr_set->sem);
  list_for_each_safe(pos, q, &vma_tlb_cntr_set->tlb_cntrs.list){
    tmp = list_entry(pos, vm_area_set_tlb_cntr_t, list);
    if(tmp->vma == vma) {
      list_move(&(tmp->list), &(vma_tlb_cntr_set->unused_tlb_cntrs.list));
      vma_tlb_cntr_set->num_cntrs--;
    }
  }
  up(&vma_tlb_cntr_set->sem);

  return;
}

vm_area_set_tlb_cntr_t *find_tlb_cntr_in_list(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set, struct vm_area_struct *vma) {
  vm_area_set_tlb_cntr_t *tmp, *vma_set_elem = NULL;
  struct list_head *pos, *q;

  down(&vma_tlb_cntr_set->sem);
  list_for_each_safe(pos, q, &vma_tlb_cntr_set->tlb_cntrs.list){
    tmp = list_entry(pos, vm_area_set_tlb_cntr_t, list);
    if(tmp->vma == vma && tmp->active == 1) {
      vma_set_elem = tmp;
    }
  }
  up(&vma_tlb_cntr_set->sem);

  return vma_set_elem;
}
