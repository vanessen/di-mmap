/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/mm.h>		/* everything */
#include <asm/pgtable.h>
#include <asm/tlbflush.h>

#include "helpers.h"
#include "rhel_compatibility.h"

pte_t *get_pte(struct mm_struct *mm, unsigned long address)
{
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *pte;

	pgd = pgd_offset(mm, address);
	if (!pgd_present(*pgd))
		return NULL;

	pud = pud_offset(pgd, address);
	if (!pud_present(*pud))
		return NULL;

	pmd = pmd_offset(pud, address);
	if (!pmd_present(*pmd))
		return NULL;

	pte = pte_offset_map(pmd, address);
	if (!pte_present(*pte)) {
		return NULL;
	}
	return pte;
}

/* Is a page mapped into its PTE */
int page_in_pte(struct vm_area_struct *vma, page_t *tagged_page) {
  unsigned long address = vma_address(tagged_page->page, vma);
  pte_t *ptep = get_pte(vma->vm_mm, address);
	  
  return (ptep != NULL && pte_present(*ptep));
}

/*
 * Check that @vma has a valid global page directory in @mm.
 *
 * On success returns 1, else returns zero
 */
static inline int vma_page_table_exists(struct vm_area_struct *vma, pid_t owner) {
  struct mm_struct *mm = vma->vm_mm;
  unsigned long address = vma->vm_start;
  pgd_t *pgd;

  pgd = pgd_offset(mm, address);
  if (pgd_present(*pgd)) {
    return 1;
  }else {
    return 0;
  }
}

/* 
 * Check to make sure that the VMA is valid, and belongs to an active process.
 * Return 0 if valid, and 1 if invalid
 */
static inline int vma_invalid(vm_area_set_t *vma_set_elem) {
  struct vm_area_struct *vma = vma_set_elem->vma;
  struct mm_struct *mm;
  pid_t owner = vma_set_elem->owner;
  BUG_ON(vma == NULL);

  if(vma_set_elem->active == 0) {
    /* This VMA entry has been closed by a process */
    return -1;
  }

  BUG_ON(vma->vm_mm == NULL);
  mm = vma->vm_mm;

  if(owner != mm->owner->pid) {
    /* This is a stale VMA entry, it was associated with another process */
    printk(KERN_INFO "BUG ON THIS owners are not correct %d and %d\n", owner, mm->owner->pid);
    return -1;
  }

  /* Finally, check if there is a valid page table setup */
  if(!vma_page_table_exists(vma, owner)) {
    /* Note that the OS will erase a page table before the VMA close callback function is executed
       and thus, this is condition may exist.  It is not an error and it is hard to detect
       the situation.  Mark the VMA as invalid.
    */
    return -1;
  }

  return 0;
}

/* Cleanup an old vma pointer */
inline void retire_vmap(vm_area_set_p *vmap_set_ptr, page_t *tagged_page) {
  if(vmap_set_ptr->vmap == NULL) {
    printk(KERN_ALERT "I don't think that I should be called here. tagged_page=%p\n",tagged_page);
  }
  if(vmap_set_ptr->vmap != NULL) {
    vmap_set_ptr->vmap->ref_count--;
    /* Do not check for a zero ref count here since the vma structure that is pointed at cannot be safely freed */
    vmap_set_ptr->vmap = NULL;
    vmap_set_ptr->tlb_flush_cntr_ss = -1;
  }
  return;
}

/* Cleanup an old vma pointer and reduce the page's count of active vmas */
inline void remove_vmap(vm_area_set_p *vmap_set_ptr, page_t *tagged_page) {
  if(vmap_set_ptr->vmap != NULL) {
    vmap_set_ptr->vmap->ref_count--;
    /* Do not check for a zero ref count here since the vma structure that is pointed at cannot be safely freed */
    vmap_set_ptr->vmap = NULL;
    vmap_set_ptr->tlb_flush_cntr_ss = -1;
  }
  tagged_page->num_vmas--;
  return;
}

/* For a set of VMAs is the page mapped into all PTEs for all processes */
int page_in_all_ptes(page_t *tagged_page) {
  vm_area_set_p *vmap_set_ptr;
  int in_all_ptes = 1, in_pte;

  if(tagged_page->num_vmas == 0) {
    in_all_ptes = -1;
  }else {
    int i;
    for(i = 0; i < tagged_page->num_vmas; i++) {
      vmap_set_ptr = &(tagged_page->vma_ptr_pool[i]);
      if(vmap_set_ptr->vmap != NULL) {
	if(vma_invalid(vmap_set_ptr->vmap)) {
	  /* There is not a valid page table, or this page is associated with an old (closed) vma / process,
	     remove the old mapping */
	  /* Note that this is not an error */
	  /* Cleanup the VMA set entry */
	  retire_vmap(vmap_set_ptr, tagged_page);
	  /* Put in a clever return code here if the list is now empty */
	  in_all_ptes = -2;
	}else {
	  /* Since there is a valid page table, check to see if the page is properly mapped into it */
	  in_pte = page_in_pte(vmap_set_ptr->vmap->vma, tagged_page);
	  if(!in_pte) {
	    in_all_ptes = in_pte;
	  }
	}
      }
    }
  }
  return in_all_ptes;
}

int page_in_vma(page_t *tagged_page, struct vm_area_struct *vma) {
  vm_area_set_p *vmap_set_ptr;
  int page_in_vma = 0, i;

  /* If there are active vmas, check to see if the page is already mapped into this specific vma */
  for(i = 0; i < tagged_page->num_vmas; i++) {
    vmap_set_ptr = &(tagged_page->vma_ptr_pool[i]);

    if(vmap_set_ptr->vmap != NULL) {
      if(vma_invalid(vmap_set_ptr->vmap)) {
	/* If the vma is invalid, remove it from the list */
	/* Move the VMA set entry to the unused list */
	retire_vmap(vmap_set_ptr, tagged_page);
      }else {
	if(vmap_set_ptr->vmap->vma == vma) {
	  page_in_vma = 1;
	}
      }
    }
  }

  return page_in_vma;
}

int page_is_writable(struct vm_area_struct *vma, page_t *tagged_page, unsigned long address) {
  pte_t *ptep = get_pte(vma->vm_mm, address);

  if((ptep != NULL && pte_present(*ptep) && pte_write(*ptep)) || tagged_page->is_writable) {
    if(DEBUG_LOGGING >= 1) {
      if(tagged_page->is_writable != 1) {
	printk("Why is the write count not equal to 1 %lx %u\n", address, tagged_page->is_writable);
      }
    }
    return 1;
  }else {
    if(DEBUG_LOGGING >= 1) {
      if(tagged_page->is_writable != 0) {
	printk("Why is the write count not equal to 0 %lx %u\n", address, tagged_page->is_writable);
      }
    }
    return 0;
  }
}

/*
 * Taken from rmap.c
 * At what user virtual address is page expected in vma?
 */
/*
	  printk(KERN_INFO "The VMA is screwed up and has no private data s=%lu f=%lu\n", vma->vm_start, vma->vm_end); 
	  if(vma->vm_private_data == NULL) {
	    printk(KERN_INFO "NULL: The VMA is screwed up and has no private data s=%lu f=%lu\n", vma->vm_start, vma->vm_end); 
	  }else {
	    printk(KERN_INFO "Is the address range zero %lu start %lu end %lu address %d valid vmas\n", vma->vm_start, vma->vm_end, address, ((struct scullp_dev *) vma->vm_private_data)->vmas);
	  }

*/
unsigned long
vma_address(struct page *page, struct vm_area_struct *vma)
{
	pgoff_t pgoff = page->index;
	unsigned long address;
	
	if(vma == NULL) {
	  printk("Why has vma_address received a null vma for page %p\n", page_address(page));
	  return 0xdeadbeef;
	}

	address = vma->vm_start + ((pgoff - vma->vm_pgoff) << PAGE_SHIFT);
	if (unlikely(address < vma->vm_start || address >= vma->vm_end)) {
		/* page should be within any vma from prio_tree_next */
		BUG_ON(!PageAnon(page));
		return -EFAULT;
	}
	return address;
}

unsigned long first_vma_address(struct page *page, vm_area_set_p *vmap_set_ptr) {
  return vma_address(page, vmap_set_ptr->vmap->vma);
}

void vma_addresses(page_t *tagged_page, char *str) {
  vm_area_set_p *vmap_set_ptr;
  char tmp_str[SHORT_STR_LEN] = "";
  int i;

  if(tagged_page->num_vmas == 0) {
    sprintf(str, "UNMAPPED");
    return;
  }

  for(i = 0; i < tagged_page->num_vmas; i++) {
    vmap_set_ptr = &(tagged_page->vma_ptr_pool[i]);
    if(vmap_set_ptr->vmap != NULL) {
      if(vmap_set_ptr->vmap->active == 1) {
	sprintf(tmp_str, "%lx", vma_address(tagged_page->page, vmap_set_ptr->vmap->vma));
      }else {
	sprintf(tmp_str, "INACTIVE");
      }
      strcat(str, tmp_str);
      if(i != tagged_page->num_vmas) {
	strcat(str, " ");
      }
    }
  }
  
  return;
}

void vm_address_regions(vm_area_set_t *vmas, char *str) {
  struct list_head *pos;
  vm_area_set_t *vma_set_elem;
  char tmp_str[SHORT_STR_LEN] = "", tlb_cnt[16] = "";
  int vm_count = 0;

  if(list_empty(&(vmas->list))) {
    sprintf(str, "UNMAPPED");
    return;
  }

  list_for_each(pos, &(vmas->list)) {
    vma_set_elem = list_entry(pos, vm_area_set_t, list);
    if(vm_count < 4) {
      if(vma_set_elem->active == 1) {
	if(vma_set_elem->global_cntr != NULL) {
#ifdef ATOMIC_TLB_CNTR
	  sprintf(tlb_cnt, " (f=%ld)", atomic64_read(&vma_set_elem->global_cntr->tlb_flush_cntr));
#else
	  sprintf(tlb_cnt, " (f=%ld)", vma_set_elem->global_cntr->tlb_flush_cntr);
#endif
	}
	sprintf(tmp_str, "%lx (c=%d)%s", vma_set_elem->vma->vm_start, vma_set_elem->ref_count, tlb_cnt);
      }else {
	sprintf(tmp_str, "INACTIVE (c=%d)", vma_set_elem->ref_count);
      }
      strcat(str, tmp_str);
      if(!list_is_last(pos, &(vmas->list))) {
	strcat(str, " ");
      }
      vm_count++;
    }else {
      strcat(str, "...");
      break;
    }
  }
  
  return;
}

/*
 * Taken from rmap.c
 * Check that @page is mapped at @address into @mm.
 *
 * On success returns with pte mapped and locked.
 */
pte_t *page_check_address(struct page *page, struct mm_struct *mm,
			  unsigned long address, spinlock_t **ptlp)
{
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *pte;
	spinlock_t *ptl;

	pgd = pgd_offset(mm, address);
	if (!pgd_present(*pgd))
		return NULL;

	pud = pud_offset(pgd, address);
	if (!pud_present(*pud))
		return NULL;

	pmd = pmd_offset(pud, address);
	if (!pmd_present(*pmd))
		return NULL;

	pte = pte_offset_map(pmd, address);
	/* Make a quick check before getting the lock */
	if (!pte_present(*pte)) {
		pte_unmap(pte);
		return NULL;
	}

	ptl = pte_lockptr(mm, pmd);
	spin_lock(ptl);
	if (pte_present(*pte) && page_to_pfn(page) == pte_pfn(*pte)) {
		*ptlp = ptl;
		return pte;
	}
	pte_unmap_unlock(pte, ptl);
	return NULL;
}

/**
 * page_remove_rmap - take down pte mapping from a page
 * @page: page to remove mapping from
 *
 * The caller needs to hold the pte lock.
 */
static void page_remove_rmap_private(struct page *page)
{
        if (atomic_add_negative(-1, &page->_mapcount)) {
#ifdef CONFIG_DEBUG_VM
                if (unlikely(page_mapcount(page) < 0)) {
                        printk (KERN_EMERG "Eeek! page_mapcount(page) went negative! (%d)\n", page_mapcount(page));
                        printk (KERN_EMERG "  page->flags = %lx\n", page->flags);
                        printk (KERN_EMERG "  page->count = %x\n", page_count(page));
                        printk (KERN_EMERG "  page->mapping = %p\n", page->mapping);
                }
#endif
                BUG_ON(page_mapcount(page) < 0);
                /*
                 * It would be tidy to reset the PageAnon mapping here,
                 * but that might overwrite a racing page_add_anon_rmap
                 * which increments mapcount after us but sets mapping
                 * before us: so leave the reset to free_hot_cold_page,
                 * and remember that it's only reliable while mapped.
                 * Leaving it set also helps swapoff to reinstate ptes
                 * faster for those pages still in swapcache.
                 */
#ifdef RHEL6
        /*
         * Now that the last pte has gone, s390 must transfer dirty
         * flag from storage key to struct page.  We can usually skip
         * this if the page is anon, so about to be freed; but perhaps
         * not if it's in swapcache - there might be another pte slot
         * containing the swap entry, but page not yet written to swap.
         */
        if ((!PageAnon(page) || PageSwapCache(page)) && page_test_dirty(page)) {
                page_clear_dirty(page);
                set_page_dirty(page);
	}
#else
                if (page_test_and_clear_dirty(page))
                        set_page_dirty(page);
#endif
                __dec_zone_page_state(page,
                                PageAnon(page) ? NR_ANON_PAGES : NR_FILE_MAPPED);
        }
}

/* 
 * Process the request to zap a single mmapped page 
 * Returns a 1 on error and 0 on success 
 */
/* 
 * Alternate methods
 * EXPORT_SYMBOL(flush_tlb_page);
 * pte = ptep_get_and_clear(mm, addr, ptep);
 * ptep_get_and_clear_full(mm, addr, pte,
 * ptep_get_and_clear((__vma)->vm_mm, __address, __ptep);
 * ptep_clear_flush(vma, address, pte);
*/
int zap_page_from_vma(struct vm_area_struct *vma, struct page *page) {
  struct mm_struct *mm = vma->vm_mm;
  unsigned long address;
  pte_t *pte;
  pte_t pteval;
  spinlock_t *ptl;

  //  printk("Thread %d is starting to zap page %p\n", current->pid, page_address(page));

  if(vma == NULL) {
    return 2;
  }
  address = vma_address(page, vma);
  if (address == -EFAULT)
    return address;
  
  /* If the pte returned is valid, the lock will also be held. */
  pte = page_check_address(page, mm, address, &ptl);
  if (!pte)
    return 1;


/*   printk("Thread %d is starting to zap page at address %lx\n", current->pid, address); */
  /* Use a single worker queue so that only one thread zaps the page ranges for each vma*/
#ifdef PTE_CLEAR_FLUSH
   pteval = ptep_clear_flush(vma, address, pte);
#else
   pteval = ptep_get_and_clear((vma)->vm_mm, address, pte);
#endif

   /* Move the dirty bit to the physical page now the pte is gone. */
   if (pte_dirty(pteval))
     set_page_dirty(page);

  /* Update high watermark before we lower rss */
  update_hiwater_rss(mm);

  /* Decrement the number of mapped pages */
  dec_mm_counter(mm, file_rss);

  page_remove_rmap_private(page);
  put_page(page); // Opposite of get_page(page) - used instead of page_cache_release(page), which is used by rmap.c

  //  printk("Thread %d is finished zapping page %lx\n", current->pid, address);

  /* If we have made it this far, unlock the PTE and return success */
  pte_unmap_unlock(pte, ptl);

  return 0;
}

int unmap_page(struct page *page) {
  put_page(page); // Opposite of get_page(page) - used instead of page_cache_release(page), which is used by rmap.c
  return 0;
}


/*
 * Modeled after zap_pte_range in memory.c
 */
static inline unsigned long scan_pte_range(struct vm_area_struct *vma, pmd_t *pmd,
					   unsigned long addr, unsigned long end, unsigned long *num_valid_pages)
{
  struct mm_struct *mm = vma->vm_mm;
  pte_t *pte;
  spinlock_t *ptl;

  pte = pte_offset_map_lock(mm, pmd, addr, &ptl);
  do {
    pte_t ptent = *pte;
    if (pte_none(ptent)) {
      continue;
    }

    if (pte_present(ptent)) {
      (*num_valid_pages)++;
      continue;
    }
  } while (pte++, addr += PAGE_SIZE, (addr != end));

  pte_unmap_unlock(pte - 1, ptl);

  return addr;
}

/*
 * Modeled after zap_pmd_range in memory.c
 */
static inline unsigned long scan_pmd_range(struct vm_area_struct *vma, pud_t *pud,
					   unsigned long addr, unsigned long end, unsigned long *num_valid_pages)
{
  pmd_t *pmd;
  unsigned long next;

  pmd = pmd_offset(pud, addr);
  do {
    next = pmd_addr_end(addr, end);
    if (!pmd_present(*pmd)) {
      continue;
    }
    next = scan_pte_range(vma, pmd, addr, next, num_valid_pages);
  } while (pmd++, addr = next, (addr != end));

  return addr;
}

/*
 * Modeled after zap_pud_range in memory.c
 */
static inline unsigned long scan_pud_range(struct vm_area_struct *vma, pgd_t *pgd,
					   unsigned long addr, unsigned long end, unsigned long *num_valid_pages)
{
  pud_t *pud;
  unsigned long next;

  pud = pud_offset(pgd, addr);
  do {
    next = pud_addr_end(addr, end);
    if (!pud_present(*pud)) {
      continue;
    }
    next = scan_pmd_range(vma, pud, addr, next, num_valid_pages);
  } while (pud++, addr = next, (addr != end));

  return addr;
}

/*
 * Modeled after unmap_page_range in memory.c
 */
static inline unsigned long scan_page_range(struct vm_area_struct *vma,
					    unsigned long addr, unsigned long end, unsigned long *num_valid_pages)
{
  pgd_t *pgd;
  unsigned long next;

  BUG_ON(addr >= end);
  pgd = pgd_offset(vma->vm_mm, addr);
  do {
    next = pgd_addr_end(addr, end);
    if (!pgd_present(*pgd)) {
      continue;
    }
    next = scan_pud_range(vma, pgd, addr, next, num_valid_pages);
  } while (pgd++, addr = next, (addr != end));

  return addr;
}

/**
 * Walk over the entire address range in a process to see if the entire page table is empty
 */
unsigned long scan_page_table(struct vm_area_struct *vma) {
  unsigned long last_addr;
  unsigned long num_valid_pages = 0;

  last_addr = scan_page_range(vma, vma->vm_start, vma->vm_end, &num_valid_pages);
  printk("Process %d finished scanning address range from %lx to %lx: last address %lx num valid pages=%ld\n", 
	 current->pid, vma->vm_start, vma->vm_end, last_addr, num_valid_pages);
  return last_addr;
}
