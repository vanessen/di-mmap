/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _MMAP_BANKED_BUFFER_H_
#define _MMAP_BANKED_BUFFER_H_

#include <linux/seq_file.h>

#include "mmap_generational_fifo_buffer.h"

#define NUM_BUFFER_BANKS 256 //2048 //1024 //256 //64

typedef struct banked_buffer_struct {
  fifo_buffer_t **banks;
  int num_banks;
  prot_vm_area_set_tlb_cntr_t active_vmas_tlb_cntrs; /* TLB counters for the set of VMAs that are mapped onto this buffer */
}banked_buffer_t;

typedef unsigned long bank_select_t;

/*******************************************************************************
 * Implement standard functions
 * Use the buffer typedef to differentiate the name of each function
 ******************************************************************************/

/* Setting up and tearing down buffers */
banked_buffer_t *init_mmap_buffer_data_banked_buffer_t(banked_buffer_t *dummy);
void cleanup_mmap_buffer_data_banked_buffer_t(banked_buffer_t *buf);

/* Manage parameters */
void reset_params_banked_buffer_t(banked_buffer_t *buf);
void init_params_banked_buffer_t(banked_buffer_t *buf, unsigned long perma_mmap_buf_size);
void cleanup_params_banked_buffer_t(banked_buffer_t *buf);

/* Actions when a device is mmaped or munmaped */
void vma_open_mmap_banked_buffer_t(banked_buffer_t *buf, struct vm_area_struct *vma, vm_area_set_tlb_cntr_t *vma_tlb_cntr);
void vma_close_mmap_banked_buffer_t(banked_buffer_t *buf, struct vm_area_struct *vma);

/* Handle adding, deleting, etc pages from the buffer */
//void acquire_lock_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page);
void release_page_lock_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page);
page_t *find_page_banked_buffer_t(banked_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id);
page_t *find_and_lock_page_banked_buffer_t(banked_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id);
long pre_allocate_entry_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page);
int is_true_page_fault_banked_buffer_t(banked_buffer_t *buf, long buffer_entry, page_t *tagged_page, long perma_mmap_buf_size);
void rmap_page_to_vma_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page, struct vm_area_struct *vma);
int insert_page_banked_buffer_t(banked_buffer_t *buf, unsigned long buffer_entry, page_t *tagged_page);
int page_made_writable_banked_buffer_t(banked_buffer_t *buf, struct vm_area_struct *vma, page_t *tagged_page);
int recover_page_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page);
void inc_num_page_faults_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page);

/* Query state of page in buffer */
int page_in_buffer_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page);
int page_being_evicted_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page);

/* Reset or tune the buffer state */
void reset_state_banked_buffer_t(banked_buffer_t *buf);
void flush_buffers_banked_buffer_t(banked_buffer_t *buf);
void sync_buffers_banked_buffer_t(banked_buffer_t *buf);
int tune_specifics_banked_buffer_t(banked_buffer_t *buf, char *cmd, int param1, int param2, int param3, int param4);
void init_specific_counters_banked_buffer_t(banked_buffer_t *buf);

/* Display buffer specific results */
void prettyprint_specifics_banked_buffer_t(banked_buffer_t *buf, struct seq_file *s, char *msg, char line_leader);
void explain_tuning_params_banked_buffer_t(banked_buffer_t *buf);
void display_mmap_buffer_state_banked_buffer_t(banked_buffer_t *buf, char *str);

void scan_buffer_banked_buffer_t(banked_buffer_t *buf, void (*fn)(void *, page_t *), void *data);

#endif /* _MMAP_BANKED_BUFFER_H_ */
