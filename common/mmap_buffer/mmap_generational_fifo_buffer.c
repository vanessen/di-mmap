/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <asm/tlbflush.h>
#include <linux/pagemap.h>

#include "mmap_fault_handler.h"
#include "mmap_generational_fifo_buffer.h"
#include "indexed_fifo_non_atomic.h"
#include "mmap_buffer_interface.h"
#include "mmap_buffer_hash_table.h"

#define SANITY_CHECK_FIFO

static atomic64_t num_write_eviction_races;

struct indexed_fifo_operations_struct *fifo_ops = &fifo_operations;

#define NUM_EVICTION_BUFFERS 1
#define EVICTION_BUFFER_DAEMON_SLEEP 30
#define USE_VQ_WORKER
/*
 * Internal prototypes
 */
static int fifo_entry_is_hot(indexed_fifo_t *lower_fifo_data, page_t *tagged_page, indexed_fifo_t *upper_fifo_data);
//int page_being_evicted(fifo_buffer_t *buf, page_t *tagged_page);

/*
 * Internal functions
 */

/*
 * Some potential code for locking down a page when doing writeback 
 *
 * unlock_page(page)
 * wait_on_page_locked(page)
 * if (PageUptodate(page))
 * lock_page(page)
 */
static inline int clear_fifo_entry(page_t *tagged_page_to_clear) {
  int err = 0, pte_state;
  //      printk(KERN_INFO "Thread %d is going to zap a page %p at location %lu:%lu (tail %lu:%lu)\n", current->pid, tagged_page_to_clear, curr_buf_head + 2 * MMAP_BUF_CLEAR_AHEAD_PATH, idx, atomic64_read(&mmap_buf_tail), atomic64_read(&mmap_buf_tail) % perma_mmap_buf_size);
  if(tagged_page_to_clear != NULL) {
    /*
     * Sanity check that the page is in the buffer and that it is not locked
     */
    pte_state = page_in_all_ptes(tagged_page_to_clear);
    if(!pte_state) {
      char str[MAX_STR_LEN]="";
      char str2[SHORT_STR_LEN]="";
      char addresses[MAX_STR_LEN] = "";
      //	display_fifo_pointers(str); /* FIXME - BVE figure out what to do with this error string */
      display_tagged_page_meta_data(tagged_page_to_clear, str2);
      vma_addresses(tagged_page_to_clear, addresses);
      printk("Why is this page (%s) that I am going to evict, not in the proper page table entry.  It was last at %s and the pointers are %s.  The pte_state is %d\n", addresses, str2, str, pte_state);
      err = 1;
    }
    
    __lock_page(tagged_page_to_clear->page);

/*     if (PageLocked(tagged_page_to_clear->page)) { */
/*       char str[MAX_STR_LEN]="", str2[SHORT_STR_LEN]=""; */
/*       char addresses[MAX_STR_LEN] = ""; */
/*       display_fifo_pointers(str); */
/*       display_tagged_page_meta_data(tagged_page_to_clear, str2); */
/*       vma_addresses(tagged_page_to_clear, addresses); */
/*       printk("Why is this page (%s) that I am going to evict, locked.  It was last at %s and the pointers are %s\n", addresses, str2, str); */
/*       err = 1; */
/*     } */

    unmap_tagged_page(tagged_page_to_clear, &num_write_eviction_races);

    unlock_page(tagged_page_to_clear->page);
  }

  return err;
}

/*
 * Flush the TLBs and clear the counters on pages in a victim buffer
 */
static inline long flush_pages_from_victim_buffer(indexed_fifo_t *fifo_data, prot_vm_area_set_t *active_vmas, buffer_map_t *page_map, long page_flush_limit) {
  long num_entries_to_clear;
/*   char str[256] = ""; */
  //  volatile unsigned long long ini_tlb = 0, end_tlb = 0, ini_flush = 0, end_flush = 0;
  long i;
  long num_pages_flushed = 0;
/*  vm_area_set_t *tmp;
 *  struct list_head *pos;
 */

  if(fifo_data->buffer != NULL) {
    //    if(mutex_trylock(&fifo_data->fifo_mutex)) {
    mutex_lock(&fifo_data->fifo_mutex);
	num_entries_to_clear = fifo_ops->num_valid_entries(fifo_data);

	/* Check to make sure that the entry at the tail pointer is not NULL and that there are a sufficient number of
	   entries to clear */
      if(fifo_ops->peek_tail(fifo_data) != NULL && num_entries_to_clear >= page_flush_limit) {
#ifndef PTE_CLEAR_FLUSH
	//rdtscll(ini_tlb); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
	/* Once a swath of the FIFO buffer has been cleared, flush all of the TLB entries */
	/* flush_tlb_mm clears all of the TLBs that are currently executing a specific address space that is defined by the
	   struct mm_struct and pointed to by the vm_mm */
	/* Grab the device lock so that something doesn't try to close a VMA while it is being flushed */
/* BVE FIXME - Note that this is probably not correct for multiple devices */
#if 0
	down(&active_vmas->sem);
	//	  flush_tlb_all(); /* Maybe I should just get them all at the same time. */

	if(!list_empty(&active_vmas->vmas->list)) {

	  list_for_each(pos, &active_vmas->vmas->list) {
	    tmp = list_entry(pos, vm_area_set_t, list);
	    if(tmp->active) {
	      //	      tmp->global_cntr->tlb_flush_cntr++;
	      //	      printk(KERN_ALERT "I am flushing the TLB for pid =%d vma %p with counter = %ld\n", tmp->owner, tmp->vma, tmp->global_cntr->tlb_flush_cntr);
	      flush_tlb_mm(tmp->vma->vm_mm);
	    }
	  }
	}
        up(&active_vmas->sem);
#endif
	//	rdtscll(end_tlb); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
#endif

	/* Advance the tail pointer up to the head, clearing each entry and releasing the tagged page lock */
	//rdtscll(ini_flush); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
	for(i = 0; i < num_entries_to_clear; i++) {
	  page_t *evicted_page = fifo_ops->peek_tail(fifo_data);
	  if(evicted_page != NULL) {
	    evicted_page = fifo_ops->pop(fifo_data);
	    /* Eventually this will be the place to do the actual data writeback. */
	    // writeback data
	    if(evicted_page->buffer_id == fifo_data->id) { /* The page is still in the victim queue and has not been recovered */
	      /* Make sure that the TLB for each VMA has been flushed since the page was put in the victim queue */
	      clear_tagged_pages_pending_vmas(evicted_page);

	      /* Clear the page markings */
	      mark_tagged_page_in_buffer(evicted_page, 0, 0);
	      evicted_page->page_valid = 0;
	      if (PageDirty(evicted_page->page)) {
		void *device = evicted_page->dev;
	      	write_page(/* NULL, */ evicted_page->filp, device, (evicted_page->page_offset << PAGE_SHIFT), &evicted_page->page);
	      }
	      flush_page(evicted_page->filp, evicted_page->dev, (evicted_page->page_offset << PAGE_SHIFT), &evicted_page->page);
	      evicted_page->dev = NULL;
	      evicted_page->filp = NULL;
	      remove_page_from_htable(page_map, evicted_page);
	    }
	    /* 
	     * if(evicted_page->buffer_id != fifo_data->id) {}
	     *
	     * If the page's buffer id does not match the fifo's buffer id, then the page has been recovered.
	     * Just remove the page entry from the queue without doing anything to it.
	     */
	  }else {
	    /* If a NULL is encountered then that slot is waiting to be filled, terminate flushing the victim queue */
	    break;
	  }
	}

	//rdtscll(end_flush); /* Don't use the rdtsc_barrier() since it seems to mess with the page fault handling */
	num_pages_flushed = i;
	/*     if(warn /\* i == 0 || i < 64 *\/) { */
	/*       fifo_ops->display_pointers(fifo_data, str); */
	/*       printk("Finished clearing %ld entries from victim FIFO (%ld requested) cycles for tlb=%llu flush=%llu: %s\n", i, num_entries_to_clear, end_tlb - ini_tlb, end_flush- ini_flush, str); */
	/*     } */
/*       } */
    }
      mutex_unlock(&fifo_data->fifo_mutex);
  }
  return num_pages_flushed;
}

/*
 * Daemon for workqueue that will continually clean a victim buffer
 */
static inline void victim_buffer_cleanup_daemon(struct mutex *buffer_lock, indexed_fifo_t *fifo_data, prot_vm_area_set_t *active_vmas, buffer_map_t *page_map) {
  int page_flush_limit = (int) (fifo_data->allocated_size >> 1);
  int num_idle_loops = 0;
  int warn_about_unready_buffer = 1;

  printk("I am starting a daemon for the fifo %d\n", fifo_data->id);
  while(fifo_data->active) {
    if(fifo_data->buffer == NULL || fifo_data->head == NULL || fifo_data->tail == NULL || fifo_data->id == 0 || fifo_data->allocated_size == 0) {
      /* If the buffer is NULL just go to sleep for a while and then check back later */
      if(warn_about_unready_buffer) {
	printk("Daemon for victim buffer %lx is idle since some meta-data is not valid: id=%d\n", (unsigned long) fifo_data, fifo_data->id);
	warn_about_unready_buffer = 0;
      }
      set_current_state(TASK_INTERRUPTIBLE);
      schedule_timeout(EVICTION_BUFFER_DAEMON_SLEEP);
    }else {
      long num_pages_flushed = 0;
      mutex_lock(buffer_lock);
      num_pages_flushed = flush_pages_from_victim_buffer(fifo_data, active_vmas, page_map, page_flush_limit);
      if(num_pages_flushed == -1) {
	mutex_unlock(buffer_lock);
	return;
      }
      
      if(num_pages_flushed == 0) {
	/* Reduce the page flush limit if a page hasn't been flushed in 30ms, so that progress is made */
	if(num_idle_loops >= 30 && page_flush_limit > 1) {
	  page_flush_limit = page_flush_limit >> 1;
/* 	  printk("I have waited for %d loops and am reducing the page flush limit to %d for the fifo %d\n", num_idle_loops, page_flush_limit, fifo_data->id); */
	  num_idle_loops = 0;
	}else {
	  num_idle_loops++;
	}
      }else {
	page_flush_limit = (int) (fifo_data->allocated_size >> 1);
	num_idle_loops = 0;
/* 	printk("I am reseting the page flush limit to %d for the fifo %d\n", page_flush_limit, fifo_data->id); */

	/* Check for VMAs that are not references and need to be removed from the buffer's active list. */
	remove_unused_vma_from_list(active_vmas);
      }

      if(!warn_about_unready_buffer) {
	/* Reset the flag to warn about inactive buffers if the buffer had been active */
	warn_about_unready_buffer = 1;
      }
      mutex_unlock(buffer_lock);
      set_current_state(TASK_INTERRUPTIBLE);
      schedule_timeout(EVICTION_BUFFER_DAEMON_SLEEP);
    }
  }
  printk("I am stopping a daemon for the fifo %d\n", fifo_data->id);
  return;
}

/* Move entries from buffers into a victim queue */
/* Note that this is not designed to compete with other threads / processing adding data to the buffer concurrently */
static long drain_buffer(indexed_fifo_t *fifo_data, indexed_fifo_t *victim_fifo_data, prot_vm_area_set_t *active_vmas, buffer_map_t *page_map) {
  long num_entries_to_flush, i, num_entries_flushed = 0;
  char str0[SHORT_STR_LEN]="", str1[SHORT_STR_LEN]="";
  page_t *drained_page;
  fidx_t victim_index;

  if(fifo_data->buffer != NULL) {
    if(fifo_data->mode == CIRCULAR_BUFFER) {
      num_entries_to_flush = fifo_data->allocated_size; 
    }else { /* fifo_data->mode == FIFO || fifo_data->mode == WINDOWED_FIFO */
      num_entries_to_flush = fifo_ops->num_valid_entries(fifo_data);
    }
  
    fifo_ops->display_pointers(fifo_data,str0);

    /* Flush all valid entries */
    for(i = 0; i < num_entries_to_flush; i++) {
      int err = 0;
      if(fifo_data->mode == CIRCULAR_BUFFER) {
	/* Starting at the single index of the fifo, flush all */
	/* Allocate a spot in mmap's internal buffer */
	long index = fifo_ops->allocate_entry(fifo_data);    
	drained_page = fifo_ops->poke(fifo_data, index);
      }else { /* fifo_data->mode == FIFO || fifo_data->mode == WINDOWED_FIFO */
	drained_page = fifo_ops->pop(fifo_data);
      }

      if(drained_page != NULL) {
	err = clear_fifo_entry(drained_page);
	
	if(!err) {
	  num_entries_flushed++;
	}

	victim_index = fifo_ops->allocate_entry(victim_fifo_data);

	if(fifo_ops->fifo_full(victim_fifo_data, 0)) {
	  flush_pages_from_victim_buffer(victim_fifo_data, active_vmas, page_map, fifo_ops->num_valid_entries(victim_fifo_data));
	}

	//	drained_page->tlb_flush_count = atomic64_read(&buf->tlb_flush_count);

	err = fifo_ops->insert(victim_fifo_data, drained_page, victim_index);
	if(err) {
	  char addresses[MAX_STR_LEN] = "";
	  vma_addresses(drained_page, addresses);
	  printk("I was unable to put page %s in the victim queue at location %lx: FIFO\n",
		 addresses, victim_index);

	}
	mark_tagged_page_in_buffer(drained_page, victim_index, victim_fifo_data->id);

      }
    }

    fifo_ops->display_pointers(fifo_data,str1);

    printk("Drained %ld of (%ld) entries from buffer %d : %s -> %s into buffer %d\n", num_entries_flushed, num_entries_to_flush, fifo_data->id, str0, str1, victim_fifo_data->id);

  }

  return num_entries_flushed;
}

/* Sync dirty entries in a buffer */
/* Note that this is not designed to compete with other threads / processing adding data to the buffer concurrently */
static long sync_buffer(indexed_fifo_t *fifo_data) {
  long num_entries_to_sync, i, num_entries_synced = 0;
  //  char str0[SHORT_STR_LEN]="", str1[SHORT_STR_LEN]="";

  if(fifo_data->buffer != NULL) {
    if(fifo_data->mode == CIRCULAR_BUFFER) {
      num_entries_to_sync = fifo_data->allocated_size; 
    }else { /* fifo_data->mode == FIFO || fifo_data->mode == WINDOWED_FIFO */
      num_entries_to_sync = fifo_ops->num_valid_entries(fifo_data);
    }
  
    //    fifo_ops->display_pointers(fifo_data,str0);

    /* Flush all valid entries */
    for(i = 0; i < num_entries_to_sync; i++) {
      page_t *tagged_page = fifo_ops->peek(fifo_data, *(fifo_data->head) + i);

      if(tagged_page != NULL) {
	retire_inactive_vm_area_pointers(tagged_page); /* Cleanup any inactive vmas */

	if(PageDirty(tagged_page->page) || page_test_dirty(page)) { /* I don't know if the second check is necessary */
	  write_page(tagged_page->filp, tagged_page->dev, (tagged_page->page_offset << PAGE_SHIFT), &tagged_page->page);
	  num_entries_synced++;
	}
      }
    }

    //    fifo_ops->display_pointers(fifo_data,str1);
    //    printk("Sync'ed %ld of (%ld) entries from buffer %d : %s -> %s\n", num_entries_synced, num_entries_to_sync, fifo_data->id, str0, str1);
  }

  return num_entries_synced;
}

/*
 * WARNING: this function needs the buffer lock
 */
void flush_buffers_fifo_buffer_t(fifo_buffer_t *buf) {
  int j;
  prot_vm_area_set_t *active_vmas = &(buf->active_vmas);
  buffer_map_t *page_map = &(buf->page_map);
  long num_entries_flushed;

  //  printk(KERN_ERR "About to drain the primary buffer\n");
  num_entries_flushed = drain_buffer(buf->primary_fifo_data, &buf->victim_fifo_buffers[0], active_vmas, page_map);
  if(num_entries_flushed != 0){
    printk("Flushed %ld entries from buffer %d\n", num_entries_flushed, buf->primary_fifo_data->id);
  }
  //  printk(KERN_ERR "About to drain the hpf buffer\n");
  num_entries_flushed = drain_buffer(buf->hpf_data, &buf->victim_fifo_buffers[0], active_vmas, page_map);
  if(num_entries_flushed != 0){
    printk("Flushed %ld entries from buffer %d\n", num_entries_flushed, buf->hpf_data->id);
  }

  for(j = 0; j < NUM_EVICTION_BUFFERS; j++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[j];
    //printk(KERN_ERR "About to drain the victim buffer\n");
    num_entries_flushed = flush_pages_from_victim_buffer(victim_fifo_data, active_vmas, page_map, fifo_ops->num_valid_entries(victim_fifo_data));
    if(num_entries_flushed != 0){
      printk("Flushed %ld entries from buffer %d\n", num_entries_flushed, victim_fifo_data->id);
    }
  }

  /* Check for VMAs that are not references and need to be removedfrom the buffer's active list. */
  remove_unused_vma_from_list(&buf->active_vmas);
}

/*
 * WARNING: this function needs the buffer lock
 */
void sync_buffers_fifo_buffer_t(fifo_buffer_t *buf) {
  int j;
  long num_entries_synced;

  //  printk(KERN_ERR "About to sync the primary buffer - ");
  num_entries_synced = sync_buffer(buf->primary_fifo_data);
  if(num_entries_synced != 0){
    printk("Sync'ed %ld entries from buffer %d\n", num_entries_synced, buf->primary_fifo_data->id);
  }
  //  printk(KERN_ERR "About to sync the hpf buffer - ");
  num_entries_synced = sync_buffer(buf->hpf_data);
  if(num_entries_synced != 0){
    printk("Sync'ed %ld entries from buffer %d\n", num_entries_synced, buf->hpf_data->id);
  }

  for(j = 0; j < NUM_EVICTION_BUFFERS; j++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[j];
    //    printk(KERN_ERR "About to sync the victim buffer - ");
    num_entries_synced = sync_buffer(victim_fifo_data);
    if(num_entries_synced != 0){
      printk("Sync'ed %ld entries from buffer %d\n", num_entries_synced, victim_fifo_data->id);
    }
  }
}

static void cleanup_mmap_buffer(fifo_buffer_t *buf, indexed_fifo_t *fifo_data, int unmap_entries) {
  long i;
  int j;
  char str[256] = "", vq_str[MAX_STR_LEN] = "", tmp_str[SHORT_STR_LEN];

  /* Cleanup a buffer */
  if(fifo_data->buffer != NULL) {

    if(unmap_entries) {
      /* Unmap any remaining entries */
      for(i = 0; i < fifo_data->allocated_size; i++) {
	clear_fifo_entry(fifo_ops->poke(fifo_data, i));
      }
      sprintf(str, " freed %ld entries from buffer %d -", fifo_data->allocated_size, fifo_data->id);
    }

    fifo_ops->free(fifo_data);
  }

  /* FIXME - BVE Move this type of string into the indexed fifo data structure and compute it when it is built */
  for(j = 0; j < NUM_EVICTION_BUFFERS; j++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[j];
    sprintf(tmp_str, " vq%d=%ld@%p", j, victim_fifo_data->allocated_size, victim_fifo_data->buffer);    
    strcat(vq_str, tmp_str);
  }

  printk(KERN_INFO "PerMA:%s buffer status primary=%ld@%p hpf=%ld@%p%s\n", str, buf->primary_fifo_data->allocated_size, buf->primary_fifo_data->buffer, buf->hpf_data->allocated_size, buf->hpf_data->buffer, vq_str);
  return;
}

static int init_mmap_buffer(indexed_fifo_t *fifo_data, long size, int buffer_id, enum fifo_mode_t buffer_mode) {
  int err = fifo_ops->init(fifo_data, size, buffer_id, buffer_mode);
  if(err) {
    return -ENOMEM;
  }
  return 0;
}

void display_mmap_buffer_state_fifo_buffer_t(fifo_buffer_t *buf, char *str) {
  char str0[SHORT_STR_LEN]="", str1[SHORT_STR_LEN]="", str2[SHORT_STR_LEN]="";
  int i;

  fifo_ops->display_pointers(buf->primary_fifo_data, str0);
  strcat(str, str0);
  strcat(str, " : ");
  fifo_ops->display_pointers(buf->hpf_data, str1);
  strcat(str, str1);
  strcat(str, " : ");
  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[i];
    fifo_ops->display_pointers(victim_fifo_data, str2);
    strcat(str, str2);
    if(i != NUM_EVICTION_BUFFERS-1) {
      strcat(str, " ");
    }
  }
}

void scan_buffer_fifo_buffer_t(fifo_buffer_t *buf, void (*fn)(void *, page_t *), void *data) {
  scan_hash_table(&buf->page_map, fn, data);
  return;
}

/* Use the workqueue to clear all of the TLBs */
#ifdef RHEL6
static void workqueue_clear_tlbs(struct work_struct *work) {
  /* Extract the structure that contains the work_struct field */
  struct fifo_workqueue_request_t *data = container_of(work, struct fifo_workqueue_request_t, q_work);
#else
static void workqueue_clear_tlbs(void *ptr) {
  struct fifo_workqueue_request_t *data = (struct fifo_workqueue_request_t *) ptr;
#endif
  //  struct fifo_zap_request_t *data = (struct fifo_zap_request_t *) ptr;
  prot_vm_area_set_t *active_vmas = data->active_vmas;
  buffer_map_t *page_map = data->page_map;
  indexed_fifo_t *fifo_data = (indexed_fifo_t *) data->fifo_data;
  struct mutex *buffer_lock = data->buffer_lock;

  victim_buffer_cleanup_daemon(buffer_lock, fifo_data, active_vmas, page_map);

  return;
}

#ifndef RHEL6
/* Hack to implement the work_pending function that does not exist in RHEL5 kernels */
static int work_pending(struct work_struct *work_request) {
  return test_bit(0, &(work_request->pending));
}
#endif

/* Issue a request to clear the victim fifo */
static void issue_workqueue_request(struct mutex *buffer_lock, indexed_fifo_t *victim_fifo_data, long num_entries_to_clear, fidx_t victim_index, int warn) {
  int req_zap_page_range = 0;
  int ret;
  int request_number;

  request_number = victim_index % ZAP_REQUEST_POOL;
  victim_fifo_data->fifo_workqueue_request_data[request_number].entries_to_clear = num_entries_to_clear;
  victim_fifo_data->fifo_workqueue_request_data[request_number].fifo_data = victim_fifo_data;
  victim_fifo_data->fifo_workqueue_request_data[request_number].buffer_lock = buffer_lock;
#ifdef RHEL6
  if(!work_pending(&victim_fifo_data->fifo_workqueue_request_data[request_number].q_work)) {
    ret = queue_work(victim_fifo_data->fifo_workqueue, &victim_fifo_data->fifo_workqueue_request_data[request_number].q_work);
#else
  if(!work_pending(&victim_fifo_data->fifo_workqueue_work_request[request_number])) {
    ret = queue_work(victim_fifo_data->fifo_workqueue, &victim_fifo_data->zap_pages_request[request_number]);
#endif
    req_zap_page_range = 1;
/*     if(warn) { */
/*       printk("Queueing a given request index %ld for %d entries -> req %d\n", victim_index, num_entries_to_clear, request_number); */
/*     } */
    if(ret == 0) {
/*       printk(KERN_INFO "Unable to queue a given request index %ld for %ld entries -> req %d\n", victim_index, num_entries_to_clear, request_number); */
      atomic64_inc(&skipped_zap_page_range);
    }
/*   }else { */
/*     if(warn) { */
/*       printk("Unable to queue a given request because it is pending index %ld for %d entries -> req %d\n", victim_index, num_entries_to_clear, request_number); */
/*     } */
  }
  return;
}

static indexed_fifo_t *get_fifo_holding_page(fifo_buffer_t *buf, page_t *tagged_page) {
  indexed_fifo_t *fifo_data;
  switch(tagged_page->buffer_id) {
  case 1: {
    fifo_data = buf->primary_fifo_data;
    break;
  }
  case 2: {
    fifo_data = buf->hpf_data;
    break;
  }
  case -1: {
    fifo_data = &buf->victim_fifo_buffers[0];
    break;
  }
  default: {
    printk(KERN_ALERT "git_fifo_holding_page did not find a valid buffer\n");
    BUG_ON(true);
  }
  }

  return fifo_data;
}

/*******************************************************************************
 * External functions
 ******************************************************************************/

void reset_params_fifo_buffer_t(fifo_buffer_t *buf) {
  buf->primary_fifo_data->clear_ahead_window = MMAP_BUF_CLEAR_AHEAD_DEFAULT_WINDOW;
  buf->victim_fifo_buffer_size = MMAP_EVICTION_BUFFER_DEFAULT_SIZE;
  buf->hot_page_fifo_buffer_size = 0; //1024;

  return;
}

/* FIXME - BVE change the use of perma_mmap_buf_size */
void init_params_fifo_buffer_t(fifo_buffer_t *buf, unsigned long perma_mmap_buf_size) {
  int i;
  long num_free_pages = 0;
  long num_primary_pages = (perma_mmap_buf_size)/*  - buf->hot_page_fifo_buffer_size - buf->victim_fifo_buffer_size) */;

  if(buf->primary_fifo_data->allocated_size != num_primary_pages) {
    /* The buffer size has changed, clear it out and free the memory */
    cleanup_mmap_buffer(buf, buf->primary_fifo_data, 1);
  }
  if(buf->hpf_data->allocated_size != buf->hot_page_fifo_buffer_size) {
    /* The buffer size has changed, clear it out and free the memory */
    cleanup_mmap_buffer(buf, buf->hpf_data, 1);
  }
  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[i];
    if(victim_fifo_data->allocated_size != buf->victim_fifo_buffer_size) {
      /* The buffer size has changed, clear it out and free the memory */
      cleanup_mmap_buffer(buf, victim_fifo_data, 1);
    }
  }

  /* Allocate a buffer if the size is none zero and there isn't an existing buffer */
  if(perma_mmap_buf_size != 0 && buf->primary_fifo_data->buffer == NULL) {
    int err = init_mmap_buffer(buf->primary_fifo_data, num_primary_pages, 1, CIRCULAR_BUFFER);
    if(err) {
      perma_mmap_buf_size = 0; /* FIXME BVE - minor bug about flush frequency and dirty threshold can be stale */
    }
  }
  num_free_pages += num_primary_pages;

  if(buf->hot_page_fifo_buffer_size != 0 && buf->hpf_data->buffer == NULL) {
    int err = init_mmap_buffer(buf->hpf_data, buf->hot_page_fifo_buffer_size, 2, CIRCULAR_BUFFER);
    if(err) {
      buf->hot_page_fifo_buffer_size = 0; /* FIXME BVE - minor bug about flush frequency and dirty threshold can be stale */
    }
  }
  num_free_pages += buf->hot_page_fifo_buffer_size;

  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[i];
    if(buf->victim_fifo_buffer_size != 0 && victim_fifo_data->buffer == NULL) {
      int err = init_mmap_buffer(victim_fifo_data, buf->victim_fifo_buffer_size, -1-i, FIFO);
      if(err) {
	buf->victim_fifo_buffer_size = 0; /* FIXME BVE - minor bug about flush frequency and dirty threshold can be stale */
      }
    }
    num_free_pages += buf->victim_fifo_buffer_size;
  }

  init_hash_table_free_pages(&buf->page_map, num_free_pages+1);
}

void cleanup_params_fifo_buffer_t(fifo_buffer_t *buf) {
  flush_buffers_fifo_buffer_t(buf);
  cleanup_mmap_buffer(buf, buf->primary_fifo_data, 0);
  cleanup_mmap_buffer(buf, buf->hpf_data, 0);
  //  cleanup_mmap_buffer(victim_fifo_data, 0);
  return;
}

void vma_open_mmap_fifo_buffer_t(fifo_buffer_t *buf, struct vm_area_struct *vma, vm_area_set_tlb_cntr_t *vma_tlb_cntr) {
  int i, j;

  if(fifo_ops->fifo_valid(buf->primary_fifo_data)) {
    printk(KERN_INFO "Encountered an error with the fifo's pointers when opening the mmaped file\n");
  }

  add_vma_to_list(&buf->active_vmas, vma, vma_tlb_cntr);

#ifdef USE_VQ_WORKER
  /* Setup the workqueue to zap page ranges */
  for(i = 0; i < ZAP_REQUEST_POOL; i++) {
    for(j = 0; j < NUM_EVICTION_BUFFERS; j++) {
      indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[j];
      if(!victim_fifo_data->active) {
	victim_fifo_data->fifo_workqueue_request_data[i].active_vmas = &buf->active_vmas;
	victim_fifo_data->fifo_workqueue_request_data[i].page_map = &buf->page_map;
      }
    }
    for(j = 0; j < NUM_EVICTION_BUFFERS; j++) {
      indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[j];
      if(!victim_fifo_data->active) {
#ifdef RHEL6
	INIT_WORK(&victim_fifo_data->fifo_workqueue_request_data[i].q_work, &workqueue_clear_tlbs);
#else
	INIT_WORK(&victim_fifo_data->fifo_workqueue_work_request[i], &workqueue_clear_tlbs, &victim_fifo_data->fifo_workqueue_request_data[i]);
#endif
      }
    }
  }

  for(j = 0; j < NUM_EVICTION_BUFFERS; j++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[j];
    if(!victim_fifo_data->active) {
      victim_fifo_data->active = 1;
      issue_workqueue_request(&buf->buffer_lock, victim_fifo_data, 0, 0, 0);
    }
  }
#endif
  return;
}

void vma_close_mmap_fifo_buffer_t(fifo_buffer_t *buf, struct vm_area_struct *vma) {
  int vma_unused;
  /* 
   * Note that there is not too much specifically needed for closing the vma on the buffers.
   * Each page is tagged with the VMAs that it was attached to and their process ID, so
   * when they are remapped, they stale VMAs will be lazily cleaned up as the pages are 
   * accessed.
   */

  /* Remove the VMA from the buffer's active list.  Then let each page notice that the vma is no longer active
     and lazily clean itself up. */
  vma_unused = inactivate_vma_in_list(&buf->active_vmas, vma);
  if(vma_unused) {
    remove_vma_from_list(&buf->active_vmas, vma);
  }

  /* Writeback dirty pages to the device so that the buffer is clean */
  sync_buffers_fifo_buffer_t(buf);
  /* Cleanup any remaining vmas that are unused */
  remove_unused_vma_from_list(&buf->active_vmas);

  if(fifo_ops->fifo_valid(buf->primary_fifo_data)) {
    printk(KERN_INFO "Encountered an error with the fifo's pointers when closing the mmaped file\n");
  }
}

/* Associate the VMA with the tagged page */
void rmap_page_to_vma_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page, struct vm_area_struct *vma) {
  vm_area_set_p *tmp_vmap_set = NULL, *vma_set_ptr = NULL;
  vm_area_set_t *buf_vma_set;
  int i;

  /* Take an unused VMA set entry, add it to the head of the list and save the VMA */
  if(!page_in_vma(tagged_page, vma)) {
    if(tagged_page->num_vmas < MAX_VMAS_PER_PAGE) {
      buf_vma_set = find_vma_in_list(&buf->active_vmas, vma);
      if(buf_vma_set == NULL) {
	printk("BUGON here, I couldn't find the vma for the buffer\n");
      }
      
      /* Before allocating a new vma pointer check to see if there are any that can be reclained. */
      for(i = 0; i < tagged_page->num_vmas; i++) {
	vma_set_ptr = &(tagged_page->vma_ptr_pool[i]);

	if(vma_set_ptr->vmap == NULL) {
	  break;
	}else if(vma_set_ptr->vmap != NULL && vma_set_ptr->vmap->active == 0) {
	  retire_vmap(vma_set_ptr, tagged_page);
	  break;
	}else {
	  vma_set_ptr = NULL;
	}
      }

      /* If there is an unused pointer early in the list, reuse it */
      if(vma_set_ptr != NULL) {
	tmp_vmap_set = vma_set_ptr;
      }else {
	tmp_vmap_set = &(tagged_page->vma_ptr_pool[tagged_page->num_vmas]);
	tagged_page->num_vmas++;      
      }
      tmp_vmap_set->vmap = buf_vma_set;
      tmp_vmap_set->vmap->ref_count++;
    }else {
      char str[MAX_STR_LEN] = "";
      vma_addresses(tagged_page, str);
      printk("OOPS I ran out of vmas for page %p - active vmas are %s\n", page_address(tagged_page->page), str);
    }
  }else {
    for(i = 0; i < tagged_page->num_vmas; i++) {
      vma_set_ptr = &(tagged_page->vma_ptr_pool[i]);

      buf_vma_set = find_vma_in_list(&buf->active_vmas, vma);
      if(buf_vma_set == NULL) {
	printk("BUGON here, I couldn't find the vma for the buffer\n");
      }
	
      if(vma_set_ptr->vmap != NULL && vma_set_ptr->vmap == buf_vma_set) {
	break;
      }
    }
    BUG_ON(vma_set_ptr == NULL);
    /*     printk(KERN_ALERT "PID=%ld already have an existing vmap pointer for page %p at offset %lx and vma %p - reset the ss counter if it is non-zero %ld\n", */
    /* 	   current->pid, tagged_page->page, tagged_page->page_offset, vma->vm_start, vma_set_ptr->tlb_flush_cntr_ss); */
    vma_set_ptr->tlb_flush_cntr_ss = -1;
  }
  return;
}

/*
 * Steps for inerting a new page into the primary fifo and evicting a page:
 * 1) Make sure that the lock for the new page is held prior to entering this function
 * 2) Atomically swap the new page with the old page in the primary buffer
 *    Now the buffer points to the new page, and the new page flags reflect its new position, but it is still locked so that no other thread
 *    can read that data.  The page flags on the old page do not yet reflect that it is in the transient state, so it will report that it is
 *    still in the buffer.
 * 
- BAD IDEA Lock the page that exists at the target location in the buffer.  The only contention for this would be making it writable.
 * 3) Check if the page is hot or not
 * 4) If the page is hot grab the lock for the target page in the hot page buffer.
 * 4a) Insert the page that was just 
 *
 */
int insert_page_fifo_buffer_t(fifo_buffer_t *buf, unsigned long curr_buf_head, page_t *tagged_page) {
  indexed_fifo_t *primary_fifo_data = buf->primary_fifo_data;
  indexed_fifo_t *hpf_data = buf->hpf_data;
  indexed_fifo_t *victim_fifo_buffers = buf->victim_fifo_buffers;
  int /* warn_on_fifo_full = 1, warn_on_not_ready = 1, num_loops = 0,*/ num_other_loops = 0;
  page_t *evicted_page = NULL;
  fidx_t victim_index;
/*   int req_zap_page_range = 0; */
/*   int ret; */
/*  int num_entries_to_clear; */
  unsigned long evicted_page_last_expected_ts;
  int evicted_page_last_expected_buffer /*, err */;

  /* Once the page is "fetched" put it in the buffer and return the page, but only if there is room in the buffer */
  /* 	printk(/\* KERN_INFO *\/ "Store page %p for address %lx at location %lu\n", page_address(tagged_page->page), address, curr_num_global_faults); */
  if(primary_fifo_data->allocated_size != 0) {
    /* Insert the page into the primary fifo and pull out a page to be evicted or pushed into the hot page fifo */
    evicted_page = fifo_ops->swap_entries(primary_fifo_data, tagged_page, curr_buf_head);
    /* Mark the new page as in the buffer */
    mark_tagged_page_in_buffer(tagged_page, curr_buf_head, primary_fifo_data->id);
    evicted_page_last_expected_buffer = 1;
    evicted_page_last_expected_ts = curr_buf_head - primary_fifo_data->allocated_size;

    if(evicted_page != NULL) { /* If there is a page that is evicted, check to see if it is hot */
      char str[256];
      fifo_ops->display_pointers(primary_fifo_data, str);
/*       printk("I have swapped out a page %lx for page %lx.  It was last at %lu and the pointers are %s\n",  */
/* 	     vma_address(evicted_page->page, evicted_page->vma), vma_address(tagged_page->page, tagged_page->vma), atomic64_read(&evicted_page->timestamp), str);       */

      if(hpf_data->allocated_size > 0 && fifo_entry_is_hot(primary_fifo_data, evicted_page, hpf_data)) { /* The evicted page is hot, move it to the hot page fifo */
	fidx_t hpf_index;
	page_t *new_hot_page = evicted_page; /* Rename the evicted page to be the new hot page */

	/* Grab the lock for the tagged page that is being moved to the hot page fifo */

	/* Grab a location for inserting into the hot page fifo */
	hpf_index = fifo_ops->allocate_entry(hpf_data);
	evicted_page = fifo_ops->swap_entries(hpf_data, new_hot_page, hpf_index);
	mark_tagged_page_in_buffer(new_hot_page, hpf_index, hpf_data->id);

	evicted_page_last_expected_buffer = 2;
	evicted_page_last_expected_ts = hpf_index - hpf_data->allocated_size;
      }
/*     }else { */
/*       printk("I am inserting a new page %lx into the primary buffer\n", vma_address(tagged_page->page, tagged_page->vma)); */
    }

    /* Now we might have placed the evicted page in the hot page queue, so check again to see if there is a valid evicted page */
    if(evicted_page != NULL) {
/*       char str[256]; */
      int err;
      int victim_fifo_buffer_idx = curr_buf_head % NUM_EVICTION_BUFFERS;
      indexed_fifo_t *victim_fifo_data = &victim_fifo_buffers[victim_fifo_buffer_idx];

/*       if(fifo_ops->fifo_full(victim_fifo_data, 0)) { */
/* 	char str[256]; */
/* 	fifo_ops->display_pointers(victim_fifo_data, str); */
/* 	printk("The fifo I am targeting is full, maybe I should try another:%s\n", str); */
/*       } */

  retry_eviction:
      //      printk("%d is going to evict page %p which has count=%d and mapping=%d\n", current->pid, page_address(evicted_page->page), page_count(tagged_page->page), page_mapcount(tagged_page->page));

      /* If the page is not in the PTE or if it is still locked, wait until it is done being manipulated */
      if(!page_in_all_ptes(evicted_page) /* || PageLocked(evicted_page->page) */) {
/* 	if(warn_on_not_ready || (num_other_loops % 1000000 == 1 && num_other_loops > 1)) { */
/* 	  fifo_ops->display_pointers(victim_fifo_data, str); */
/* 	  printk("The page that is being evicted %lx is not yet in the PTE.  I am waiting to install page %lx.  cur_buf_head =%lu and the timestamp =%lu : %s\n", vma_address(evicted_page->page, evicted_page->vma), vma_address(tagged_page->page, tagged_page->vma), curr_buf_head, atomic64_read(&evicted_page->timestamp), str); */
/* 	  warn_on_not_ready = 0; */
/* 	  //	  num_other_loops = 0; */

/* /\* 	  num_entries_to_clear = fifo_ops->num_valid_entries(victim_fifo_data); *\/ */
/* /\* 	  if(num_entries_to_clear == victim_fifo_data->allocated_size || num_loops % 1000000 == 1) { *\/ */
/* /\* 	      if(num_other_loops % 1000000 == 1 && num_other_loops > 1) { *\/ */
/* /\* 		printk("WARNING thread %d manually trying to flush %d entries from the victim fifo since the number of loops for inserting %lx at %ld is just going up %d\n", current->pid, num_entries_to_clear, vma_address(evicted_page->page, evicted_page->vma), victim_index, num_loops); *\/ */
/* /\* 	      } *\/ */
/* /\* 	      /\\* When threads start getting stuck because the fifo is full, do not issue a request if the workqueue has started clearing entries *\\/ *\/ */
/* /\* 	      issue_workqueue_request(buffer_data, num_entries_to_clear, victim_index); *\/ */

/* 	} */
	yield();
	num_other_loops++;
	goto retry_eviction;
      }

      //      if(curr_buf_head - primary_fifo_data->allocated_size != atomic64_read(&evicted_page->timestamp)) {
      if(evicted_page_last_expected_ts != evicted_page->timestamp || evicted_page_last_expected_buffer != evicted_page->buffer_id) {
	char str2[64];
	char addresses[MAX_STR_LEN] = "";
	display_tagged_page_meta_data(evicted_page, str2);
	vma_addresses(evicted_page, addresses);
	printk("The page that is being evicted %s is no longer where it should be.  last buffer=%d & ts =%lu and the timestamp =%lu : %s\n", 
	       addresses/* vma_address(evicted_page->page, evicted_page->vma) */, evicted_page_last_expected_buffer, evicted_page_last_expected_ts, evicted_page->timestamp, str2);
      }

      if(page_being_evicted(buf, evicted_page)) {
	char addresses[MAX_STR_LEN] = "";
	vma_addresses(evicted_page, addresses);
	printk("The page that is being evicted %s is already in the victim buffer.  cur_buf_head =%lu and the timestamp =%lu\n", addresses/* vma_address(evicted_page->page, evicted_page->vma) */, curr_buf_head, evicted_page->timestamp);
      }

      /* Now clear either the page that was evicted from the primary fifo, or the one that was evicted from the hot page fifo */
      err = clear_fifo_entry(evicted_page);
      if(err) {
	char addresses[MAX_STR_LEN] = "";
	vma_addresses(evicted_page, addresses);
	printk("The page that is being evicted %s had problems.  cur_buf_head =%lu and the timestamp =%lu\n", addresses/* vma_address(evicted_page->page, evicted_page->vma) */, curr_buf_head, evicted_page->timestamp);
      }

      victim_index = fifo_ops->allocate_entry(victim_fifo_data);
/*       fifo_ops->display_pointers(victim_fifo_data, str); */
/*       printk("I am going to put page %lx in the victim queue at location %lu: FIFO %s\n", */
/* 	     vma_address(evicted_page->page, evicted_page->vma), victim_index, str); */

#ifdef foobar //USE_VQ_WORKER
      while(true) {
	/* Insert the page into the fifo at the assigned spot.  This will fail if the buffer is full, so retry until it succeeds */
	int err = fifo_ops->insert(victim_fifo_data, evicted_page, victim_index);
	if(!err) {
/* 	  if(num_loops > 1000000) { */
/* 	  fifo_ops->display_pointers(victim_fifo_data, str); */
/* 	  printk("I have put page %lx in the victim queue at location %lu: FIFO %s\n", */
/* 		 vma_address(evicted_page->page, evicted_page->vma), victim_index, str); */
/* 	  } */
	  mark_tagged_page_in_buffer(evicted_page, victim_index, victim_fifo_data->id);
/* 	  if(!warn_on_fifo_full) { */
/* 	    printk("I finally put page %lx in the victim queue at location %lu: FIFO %s\n", */
/* 		   vma_address(evicted_page->page, evicted_page->vma), victim_index, str); */
/* 	  } */
	  break;
	}else {
	  //	      printk(KERN_INFO "Thread %d is waiting for the tail %lu : %lu to advance (head = %lu : %lu)\n", current->pid, curr_buf_tail, mmap_buf_tail_idx, curr_buf_head, mmap_buf_head_idx);
	  /* Temporarily use the skipped zap page range to count how many times the fifo is full */
	  if(warn_on_fifo_full || (num_loops % 1000000 == 1 && num_loops > 1)) {
	    atomic64_inc(&perma_buffer_stalled);

	    /* If the victim fifo is full, issue a request to clear it */
	    num_entries_to_clear = fifo_ops->num_valid_entries(victim_fifo_data);
	    if(num_entries_to_clear == victim_fifo_data->allocated_size || num_loops % 1000000 == 1) {
	      if(num_loops % 1000000 == 1 && num_loops > 1) {
		char addresses[MAX_STR_LEN] = "";
		vma_addresses(evicted_page, addresses);
		printk("WARNING thread %d manually trying to flush %d entries from the victim fifo since the number of loops for inserting %s at %ld is just going up %d\n", current->pid, num_entries_to_clear, addresses/* vma_address(evicted_page->page, evicted_page->vma) */, victim_index, num_loops);
/* 		flush_pages_from_victim_buffer(victim_fifo_data, evicted_page->vma, fifo_ops->num_valid_entries(victim_fifo_data)/\* atomic64_read(&victim_fifo_data->num_pages_in_buffer) *\/, 1); */
/* 	      }else { */
	      }
/* 	      if(num_loops % 1000000 == 1 && num_loops > 1) { */
/* 		set_current_state(TASK_INTERRUPTIBLE); */
/* 		schedule_timeout(1); */
/* 		//		nsleep(100); */
/* 		//		flush_workqueue(victim_fifo_data->zap_workqueue); */
/* 	      } */
/* 	      } */
	    }
	  }
	  yield();
	  num_loops++;

/* 	  if(num_loops % 100000 == 1) { */
/* 	    fifo_ops->display_pointers(victim_fifo_data, str); */
/* 	    printk("Thread %d I cannot put page %lx in the victim queue at location %lu, try %d: FIFO %s\n", current->pid,  */
/* 		   vma_address(evicted_page->page, evicted_page->vma), victim_index, num_loops, str); */
/* 	  } */
	}
	warn_on_fifo_full = 0;
      }
#else
      if(fifo_ops->fifo_full(victim_fifo_data, 0)) {
	prot_vm_area_set_t *active_vmas = &(buf->active_vmas);
	flush_pages_from_victim_buffer(victim_fifo_data, active_vmas, &(buf->page_map), fifo_ops->num_valid_entries(victim_fifo_data));
      }

      err = fifo_ops->insert(victim_fifo_data, evicted_page, victim_index);
      if(err) {
	char addresses[MAX_STR_LEN] = "";
	vma_addresses(evicted_page, addresses);
	printk("I was unable to put page %s in the victim queue at location %lx: FIFO\n",
	       addresses, victim_index);

      }
      mark_tagged_page_in_buffer(evicted_page, victim_index, victim_fifo_data->id);
#endif
    }
  }

  return 0;
}

/* 
 * Prior to entering recover page it is necessary to hold the page lock for the tagged_page
 */
int recover_page_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page) {
  /* Allocate a spot in mmap's internal buffer */
  long buffer_entry = fifo_ops->allocate_entry(buf->primary_fifo_data); //pre_allocate_fifo_entry();
  indexed_fifo_t *fifo_data;

  /* Adjust the page count for the victim queue */
  fifo_data = get_fifo_holding_page(buf, tagged_page);
  fifo_data->total_page_fault_count -= tagged_page->num_page_faults;
  
  recover_tagged_pages_pending_vmas(tagged_page);

  insert_page_fifo_buffer_t(buf, buffer_entry, tagged_page);
  return 0;
}

void inc_num_page_faults_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page) {
  indexed_fifo_t *fifo_data;
  tagged_page->num_page_faults++;
  fifo_data = get_fifo_holding_page(buf, tagged_page);
  fifo_data->total_page_fault_count++;
  return;
}

void release_page_lock_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page) {
  BUG_ON(1); /* Unimplemented */
  return;
}

page_t *find_page_fifo_buffer_t(fifo_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id) {
  page_t *tagged_page = find_page_in_htable(&buf->page_map, physical_addr, dev_id);

  return tagged_page;
}

page_t *find_and_lock_page_fifo_buffer_t(fifo_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id) {
  BUG_ON(1); /* Unimplemented */
  return NULL;
}

/* Advance the head of the mmap buffer and return the allocated spot */
/* inline  */long pre_allocate_entry_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page) {
  return fifo_ops->allocate_entry(buf->primary_fifo_data);
}

int is_true_page_fault_fifo_buffer_t(fifo_buffer_t *buf, long buffer_entry, page_t *tagged_page, long perma_mmap_buf_size) {
  return 1;
}

/* inline  */void reset_state_fifo_buffer_t(fifo_buffer_t *buf) {
  int i;
  fifo_ops->reset_pointers(buf->primary_fifo_data);
  fifo_ops->reset_pointers(buf->hpf_data);
  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[i];
    fifo_ops->reset_pointers(victim_fifo_data);
  }
  atomic64_set(&buf->hot_page_threshold, 2);
  atomic64_set(&buf->num_hot_pages, 0);
  return;
}

int page_made_writable_fifo_buffer_t(fifo_buffer_t *buf, struct vm_area_struct *vma, page_t *tagged_page) {
  mark_tagged_page_writable(tagged_page);

  atomic64_inc(&total_num_pages_written);

  return 0;
}

/* Note that these statistics are collected without holding a bank lock */
void prettyprint_specifics_fifo_buffer_t(fifo_buffer_t *buf, struct seq_file *s, char *msg, char line_leader) {
  indexed_fifo_t *primary_fifo_data = buf->primary_fifo_data;
  indexed_fifo_t *hpf_data = buf->hpf_data;
  indexed_fifo_t *victim_fifo_buffers = buf->victim_fifo_buffers;
  char msg2[SHORT_STR_LEN], msg3[SHORT_STR_LEN], msg4[MAX_STR_LEN] = "";
  char mmaped_address_ranges[MED_STR_LEN] = "";
  unsigned long primary_fifo_avg_num_page_faults_i = 0;
  unsigned long primary_fifo_avg_num_page_faults_f = 0;
  unsigned long hpf_avg_num_page_faults_i = 0;
  unsigned long hpf_avg_num_page_faults_f = 0;
  int i;

  if(primary_fifo_data->allocated_size != 0) {
    primary_fifo_avg_num_page_faults_i = primary_fifo_data->total_page_fault_count / primary_fifo_data->allocated_size;
    primary_fifo_avg_num_page_faults_f = (primary_fifo_data->total_page_fault_count * 100) / primary_fifo_data->allocated_size - (primary_fifo_avg_num_page_faults_i * 100);
  }
  if(hpf_data->allocated_size != 0) {
    hpf_avg_num_page_faults_i = hpf_data->total_page_fault_count / hpf_data->allocated_size;
    hpf_avg_num_page_faults_f = (hpf_data->total_page_fault_count * 100) / hpf_data->allocated_size - (hpf_avg_num_page_faults_i * 100);
  }

  fifo_ops->display_pointers(primary_fifo_data, msg2);
  fifo_ops->display_pointers(hpf_data, msg3);
  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &victim_fifo_buffers[i];
    char tmp_msg[SHORT_STR_LEN];
    fifo_ops->display_pointers(victim_fifo_data, tmp_msg);
    strcat(msg4, tmp_msg);
  }

  vm_address_regions(&(buf->active_vmas.vmas), mmaped_address_ranges);

  seq_printf(s, "%c     buffer=GEN_FIFO primary_fifo:%s avg_faults=%ld.%02ld hot_page_fifo:%s avg_faults=%ld.%02ld head_tail_window=%lu write_eviction_race=%lu victim_fifo:%s vm_areas:%s\n", 
	     line_leader, msg2, primary_fifo_avg_num_page_faults_i, primary_fifo_avg_num_page_faults_f, msg3, hpf_avg_num_page_faults_i, hpf_avg_num_page_faults_f,
	     primary_fifo_data->clear_ahead_window, atomic64_read(&num_write_eviction_races), msg4, mmaped_address_ranges);

  return;

}

int tune_specifics_fifo_buffer_t(fifo_buffer_t *buf, char *cmd, int param1, int param2, int param3, int param4) {
  int err = 0;
  if(strcasecmp(cmd, MMAP_FIFO_CLEAR_AHEAD_WINDOW) == 0) {
    buf->primary_fifo_data->clear_ahead_window = param1;
  }else if(strcasecmp(cmd, MMAP_HPF_BUFFER) == 0) {
    buf->hot_page_fifo_buffer_size = param1;
  }else if(strcasecmp(cmd, MMAP_EVICTION_BUFFER) == 0) {
    buf->victim_fifo_buffer_size = param1;
  }else if(strcasecmp(cmd, MMAP_FLUSH_BUFFERS) == 0) {
    flush_buffers_fifo_buffer_t(buf);
  }else if(strcasecmp(cmd, MMAP_SYNC_BUFFERS) == 0) {
    sync_buffers_fifo_buffer_t(buf);
  }else {
    err = 1;
  }

  return err;
}

void explain_tuning_params_fifo_buffer_t(fifo_buffer_t *buf) {
  printk("\t%s\n", MMAP_FIFO_CLEAR_AHEAD_WINDOW);
  printk("\t%s\t\t\t%s\n", MMAP_HPF_BUFFER, MMAP_HPF_BUFFER_STR);
  printk("\t%s\n", MMAP_EVICTION_BUFFER);
  printk("\t%s\n", MMAP_FLUSH_BUFFERS);
  printk("\t%s\n", MMAP_SYNC_BUFFERS);
  return;
}

void init_specific_counters_fifo_buffer_t(fifo_buffer_t *buf) {
  atomic64_set(&num_write_eviction_races, 0);
  return;
}

/* 
 * Use the timestamp (buffer location) and buffer id values for each page to determine if it is in a buffer.
 * In the case of one page being evicted from a buffer, while another one is being inserted, there is a condition
 * where the evicted page still thinks that it is in the buffer, but the new page doesn't yet report that it is in
 * that slot as long as the locks are nested.
 */
int page_in_buffer_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page) {
  int in_buffer = is_tagged_page_in_a_buffer(tagged_page, 1) || is_tagged_page_in_a_buffer(tagged_page, 2);
/*   if(in_buffer) { */
/*     unsigned long timestamp = atomic64_read(&tagged_page->timestamp); */
/*     int buffer_id = tagged_page->buffer_id; */
/*     char str[256]; */
/*     char str2[256] = "there is a null entry at this location"; */
/*     page_t *tmp; */
    
/*     if(buffer_id == 1) { */
/*       tmp = fifo_ops->peek(primary_fifo_data, timestamp); */
/*       fifo_ops->display_pointers(primary_fifo_data, str); */
/*     }else if(buffer_id == 2) { */
/*       tmp = fifo_ops->peek(hpf_data, timestamp); */
/*       fifo_ops->display_pointers(hpf_data, str); */
/*     }else { */
/*       printk("Why is my buffer id 0\n"); */
/*     } */

/*     if(tmp != NULL) { */
/*       sprintf(str2, "the other page has timestamp %lu", atomic64_read(&tmp->timestamp)); */
/*     } */
/*     if(tmp != tagged_page) { */
/*       printk(KERN_INFO "Page %lx does not actually exist in buffer %d at location %lu, even though it has that timestamp. The other page %lx has timestamp %s %s\n",  */
/* 	     vma_address(tagged_page->page, tagged_page->vma), buffer_id, timestamp, vma_address(tmp->page, tmp->vma), str2, str); */
/*       /\* The page is not actually in the buffer, it just has stale data *\/ */
/*       in_buffer = 0; */
/*     }     */
/*   } */
  return in_buffer;

/*   return is_tagged_page_in_a_buffer(tagged_page, 1) || is_tagged_page_in_a_buffer(tagged_page, 2); */
}

int page_being_evicted_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page) {
  int in_victim_buffer = 0;
  int i;
  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[i];
    in_victim_buffer = is_tagged_page_in_a_buffer(tagged_page, victim_fifo_data->id);
    if(in_victim_buffer) {
      break;
    }
  }
  /* If it is not in the victim buffer already, see if it is on its way there. */
/*   if(!in_victim_buffer) { */
/*     unsigned long timestamp = atomic64_read(&tagged_page->timestamp); */
/*     int buffer_id = tagged_page->buffer_id; */
/*     char str[256]; */
/*     char str2[256] = "there is a null entry at this location"; */
/*     page_t *tmp; */
    
/*     if(buffer_id == 1) { */
/*       tmp = fifo_ops->peek(primary_fifo_data, timestamp); */
/*       fifo_ops->display_pointers(primary_fifo_data, str); */
/*     }else if(buffer_id == 2) { */
/*       tmp = fifo_ops->peek(hpf_data, timestamp); */
/*       fifo_ops->display_pointers(hpf_data, str); */
/*     }else { */
/*       printk("Why is my buffer id 0\n"); */
/*     } */

/*     if(tmp != NULL) { */
/*       sprintf(str2, "the other page has timestamp %lu", atomic64_read(&tmp->timestamp)); */
/*     } */
/*     if(tmp != tagged_page) { */
/*       printk(KERN_INFO "Page %lx does not actually exist in buffer %d at location %lu, even though it has that timestamp. The other page %lx has timestamp %s %s\n",  */
/* 	     vma_address(tagged_page->page, tagged_page->vma), buffer_id, timestamp, vma_address(tmp->page, tmp->vma), str2, str); */
/*       /\* The page is not actually in the buffer, it just has stale data *\/ */
/*       in_buffer = 0; */
/*     }     */
/*   } */
  return in_victim_buffer;
}

static int fifo_entry_is_hot(indexed_fifo_t *lower_fifo_data, page_t *tagged_page, indexed_fifo_t *upper_fifo_data) {
  unsigned long lower_fifo_average_num_page_faults;
  unsigned long num_page_faults;

  if(tagged_page == NULL) { return 0; }

  lower_fifo_average_num_page_faults = lower_fifo_data->total_page_fault_count / lower_fifo_data->allocated_size;
  num_page_faults = tagged_page->num_page_faults;

#if 0
  if((num_page_faults >= atomic64_read(&hot_page_threshold))) {
    unsigned long cur_num_hot_pages = atomic64_inc_return(&num_hot_pages);
    if(cur_num_hot_pages == hpf_data->allocated_size) {
      unsigned long new_hot_page_threshold = atomic64_inc_return(&hot_page_threshold);
      atomic64_set(&num_hot_pages, 0);
      printk("Increasing the hot page threshold to %lu\n", new_hot_page_threshold);
      return 0;
    }
    return 1;
  }else {
    return 0;
  }
#else
  if((num_page_faults > lower_fifo_average_num_page_faults)) {
    unsigned long lower_delta = num_page_faults - lower_fifo_average_num_page_faults;
    unsigned long upper_fifo_average_num_page_faults = 0; 
    unsigned long upper_fifo_num_pages_in_buffer = upper_fifo_data->allocated_size;

    if(upper_fifo_num_pages_in_buffer > 0) {
      upper_fifo_average_num_page_faults = upper_fifo_data->total_page_fault_count / upper_fifo_num_pages_in_buffer;
    }

    if(num_page_faults >= upper_fifo_average_num_page_faults || 
       ((upper_fifo_average_num_page_faults - num_page_faults) <= lower_delta)) { /* If the number of page faults is higher than the upper fifo's average, or
										    if the number of page faults is closer to the upper average than the lower averge
										    it is a hot page */
/*       if(((upper_fifo_average_num_page_faults - num_page_faults) <= lower_delta) && (num_page_faults < upper_fifo_average_num_page_faults)) { */
/*     printk(KERN_INFO "The page %p is a hot page and has %lu faults.  The lower average is %lu, the lower total number of faults is %lu and the number of pages is %lu. The upper average is %lu, the lower total number of faults is %lu and the number of pages is %lu.\n", */
/* 	   page_address(tagged_page->page), num_page_faults,  */
/* 	   lower_fifo_average_num_page_faults, atomic64_read(&lower_fifo_data->total_page_fault_count), atomic64_read(&lower_fifo_data->num_pages_in_buffer), */
/* 	   upper_fifo_average_num_page_faults, atomic64_read(&upper_fifo_data->total_page_fault_count), atomic64_read(&upper_fifo_data->num_pages_in_buffer)); */
/*     } */
      return 1;
    }else {
      return 0;
    }
  }else {
    return 0;
  }
#endif
}

fifo_buffer_t *init_mmap_buffer_data_fifo_buffer_t(fifo_buffer_t *dummy) {
  int i;
  char queue_name[MAX_STR_LEN];
  fifo_buffer_t *buf;

  printk(KERN_INFO "Initializing data structures for the generational fifo buffer\n");

  buf = kmalloc(sizeof(fifo_buffer_t), GFP_KERNEL | __GFP_ZERO);
  buf->primary_fifo_data = kmalloc(sizeof(indexed_fifo_t), GFP_KERNEL | __GFP_ZERO);
  buf->hpf_data = kmalloc(sizeof(indexed_fifo_t), GFP_KERNEL | __GFP_ZERO);
  buf->victim_fifo_buffers = kmalloc(NUM_EVICTION_BUFFERS * sizeof(indexed_fifo_t), GFP_KERNEL | __GFP_ZERO);
  
#ifdef USE_VQ_WORKER
  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[i];
    sprintf(queue_name, "perma-vqflush%2d", -1-i);
    victim_fifo_data->fifo_workqueue = create_singlethread_workqueue(queue_name);
    /* Allocate storage for the workqueue requests */
#ifndef RHEL6
    victim_fifo_data->fifo_workqueue_work_request = kmalloc(ZAP_REQUEST_POOL * sizeof(struct work_struct), GFP_KERNEL);
#endif
    victim_fifo_data->fifo_workqueue_request_data = kmalloc(ZAP_REQUEST_POOL * sizeof(struct fifo_workqueue_request_t), GFP_KERNEL);
    victim_fifo_data->active = 0;
  }
#endif
  /* Initialize the vma sets */
  init_prot_vm_area_set(&buf->active_vmas);

  /* Initialize the hash table */
  init_hash_table(&buf->page_map);

  /* Initialize a mutex for the entire buffer */
  mutex_init(&buf->buffer_lock);
  printk(KERN_INFO "I am using a mutex for the buffer locking\n");
  return buf;
}

void cleanup_mmap_buffer_data_fifo_buffer_t(fifo_buffer_t *buf) {
  int i;
  printk(KERN_INFO "Cleaning up data structures for the generational fifo buffer\n");

  for(i = 0; i < NUM_EVICTION_BUFFERS; i++) {
    indexed_fifo_t *victim_fifo_data = &buf->victim_fifo_buffers[i];
    victim_fifo_data->active = 0;
#ifdef USE_VQ_WORKER
    printk(KERN_INFO "About to kill the workqueues\n");
    destroy_workqueue(victim_fifo_data->fifo_workqueue);
#endif
#ifndef RHEL6
    kfree(victim_fifo_data->fifo_workqueue_work_request);
#endif
    kfree(victim_fifo_data->fifo_workqueue_request_data);
    victim_fifo_data->fifo_workqueue = NULL;
#ifndef RHEL6
    victim_fifo_data->fifo_workqueue_work_request = NULL;
#endif
    victim_fifo_data->fifo_workqueue_request_data = NULL;
  }

  free_prot_vm_area_set(&buf->active_vmas);

  free_hash_table(&buf->page_map);

  kfree(buf->primary_fifo_data);
  kfree(buf->hpf_data);
  kfree(buf->victim_fifo_buffers);
  kfree(buf);

  return;
}
