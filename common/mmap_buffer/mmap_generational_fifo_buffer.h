/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _PERMA_MMAP_GENERATIONAL_FIFO_BUFFER_H_
#define _PERMA_MMAP_GENERATIONAL_FIFO_BUFFER_H_

#include <linux/seq_file.h>
#include <linux/workqueue.h>
#include "indexed_fifo_non_atomic.h"

#define ZAP_REQUEST_POOL 64 //1024

typedef struct fifo_buffer_struct {
  indexed_fifo_t *primary_fifo_data;
  indexed_fifo_t *hpf_data;
  indexed_fifo_t *victim_fifo_buffers;

  atomic64_t hot_page_threshold;
  atomic64_t num_hot_pages;
  unsigned long primary_fifo_buffer_size;
  unsigned long hot_page_fifo_buffer_size;
  unsigned long victim_fifo_buffer_size;

  prot_vm_area_set_t active_vmas; /* Set of VMAs that are mapped onto this buffer */

  buffer_map_t page_map;

  struct mutex buffer_lock; /* Provide a lock for the entire set of buffers */
}fifo_buffer_t;

#define MMAP_BUF_CLEAR_AHEAD_DEFAULT_WINDOW 32768

#define MMAP_FIFO_CLEAR_AHEAD_WINDOW "fifo_clear_window"

#define MAX_NUM_SKIPPED_FIFO_ENTRIES 16

//extern struct buffer_operations_struct fifo_buffer_ops;
extern fifo_buffer_t fifo_buffer_data;

#define MMAP_HPF_BUFFER "mmap_hpf_buffer"
#define MMAP_EVICTION_BUFFER "mmap_eviction_buffer"
#define MMAP_FLUSH_BUFFERS "mmap_flush_buffers"
#define MMAP_SYNC_BUFFERS "mmap_sync_buffers"

#define MMAP_HPF_BUFFER_STR "Set the size of the hotpage fifo buffer (Need to reset_mmap after resize)"

#define MMAP_EVICTION_BUFFER_DEFAULT_SIZE 1024 //8192 // 1024
//#define MMAP_EVICTION_BUFFER_DEFAULT_SIZE 16384
//#define MMAP_EVICTION_BUFFER_DEFAULT_SIZE 32768
//#define MMAP_EVICTION_BUFFER_DEFAULT_SIZE 65536

/*******************************************************************************
 * Implement standard functions
 * Use the buffer typedef to differentiate the name of each function
 ******************************************************************************/

/* Setting up and tearing down buffers */
fifo_buffer_t *init_mmap_buffer_data_fifo_buffer_t(fifo_buffer_t *dummy);
void cleanup_mmap_buffer_data_fifo_buffer_t(fifo_buffer_t *buf);

/* Manage parameters */
void reset_params_fifo_buffer_t(fifo_buffer_t *buf);
void init_params_fifo_buffer_t(fifo_buffer_t *buf, unsigned long perma_mmap_buf_size);
void cleanup_params_fifo_buffer_t(fifo_buffer_t *buf);

/* Actions when a device is mmaped or munmaped */
void vma_open_mmap_fifo_buffer_t(fifo_buffer_t *buf, struct vm_area_struct *vma, vm_area_set_tlb_cntr_t *vma_tlb_cntr);
void vma_close_mmap_fifo_buffer_t(fifo_buffer_t *buf, struct vm_area_struct *vma);

/* Handle adding, deleting, etc pages from the buffer */
void release_page_lock_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page);
page_t *find_page_fifo_buffer_t(fifo_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id);
page_t *find_and_lock_page_fifo_buffer_t(fifo_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id);
long pre_allocate_entry_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page);
int is_true_page_fault_fifo_buffer_t(fifo_buffer_t *buf, long buffer_entry, page_t *tagged_page, long perma_mmap_buf_size);
void rmap_page_to_vma_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page, struct vm_area_struct *vma);
int insert_page_fifo_buffer_t(fifo_buffer_t *buf, unsigned long buffer_entry, page_t *tagged_page);
int page_made_writable_fifo_buffer_t(fifo_buffer_t *buf, struct vm_area_struct *vma, page_t *tagged_page);
int recover_page_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page);
void inc_num_page_faults_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page);

/* Query state of page in buffer */
int page_in_buffer_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page);
int page_being_evicted_fifo_buffer_t(fifo_buffer_t *buf, page_t *tagged_page);

/* Reset or tune the buffer state */
void reset_state_fifo_buffer_t(fifo_buffer_t *buf);
void flush_buffers_fifo_buffer_t(fifo_buffer_t *buf);
void sync_buffers_fifo_buffer_t(fifo_buffer_t *buf);
int tune_specifics_fifo_buffer_t(fifo_buffer_t *buf, char *cmd, int param1, int param2, int param3, int param4);
void init_specific_counters_fifo_buffer_t(fifo_buffer_t *buf);

/* Display buffer specific results */
void prettyprint_specifics_fifo_buffer_t(fifo_buffer_t *buf, struct seq_file *s, char *msg, char line_leader);
void explain_tuning_params_fifo_buffer_t(fifo_buffer_t *buf);
void display_mmap_buffer_state_fifo_buffer_t(fifo_buffer_t *buf, char *str);

void scan_buffer_fifo_buffer_t(fifo_buffer_t *buf, void (*fn)(void *, page_t *), void *data);

#endif /* _PERMA_MMAP_GENERATIONAL_FIFO_BUFFER_H_ */
