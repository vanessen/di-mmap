/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _MMAP_BUFFER_INTERFACE_
#define _MMAP_BUFFER_INTERFACE_

#include "mmap_generational_fifo_buffer.h"
#include "mmap_banked_buffer.h"

/* Define a standard interface into the buffering scheme using macros that are later redefined */

/* Found this nice macro trick for type-based overloading in C: http://locklessinc.com/articles/overloading/ */

// warning: dereferencing type-punned pointer will break strict-aliasing rules
/* #define gcc_overload(A)\ */
/* 	__builtin_choose_expr(__builtin_types_compatible_p(typeof(A), struct s1),\ */
/* 		gcc_overload_s1(*(struct s1 *)&A),\ */
/* 	__builtin_choose_expr(__builtin_types_compatible_p(typeof(A), struct s2),\ */
/* 		gcc_overload_s2(*(struct s2 *)&A),(void)0)) */

/* Setting up and tearing down buffers */
#define init_mmap_buffer_data(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      init_mmap_buffer_data_fifo_buffer_t(*(fifo_buffer_t **)&T),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      init_mmap_buffer_data_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))
#define cleanup_mmap_buffer_data(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      cleanup_mmap_buffer_data_fifo_buffer_t(*(fifo_buffer_t **)&T),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      cleanup_mmap_buffer_data_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

/* Managing parameters */
#define reset_params(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      reset_params_fifo_buffer_t(*(fifo_buffer_t **)&T),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      reset_params_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

#define init_params(T, A) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      init_params_fifo_buffer_t(*(fifo_buffer_t **)&T, A),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      init_params_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

#define cleanup_params(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      cleanup_params_fifo_buffer_t(*(fifo_buffer_t **)&T), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      cleanup_params_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

/* Actions when a device is mmaped or munmaped */
#define vma_open_mmap(T, A, B) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      vma_open_mmap_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      vma_open_mmap_banked_buffer_t(*(banked_buffer_t **)&T, A, B),(void)0))

#define vma_close_mmap(T, A) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      vma_close_mmap_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      vma_close_mmap_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

/* Handle adding, deleting, etc pages from the buffer */
#define release_page_lock(T, A)						\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      release_page_lock_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      release_page_lock_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

#define find_page(T, A, B)						\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      find_page_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      find_page_banked_buffer_t(*(banked_buffer_t **)&T, A, B),(void)0))

#define find_and_lock_page(T, A, B)						\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      find_and_lock_page_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      find_and_lock_page_banked_buffer_t(*(banked_buffer_t **)&T, A, B),(void)0))

#define pre_allocate_entry(T, A)						\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      pre_allocate_entry_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      pre_allocate_entry_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

#define is_true_page_fault(T, A, B, C) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      is_true_page_fault_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B, C), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      is_true_page_fault_banked_buffer_t(*(banked_buffer_t **)&T, A, B, C),(void)0))

#define rmap_page_to_vma(T, A, B)					\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      rmap_page_to_vma_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      rmap_page_to_vma_banked_buffer_t(*(banked_buffer_t **)&T, A, B),(void)0))

#define insert_page(T, A, B) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      insert_page_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      insert_page_banked_buffer_t(*(banked_buffer_t **)&T, A, B),(void)0))

#define page_made_writable(T, A, B) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      page_made_writable_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      page_made_writable_banked_buffer_t(*(banked_buffer_t **)&T, A, B),(void)0))

#define recover_page(T, A) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      recover_page_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      recover_page_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

#define inc_num_page_faults(T, A) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      inc_num_page_faults_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      inc_num_page_faults_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

/* Query state of page in buffer */
#define page_in_buffer(T, A) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      page_in_buffer_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      page_in_buffer_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

#define page_being_evicted(T, A) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      page_being_evicted_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      page_being_evicted_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

/* Reset or tune the buffer state */
#define reset_state(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
		reset_state_fifo_buffer_t(*(fifo_buffer_t **)&T),\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
		reset_state_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

#define flush_buffers(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      flush_buffers_fifo_buffer_t(*(fifo_buffer_t **)&T),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      flush_buffers_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

#define sync_buffers(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      sync_buffers_fifo_buffer_t(*(fifo_buffer_t **)&T),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      sync_buffers_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

#define tune_specifics(T, A, B, C, D, E) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      tune_specifics_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B, C, D, E),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      tune_specifics_banked_buffer_t(*(banked_buffer_t **)&T, A, B, C, D, E),(void)0))

#define init_specific_counters(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      init_specific_counters_fifo_buffer_t(*(fifo_buffer_t **)&T),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      init_specific_counters_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

/* Display buffer specific results */
#define prettyprint_specifics(T, A, B, C)					\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      prettyprint_specifics_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B, C), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      prettyprint_specifics_banked_buffer_t(*(banked_buffer_t **)&T, A, B, C),(void)0))

#define explain_tuning_params(T) \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      explain_tuning_params_fifo_buffer_t(*(fifo_buffer_t **)&T),	\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      explain_tuning_params_banked_buffer_t(*(banked_buffer_t **)&T),(void)0))

#define display_mmap_buffer_state(T, A)					\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      display_mmap_buffer_state_fifo_buffer_t(*(fifo_buffer_t **)&T, A), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      display_mmap_buffer_state_banked_buffer_t(*(banked_buffer_t **)&T, A),(void)0))

#define scan_buffer(T, A, B)						\
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), fifo_buffer_t *),\
			      scan_buffer_fifo_buffer_t(*(fifo_buffer_t **)&T, A, B), \
	__builtin_choose_expr(__builtin_types_compatible_p(typeof(T), banked_buffer_t *),\
			      scan_buffer_banked_buffer_t(*(banked_buffer_t **)&T, A, B),(void)0))


#endif /* _MMAP_BUFFER_INTERFACE_ */
