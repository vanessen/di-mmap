/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/mm.h>		/* everything */
#include <asm/pgtable.h>
#include "mmap_banked_buffer.h"
#include "tagged_page.h"
#include "mmap_buffer_interface.h"

/*******************************************************************************
 * Internal functions
 ******************************************************************************/

/* Pages have to be banked so that all pages on a single PTE wind up on the same
 * bank, to avoid PTE lock contention.  With 4K PTE entries and 64 bit pointers,
 * there are 512 entries on each PTE.
 */
#define PAGE_HASH(x) ((x >> 9) % NUM_BUFFER_BANKS)

/* static inline pgoff_t page_index(page_t *tagged_page) { */
/*   return tagged_page->page->index; */
/* } */

static inline unsigned long compute_bank_addr(pgoff_t/* unsigned long */ physical_addr) {
  return PAGE_HASH(physical_addr);
}

static inline fifo_buffer_t *select_buffer_bank(banked_buffer_t *buf, page_t *tagged_page) {
  unsigned long idx = compute_bank_addr(tagged_page->page_offset); /* FIXME BVE make this field page_index */
  //  unsigned long idx = compute_bank_addr(page_index(tagged_page->page));
  return buf->banks[idx];
}

/*******************************************************************************
 * External functions
 ******************************************************************************/
banked_buffer_t *init_mmap_buffer_data_banked_buffer_t(banked_buffer_t *dummy) {
  int i;
  banked_buffer_t *buf;

  printk(KERN_INFO "Initializing data structures for the banked buffer\n");

  buf = kmalloc(sizeof(banked_buffer_t), GFP_KERNEL | __GFP_ZERO);
  buf->num_banks = NUM_BUFFER_BANKS;
  buf->banks = kmalloc(NUM_BUFFER_BANKS * sizeof(fifo_buffer_t *), GFP_KERNEL | __GFP_ZERO);
  /* Initialize the vma set counters */
  init_prot_vm_area_tlb_cntr_set(&buf->active_vmas_tlb_cntrs);
  for(i = 0; i < NUM_BUFFER_BANKS; i++) {
    /* Do not try to acquire the bank locks here because they have not been initialized */
    buf->banks[i] = init_mmap_buffer_data(buf->banks[i]);
  }

  return buf;
}

void cleanup_mmap_buffer_data_banked_buffer_t(banked_buffer_t *buf) {
  fifo_buffer_t *bank;
  int i;

  printk(KERN_INFO "Cleaning up data structures for the banked buffer\n");

  for(i = 0; i < NUM_BUFFER_BANKS; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    cleanup_mmap_buffer_data(bank);
    mutex_unlock(&bank->buffer_lock);
  }
  free_prot_vm_area_tlb_cntr_set(&buf->active_vmas_tlb_cntrs);
  kfree(buf->banks);
  kfree(buf);

  return;
}

void reset_params_banked_buffer_t(banked_buffer_t *buf){
  fifo_buffer_t *bank;
  int i;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    reset_params(bank);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

void init_params_banked_buffer_t(banked_buffer_t *buf, unsigned long perma_mmap_buf_size){
  fifo_buffer_t *bank;
  int i;
  int bank_size = perma_mmap_buf_size / NUM_BUFFER_BANKS;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    init_params(bank, bank_size);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

void cleanup_params_banked_buffer_t(banked_buffer_t *buf){
  fifo_buffer_t *bank;
  int i;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    cleanup_params(bank);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

/* Actions when a device is mmaped or munmaped */
void vma_open_mmap_banked_buffer_t(banked_buffer_t *buf, struct vm_area_struct *vma, vm_area_set_tlb_cntr_t *vma_tlb_cntr){
  fifo_buffer_t *bank;
  int i;
  vm_area_set_tlb_cntr_t *cntr;

  cntr = add_tlb_cntr_to_list(&buf->active_vmas_tlb_cntrs, vma);

  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    vma_open_mmap(bank, vma, cntr);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

void vma_close_mmap_banked_buffer_t(banked_buffer_t *buf, struct vm_area_struct *vma){
  fifo_buffer_t *bank;
  int i;

  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    vma_close_mmap(bank, vma);
    mutex_unlock(&bank->buffer_lock);
  }

  /* Once all of the banks have marked their active VMA pointers as inactive, the TLB counter is no longer used */
  remove_tlb_cntr_from_list(&buf->active_vmas_tlb_cntrs, vma); /* BVE not sure when this should occur */

  return;
}

/* Handle adding, deleting, etc pages from the buffer */
/* void acquire_lock_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page){ */
/*   fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page); */
/*   mutex_lock(&bank->buffer_lock); */
/*   return; */
/* } */

void release_page_lock_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  mutex_unlock(&bank->buffer_lock);
  return;
}

page_t *find_page_banked_buffer_t(banked_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id) {
  BUG_ON(1);  /* Unimplemented */
  return NULL;
}

page_t *find_and_lock_page_banked_buffer_t(banked_buffer_t *buf, pgoff_t physical_addr, dev_t dev_id) {
  fifo_buffer_t *bank;
  unsigned long idx = compute_bank_addr(physical_addr);

  bank = buf->banks[idx];
  mutex_lock(&bank->buffer_lock);
  return find_page(bank, physical_addr, dev_id);
}

long pre_allocate_entry_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return pre_allocate_entry(bank, tagged_page);
}

int is_true_page_fault_banked_buffer_t(banked_buffer_t *buf, long buffer_entry, page_t *tagged_page, long perma_mmap_buf_size){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return is_true_page_fault(bank, buffer_entry, tagged_page, perma_mmap_buf_size);
}

void rmap_page_to_vma_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page, struct vm_area_struct *vma) {
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return rmap_page_to_vma(bank, tagged_page, vma);
}

int insert_page_banked_buffer_t(banked_buffer_t *buf, unsigned long buffer_entry, page_t *tagged_page){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return insert_page(bank, buffer_entry, tagged_page);
}

int page_made_writable_banked_buffer_t(banked_buffer_t *buf, struct vm_area_struct *vma, page_t *tagged_page){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return page_made_writable(bank, vma, tagged_page);
}

int recover_page_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return recover_page(bank, tagged_page);
}

void inc_num_page_faults_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page) {
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return inc_num_page_faults(bank, tagged_page);  
}

/* Query state of page in buffer */
int page_in_buffer_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return page_in_buffer(bank, tagged_page);
}

int page_being_evicted_banked_buffer_t(banked_buffer_t *buf, page_t *tagged_page){
  fifo_buffer_t *bank = select_buffer_bank(buf, tagged_page);
  return page_being_evicted(bank, tagged_page);
}

/* Reset or tune the buffer state */
void reset_state_banked_buffer_t(banked_buffer_t *buf){
  fifo_buffer_t *bank;
  int i;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    reset_state(bank);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

void flush_buffers_banked_buffer_t(banked_buffer_t *buf){
  fifo_buffer_t *bank;
  int i;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    flush_buffers(bank);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

void sync_buffers_banked_buffer_t(banked_buffer_t *buf){
  fifo_buffer_t *bank;
  int i;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    sync_buffers(bank);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

int tune_specifics_banked_buffer_t(banked_buffer_t *buf, char *cmd, int param1, int param2, int param3, int param4){
  fifo_buffer_t *bank;
  int i, err = 0;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    err += tune_specifics(bank, cmd, param1, param2, param3, param4);
    mutex_unlock(&bank->buffer_lock);
  }
  return err;
}

void init_specific_counters_banked_buffer_t(banked_buffer_t *buf){
  fifo_buffer_t *bank;
  int i;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
    mutex_lock(&bank->buffer_lock);
    init_specific_counters(bank);
    mutex_unlock(&bank->buffer_lock);
  }
  return;
}

/*******************************************************************************
 * LOCK FREE scanning routines - BEGIN
 * Note that all of the following routines will scan the buffer banks without
 * holding a lock.
 ******************************************************************************/

/* Display buffer specific results - 
 * Iterate over the banks without holding the bank lock.  This allows the operation to incur less overhead, and 
 * it will still work when the driver is hung.  Also, even with the bank lock aquired, the collected results will 
 * not be consistent, since no lock is held across all banks during the entire collection.
 */
void prettyprint_specifics_banked_buffer_t(banked_buffer_t *buf, struct seq_file *s, char *msg, char line_leader){
  fifo_buffer_t *bank;
  int i;

  seq_printf(s, "%c     buffer=BANKED_BUFFER num_banks=%d\n", 
	  line_leader, buf->num_banks);
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
/*     mutex_lock(&bank->buffer_lock); */
    prettyprint_specifics(bank, s, msg, line_leader);
/*     mutex_unlock(&bank->buffer_lock); */
  }
  return;
}

void explain_tuning_params_banked_buffer_t(banked_buffer_t *buf){
  fifo_buffer_t *bank;
  /* Only display the bank tuning parameters once */
  bank = buf->banks[0];
/*   mutex_lock(&bank->buffer_lock); */
  explain_tuning_params(bank);
/*   mutex_unlock(&bank->buffer_lock); */
  return;
}

void display_mmap_buffer_state_banked_buffer_t(banked_buffer_t *buf, char *str){
  fifo_buffer_t *bank;
  int i;
  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
/*     mutex_lock(&bank->buffer_lock); */
    display_mmap_buffer_state(bank, str);
/*     mutex_unlock(&bank->buffer_lock); */
  }
  return;
}

/*
 * Scan each buffer bank - do not hold the bank lock, allow this to be imprecise for performance reasons.
 */
void scan_buffer_banked_buffer_t(banked_buffer_t *buf, void (*fn)(void *, page_t *), void *data) {
  fifo_buffer_t *bank;
  int i;

  for(i = 0; i < buf->num_banks; i++) {
    bank = buf->banks[i];
/*     mutex_lock(&bank->buffer_lock); */
    scan_buffer(bank, fn, data);
/*     mutex_unlock(&bank->buffer_lock); */
  }
  return;
}

/*******************************************************************************
 * LOCK FREE scanning routines - END
 * Note that all of the previous routines will scan the buffer banks without
 * holding a lock.
 ******************************************************************************/
