/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/hash.h>
#include "mmap_buffer_hash_table.h"

void init_hash_table(buffer_map_t *page_map) {
  int i;

  /* Initialize the hash table */
  page_map->htable = kmalloc(HTABLE_SIZE * sizeof(struct hlist_head), GFP_KERNEL);
  
  for(i = 0; i < HTABLE_SIZE; i++) {
    INIT_HLIST_HEAD(&page_map->htable[i]);
  }

  page_map->total_num_pages = 0;

  INIT_HLIST_HEAD(&page_map->free_pages);

  return;
}

void init_hash_table_free_pages(buffer_map_t *page_map, long num_free_pages) {
  long i;
  page_t *tmp;

  if(num_free_pages == page_map->total_num_pages) {
    return;    
  }else {
    printk(KERN_INFO "I am going to initialize the free page list with %ld entries and there are currently %ld entries: a page is %ld bytes\n", num_free_pages, page_map->total_num_pages, sizeof(page_t));
  }

  if(page_map->total_num_pages < num_free_pages) { /* Add some pages to the free list */
    long num_pages_to_add = num_free_pages - page_map->total_num_pages;

    for(i = 0; i < num_pages_to_add; i++) {
      tmp = kmalloc(sizeof(page_t), GFP_KERNEL);
      init_tagged_page_meta_data(tmp);
      hlist_add_head(&tmp->hchain, &page_map->free_pages);
    }

    page_map->total_num_pages = num_free_pages;
  }else if(page_map->total_num_pages > num_free_pages) { /* Remove some pages from the free list */
    long num_pages_to_free = page_map->total_num_pages - num_free_pages;
    long num_freed = 0;
    struct hlist_node *pos, *n;

    BUG_ON(hlist_empty(&page_map->free_pages));
      
    hlist_for_each_safe(pos, n, &page_map->free_pages) {
      tmp = hlist_entry(pos, page_t, hchain);
      hlist_del(pos);
      kfree(tmp);
      num_freed++;
      if(num_freed == num_pages_to_free) {
	break;
      }
    }

    page_map->total_num_pages = num_free_pages;    
  }

  printk(KERN_INFO "Done initializing the free page list with %ld entries\n", page_map->total_num_pages);

  return;
}

void free_hash_table(buffer_map_t *page_map) {
  struct hlist_node *pos, *n;
  page_t *tmp;
  int i, len = 0, zeros = 0, ones = 0, twos = 0, more = 0;

  if(page_map == NULL || page_map->htable == NULL) {
    printk("Why is this hash table pointer NULL\n");
  }

  for(i = 0; i < HTABLE_SIZE; i++) {
    len = 0;

    if(page_map->htable[i].first == LIST_POISON1) {
      printk(KERN_INFO "******************** this htable entry is poison1\n");
      continue;
    }else if(page_map->htable[i].first == LIST_POISON2) {
      printk(KERN_INFO "******************** this htable entry is poison2\n");
      continue;
    }

    /* Lookup the hash table entry and then search the chain */
    hlist_for_each_safe(pos, n, &page_map->htable[i]) {
      tmp = hlist_entry(pos, page_t, hchain);
      hlist_del(pos);
      free_tagged_page_meta_data(tmp);
      kfree(tmp);
      len++;
    }
    if(len == 0) {
      zeros++;
    }else if(len == 1) {
      ones++;
    }else if(len == 2) {
      twos++;
    }else {
      more++;
    }
/*     if(len > 1) { */
/*       printk("For the hashtable entry %d there were %d chained pages\n", i, len); */
/*     } */
  }

  kfree(page_map->htable);

  len = 0;
  hlist_for_each_safe(pos, n, &page_map->free_pages) {
    tmp = hlist_entry(pos, page_t, hchain);
    hlist_del(pos);
    free_tagged_page_meta_data(tmp);
    kfree(tmp);
    len++;
  }

  printk("For the hashtable there were 0=%d 1=%d 2=%d m=%d chained pages and %d free pages\n", zeros, ones, twos, more, len);
}

/* Hash function taken from
 * http://www.concentric.net/~ttwang/tech/inthash.htm
 * Which derived it from Robert Jenkins' work
 */
static inline unsigned long hash64shift(unsigned long val) {
  val = (~val) + (val << 21); /* val = (val << 21) - val - 1; */
  val = val ^ (val >> 24); /* logical right shift */
  val = (val + (val << 3)) + (val << 8); /* val * 265 */
  val = val ^ (val >> 14); /* logical right shift */
  val = (val + (val << 2)) + (val << 4); /* val * 21 */
  val = val ^ (val >> 28); /* logical right shift */
  val = val + (val << 31);
 return val;
}

/* Remove the low order 9 bits since they are used for bank select */
static inline unsigned long bank_hash(unsigned long val) {
  unsigned long key = ((val >> (9 + 6)) << 9) | (val & 0x1ff);
  return key;
  //  return val >> 9;
}

/* Combine the lower 16 bits of the device id with the lower 48 bits of the address */
static inline unsigned long compute_key(pgoff_t address, dev_t dev_id) {
  unsigned long key1 = bank_hash((((unsigned long) dev_id) << 48) | address);
/*   unsigned long key0 = hash64shift((((unsigned long) dev_id) << 48) | address); */
/*   unsigned long key1 = hash64shift(key0); */
  return key1 & HKEY_MASK;
/*   unsigned long key0 = hash_64((((unsigned long) dev_id) << 48) | address, 64); */
/*   unsigned long key1 = hash_64(key0, HKEY_SIZE); */
/*   return key1; */
}

/* void add_page_to_htable(struct hlist_head *htable, page_t *tagged_page) { */
/*   unsigned long key = compute_key(tagged_page->page_offset, tagged_page->dev_id); */
/*   hlist_add_head(&tagged_page->hchain, &htable[key]); */
/*   return; */
/* } */

/* BVE FIXME the tagged page is a hack */
page_t *find_page_in_htable(buffer_map_t *page_map, pgoff_t page_offset, dev_t dev_id) {
  /* Combine the address and device id as the input to the key space */
  /* Hash the result and truncate the hash key to log2(HTABLE_SIZE) */
  unsigned long key = compute_key(page_offset, dev_id);
  struct hlist_node *pos;
  page_t *tmp, *tagged_page = NULL;
  int len = 0;

  /* Lookup the hash table entry and then search the chain */
  hlist_for_each(pos, &page_map->htable[key]) {
    tmp = hlist_entry(pos, page_t, hchain);
    if(tmp != NULL && tmp->page_offset == page_offset && tmp->dev_id == dev_id) {
      tagged_page = tmp;
    }
    len++;
  }
  
  if(DEBUG_HASH_TABLE && len > 2) {
    printk("The chain for address %lx dev %d (key=%lx) was %d\n", page_offset, dev_id, key, len);
    hlist_for_each(pos, &page_map->htable[key]) {
      tmp = hlist_entry(pos, page_t, hchain);
      if(tmp != NULL) {
	printk("pages are address %lx dev %d (key=%lx)\n", tmp->page_offset, tmp->dev_id, key);
      }else {
	printk("How is this null\n");
      }
    }
  }

  /* BVE FIXME - fetch a blank page from the free list */
  if(tagged_page == NULL) {
/*     printk(KERN_ALERT "I was unable to find the page for addr=%lx dev=%d, get a new one off of the free list\n", page_offset, dev_id); */
    BUG_ON(hlist_empty(&page_map->free_pages));
    if(!hlist_empty(&page_map->free_pages)) {
/* 	 printk(KERN_ALERT "The free list is not empty\n"); */
    /* Take a tagged page structure off of the free list and put it at the head of the htable */
	 /* Select the head of the free list */
    tagged_page = hlist_entry(page_map->free_pages.first, page_t, hchain);

    /* Remove it from the free list */
    hlist_del_init(&tagged_page->hchain);
    //    printk(KERN_ALERT "I have removed page %p from the free list and 
    hlist_add_head(&tagged_page->hchain, &(page_map->htable[key]));

    BUG_ON(tagged_page == NULL);

/*     if(tagged_page == NULL) { */
/*       printk(KERN_INFO "Hmm, I didn't find a page here \n"); */
/*     }else { */
    tagged_page->page_valid = 0;
    tagged_page->page_offset = page_offset;
    tagged_page->dev_id = dev_id;
    if(tagged_page->page != NULL) {
      /* Set the page's index field here as well */
      tagged_page->page->index = page_offset;
    }

    /* Do we need to reinit the meta data */
/* 	init_tagged_page_meta_data(tagged_page); */
/* 	tagged_page->page = page; */
/* 	tagged_page->page_offset = idx; */
/* 	tagged_page->dev_id = crd->crd_number; */

    /* Where can I get the device and associated counters. */
/* 	tagged_page->dev = crd; */
/* 	tagged_page->perf_cntr = &crd->perf_cntr; */

/*     } */
/*        printk(KERN_ALERT "Return an empty page\n"); */
    }else{
      printk(KERN_ALERT "NO empty page****************************************\n");
    }
/*     hlist_add_head(&tagged_page->hchain, &htable[key]); */
/*     page = tagged_page; */

  }
  return tagged_page;
}

void remove_page_from_htable(buffer_map_t *page_map, page_t *tagged_page) {
  hlist_del_init(&tagged_page->hchain);
  hlist_add_head(&tagged_page->hchain, &(page_map->free_pages));

/*   unsigned long key = compute_key(tagged_page->page_offset, tagged_page->dev_id); */
/*   struct hlist_node *pos, *n; */
/*   page_t *tmp; */

/*   /\* Lookup the hash table entry and then search the chain *\/ */
/*   hlist_for_each_safe(pos, n, htable[key]) { */
/*     tmp = hlist_entry(pos, page_t, hchain); */
/*     if(tmp != NULL && tmp == tagged_page) { */
/*       hlist_del(pos); */
/*     } */
/*   } */

  return;
}

int scan_hash_table(buffer_map_t *page_map, void (*fn)(void *, page_t *), void *data) {
  struct hlist_node *pos;
  page_t *tagged_page = NULL;
  int i = 0;

  for(i = 0; i < HTABLE_SIZE; i++) {
    /* Lookup the hash table entry and then search the chain */
    hlist_for_each(pos, &page_map->htable[i]) {
      tagged_page = hlist_entry(pos, page_t, hchain);

      if(tagged_page != NULL && fn != NULL) {
	fn(data, tagged_page);
      }
    }
  }
  return 0;
}
