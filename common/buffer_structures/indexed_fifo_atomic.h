/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _INDEXED_FIFO_H_
#define _INDEXED_FIFO_H_

#include "tagged_page.h"
#include "multi_level_array.h"
#include "mmap_buffer_hash_table.h"

typedef unsigned long fidx_t;

#define MAX_NUM_THREADS_BEFORE_WARNING 256
enum fifo_mode_t {FIFO=1, WINDOWED_FIFO=2, CIRCULAR_BUFFER=3};

struct fifo_workqueue_request_t {
#ifdef RHEL6
  struct work_struct q_work;
#endif
  prot_vm_area_set_t *active_vmas;
  buffer_map_t *page_map;
  int entries_to_clear;
/*   void *channel_stats; /\* BVE TYPE HACK *\/ */
//  indexed_fifo_t *fifo_data;
  void *fifo_data; /* BVE TYPE HACK */
  struct mutex *buffer_lock;
};

typedef struct indexed_fifo_struct {
  multi_level_array_t *buffer; /* page_t ** */
  long allocated_size;
  enum fifo_mode_t mode; /* What mode is this fifo / buffer executing in */
  atomic64_t *head; /* Youngest page: Insert new pages at the head */
  atomic64_t *tail; /* Oldest page: Flush old pages from the tail */

  /* Provide a next tail pointer and a window that will be used to batch clear entries */
  atomic64_t *next_tail; /* Older page that is the next location to flush */
  unsigned long clear_ahead_window;

  /* Given the following counters we can compute the average number of faults per page, which is used to calculate if a page is hot */
  atomic64_t total_page_fault_count;

  int id;
  int active;

  struct mutex fifo_mutex; /* Create a mutex in case it is needed */
  //  struct rw_semaphore fifo_rwsem; /* Create a read-write semaphore to allow multiple readers and one writer */

  /* Create a workqueue that can clear the fifo if necessary */
  struct workqueue_struct *fifo_workqueue;
#ifndef RHEL6
  struct work_struct *fifo_workqueue_work_request; /* Dynamically allocate storage since we cannot get enough statically */
#endif
  struct fifo_workqueue_request_t *fifo_workqueue_request_data; /* Dynamically allocate storage since we cannot get enough statically */
}indexed_fifo_t;

struct indexed_fifo_operations_struct {
  page_t *(*pop)(indexed_fifo_t *fifo_data);
  page_t *(*peek)(indexed_fifo_t *fifo_data, fidx_t index);
  page_t *(*peek_tail)(indexed_fifo_t *fifo_data);
  page_t *(*poke)(indexed_fifo_t *fifo_data, fidx_t index);
  fidx_t (*allocate_entry)(indexed_fifo_t *fifo_data);
  int (*insert)(indexed_fifo_t *fifo_data, page_t *tagged_page, fidx_t index);
  page_t *(*swap_entries)(indexed_fifo_t *fifo_data, page_t *tagged_page, fidx_t index);
  void (*reset_pointers)(indexed_fifo_t *fifo_data);
  int (*init)(indexed_fifo_t *fifo_data, unsigned long size, int fifo_id, enum fifo_mode_t fifo_mode);
  void (*free)(indexed_fifo_t *fifo_data);
  fidx_t (*compute_offset_to_clear)(indexed_fifo_t *fifo_data);
  fidx_t (*compute_index_to_clear)(indexed_fifo_t *fifo_data, fidx_t index);
  fidx_t (*advance_tail_ptr_by_window)(indexed_fifo_t *fifo_data);
  fidx_t (*advance_next_tail_ptr_by_window)(indexed_fifo_t *fifo_data);
  //  static inline int (*fifo_pointers_valid)(unsigned long head, unsigned long tail, unsigned long size);

  int (*num_empty_spaces)(indexed_fifo_t *fifo_data);
  int (*num_empty_spaces_between_indexed_head_and_tail)(indexed_fifo_t *fifo_data, fidx_t indexed_head);
  int (*num_empty_spaces_between_indexed_head_and_next_tail)(indexed_fifo_t *fifo_data, fidx_t indexed_head);
  long (*num_valid_entries)(indexed_fifo_t *fifo_data);

  void (*display_pointers)(indexed_fifo_t *fifo_data, char *str);
  int (*fifo_valid)(indexed_fifo_t *fifo_data);
  //  int (*compute_fifo_full_using_indexed_head)(indexed_fifo_t *fifo_data, fidx_t indexed_head, int warn_on_full);
  int (*fifo_full)(indexed_fifo_t *fifo_data, int warn_on_full);
  int (*fifo_empty)(indexed_fifo_t *fifo_data);
};


extern struct indexed_fifo_operations_struct fifo_operations;

#endif // _INDEXED_FIFO_H_
