/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/module.h>
#include <linux/errno.h>
#include "fifo_common_ops.h"

/* Query the state of the fifo or check that pointers are valid */

inline long distance_between_head_and_tail(unsigned long head, unsigned long tail) {
  long distance = 0;
  if(head >= tail) {
    distance = head - tail;
  }else { /* head < tail */
    distance = head + (ULONG_MAX - tail);
  }
  return distance;
}

/* static inline long distance_between_tail_and_head(unsigned long head, unsigned long tail) { */
/*   long distance = 0; */
/*   if(head >= tail) { */
/*     distance = (ULONG_MAX - head) + tail;  /\* BVE FIXME this is wrong *\/ */
/*   }else { /\* head < tail *\/ */
/*     distance = tail - head; */
/*   } */
/*   return distance; */
/* } */

/*
 * Have the pointers wrapped around somehow
 */
inline int fifo_pointers_valid(unsigned long head, unsigned long tail, unsigned long size) {
  unsigned long head_idx = head % size;
  unsigned long head_color = head / size;
  unsigned long tail_idx = tail % size;
  unsigned long tail_color = tail / size;
  long head_tail_dist = distance_between_head_and_tail(head, tail);

  /* When allowing parallel access to the head of the fifo, the head can be size + # threads entries away from the tail pointer.
   * If it were size away, then the fifo is full, but there could be future head slots allocated for each thread.
   */
  //  if((head > tail && head - tail > size) || (head < tail && head + (ULONG_MAX - tail) > size)) {
  if(head_tail_dist > size + MAX_NUM_THREADS_BEFORE_WARNING) {
    printk(KERN_INFO "The head pointer has separated from the tail pointer: h=%lu(%lu) t=%lu(%lu) s=%lu\n", head, head_idx, tail, tail_idx, size);
    return 0;
/*   }else if(head_idx > tail_idx && head_color != tail_color) { */
/*     printk(KERN_INFO "The head pointer has wrapped the tail pointer: h=%lu(%lu) t=%lu(%lu) s=%lu\n", head, head_idx, tail, tail_idx, size); */
/*     return 0; */
  }else if(head_idx < tail_idx && head_color == tail_color) {
    printk(KERN_INFO "The tail pointer has wrapped the head pointer: h=%lu(%lu) t=%lu(%lu) s=%lu\n", head, head_idx, tail, tail_idx, size);
    return 0;
  }else {
    //    printk(KERN_INFO "The head and tail pointer are fine h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
    return 1;
  }
}

/*
 * How much space is there between two pointers
 */
inline int num_empty_spaces_between_pointers(unsigned long head, unsigned long tail, unsigned long size) {
  unsigned long head_idx = head % size;
  unsigned long head_color = head / size;
  unsigned long tail_idx = tail % size;
  unsigned long tail_color = tail / size;
  int num_empty_spaces = 0;

  if(!fifo_pointers_valid(head, tail, size)) {
    return num_empty_spaces;
  }

  if(head_idx >= tail_idx && head_color == tail_color) {
    num_empty_spaces = tail_idx + size - head_idx;
  }else if(head_idx <= tail_idx && head_color != tail_color) {
    num_empty_spaces = tail_idx - head_idx;
  }else {
    printk(KERN_INFO "Unknown pointer state: h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
  }

  //printk(KERN_INFO "There are %d empty spaces in the fifo: h=%lu(%lu) t=%lu(%lu)\n", num_empty_spaces, head, head_idx, tail, tail_idx);
  return num_empty_spaces;
}

/*
 * Using raw pointers, check if the fifo is full.  (i.e. when the head and tail have the same index and the colors differ)
 * Calculating if the fifo is full is harder when a head pointer can be pre-assigned.
 */
inline int fifo_full_raw_pointers(unsigned long head, unsigned long tail, unsigned long size, int warn_on_full) {
  unsigned long head_idx = head % size;
/*   unsigned long head_color = head / size; */
  unsigned long tail_idx = tail % size;
/*   unsigned long tail_color = tail / size; */
  //  int is_full = head_idx >= tail_idx && head_color != tail_color;
  int is_full = (head >= tail) ? head - tail >= size : head + (ULONG_MAX - tail) >= size;
  
  if(is_full && warn_on_full) {
    printk(KERN_INFO "The fifo is full: h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
  }

  return is_full;
}


/*
 * The fifo is empty when the head and tail have the same index and the colors are the same
 */
inline int fifo_empty_raw_pointers(unsigned long head, unsigned long tail, unsigned long size) {
  unsigned long head_idx = head % size;
  unsigned long head_color = head / size;
  unsigned long tail_idx = tail % size;
  unsigned long tail_color = tail / size;
  int is_empty = head_idx == tail_idx && head_color == tail_color;

  if(is_empty) {
    printk(KERN_INFO "The fifo is empty: h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
  }

  return is_empty;
}
