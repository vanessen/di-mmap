/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/errno.h>
#include "indexed_fifo.h"
#include "rhel_compatibility.h"

/* 
 * Local function prototypes
 */
static inline int has_valid_pointers(indexed_fifo_t *fifo_data);

/* Query the state of the fifo or check that pointers are valid */

static inline long distance_between_head_and_tail(unsigned long head, unsigned long tail) {
  long distance = 0;
  if(head >= tail) {
    distance = head - tail;
  }else { /* head < tail */
    distance = head + (ULONG_MAX - tail);
  }
  return distance;
}

/* static inline long distance_between_tail_and_head(unsigned long head, unsigned long tail) { */
/*   long distance = 0; */
/*   if(head >= tail) { */
/*     distance = (ULONG_MAX - head) + tail;  /\* BVE FIXME this is wrong *\/ */
/*   }else { /\* head < tail *\/ */
/*     distance = tail - head; */
/*   } */
/*   return distance; */
/* } */

/*
 * Have the pointers wrapped around somehow
 */
static inline int fifo_pointers_valid(unsigned long head, unsigned long tail, unsigned long size) {
  unsigned long head_idx = head % size;
  unsigned long head_color = head / size;
  unsigned long tail_idx = tail % size;
  unsigned long tail_color = tail / size;
  long head_tail_dist = distance_between_head_and_tail(head, tail);

  /* When allowing parallel access to the head of the fifo, the head can be size + # threads entries away from the tail pointer.
   * If it were size away, then the fifo is full, but there could be future head slots allocated for each thread.
   */
  //  if((head > tail && head - tail > size) || (head < tail && head + (ULONG_MAX - tail) > size)) {
  if(head_tail_dist > size + MAX_NUM_THREADS_BEFORE_WARNING) {
    printk(KERN_INFO "The head pointer has separated from the tail pointer: h=%lu(%lu) t=%lu(%lu) s=%lu\n", head, head_idx, tail, tail_idx, size);
    return 0;
/*   }else if(head_idx > tail_idx && head_color != tail_color) { */
/*     printk(KERN_INFO "The head pointer has wrapped the tail pointer: h=%lu(%lu) t=%lu(%lu) s=%lu\n", head, head_idx, tail, tail_idx, size); */
/*     return 0; */
  }else if(head_idx < tail_idx && head_color == tail_color) {
    printk(KERN_INFO "The tail pointer has wrapped the head pointer: h=%lu(%lu) t=%lu(%lu) s=%lu\n", head, head_idx, tail, tail_idx, size);
    return 0;
  }else {
    //    printk(KERN_INFO "The head and tail pointer are fine h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
    return 1;
  }
}

/*
 * How much space is there between two pointers
 */
static inline int num_empty_spaces_between_pointers(unsigned long head, unsigned long tail, unsigned long size) {
  unsigned long head_idx = head % size;
  unsigned long head_color = head / size;
  unsigned long tail_idx = tail % size;
  unsigned long tail_color = tail / size;
  int num_empty_spaces = 0;

  if(!fifo_pointers_valid(head, tail, size)) {
    return num_empty_spaces;
  }

  if(head_idx >= tail_idx && head_color == tail_color) {
    num_empty_spaces = tail_idx + size - head_idx;
  }else if(head_idx <= tail_idx && head_color != tail_color) {
    num_empty_spaces = tail_idx - head_idx;
  }else {
    printk(KERN_INFO "Unknown pointer state: h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
  }

  //printk(KERN_INFO "There are %d empty spaces in the fifo: h=%lu(%lu) t=%lu(%lu)\n", num_empty_spaces, head, head_idx, tail, tail_idx);
  return num_empty_spaces;
}

/*
 * How much space is there between the head and tail pointers
 */
inline int num_empty_spaces(indexed_fifo_t *fifo_data) {
  unsigned long head = atomic64_read(fifo_data->head);
  unsigned long tail = atomic64_read(fifo_data->tail);
  unsigned long size = fifo_data->allocated_size;

  return num_empty_spaces_between_pointers(head, tail, size);
}

/*
 * How much space is there between an indexed head and the tail pointer
 */
inline int num_empty_spaces_between_indexed_head_and_tail(indexed_fifo_t *fifo_data, fidx_t indexed_head) {
  unsigned long tail = atomic64_read(fifo_data->tail);
  unsigned long size = fifo_data->allocated_size;

  return num_empty_spaces_between_pointers(indexed_head, tail, size);
}

/*
 * How much space is there between an indexed head and the next_tail pointer
 */
inline int num_empty_spaces_between_indexed_head_and_next_tail(indexed_fifo_t *fifo_data, fidx_t indexed_head) {
  unsigned long next_tail = atomic64_read(fifo_data->next_tail);
  unsigned long size = fifo_data->allocated_size;

  return num_empty_spaces_between_pointers(indexed_head, next_tail, size);
}

/*
 * How much space is there between the head and tail pointers
 */
inline long num_valid_entries(indexed_fifo_t *fifo_data) {
  unsigned long head = atomic64_read(fifo_data->head);
  unsigned long tail = atomic64_read(fifo_data->tail);
  unsigned long size = fifo_data->allocated_size;
/*   unsigned long head_idx = head % size; */
/*   unsigned long head_color = head / size; */
/*   unsigned long tail_idx = tail % size; */
/*   unsigned long tail_color = tail / size; */
  long num_valid_entries = 0;
  unsigned long head_tail_distance = 0;
  
  if(!fifo_pointers_valid(head, tail, size)) {
    return num_valid_entries;
  }

  /* The number of valid entries is at most the distance between the head and tail pointers, or the size if the head is speculative */
  head_tail_distance = distance_between_head_and_tail(head, tail);
  num_valid_entries = min(head_tail_distance, size);

/*   if(head_idx >= tail_idx && head_color == tail_color) { */
/*     num_valid_entries = head_idx - tail_idx; */
/*   }else if(head_idx <= tail_idx && head_color == tail_color + 1) { */
/*     num_valid_entries = head_idx + size - tail_idx; */
/*   }else if(head_color >= tail_color + 1) { */
/*     num_valid_entries = head_idx + size - tail_idx; */
/*   }else { */
/*     printk(KERN_INFO "Unknown pointer state: h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx); */
/*   } */

  //printk(KERN_INFO "There are %d valid entries in the fifo: h=%lu(%lu) t=%lu(%lu)\n", num_valid_entries, head, head_idx, tail, tail_idx);
  return num_valid_entries;
}


void display_pointers(indexed_fifo_t *fifo_data, char *str) {
  unsigned long size = fifo_data->allocated_size;

  if(has_valid_pointers(fifo_data)) {
    unsigned long head = atomic64_read(fifo_data->head);
    unsigned long tail = atomic64_read(fifo_data->tail);
    unsigned long head_idx = head % size;
    unsigned long tail_idx = tail % size;

    sprintf(str, "b=%d s=%lu h=%lu(%lu) t=%lu(%lu)", fifo_data->id, size, head, head_idx, tail, tail_idx);
  }else {
    sprintf(str, "s=%lu", size);
  }
}

int fifo_valid(indexed_fifo_t *fifo_data) {
  int err1 = 0, err2 = 0;
  err1 = !fifo_pointers_valid(atomic64_read(fifo_data->head), atomic64_read(fifo_data->tail), fifo_data->allocated_size);
  err2 = !fifo_pointers_valid(atomic64_read(fifo_data->head), atomic64_read(fifo_data->tail), fifo_data->allocated_size);
  //  printk("The fifo pointers are at h=%lu:%lu t=%lu:%lu nt=%lu:%lu\n", atomic64_read(&mmap_buf_head), atomic64_read(&mmap_buf_head) % perma_mmap_buf_size, atomic64_read(&mmap_buf_tail), atomic64_read(&mmap_buf_tail) % perma_mmap_buf_size, atomic64_read(&mmap_buf_next_tail), atomic64_read(&mmap_buf_next_tail) % perma_mmap_buf_size);
  return err1 + err2;
}

/*
 * Using raw pointers, check if the fifo is full.  (i.e. when the head and tail have the same index and the colors differ)
 * Calculating if the fifo is full is harder when a head pointer can be pre-assigned.
 */
static inline int fifo_full_raw_pointers(unsigned long head, unsigned long tail, unsigned long size, int warn_on_full) {
  unsigned long head_idx = head % size;
/*   unsigned long head_color = head / size; */
  unsigned long tail_idx = tail % size;
/*   unsigned long tail_color = tail / size; */
  //  int is_full = head_idx >= tail_idx && head_color != tail_color;
  int is_full = (head >= tail) ? head - tail >= size : head + (ULONG_MAX - tail) >= size;
  
  if(is_full && warn_on_full) {
    printk(KERN_INFO "The fifo is full: h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
  }

  return is_full;
}

/* 
 * Check if the fifo is full with respect to a given head index
 */
inline int compute_fifo_full_using_indexed_head(indexed_fifo_t *fifo_data,  fidx_t indexed_head, int warn_on_full) {
  unsigned long tail = atomic64_read(fifo_data->tail);
  unsigned long size = fifo_data->allocated_size;

  return fifo_full_raw_pointers(indexed_head, tail, size, warn_on_full);
}

/*
 * The fifo is full when the head and tail have the same index and the colors differ
 */
int fifo_full(indexed_fifo_t *fifo_data, int warn_on_full) {
  unsigned long head = atomic64_read(fifo_data->head);
  unsigned long tail = atomic64_read(fifo_data->tail);
  unsigned long size = fifo_data->allocated_size;

  return fifo_full_raw_pointers(head, tail, size, warn_on_full);
}

/*
 * The fifo is empty when the head and tail have the same index and the colors are the same
 */
inline int fifo_empty(indexed_fifo_t *fifo_data) {
  unsigned long head = atomic64_read(fifo_data->head);
  unsigned long tail = atomic64_read(fifo_data->tail);
  unsigned long size = fifo_data->allocated_size;
  unsigned long head_idx = head % size;
  unsigned long head_color = head / size;
  unsigned long tail_idx = tail % size;
  unsigned long tail_color = tail / size;
  int is_empty = head_idx == tail_idx && head_color == tail_color;

  if(is_empty) {
    printk(KERN_INFO "The fifo is empty: h=%lu(%lu) t=%lu(%lu)\n", head, head_idx, tail, tail_idx);
  }

  return is_empty;
}

/* Functions to put data into the fifo and take it out. */

/* Hold off on implementing this because it is kind of tricky for a "parallel" fifo */
/* int push(page_t *tagged_page) { /\* enqueue *\/ */
/*   fidx_t head = atomic64_inc_return(&fifo_data->head)-1; */
/*   fidx_t idx = head % fifo_data->allocated_size; */
/*   int err; */

/*   /\*  */
/*    * Check to make sure that the fifo is not full before doing the insertion. */
/*    * Do not let an insertion happen if the selected index is past the tail.  */
/*    *\/ */
/*   if(fifo_full_indexed_head(fifo_data, index, 0)) { */
/*     return -1; */
/*   } */
  
/*   int err = multi_level_array_insertion((void ***) fifo_data->buffer, idx, tagged_page); */
/*   if(err) { */
/*     printk(KERN_INFO "Thread %d was unable to insert a page %p at index %lu where the head is %lu and tail is at %lu\n", current->pid, tagged_page, idx, index, fifo_data->tail); */
/*   } */

/*   return err; */

/* } */

static inline void dec_hot_page_counters(indexed_fifo_t *fifo_data, page_t *tagged_page) {
  atomic64_sub(atomic64_read(&tagged_page->num_page_faults), &fifo_data->total_page_fault_count);
  return;
}

static inline void inc_hot_page_counters(indexed_fifo_t *fifo_data, page_t *tagged_page) {
  atomic64_add(atomic64_read(&tagged_page->num_page_faults), &fifo_data->total_page_fault_count);
  return;
}

/* Note that the pop method is not thread safe */
page_t *pop(indexed_fifo_t *fifo_data) { /* dequeue */
  page_t *tagged_page = NULL;
  /* The tail index points to the oldest valid entry */
  /* You cannot increment the counter before clearing the location at the previous value, otherwise there is a race that makes
     it look like there is space when there isn't */
  fidx_t index = atomic64_read(fifo_data->tail) % fifo_data->allocated_size;

  tagged_page = multi_level_array_xchg(fifo_data->buffer, index, NULL);

  if(tagged_page != NULL) {
    dec_hot_page_counters(fifo_data, tagged_page);
  }

  /* Increment the tail pointer */
  atomic64_inc(fifo_data->tail);
  return tagged_page;
}

page_t *peek(indexed_fifo_t *fifo_data, fidx_t index) {
  page_t *tagged_page = NULL;
  fidx_t idx = index % fifo_data->allocated_size;

  tagged_page = multi_level_array_lookup(fifo_data->buffer, idx);

  return tagged_page;
}

page_t *peek_tail(indexed_fifo_t *fifo_data) {
  page_t *tagged_page = peek(fifo_data, atomic64_read(fifo_data->tail));
  return tagged_page;
}

/* 
 * Poke a hole in the buffer and return the data that was there
 */
page_t *poke(indexed_fifo_t *fifo_data, fidx_t index) {
  page_t *tagged_page = NULL;
  fidx_t idx = index % fifo_data->allocated_size;

  tagged_page = multi_level_array_xchg(fifo_data->buffer, idx, NULL);
  //  tagged_page = multi_level_array_lookup((void ***) fifo_data->buffer, index);

  if(tagged_page != NULL) {
    dec_hot_page_counters(fifo_data, tagged_page);
  }

  return tagged_page;
}

/* 
 * The head pointer points to the an invalid entry and the next slot to be used.
 * Increment the pointer and return the previous value.
 */
fidx_t allocate_entry(indexed_fifo_t *fifo_data) {
  return atomic64_inc_return(fifo_data->head) - 1;
}

int insert(indexed_fifo_t *fifo_data, page_t *tagged_page, fidx_t index) {
  fidx_t idx = index % fifo_data->allocated_size;
  int err;

  /* 
   * Check to make sure that the fifo is not full before doing the insertion.
   * Do not let an insertion happen if the selected index is past the tail. 
   */
  if(compute_fifo_full_using_indexed_head(fifo_data, index, 0)) {
    return -1;
  }
  
  err = multi_level_array_insertion(fifo_data->buffer, idx, tagged_page);
  if(err) {
    printk(KERN_INFO "Thread %d was unable to insert a page %p at index %lu where the head is %lu and tail is at %lu\n", current->pid, tagged_page, idx, index, atomic64_read(fifo_data->tail));
  }else {
    if(tagged_page != NULL) {
      inc_hot_page_counters(fifo_data, tagged_page);
    }
  }

  return err;
}

/* 
 * This function will replace an old entry in the fifo with the new one, and return the old one.
 * Using this you can implement a singled indexed fifo (i.e. head only).
 */
page_t *swap_entries(indexed_fifo_t *fifo_data, page_t *tagged_page, fidx_t index) {
  fidx_t idx = index % fifo_data->allocated_size;
  page_t *old_tagged_page;

  old_tagged_page = multi_level_array_xchg(fifo_data->buffer, idx, tagged_page);

  if(old_tagged_page != NULL) {
    dec_hot_page_counters(fifo_data, old_tagged_page);
  }
  if(tagged_page != NULL) {
    inc_hot_page_counters(fifo_data, tagged_page);
  }

  return old_tagged_page;
}

static inline int fifo_mode_valid(indexed_fifo_t *fifo_data) {
  int valid = 0;
  if(fifo_data->buffer != NULL || fifo_data->allocated_size != 0) {
    /* If the fifo has a valid buffer or size, its mode must be valid */
    if(fifo_data->mode != FIFO && fifo_data->mode != WINDOWED_FIFO && fifo_data->mode != CIRCULAR_BUFFER) {
      printk("Invalid fifo mode %d for buffer %d\n", fifo_data->mode, fifo_data->id);
      //    BUG_ON(fifo_data->mode != FIFO && fifo_data->mode != WINDOWED_FIFO && fifo_data->mode != CIRCULAR_BUFFER);
    }else {
      valid = 1;
    }    
  }
  return valid;
}

static void init_pointers(indexed_fifo_t *fifo_data) {
  fifo_mode_valid(fifo_data);
  switch(fifo_data->mode) {
  case FIFO: {
    fifo_data->head = kmalloc(sizeof(atomic64_t), GFP_KERNEL);
    fifo_data->tail = kmalloc(sizeof(atomic64_t), GFP_KERNEL);
    fifo_data->next_tail = NULL;
    //    fifo_data->next_tail = fifo_data->tail; /* Next tail is synonymous with tail */
    break;
  }
  case WINDOWED_FIFO: {
    fifo_data->head = kmalloc(sizeof(atomic64_t), GFP_KERNEL);
    fifo_data->tail = kmalloc(sizeof(atomic64_t), GFP_KERNEL);
    fifo_data->next_tail = kmalloc(sizeof(atomic64_t), GFP_KERNEL);
    break;
  }
  case CIRCULAR_BUFFER: {
    fifo_data->head = kmalloc(sizeof(atomic64_t), GFP_KERNEL);
    fifo_data->tail = fifo_data->head; /* Tail and head are the same counter */
    fifo_data->next_tail = fifo_data->tail; /* Next tail is synonymous with tail */
    break;
  }
  }
  return;
}

static void free_pointers(indexed_fifo_t *fifo_data) {
  fifo_mode_valid(fifo_data);

  switch(fifo_data->mode) {
  case FIFO: {
    kfree(fifo_data->head);
    kfree(fifo_data->tail);
    break;
  }
  case WINDOWED_FIFO: {
    kfree(fifo_data->head);
    kfree(fifo_data->tail);
    kfree(fifo_data->next_tail);
    break;
  }
  case CIRCULAR_BUFFER: {
    kfree(fifo_data->head);
    break;
  }
  }
  fifo_data->head = NULL;
  fifo_data->tail = NULL;
  fifo_data->next_tail = NULL;
  return;
}


static inline int has_valid_pointers(indexed_fifo_t *fifo_data) {
  int pointers_valid = 0;
  int mode_valid = fifo_mode_valid(fifo_data);

  switch(fifo_data->mode) {
  case FIFO: {
    pointers_valid = (fifo_data->head != NULL) && (fifo_data->tail != NULL) && (fifo_data->head != fifo_data->tail);
    break;
  }
  case WINDOWED_FIFO: {
    pointers_valid = (fifo_data->head != NULL) && (fifo_data->tail != NULL) && (fifo_data->head != fifo_data->tail) && (fifo_data->tail != fifo_data->next_tail);
    break;
  }
  case CIRCULAR_BUFFER: {
    pointers_valid = (fifo_data->head != NULL) && (fifo_data->head == fifo_data->tail);
    break;
  }
  }
  if(mode_valid && !pointers_valid) {
    printk("Pointers are invalid(%d) for buffer %d with mode %d: h=%p t=%p nt=%p\n", pointers_valid, fifo_data->id, fifo_data->mode, fifo_data->head, fifo_data->tail, fifo_data->next_tail);
  }
  return pointers_valid;
}

void reset_pointers(indexed_fifo_t *fifo_data) {
  if(has_valid_pointers(fifo_data)) {
    switch(fifo_data->mode) {
    case FIFO: {
      atomic64_set(fifo_data->head, 0);
      atomic64_set(fifo_data->tail, 0);
      break;
    }
    case WINDOWED_FIFO: {
      atomic64_set(fifo_data->head, 0);
      atomic64_set(fifo_data->tail, 0);
      atomic64_set(fifo_data->next_tail, 0);
      break;
    }
    case CIRCULAR_BUFFER: {
      atomic64_set(fifo_data->head, 0);
      break;
    }
    }
  }
  atomic64_set(&fifo_data->total_page_fault_count, 0);
  return;
}

int init(indexed_fifo_t *fifo_data, unsigned long size, int fifo_id, enum fifo_mode_t fifo_mode) {
  fifo_data->buffer = multi_level_array_initialization(size);
  if(fifo_data->buffer == NULL) {
    fifo_data->allocated_size = 0;
    printk(KERN_INFO "PerMA: unable to allocate a buffer with %ld entries, setting buffer size to zero\n", size);
    return -ENOMEM;
  }

  mutex_init(&fifo_data->fifo_mutex);
  fifo_data->id = fifo_id;
  fifo_data->mode = fifo_mode;
  fifo_data->allocated_size = size;
  if(!has_valid_pointers(fifo_data)) {
    init_pointers(fifo_data);
  }
  reset_pointers(fifo_data);
  printk(KERN_INFO "PerMA: allocated a buffer (mode=%d) for fifo %d with %ld entries\n", fifo_data->mode, fifo_data->id, size);
  return 0;
}

void free(indexed_fifo_t *fifo_data) {
  multi_level_array_free(fifo_data->buffer);
  fifo_data->buffer = NULL;
  fifo_data->allocated_size = 0;
  free_pointers(fifo_data);
  fifo_data->mode = 0;
  reset_pointers(fifo_data);
  return;
}

inline fidx_t compute_offset_to_clear(indexed_fifo_t *fifo_data) {
  fidx_t offset = 2 * fifo_data->clear_ahead_window;
  return offset;
}

fidx_t compute_index_to_clear(indexed_fifo_t *fifo_data, fidx_t index) {
  fidx_t offset = compute_offset_to_clear(fifo_data);
  fidx_t clearing_index = index + offset;
  return clearing_index;
}

fidx_t advance_tail_ptr_by_window(indexed_fifo_t *fifo_data) {
  fidx_t new_tail_idx;

  new_tail_idx = atomic64_add_return(fifo_data->clear_ahead_window, fifo_data->tail) % fifo_data->allocated_size;
  return new_tail_idx;
}


fidx_t advance_next_tail_ptr_by_window(indexed_fifo_t *fifo_data) {
  fidx_t new_next_tail_idx;

  new_next_tail_idx = atomic64_add_return(fifo_data->clear_ahead_window, fifo_data->next_tail) % fifo_data->allocated_size;
  return new_next_tail_idx;
}


struct indexed_fifo_operations_struct fifo_operations = {
  .pop = pop,
  .peek = peek,
  .peek_tail = peek_tail,
  .poke = poke,
  .allocate_entry = allocate_entry,
  .insert = insert,
  .swap_entries = swap_entries,
  .reset_pointers = reset_pointers,
  .init = init,
  .free = free,
  .compute_offset_to_clear = compute_offset_to_clear,
  .compute_index_to_clear = compute_index_to_clear,
  .advance_tail_ptr_by_window = advance_tail_ptr_by_window,
  .advance_next_tail_ptr_by_window = advance_next_tail_ptr_by_window,
  //  .fifo_pointers_valid = fifo_pointers_valid,

  .num_empty_spaces = num_empty_spaces,
  .num_empty_spaces_between_indexed_head_and_tail = num_empty_spaces_between_indexed_head_and_tail,
  .num_empty_spaces_between_indexed_head_and_next_tail = num_empty_spaces_between_indexed_head_and_next_tail,
  .num_valid_entries = num_valid_entries,

  .display_pointers = display_pointers,
  .fifo_valid = fifo_valid,
  //  .compute_fifo_full_using_indexed_head = compute_fifo_full_using_indexed_head,
  .fifo_full = fifo_full,
  .fifo_empty = fifo_empty,
};
