/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/mm.h>		/* everything */
#include <asm/tlbflush.h>

#include "tagged_page.h"
#include "helpers.h"
#include "mmap_fault_handler.h"
#include "rhel_compatibility.h"

void clear_tagged_page_meta_data(page_t *tagged_page) {
  tagged_page->page_valid = 0;
  tagged_page->page_offset = 0;
  tagged_page->dev_id = 0;

  tagged_page->timestamp = 0;
  tagged_page->last_timestamp = 0;
  tagged_page->buffer_id = 0;
  tagged_page->last_buffer_id = 0;
  tagged_page->num_page_faults = 0;
  tagged_page->is_writable = 0;

  INIT_HLIST_NODE(&tagged_page->hchain);

  return;
}

void init_tagged_page_meta_data(page_t *tagged_page) {
  int i;
  vm_area_set_p *tmp;
  gfp_t gfp_flags;

  clear_tagged_page_meta_data(tagged_page);
  tagged_page->num_vmas = 0;
  for(i = 0; i < MAX_VMAS_PER_PAGE; i++) {
    tmp = &tagged_page->vma_ptr_pool[i];
    tmp->vmap = NULL;
    tmp->tlb_flush_cntr_ss = -1;
  }

  /*
   * Must use NOIO because we don't want to recurse back into the
   * block or filesystem layers from page reclaim.
   *
   * Cannot support XIP and highmem, because our ->direct_access
   * routine for XIP must return memory that is always addressable.
   * If XIP was reworked to use pfns and kmap throughout, this
   * restriction might be able to be lifted.
   */
  gfp_flags = GFP_NOIO | __GFP_ZERO |  __GFP_HIGHMEM;

#ifdef SIM_NO_PAGE_COPY /* Simulator does not use page copy */
  tagged_page->page = NULL;
#else
  /* Allocate a page for storage */
  tagged_page->page = alloc_pages_current(gfp_flags, ALLOC_PAGES_ORDER);
  //	page = alloc_pages(gfp_flags, ALLOC_PAGES_ORDER);
  //	page = alloc_pages_node(numa_node, gfp_flags, ALLOC_PAGES_ORDER);

  if(tagged_page->page == NULL) {
    printk("Allocation for tagged page failed\n");
  }
  BUG_ON(tagged_page->page == NULL);
#endif

  tagged_page->filp = NULL;

  return;
}

void free_tagged_page_meta_data(page_t *tagged_page) {

#ifdef SIM_NO_PAGE_COPY /* Simulator does not use page copy */
  BUG_ON(tagged_page->page != NULL);
#else
  /* Free the allocated page */
  __free_pages(tagged_page->page, ALLOC_PAGES_ORDER);
#endif

  return;
}

void retire_inactive_vm_area_pointers(page_t *tagged_page) {
  int i;
  vm_area_set_p *vma_set_ptr = NULL;

  for(i = 0; i < tagged_page->num_vmas; i++) {
    vma_set_ptr = &tagged_page->vma_ptr_pool[i];
    
    if(vma_set_ptr->vmap != NULL && vma_set_ptr->vmap->active == 0) {
      /* Clear the VMA set entry */
      retire_vmap(vma_set_ptr, tagged_page);
    }
  }
  return;
}

void display_tagged_page_meta_data(page_t *tagged_page, char *str) {
  sprintf(str, "[current b=%d ts=%lu old b=%d ts=%lu]", 
	  tagged_page->buffer_id, tagged_page->timestamp, tagged_page->last_buffer_id, tagged_page->last_timestamp);
  return;
}

static inline int remove_vma_mapping(page_t *tagged_page_to_clear, struct vm_area_struct *vma) {
  int mapping_removed = 0;

  //    if(!check_vma_page_table(vma)) { /* If the vma is closing then there will not be a valid page table (i.e. no PGD) */
      int err = zap_page_from_vma(vma, tagged_page_to_clear->page);
      if(err) {
	printk(KERN_INFO "Thread %d was unable to properly zap page %p(%lx), returned error %d ", current->pid, page_address(tagged_page_to_clear->page), vma_address(tagged_page_to_clear->page, vma), err);
	//	printk(KERN_INFO "Thread %d was unable to properly zap page %p at location %lu (head %lu future_tail %lu), returned error %d ", current->pid, page_address(tagged_page_to_clear->page), atomic64_read(&mmap_buf_tail), atomic64_read(&mmap_buf_head), atomic64_read(&mmap_buf_next_tail), err);
	if(err == -EFAULT) {
	  printk(KERN_INFO "page is not mapped into this VMA\n");
	}else if(err == 1) {
	  printk(KERN_INFO "page does not exist in page table\n");
	}else if(err == 2) {
	  printk(KERN_INFO "vma is NULL\n");
	}else {
	  printk("\n");
	}
      }else {
	mapping_removed = 1;
      }
      //    }

    return mapping_removed;
}

int unmap_tagged_page(page_t *tagged_page_to_clear, atomic64_t *num_write_eviction_races) {
    unsigned long curr_timestamp = tagged_page_to_clear->timestamp;
    char addresses[MAX_STR_LEN] = "", err_msg1[MAX_STR_LEN] = "", err_msg2[MAX_STR_LEN] = "";
    unsigned short page_is_writable;
    vm_area_set_p *vma_set_ptr;
    int was_page_in_all_ptes = 0, mapping_removed = 0;
    int i, num_active_vmas = tagged_page_to_clear->num_vmas;
    /* Pull the channel stats for the device that this page is associated with */
    //    channel_profile_info *channel_stats = &tagged_page_to_clear->perf_cntr->read;

    /* Get the list of possible addresses for each vma */
    vma_addresses(tagged_page_to_clear, addresses);
/*     printk("Thread %d is unmapping a page %p with addresses (%s)\n", current->pid, page_address(tagged_page_to_clear->page), addresses); */

    /* Reread a few variables now that we have the lock for the tagged page */
    page_is_writable = tagged_page_to_clear->is_writable;
    curr_timestamp = tagged_page_to_clear->timestamp;
    /* Check to see if the page was all PTEs for each VMA */
    was_page_in_all_ptes = page_in_all_ptes(tagged_page_to_clear);
    sprintf(err_msg1, "CPU %d : Why is this page %p (address=%s) loc=%lu", GET_CPU_ID(), page_address(tagged_page_to_clear->page), addresses, curr_timestamp);
    //    sprintf(err_msg2, "Do I have a valid page table %d or channel %d and why is the flag %hu\n", had_valid_page_tables, channel_stats != NULL, page_is_writable);

/*     printk(KERN_ALERT "I am going to unmap a page %s\n", addresses); */

    for(i = 0; i < num_active_vmas; i++) {
      vma_set_ptr = &tagged_page_to_clear->vma_ptr_pool[i];

      if(vma_set_ptr->vmap != NULL) {

	/*       if(PageDirty(tagged_page_to_clear->page)) { */
	/* 	printk(KERN_ALERT "EARLY writeback of page %p\n", tagged_page_to_clear->page); */
	/* 	write_page(vma_set_ptr->vmap->vma, tagged_page_to_clear->filp, tagged_page_to_clear->dev, (tagged_page_to_clear->page_offset << PAGE_SHIFT), tagged_page_to_clear->page); */
	/*       } */

	/* Remove the page from the PTE first, then writeback any dirty pages */
	mapping_removed = remove_vma_mapping(tagged_page_to_clear, vma_set_ptr->vmap->vma);
	/*       if(mapping_removed && channel_stats != NULL) { */
	/* 	atomic64_inc(&channel_stats->num_zapped_page_ranges); */
	/*       } */
      
	/* BVE FIXME There should be a check here to make sure that the mapping was removed */

	/* Update the tlb flush counter on the VMA set entry */
#if 1
#ifdef ATOMIC_TLB_CNTR
	vma_set_ptr->tlb_flush_cntr_ss = atomic64_read(&vma_set_ptr->vmap->global_cntr->tlb_flush_cntr);
#else
	vma_set_ptr->tlb_flush_cntr_ss = vma_set_ptr->vmap->global_cntr->tlb_flush_cntr;
#endif
#else
	/* Clear the VMA set entry */
	retire_vmap(vma_set_ptr, tagged_page_to_clear);
#endif
      }
    }

    /* Once all of the page tables are cleared, check the dirty status of the page */
    if(page_is_writable && !PageDirty(tagged_page_to_clear->page)) {
      if(!was_page_in_all_ptes) {
	/* Race has occurred between taking a page from being read-only to read-write and it being evicted from the buffer.
	   Just continue with the eviction and the write fault will re-fault. */
	atomic64_inc(num_write_eviction_races);
	/* Quietly decrement the writable counter back to zero */
	tagged_page_to_clear->is_writable--;
      }else {
	printk("%s writable, but not dirty.  %s\n", err_msg1, err_msg2);
	tagged_page_to_clear->is_writable = 42;
	printk("Changed the writable tag to poison value\n");
      }
    }

    return 0;
}

/* Note: the semaphore for the VMA linked list has to be held */
int clear_tagged_pages_pending_vmas(page_t *tagged_page_to_clear/* , atomic64_t *num_write_eviction_races */) {
    vm_area_set_p *vma_set_ptr;
    int i, num_active_vmas = tagged_page_to_clear->num_vmas;

    /* There is no need to lock the VMA TLB counter list since the TLB flush counter is not accessed if the 
       VMA is no longer active.  This allows the buffer to deallocate the TLB counter structure when the
       process is closed and there are still pages that might reference the stale counter. */
    for(i = 0; i < num_active_vmas/* MAX_VMAS_PER_PAGE */; i++) {
      vma_set_ptr = &tagged_page_to_clear->vma_ptr_pool[i];

      if(vma_set_ptr->vmap != NULL && vma_set_ptr->vmap->active) {
#ifdef ATOMIC_TLB_CNTR
	if(vma_set_ptr->tlb_flush_cntr_ss >= atomic64_read(&vma_set_ptr->vmap->global_cntr->tlb_flush_cntr)) {
	  if(vma_set_ptr->tlb_flush_cntr_ss > atomic64_read(&vma_set_ptr->vmap->global_cntr->tlb_flush_cntr)) {
	    printk(KERN_ALERT "ERORR: page %p I think that I need to flush the tlb %p because the counters are %lx and %lx\n", 
		   tagged_page_to_clear, vma_set_ptr->vmap->vma, vma_set_ptr->tlb_flush_cntr_ss, atomic64_read(&vma_set_ptr->vmap->global_cntr->tlb_flush_cntr));
	  }
	  //	BUG_ON(vma_set_ptr->tlb_flush_cntr_ss > atomic64_read(&vma_set_ptr->vmap->global_cntr->tlb_flush_cntr));
	  //	printk(KERN_ALERT "I think that I need to flush the tlb because the counters are %lx and %lx\n", vma_set_ptr->tlb_flush_cntr_ss, atomic64_read(&vma_set_ptr->vmap->global_cntr->tlb_flush_cntr));

#ifndef PTE_CLEAR_FLUSH
	  /* BVE FIXME put in a lock to the active vmas to make sure that they don't go away */
	  flush_tlb_mm(vma_set_ptr->vmap->vma->vm_mm);
	  atomic64_inc(&vma_set_ptr->vmap->global_cntr->tlb_flush_cntr);
#endif
	}
#else
	if(vma_set_ptr->tlb_flush_cntr_ss >= vma_set_ptr->vmap->global_cntr->tlb_flush_cntr) {
	  //	BUG_ON(vma_set_ptr->tlb_flush_cntr_ss > vma_set_ptr->vmap->global_cntr->tlb_flush_cntr);
	  if(vma_set_ptr->tlb_flush_cntr_ss > vma_set_ptr->vmap->global_cntr->tlb_flush_cntr) {
	    printk(KERN_ALERT "ERORR: I think that I need to flush the tlb because the counters are %lx and %lx\n", vma_set_ptr->tlb_flush_cntr_ss, vma_set_ptr->vmap->global_cntr->tlb_flush_cntr);
	  }
	  //printk(KERN_ALERT "I think that I need to flush the tlb because the counters are %lx and %lx\n", vma_set_ptr->tlb_flush_cntr_ss, vma_set_ptr->vmap->global_cntr->tlb_flush_cntr);
#ifndef PTE_CLEAR_FLUSH
	  flush_tlb_mm(vma_set_ptr->vmap->vma->vm_mm);
	  vma_set_ptr->vmap->global_cntr->tlb_flush_cntr++;
#endif
	}
#endif
      }

      remove_vmap(vma_set_ptr, tagged_page_to_clear);
    }

    /* Make sure that the page no longer has any VMA mappings */
    /* BUG_ON */if(tagged_page_to_clear->num_vmas != 0) {
      printk(KERN_ALERT "This should be a bug\n");
    }
    return 0;
}

int recover_tagged_pages_pending_vmas(page_t *tagged_page_to_clear) {
    vm_area_set_p *vma_set_ptr;
    int i, num_active_vmas = tagged_page_to_clear->num_vmas;

    for(i = 0; i < num_active_vmas; i++) {
      vma_set_ptr = &tagged_page_to_clear->vma_ptr_pool[i];

      if(vma_set_ptr->vmap != NULL && !vma_set_ptr->vmap->active) {
	printk(KERN_ALERT "What should I do with this entry %p\n", vma_set_ptr->vmap->vma);
      }
      /* Clear the tlb flush counter snapshot */
      if(vma_set_ptr->tlb_flush_cntr_ss != -1) {
	/* 	printk(KERN_ALERT "PID=%ld - For page %p offset %lx I think that I need to delete this vma %p with tlb pointer %ld\n",  */
	/* 	       current->pid, tagged_page_to_clear->page, tagged_page_to_clear->page_offset, vma_set_ptr->vmap->vma->vm_start, vma_set_ptr->tlb_flush_cntr_ss); */
	retire_vmap(vma_set_ptr, tagged_page_to_clear);
      }
    }

    return 0;
}

/* int remove_vma_mapping_from_page(page_t *tagged_page, struct vm_area_struct *requested_vma) { */
/*     struct list_head *pos, *q; */
/*     vm_area_set_p *vma_set_elem; */
/*     int vma_set_empty = 0; */

/*     if(down_interruptible(&tagged_page->page_lock)) { */
/*       /\* semaphore not acquired; received a signal ... *\/ */
/*       printk(KERN_INFO "removing vma mapping from page %p was interrupted by a signal\n", page_address(tagged_page->page)); */
/*     } */

/*     list_for_each_safe(pos, q, &tagged_page->active_vmas.list) { */
/*       vma_set_elem = list_entry(pos, vm_area_set_p, list); */

/*       /\* Remove the mapping if the vma matches the target vma, or if there was no target vma *\/ */
/*       if(vma_set_elem->vma == requested_vma) { */
/* 	/\* Remove the page from the PTE first *\/ */
/* 	remove_vma_mapping(tagged_page, vma_set_elem->vma); */
	
/* 	/\* Move the VMA set entry to the unused list *\/ */
/* 	list_del(&(vma_set_elem->list)); */
/* 	vma_set_elem->vma = NULL; */
/* 	list_add(&(vma_set_elem>list), &(tagged_page->unused_vmas.list)); */
/*       } */
/*     } */

/*     vma_set_empty = list_empty(&tagged_page->active_vmas.list); */

/*     up(&tagged_page->page_lock); */
/*     return vma_set_empty; */
/* } */

int mark_tagged_page_in_buffer(page_t *tagged_page, unsigned long curr_buf_head, int buffer_id) {
  unsigned long last_timestamp = tagged_page->timestamp;
  int old_buffer_id = tagged_page->buffer_id;
  tagged_page->timestamp = curr_buf_head;
  tagged_page->buffer_id = buffer_id;

  tagged_page->last_timestamp = last_timestamp;
  tagged_page->last_buffer_id = old_buffer_id;

  if(last_timestamp != 0 && old_buffer_id == buffer_id) {
    char addresses[MAX_STR_LEN] = "";
    vma_addresses(tagged_page, addresses);
    printk(KERN_INFO "Thread %d inserted a duplicate page %p(%s) into buffer %d at index %lu when it was at %lu\n", 
	   current->pid, tagged_page, addresses, buffer_id, curr_buf_head, last_timestamp);
    return 1;
  }
  return 0;
}

int mark_tagged_page_writable(page_t *tagged_page) {
  unsigned short page_is_writable = tagged_page->is_writable;
  if(page_is_writable != 0) {
    printk("Why is this page %p already writable %hu\n", tagged_page, page_is_writable);
    return 1;
  }

  tagged_page->is_writable++;
  return 0;
}

int is_tagged_page_in_a_buffer(page_t *tagged_page, int buffer_id) {
  return tagged_page->buffer_id == buffer_id;
}
