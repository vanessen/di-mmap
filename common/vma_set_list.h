/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _VMA_SET_LIST_H_
#define _VMA_SET_LIST_H_

#include <linux/mm.h>		/* everything */
#include <linux/list.h>
#include <linux/semaphore.h>

/* For ZeptOS / Intrepid, the most number of processes per node is 4, one per core */ 
#define MAX_VMAS_PER_PAGE 8 /* Temporarily reduce the number of supported VMAs per page to reduce the memory overhead.  FIXME_BVE */
#define MAX_VMAS_PER_BANK 69 /* Weird number so that the vm_area_set_* structures are 4608 bytes */

#define ATOMIC_TLB_CNTR

/* Circularly linked list of TLB counters for VM areas */
typedef struct vm_area_set_tlb_cntr {
  struct vm_area_struct *vma;
  int active; /* Is the VMA in this structure associated with an active process */
  pid_t owner; /* Record the process ID of the VMA's owner task to check for consistency (4B) */
#ifdef ATOMIC_TLB_CNTR
  atomic64_t tlb_flush_cntr; /* Counter to mark when the last time the TLB for this process was flushed */
#else
  long tlb_flush_cntr; /* Counter to mark when the last time the TLB for this process was flushed */
#endif
  //  int ref_count; /* Number of banks that reference the counter */
  //  struct rw_semaphore sem;  /* 32B */
  struct list_head list; /* kernel's list structure (16B) */
  char pad[24]; /* Pad the data structure to fill a 64 byte cache line */
}vm_area_set_tlb_cntr_t;

/* Circularly linked list of VM areas */
typedef struct vm_area_set {
  struct vm_area_struct *vma;
  int active; /* Is the VMA in this structure associated with an active process */
  pid_t owner; /* Record the process ID of the VMA's owner task to check for consistency */
  int ref_count; /* How many active pages reference this vma */
  vm_area_set_tlb_cntr_t *global_cntr;
  struct list_head list; /* kernel's list structure */
  char pad[16]; /* Pad the data structure to fill a 64 byte cache line */
}vm_area_set_t;

/* Circularly linked list of pointers to VM areas */
typedef struct vm_area_set_pointer {
  vm_area_set_t *vmap;
  long tlb_flush_cntr_ss; /* Snapshot of the tlb flush counter */
}vm_area_set_p;

/* Structure with a semaphore to protect the VMA linked list */
typedef struct prot_vm_area_set {
  vm_area_set_t vmas;
  int num_vmas; /* number of active mappings */
  vm_area_set_t unused_vmas;
  struct semaphore sem;
  vm_area_set_t vma_set_pool[MAX_VMAS_PER_BANK];
  char pad[32];
}prot_vm_area_set_t;

typedef struct prot_vm_area_set_tlb_cntr {
  vm_area_set_tlb_cntr_t tlb_cntrs;
  int num_cntrs; /* number of active mappings */
  vm_area_set_tlb_cntr_t unused_tlb_cntrs;
  vm_area_set_tlb_cntr_t tlb_cntr_pool[MAX_VMAS_PER_BANK];
  struct semaphore sem;
  char pad[32];
}prot_vm_area_set_tlb_cntr_t;

void init_prot_vm_area_set(prot_vm_area_set_t *vma_set);
void free_prot_vm_area_set(prot_vm_area_set_t *vma_set);
void add_vma_to_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma, vm_area_set_tlb_cntr_t *vma_tlb_cntr);
void remove_vma_from_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma);
int inactivate_vma_in_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma);
vm_area_set_t *find_vma_in_list(prot_vm_area_set_t *vma_set, struct vm_area_struct *vma);
void remove_unused_vma_from_list(prot_vm_area_set_t *vma_set);

void init_prot_vm_area_tlb_cntr_set(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set);
void free_prot_vm_area_tlb_cntr_set(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set);
vm_area_set_tlb_cntr_t *add_tlb_cntr_to_list(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set, struct vm_area_struct *vma);
void remove_tlb_cntr_from_list(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set, struct vm_area_struct *vma);
vm_area_set_tlb_cntr_t *find_tlb_cntr_in_list(prot_vm_area_set_tlb_cntr_t *vma_tlb_cntr_set, struct vm_area_struct *vma);

#endif /* _VMA_SET_LIST_H_ */
