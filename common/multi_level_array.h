/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _MULTI_LEVEL_ARRAY_H_
#define _MULTI_LEVEL_ARRAY_H_

/* 
 * This is a multi-level array that simulate a large contiguous array, but does not require
 * a big chunk of contiguous memory, which is hard to get in the kernel.
 * Note that it is modeled after a multi-level page table.
 */

#define PTE_BIT_SHIFT 9
#define MAX_NUM_ENTRIES_PER_PAGE 512
#define PERMA_PTE_MASK 0x00000000000001ff

#define MULTI_LEVEL_ARRAY_ALLOC_PAGES_ORDER 1 //      3

typedef struct multi_level_array_struct {
  void ***array;
  unsigned long pgd_mask; /* Bit mask for the top level of the multi-level array */
  unsigned long pte_mask; /* Bit mask for the lower level of the multi-level array */
  unsigned long pte_bit_shift; /* Shift value to align for the PGD top level */
  unsigned long num_top_level_entries;
  unsigned long num_entries_per_pgd;
}multi_level_array_t;

int multi_level_array_insertion(multi_level_array_t *multi_level_array, unsigned long super_page_idx, void *page);
int multi_level_array_clear_entry(multi_level_array_t *multi_level_array, unsigned long super_page_idx);
void *multi_level_array_lookup(multi_level_array_t *multi_level_array, unsigned long super_page_idx);
void *multi_level_array_xchg(multi_level_array_t *multi_level_array, unsigned long super_page_idx, void *new_page);
multi_level_array_t *multi_level_array_initialization(long num_array_entries);
void multi_level_array_free(multi_level_array_t *multi_level_array);

#endif // _MULTI_LEVEL_ARRAY_H_
