/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _TAGGED_PAGE_H_
#define _TAGGED_PAGE_H_

#include <linux/list.h>

#include "vma_set_list.h"

#define ALLOC_PAGES_ORDER 0 /* mmaping is more straightforward with 0th order pages */

/* 88 + 128 for vma_ptr_pool = 216 */
typedef struct tagged_page {
  struct page *page;
  pgoff_t page_offset; /* Offset of the page into the device */
  dev_t dev_id; /* The device that this page is associated with */
  struct file *filp; /* The file that this page is associated with */

  unsigned long timestamp;
  unsigned long last_timestamp;
  /* Pack the following fields into a single 64 bit word */
  unsigned int page_valid : 1;
  unsigned short is_writable : 7;
  int num_vmas : 8;
  int buffer_id : 16;
  int last_buffer_id : 16;
  int pad : 16;
  /* End pack */
  long num_page_faults;
  /* Initialize the structure so that it has at least one VMA */
  vm_area_set_p vma_ptr_pool[MAX_VMAS_PER_PAGE]; /* A pool of vm area set pointers data structures */
  void *dev; /* BVE TYPE HACK */ /* The device that this page is associated with */
  //  profile_info *perf_cntr; /* Pointer to performance counters on the character ramdisk device */

  struct hlist_node hchain; /* Position in the hash table page lists */
  //  char pad[0]; /* Pad out to ??? bytes per page */
}page_t;

void clear_tagged_page_meta_data(page_t *tagged_page);
void init_tagged_page_meta_data(page_t *tagged_page);
void free_tagged_page_meta_data(page_t *tagged_page);
void display_tagged_page_meta_data(page_t *tagged_page, char *str);
void retire_inactive_vm_area_pointers(page_t *tagged_page);
int unmap_tagged_page(page_t *tagged_page, atomic64_t *num_write_eviction_races);
int clear_tagged_pages_pending_vmas(page_t *tagged_page);
int recover_tagged_pages_pending_vmas(page_t *tagged_page_to_clear);
//int remove_vma_mapping_from_page(page_t *tagged_page, struct vm_area_struct *requested_vma);
int mark_tagged_page_in_buffer(page_t *tagged_page, unsigned long curr_buf_head, int buffer_id);
int mark_tagged_page_writable(page_t *tagged_page);
int is_tagged_page_in_a_buffer(page_t *tagged_page, int buffer_id);

#endif /* _TAGGED_PAGE_H_ */
