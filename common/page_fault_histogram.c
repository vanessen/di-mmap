/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#include <linux/blkdev.h>

#include "page_fault_histogram.h"
#include "mmap_buffer_interface.h"
#include "mmap_fault_handler.h"

void update_page_fault_histogram(struct histogram_t *histogram, unsigned long num_faults) {
  if(num_faults >= NUM_BUCKETS-1) {
    histogram->page_activity[NUM_BUCKETS-1] += 1;
    histogram->num_active_pages++;
    histogram->num_overflow_page_faults += num_faults;
  }else if(num_faults > 0) {
    /* The radix tree seems to have pages for uninitialized entries, ignore things with 0 faults */
    histogram->page_activity[num_faults] += 1;
    histogram->num_active_pages++;
  }else { /* Count the number of inactive pages -- 0 faults */
    histogram->page_activity[num_faults] += 1;    
  }
  
  histogram->num_page_faults += num_faults;
  return;
}

void clear_page_fault_histogram(struct histogram_t *histogram) {
  int i;
  histogram->dev_id = 0;
  for(i = 0; i < NUM_BUCKETS; i++) {
    histogram->page_activity[i] = 0;
  }
  histogram->num_active_pages = 0;
  histogram->num_page_faults = 0;
  histogram->num_overflow_page_faults = 0;
  return;
}

void stringify_page_fault_histogram(dev_t device_id, struct histogram_t *histogram, char *line0, char *line1, char *line2, char *line3, char *line4) {
  char header[MAX_STR_LEN] = "", histogram_str[LONG_STR_LEN] = "", percentage_of_pages[LONG_STR_LEN] = "", percentage_of_faults[LONG_STR_LEN] = "";
  char tmp[16];
  int i;
  long percentage;
  int avg_num_overflow_page_faults;
  
  if(histogram->page_activity[NUM_BUCKETS-1] > 0) {
    avg_num_overflow_page_faults = histogram->num_overflow_page_faults/histogram->page_activity[NUM_BUCKETS-1];
  }else {
    avg_num_overflow_page_faults = NUM_BUCKETS-1;
  }

  for(i = 0; i < NUM_BUCKETS; i++) {
    if(i == NUM_BUCKETS-1) {
      sprintf(tmp, "%11d+ ", avg_num_overflow_page_faults);
    }else {
      sprintf(tmp, "%12d ", i);
    }
    strcat(header, tmp);
    sprintf(tmp, "%12lu ", histogram->page_activity[i]);
    strcat(histogram_str, tmp);
    if(i > 0) {
      percentage = histogram->num_active_pages == 0 ? 0 : (histogram->page_activity[i] * 10000) / histogram->num_active_pages;
      sprintf(tmp, "%9lu.%02lu ", percentage / 100, percentage % 100);
    }else {
      sprintf(tmp, "%9lu.%02lu ", (unsigned long) 0, (unsigned long) 0);
    }
    strcat(percentage_of_pages, tmp);
    if(i > 0) {
      int num_faults_per_bucket = i;
      if(i == NUM_BUCKETS-1) {
	num_faults_per_bucket = avg_num_overflow_page_faults;
      }
      percentage = histogram->num_page_faults == 0 ? 0 : (num_faults_per_bucket * histogram->page_activity[i] * 10000) / histogram->num_page_faults;
      sprintf(tmp, "%9lu.%02lu ", percentage / 100, percentage % 100);
    }else {
      sprintf(tmp, "%9lu.%02lu ", (unsigned long) 0, (unsigned long) 0);
    }
    strcat(percentage_of_faults, tmp);
  }

  sprintf(line0, "Histogram of page activity for device %d:%d:\n", MAJOR(device_id), MINOR(device_id));
  sprintf(line1, "%16s: %s:  page faults : num active pages\n", "bucket", header);
  sprintf(line2, "%16s: %s: %12ld : %16ld\n", "count", histogram_str, histogram->num_page_faults, histogram->num_active_pages);
  sprintf(line3, "%16s: %s\n", "% act. page", percentage_of_pages);
  sprintf(line4, "%16s: %s\n", "% page faults", percentage_of_faults);
}

void print_page_fault_histogram(dev_t device_id, struct histogram_t *histogram) {
  char line0[LONG_STR_LEN];
  char line1[LONG_STR_LEN];
  char line2[LONG_STR_LEN];
  char line3[LONG_STR_LEN];
  char line4[LONG_STR_LEN];

  stringify_page_fault_histogram(device_id, histogram, line0, line1, line2, line3, line4);

  printk(KERN_INFO "%s", line0);
  printk(KERN_INFO "%s", line1);
  printk(KERN_INFO "%s", line2);
  printk(KERN_INFO "%s", line3);
  printk(KERN_INFO "%s", line4);
}

void compute_page_fault_histogram(void *data, page_t *tagged_page) {
  struct histogram_t *histogram = (struct histogram_t *) data;

  BUG_ON(tagged_page == NULL);

  if(tagged_page->dev_id == histogram->dev_id) {
    update_page_fault_histogram(histogram, tagged_page->num_page_faults);
  }
  return;
}

void log_page_fault_histogram(dev_t device_id) {
  struct histogram_t *histogram;

  if(device_id == 0) {return;}

  histogram = kmalloc(sizeof(struct histogram_t), GFP_KERNEL);
  clear_page_fault_histogram(histogram);

  histogram->dev_id = device_id;
  scan_buffer(buf_data, &compute_page_fault_histogram, histogram);
  print_page_fault_histogram(device_id, histogram);

  kfree(histogram);

  return;
}

void report_page_fault_histogram(dev_t device_id, char *line0, char *line1, char *line2, char *line3, char *line4) {
  struct histogram_t *histogram;

  histogram = kmalloc(sizeof(struct histogram_t), GFP_KERNEL);
  clear_page_fault_histogram(histogram);

  histogram->dev_id = device_id;
  scan_buffer(buf_data, &compute_page_fault_histogram, histogram);
  stringify_page_fault_histogram(device_id, histogram, line0, line1, line2, line3, line4);

  kfree(histogram);

  return;
}
