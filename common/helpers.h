/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

#ifndef _PERMA_HELPERS_H_
#define _PERMA_HELPERS_H_

#include <linux/mm.h>		/* everything */
#include <asm/pgtable.h>

#include "report_stats.h"
#include "vma_set_list.h"
#include "tagged_page.h"

extern int DEBUG_LOGGING;

//#define PTE_CLEAR_FLUSH

pte_t *get_pte(struct mm_struct *mm, unsigned long address);
inline void retire_vmap(vm_area_set_p *vmap_set_ptr, page_t *tagged_page);
inline void remove_vmap(vm_area_set_p *vmap_set_ptr, page_t *tagged_page);
int page_in_pte(struct vm_area_struct *vma, page_t *tagged_page);
int page_in_all_ptes(page_t *tagged_page);
int page_in_vma(page_t *tagged_page, struct vm_area_struct *vma);
int page_is_writable(struct vm_area_struct *vma, page_t *tagged_page, unsigned long address);
unsigned long vma_address(struct page *page, struct vm_area_struct *vma);
void vma_addresses(page_t *tagged_page, char *str);
void vm_address_regions(vm_area_set_t *vmas, char *str);
pte_t *page_check_address(struct page *page, struct mm_struct *mm, unsigned long address, spinlock_t **ptlp);
int zap_page_from_vma(struct vm_area_struct *vma, struct page *page);
int unmap_page(struct page *page);
unsigned long scan_page_table(struct vm_area_struct *vma);

#endif /* _PERMA_HELPERS_H_ */
