/*
 * Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-559080. 
 * All rights reserved.
 * 
 * This file is part of DI-MMAP, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Our Notice and GNU Lesser General Public License.
 *   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License (as published by the Free
 * Software Foundation) version 2.1 dated February 1999.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
 * 
 * Our Preamble Notice
 * 
 * A. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at the Lawrence
 * Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * B. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or process
 * disclosed, or represents that its use would not infringe privately-owned rights.
 * 
 * C. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring by
 * the United States Government or Lawrence Livermore National Security, LLC. The
 * views and opinions of authors expressed herein do not necessarily state or
 * reflect those of the United States Government or Lawrence Livermore National
 * Security, LLC, and shall not be used for advertising or product endorsement
 * purposes.
 * 
 */

/*  -*- C -*-
 * perma_mmap.c -- memory mapping for the PerMA block ram device
 *                 derived from the LDD3 scullp char module
 *
 *  3/3/2011: Brian Van Essen - vanessen1@llnl.gov
 *    - Added a global atomic counter to track the total number of page faults
 *    - Put in a trylock to prohibit multiple threads from simultaneously trying to zap the page
 *      range.  This had a huge improvement in overall performance.
 *
 *  3/4/2011: Brian Van Essen - vanessen1@llnl.gov
 *    - Put in a LRU style tag for each page so that we can track how long ago (in terms of page faults)
 *      was it last mapped into the user space.  This allows us to only delay those pages that haven't
 *      been accessed in a long time.  Note that I tried to get a similar effect with a circular buffer
 *      and a per-page zapping, but the overhead for allowing each thread to independently zap a page is bad.
 *    - Added support for filling in page in the memory space via the mmap command.
 *
 *  4/1/2011: Brian Van Essen - vanessen1@llnl.gov
 *    - Added support for tracking the writing back of dirty pages.
 *
 *  4/15/2011: Brian Van Essen - vanessen1@llnl.gov
 *    - Added support for using worker queues to perform the zap page range
 *
 * Debugging metrics to add:
 *  - make stats atomic
 *  - number of threads yielding?
 *  - number of threads requesting sleep
 *  - dynamically allow logging of stats
 */

#include <linux/module.h>

#include <linux/mm.h>		/* everything */
#include <linux/errno.h>	/* error codes */
#include <asm/pgtable.h>
#include <linux/delay.h>
#include <linux/time.h>
#include <linux/fs.h>

#include "mmap_fault_handler.h"
#include "rhel_compatibility.h"
#include "mmap_buffer_interface.h"
#include "page_fault_histogram.h"

#ifdef RHEL6
#define PERMA_SIGBUS VM_FAULT_SIGBUS
#else
#define PERMA_SIGBUS NOPAGE_SIGBUS
#endif

/* Global and loadable variables - do not initialize in reset_mmap_params, they are controlled by module load params*/
long perma_mmap_buf_size = 0;

/* MMAP metrics */
atomic64_t total_num_pages_written;
atomic64_t skipped_zap_page_range;
atomic64_t perma_buffer_stalled;

/* MMAP global state */
static atomic64_t num_global_faults;
static int disable_writeback_delays = 0; /* Allow the writeback to go faster when a VMA is closed */

/* MMAP interface to buffer */
//struct buffer_operations_struct *buf_ops;
//buffer_data_t *buf_data;
//fifo_buffer_t *buf_data;
banked_buffer_t *buf_data;

void reset_mmap_params() {
  /* buf_ops-> */reset_params(buf_data);

  init_mmap_params();
}

void init_mmap_params() {
  atomic64_set(&skipped_zap_page_range, 0);
  atomic64_set(&perma_buffer_stalled, 0);

  /* buf_ops-> */init_params(buf_data, perma_mmap_buf_size);
}

void cleanup_mmap_params() {
  /* buf_ops-> */cleanup_params(buf_data);
}

void reset_mmap_state() {
  atomic64_set(&num_global_faults, 0);
  reset_state(buf_data);
}

/*
 * The getpage method: the core of the file. It retrieves the
 * page required from the PerMA device and returns it to the
 * user. The count for the page must be incremented, because
 * it is automatically decremented at page unmap.
 *
 * For this reason, "order" must be zero. Otherwise, only the first
 * page has its count incremented, and the allocating module must
 * release it as a whole block. Therefore, it isn't possible to map
 * pages from a multipage block: when they are unmapped, their count
 * is individually decreased, and would drop to 0.
 *
 *
 * Potential things that I may need to be doing
 * shmem.c - mark_page_accessed(page) - seems to be the opposite of page_cache_release
 * find_lock_page(mapping, idx)
 * wait_on_page_locked(swappage)
 * 
 */
int perma_getpage(struct vm_area_struct *vma, unsigned long address, 
		  struct page **pagep, int *type)
{
	unsigned long offset, offset_pages;
	void *device = vma->vm_private_data;
	int error = 0;
	page_t *tagged_page = NULL;
	long buffer_entry;
	unsigned short page_is_writable;
	int err = 0;

	offset = (address - vma->vm_start) + (vma->vm_pgoff << PAGE_SHIFT);
	if (unlikely(address < vma->vm_start || address >= vma->vm_end)) {
	  return -EFAULT;
	}
	//	if (perma_crd_max_size != 0 && offset >= perma_crd_max_size) goto out; /* out of range */

	/* Update the number of page faults */
	atomic64_inc(&num_global_faults);

	/*
	 * Now retrieve the perma_crd device from the list, then the page.
	 * If the device has holes, fill them.
	 */
	offset_pages = offset >> PAGE_SHIFT; /* offset_pages is a number of pages */

	
	/* critical region (semaphore acquired) ... */
	tagged_page = find_and_lock_page(buf_data, offset_pages, get_device_id(device));
	BUG_ON(tagged_page == NULL);

	/* got it, now increment the count */
	/* Page count should be at least one since it is marked as used when it is allocated */
	if(tagged_page->page != NULL && tagged_page->page_valid) {
	  get_page(tagged_page->page);
	  if (type) {
	    *type = VM_FAULT_MINOR;
	  }
	}

	/* Associate the VMA with the tagged page */
	rmap_page_to_vma(buf_data, tagged_page, vma);

	if(page_in_buffer(buf_data, tagged_page)) {
	  /* Some other thread already put it in the buffer */
	  inc_num_page_faults(buf_data, tagged_page);
	  goto out;
	}

	if(page_being_evicted(buf_data, tagged_page)) {
	  /* This page is being evicted, recover it from the victim queue and put it back into the buffer management queues.
	     However, the data does not have to be fetched back in, so do not delay. */
/* 	    printk("Thread %d: I am faulting on a page %lx that is being evicted %d: %d %ld %d %ld\n", current->pid, address, num_faults, */
/* 		   tagged_page->buffer_id, atomic64_read(&tagged_page->timestamp), tagged_page->last_buffer_id, atomic64_read(&tagged_page->last_timestamp)); */
	  /* Move the page between buffers before incrementing the fault count so that the page fault count is correct on the old buffer */
	  recover_page(buf_data, tagged_page);
	  /* Make sure to increase the pages fault count */
	  inc_num_page_faults(buf_data, tagged_page);
	  goto out;
	}

	page_is_writable = tagged_page->is_writable;
	if(page_is_writable != 0) {
	  printk("ERROR: Why is this page %p (address=%lx) writable %hu when it was not in the buffer: it was at %lu in buffer %d and last at %lu in buffer %d\n", page_address(tagged_page->page), address, page_is_writable, tagged_page->timestamp, tagged_page->buffer_id, tagged_page->last_timestamp, tagged_page->last_buffer_id);
	}

	/* There is no valid data for this page, read if from the device */
	if(!tagged_page->page_valid) {
	  /* Allocate a spot in mmap's internal buffer */
	  buffer_entry = pre_allocate_entry(buf_data, tagged_page);

	  err = read_page(vma->vm_file, device, offset, &tagged_page->page);
	  if(err == 0) {
	    tagged_page->page_valid = 1;
	    get_page(tagged_page->page);
	  }
	  BUG_ON(tagged_page->page == NULL);
	  tagged_page->dev = device;
	  BUG_ON(tagged_page->filp != NULL && tagged_page->filp != vma->vm_file);
	  tagged_page->filp = vma->vm_file;
	  //	  tagged_page->perf_cntr = &dev->perf_cntr;

/* 	if(page_in_pte(vma, tagged_page, address)) { */
/* 	  printk("How is the page %lx already in the PTE when I haven't added it to the buffer yet timestamp = %lu\n", address, atomic64_read(&tagged_page->timestamp)); */
/* 	} */

	/* Make sure that the process does not migrate during its delay, otherwise the local target buffer may become remote. */
	/* TBD */

	/* Check to see if the page was recently in the page cache, or if it is old or has never been mapped.  Incur a delay for 
	   mapping new pages, or ones that haven't been mapped within buffer size number of recent page faults.
	*/

	  /* Putting the delay here, prior to updating the timestamp will force multiple threads to wait for the same page to load, if this fault
	     handler isn't done when they try to access the page.  While this may be suboptimal from a modeling point of view, it prevents us from
	     having to block concurrent accesses to the same page.
	  */
	  if (type)
	    *type = VM_FAULT_MAJOR;

	  /* If there was not a valid page of data for this page in the buffer then the page hasn't been seen recently,
	   * restart the fault count at 1. */
	  tagged_page->num_page_faults = 1;
	  insert_page(buf_data, buffer_entry, tagged_page);
	}

  out:
	release_page_lock(buf_data, tagged_page);
	*pagep = tagged_page->page;
	return error;
}

#if 0
int perma_page_mkwrite(struct vm_area_struct *vma, struct page *page) {
  /* Compute the virtual address from the VMA regions starting address and the pages index into the VMA space */
  unsigned long address = vma_address(page, vma);
  page_t *tagged_page = NULL;
  struct scullp_dev *dev = vma->vm_private_data;
  unsigned long offset, offset_pages;
  sector_t sector;

  offset = (address - vma->vm_start) + (vma->vm_pgoff << PAGE_SHIFT);
  offset_pages = offset >> PAGE_SHIFT; /* offset_pages is a number of pages */
  sector = offset_pages << PAGE_SECTORS_SHIFT;
  tagged_page = perma_crd_lookup_page(dev, sector); /* insert pages to fill hole or extend end-of-file */
  BUG_ON(!tagged_page); /* At this point we should be able to find a page. */
  BUG_ON(tagged_page->page != page);

  /* critical region (semaphore acquired) ... */
  /*
   * Cases:
   * page not in pte && page in buffer -> nopage fault that is writable and hasn't been installed to PTE yet
   * page in pte && page in buffer -> duplicate fault
   * page in pte && page not in buffer -> error !!!
   * page not in pte && page not in buffer -> race, let the software retry the fault
  */

  if(!/* buf_ops-> */page_in_buffer(buf_data, tagged_page)) {
    /* Race has occurred between taking a page from being read-only to read-write and it being evicted from the buffer.
       Just bail out without an error, and the upper level code will notice that the page
       is now gone and re-fault */
    if(page_in_pte(vma, tagged_page)) {
      char str[MAX_STR_LEN], str2[MAX_STR_LEN], str3[MAX_STR_LEN] = "";
      display_mmap_buffer_state(buf_data, str);
      display_tagged_page_meta_data(tagged_page, str2);
      vma_addresses(tagged_page, str3);
      printk("Why is this page %p in the PTE but no longer in the buffer %lx (address=%s).  Its meta data is %s and the pointers are %s.  The page count is %d and mapping is %d\n",
	     page_address(page), address, str3, str2, str, page_count(tagged_page->page), page_mapcount(tagged_page->page));
    }
    goto out_unlock;
  }
  
  if(/* buf_ops-> */page_being_evicted(buf_data, tagged_page)) {
    /* Race has occurred between taking a page from being read-only to read-write and it being evicted from the buffer.
       Just bail out without an error, and the upper level code will notice that the page
       is now gone and re-fault */
/*     printk("Thread %d: I am write faulting on a page %lx that is being evicted: %d %ld %d %ld\n", current->pid, address,  */
/* 	   tagged_page->buffer_id, atomic64_read(&tagged_page->timestamp), tagged_page->last_buffer_id, atomic64_read(&tagged_page->last_timestamp)); */
    goto out_unlock;
  }

  if(page_is_writable(vma, tagged_page, address)) {
    /* Some other thread already made it writeable */
    goto out_unlock;
  }

  /* buf_ops-> */page_made_writable(buf_data, vma, tagged_page);

 out_unlock:

  /* Return zero to indicate that there was no error */
  return 0;
}
#endif

#ifdef RHEL6
/*
 * The fault method: the entry point to the file
 */
int perma_vma_fault(struct vm_area_struct *vma, struct vm_fault *vmf) {
  int error;
  int ret;

  //  printk(KERN_ALERT "I have a fault for vm_start=%lx vm_pgoff=%lx pgoff=%lx vm_end=%lx\n", vma->vm_start, vma->vm_pgoff,vmf->pgoff, vma->vm_end);
  /* vmf->pgoff includes the base value of vma->vm_pgoff, which is how far the vma is into the address region.
   * Therefore the vma->vm_pgoff needs to be subtracted off before computing the address */
  error = perma_getpage(vma, vma->vm_start + ((vmf->pgoff - vma->vm_pgoff) << PAGE_SHIFT), &vmf->page, &ret);
  if (error)
    return ((error == -ENOMEM) ? VM_FAULT_OOM : VM_FAULT_SIGBUS);
  
  return ret | VM_FAULT_LOCKED;
}
#else
/*
 * The nopage method: the entry point to the file
 */
struct page *perma_vma_nopage(struct vm_area_struct *vma,
                                unsigned long address, int *type)
{
  struct page *page = NULL;
  int error;

  error = perma_getpage(vma, address, &page, type);
  if (error)
    return ((error == -ENOMEM) ? NOPAGE_OOM : NOPAGE_SIGBUS);

  return page;
}
#endif

/*
 * open and close: just keep track of how many times the device is
 * mapped, to avoid releasing it.
 */

void perma_vma_open(struct vm_area_struct *vma)
{
	void *device = vma->vm_private_data;
	prot_vm_area_set_t *active_vmas = get_device_active_vmas(device);

	add_vma_to_list(active_vmas, vma, NULL);

	printk(KERN_ALERT "Process %d memory mapped (opened) the device %d starting at address %lx to %lx\n", current->pid, get_device_id(device), vma->vm_start, vma->vm_end);

	disable_writeback_delays = 0;
	vma_open_mmap(buf_data, vma, NULL);
}

void perma_vma_close(struct vm_area_struct *vma)
{
	void *device = vma->vm_private_data;
	prot_vm_area_set_t *active_vmas = get_device_active_vmas(device);

	/* Compute and print out the histogram for this device */
	log_page_fault_histogram(get_device_id(device));

	/* Temporarily disable the writeback delays so that simulation goes faster */
	disable_writeback_delays = 0;
	printk(KERN_ALERT "The user process %d is closing the device %d starting at address %lx to %lx: disable_writeback_delays=%d\n", current->pid, get_device_id(device), vma->vm_start, vma->vm_end, disable_writeback_delays);
	vma_close_mmap(buf_data, vma);
	disable_writeback_delays = 0;

	remove_vma_from_list(active_vmas, vma);

}

/*
 * Check all entries of all page tables
 */
void scan_page_tables(prot_vm_area_set_t *active_vmas) {
  vm_area_set_t *tmp;
  struct list_head *pos, *q;

  down(&active_vmas->sem);
  list_for_each_safe(pos, q, &active_vmas->vmas.list){
    tmp = list_entry(pos, vm_area_set_t, list);
    printk("Process %d is scaning the page table %lx for stray entries\n", current->pid, tmp->vma->vm_start);
    scan_page_table(tmp->vma);
  }
  up(&active_vmas->sem);
}

struct vm_operations_struct perma_vm_ops = {
	.open =     perma_vma_open,
	.close =    perma_vma_close,
#ifdef RHEL6
	.fault = perma_vma_fault,
#else
	.nopage =   perma_vma_nopage,
#endif
	//	.page_mkwrite = perma_page_mkwrite,
};


int perma_mmap(struct file *filp, struct vm_area_struct *vma)
{
/*	struct inode *inode = filp->f_dentry->d_inode;*/

	/* refuse to map if order is not 0 */
/* 	if (scullp_devices[iminor(inode)].order) */
/* 		return -ENODEV; */

	/* don't do anything here: "nopage" will set up page table entries */
	vma->vm_ops = &perma_vm_ops;
	vma->vm_flags |= VM_RESERVED;
	vma->vm_private_data = filp->private_data;
	printk(KERN_ALERT "We have mmaped a region that has a file %p and a vma->vm_file %p and address %lx to %lx\n", filp, vma->vm_file, vma->vm_start, vma->vm_end);
	perma_vma_open(vma);

	return 0;
}
