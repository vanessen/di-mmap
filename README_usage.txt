Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
Produced at the Lawrence Livermore National Laboratory. 
Written by Brian Van Essen <vanessen1@llnl.gov>. 
LLNL-CODE-559080. 
All rights reserved.

This file is part of DI-MMAP, Version 1.0. 
For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html

--------------------------------------------------------------------------------

DI-MMAP:

    Helper script - setup_dimmap.sh:

        To install the module and setup a mount point there is a helper script called
        setup_dimmap.sh in bin:

        di-mmap.git/bin/setup_dimmap.sh -i -s <size in pages> -b <backing directory>

        this will install the module, set the size, and set the backing directory

        To remove the module

        di-mmap.git/bin/setup_dimmap.sh -r

    Manual Interaction:

        Installing the module:

        sudo insmod <path>/di-mmap-runtime.ko perma_mmap_buf_size=<size in pages>

        Mounting the di-mmap-fs file system:

        sudo mount -t di-mmap-fs -o backing=<backing_dir> di-mmap-fs <mount_point>

        Direct file link:

        sudo di-mmap-ctrl /dev/di-mmap/runtimeA-mmap<N> /dev/<blockdev>

        Removing direct file link:

        sudo di-mmap-ctrl /dev/di-mmap/runtimeA-mmap<N> 0 0

        Unmounting the di-mmap-fs filesystem:

        sudo umount <mount_point>

        Removing the module:

        sudo rmmod di-mmap-runtime

PerMA Sim:

    Helper script - setup_dimmap.sh:

        To install the module and setup a mount point there is a helper script called
        setup_dimmap.sh in bin:

        di-mmap.git/bin/setup_perma_sim.sh -i -n <# devices> -s <size in pages>

        this will install the module, set the size, and set the number of
        simulated NVRAM devices

        To remove the module

        di-mmap.git/bin/setup_perma_sim.sh -r

    Manual Interaction:

        Installing the module:

        sudo insmod <path>/perma_mmap_ramdisk.ko perma_devs=<num_devices> perma_mmap_buf_size=<size in pages>

        This will create N ramdisk with the naming scheme:
        /dev/perma-mmap-ramdisk<N>

