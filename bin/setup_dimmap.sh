#!/bin/sh

# Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
# Produced at the Lawrence Livermore National Laboratory. 
# Written by Brian Van Essen <vanessen1@llnl.gov>. 
# LLNL-CODE-559080. 
# All rights reserved.
# 
# This file is part of DI-MMAP, Version 1.0. 
# For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
# 
# Please also read this link – Our Notice and GNU Lesser General Public License.
#   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License (as published by the Free
# Software Foundation) version 2.1 dated February 1999.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
# License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License along
# with this program; if not, write to the Free Software Foundation, Inc., 
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# 
# OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
# 
# Our Preamble Notice
# 
# A. This notice is required to be provided under our contract with the
# U.S. Department of Energy (DOE). This work was produced at the Lawrence
# Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
# 
# B. Neither the United States Government nor Lawrence Livermore National
# Security, LLC nor any of their employees, makes any warranty, express or
# implied, or assumes any liability or responsibility for the accuracy,
# completeness, or usefulness of any information, apparatus, product, or process
# disclosed, or represents that its use would not infringe privately-owned rights.
# 
# C. Also, reference herein to any specific commercial products, process, or
# services by trade name, trademark, manufacturer or otherwise does not
# necessarily constitute or imply its endorsement, recommendation, or favoring by
# the United States Government or Lawrence Livermore National Security, LLC. The
# views and opinions of authors expressed herein do not necessarily state or
# reflect those of the United States Government or Lawrence Livermore National
# Security, LLC, and shall not be used for advertising or product endorsement
# purposes.

NUM_DEVICES=1
BUF_SIZE=262144 # 1GB buffer by default
BACKING_DIR=/tmp

usage() {
    echo "usage: setup_perma_sim.sh -i [-s # buffer pages] [-b backing directory]" # File_size_on_temp_partition"
    echo "usage: setup_perma_sim.sh -r "
    echo "usage:   cleans up and unmounts the PerMA simulator"
    exit 1
}

while getopts  "ris:hb:" flag
do
#  echo "$flag" $OPTIND $OPTARG
  case "$flag" in
      i) COMMAND=insmod;;
      r) COMMAND=rmmod;;
      s) BUF_SIZE=$OPTARG;;
      b) BACKING_DIR=$OPTARG;;
      h) usage;;
      *) usage;;
  esac
done

BIN_DIR=`dirname $0`
GIT_DIR=`dirname ${BIN_DIR}`
MODULE_PATH=${GIT_DIR}/modules/`uname -r`
MODULE=di-mmap-runtime
TMP_UDEV_FILE=/tmp/10-di-mmap.rules
UDEV_FILE=/etc/udev/rules.d/10-di-mmap.rules

if [ "${COMMAND}" = "insmod" ]; then

OPTIONS="perma_mmap_buf_size=${BUF_SIZE}"

#Make sure that the device driver is mounted
MODNAME=`lsmod | grep $MODULE`
if [ -z "${MODNAME}" ]; then
   CMD="sudo insmod ${MODULE_PATH}/${MODULE}.ko ${OPTIONS}"
   echo $CMD
   $CMD

# use udev to setup rules that will provide the correct permissions
# udevadm info --query=all -n /dev/di-mmap/runtimeA-ctl --attribute-walk

echo "Setup udev rule file in ${UDEV_FILE}"
cat > ${TMP_UDEV_FILE} <<EOF
# Enter di-mmap device bindings here.
#

SUBSYSTEM=="di-mmap-runtime[A-Z]", MODE="0666"
EOF

sudo mv ${TMP_UDEV_FILE} ${UDEV_FILE}
sudo chmod go+r ${UDEV_FILE}

CMD="sudo chmod go+rwx /dev/di-mmap"
echo $CMD
$CMD

if [ ! -d "/mnt/dimmap" ]; then
    sudo mkdir /mnt/dimmap
fi

CMD="sudo mount -t di-mmap-fs -o backing=${BACKING_DIR} di-mmap-fs /mnt/dimmap"
echo $CMD
$CMD
else
echo "$MODNAME is already installed -- possibly with different option"
fi

elif [ "${COMMAND}" = "rmmod" ]; then
    FILES=/dev/di-mmap/*mmap*
    for f in $FILES
      do
      echo "Processing $f file..."
      # take action on each file. $f store current file name
      if [ -e $f ]; then
	  CMD="sudo ${BIN_DIR}/di-mmap-ctrl $f 0 0"
	  echo $CMD
	  $CMD
      fi
    done

    CMD="sudo umount /mnt/dimmap"
    echo $CMD
    $CMD

    CMD="sudo rmmod ${MODULE}"
    echo $CMD
    $CMD
fi
