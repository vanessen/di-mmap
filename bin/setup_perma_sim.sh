#!/bin/sh

# Copyright (c) 2012, Lawrence Livermore National Security, LLC. 
# Produced at the Lawrence Livermore National Laboratory. 
# Written by Brian Van Essen <vanessen1@llnl.gov>. 
# LLNL-CODE-559080. 
# All rights reserved.
# 
# This file is part of DI-MMAP, Version 1.0. 
# For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
# 
# Please also read this link – Our Notice and GNU Lesser General Public License.
#   http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
# 
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License (as published by the Free
# Software Foundation) version 2.1 dated February 1999.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the IMPLIED WARRANTY OF MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE. See the terms and conditions of the GNU General Public
# License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License along
# with this program; if not, write to the Free Software Foundation, Inc., 
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# 
# OUR NOTICE AND TERMS AND CONDITIONS OF THE GNU GENERAL PUBLIC LICENSE
# 
# Our Preamble Notice
# 
# A. This notice is required to be provided under our contract with the
# U.S. Department of Energy (DOE). This work was produced at the Lawrence
# Livermore National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
# 
# B. Neither the United States Government nor Lawrence Livermore National
# Security, LLC nor any of their employees, makes any warranty, express or
# implied, or assumes any liability or responsibility for the accuracy,
# completeness, or usefulness of any information, apparatus, product, or process
# disclosed, or represents that its use would not infringe privately-owned rights.
# 
# C. Also, reference herein to any specific commercial products, process, or
# services by trade name, trademark, manufacturer or otherwise does not
# necessarily constitute or imply its endorsement, recommendation, or favoring by
# the United States Government or Lawrence Livermore National Security, LLC. The
# views and opinions of authors expressed herein do not necessarily state or
# reflect those of the United States Government or Lawrence Livermore National
# Security, LLC, and shall not be used for advertising or product endorsement
# purposes.

NUM_DEVICES=1
BUF_SIZE=262144 # 1GB buffer by default

usage() {
    echo "usage: setup_perma_sim.sh -i [-n # devices] [-s # buffer pages]" # File_size_on_temp_partition"
    echo "usage: setup_perma_sim.sh -r "
    echo "usage:   cleans up and unmounts the PerMA simulator"
    exit 1
}

while getopts  "rin:s:h" flag
do
#  echo "$flag" $OPTIND $OPTARG
  case "$flag" in
      i) COMMAND=insmod;;
      r) COMMAND=rmmod;;
      n) NUM_DEVICES=$OPTARG;;
      s) BUF_SIZE=$OPTARG;;
      h) usage;;
      *) usage;;
  esac
done

BIN_DIR=`dirname $0`
GIT_DIR=`dirname ${BIN_DIR}`
MODULE_PATH=${GIT_DIR}/modules/`uname -r`
MODULE=perma_mmap_ramdisk

if [ "${COMMAND}" = "insmod" ]; then

OPTIONS="perma_devs=${NUM_DEVICES} perma_mmap_buf_size=${BUF_SIZE}"

#Make sure that the device driver is mounted
MODNAME=`lsmod | grep $MODULE`
if [ -z "${MODNAME}" ]; then
#   echo "I couldn't find the module ${MODULE}"
   CMD="sudo insmod ${MODULE_PATH}/${MODULE}.ko ${OPTIONS}"
   echo $CMD
   $CMD

   sleep 1
   for (( i = 0 ; i < ${NUM_DEVICES} ; i++ ))
     do
     CMD="sudo chmod go+rw /dev/perma-mmap-ramdisk${i}"
     echo $CMD
     $CMD
   done
else
echo "$MODNAME is already installed -- possibly with different option"
fi

elif [ "${COMMAND}" = "rmmod" ]; then

  CMD="sudo rmmod ${MODULE}"
  echo $CMD
  $CMD
fi
